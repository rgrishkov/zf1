<?php
class ZendExt_Utils_FilesystemUtils
{
	
	public static function createDirectory($name) 
	{
		if (!is_dir($name)) {
			$result = mkdir($name, 0777);
			if (!$result) return false;
		} else {
			if (!is_writable($name)) return false;
			else return true;
		}
	}
	
	public static function configureSiteUploadFolders($externalId)
	{
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');

		if (is_dir($config->upload_path->sites.'/'.$externalId) == false) {
			self::createDirectory($config->upload_path->sites.'/'.$externalId);
		}
		
		if (is_dir($config->upload_path->sites.'/'.$externalId.'/images') == false) {
			self::createDirectory($config->upload_path->sites.'/'.$externalId.'/images');
		}
		
		if (is_dir($config->upload_path->sites.'/'.$externalId.'/thumbnails') == false) {
			self::createDirectory($config->upload_path->sites.'/'.$externalId.'/thumbnails');
		}
	}
	
	public static function configureCampaignUploadFolders($externalId)
	{
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');

		if (is_dir($config->upload_path->campaigns.'/'.$externalId) == false) {
			self::createDirectory($config->upload_path->campaigns.'/'.$externalId);
		}
		
		if (is_dir($config->upload_path->campaigns.'/'.$externalId.'/images') == false) {
			self::createDirectory($config->upload_path->campaigns.'/'.$externalId.'/images');
		}
		
		if (is_dir($config->upload_path->campaigns.'/'.$externalId.'/thumbnails') == false) {
			self::createDirectory($config->upload_path->campaigns.'/'.$externalId.'/thumbnails');
		}
	}

    public static function configureTabsUploadFolders($externalId)
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');

        if (is_dir($config->upload_path->tabs.'/'.$externalId) == false) {
            self::createDirectory($config->upload_path->tabs.'/'.$externalId);
        }
    }

	public static function configureCampaignCSVFolders($externalId)
	{
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');

		if (is_dir($config->upload_path->csv.'/'.$externalId) == false) {
			self::createDirectory($config->upload_path->csv.'/'.$externalId);
		}
	}

    public static function deleteSiteImage($siteExternalId, $imageName)
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');

        $imagePath = $config->upload_path->sites.'/'.$siteExternalId.'/images/'.$imageName;

        if (file_exists($imagePath)) {
            unlink($imagePath);
        }

        $thumbnailPath = $config->upload_path->sites.'/'.$siteExternalId.'/thumbnails/'.$imageName;

        if (file_exists($thumbnailPath)) {
            unlink($thumbnailPath);
        }
    }

    public static function deleteCampaignImage($campaignExternalId, $imageName)
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');

        $imagePath = $config->upload_path->campaigns.'/'.$campaignExternalId.'/images/'.$imageName;

        if (file_exists($imagePath)) {
            unlink($imagePath);
        }

        $thumbnailPath = $config->upload_path->campaigns.'/'.$campaignExternalId.'/thumbnails/'.$imageName;

        if (file_exists($thumbnailPath)) {
            unlink($thumbnailPath);
        }
    }

    public static function deleteTabImage($campaignExternalId, $imageName)
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');

        $imagePath = $config->upload_path->tabs.'/'.$campaignExternalId.'/'.$imageName;

        if (file_exists($imagePath)) {
            unlink($imagePath);
        }
    }
	
}