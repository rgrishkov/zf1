<?php
class ZendExt_Utils_ConfigUtils
{
    private $competitionType = COMPETITION_TYPE_BASIC_COMPETITION;

    public function setCompetitionType($value) {
        $this->competitionType = $value;
    }

    public static function getConfig($elementName)
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/user_fields.ini', $elementName);
        return $config;
    }

    public function getCustomLabel(Zend_Form_Element $element)
    {
        $config = ZendExt_Utils_ConfigUtils::getConfig($element->getName());
        $competitionType = $this->competitionType;

        if (isset($config->$competitionType->label)) {
            return $config->$competitionType->label;
        } else {
            return $config->label;
        }
    }

    public function getCustomDescription(Zend_Form_Element $element)
    {
        $config = ZendExt_Utils_ConfigUtils::getConfig($element->getName());
        $competitionType = $this->competitionType;

        if (isset($config->$competitionType->descrition)) {
            return $config->$competitionType->descrittion;
        } else {
            return $config->descrittion;
        }
    }

    public static function getBaseUrl()
    {
        if (empty($_SERVER['HTTPS'])) {
            $baseUrl = 'http://';
        } else {
            $baseUrl = 'https://';
        }
        $baseUrl .= $_SERVER['SERVER_NAME'];

        return $baseUrl;
    }
}