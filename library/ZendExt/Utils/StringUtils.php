<?php
class ZendExt_Utils_StringUtils
{
	public static function randomStr($length)
	{
		$set = array("a","A","b","B","c","C","d","D","e","E","f","F","g","G","h","H","i","I","j","J","k","K","l","L","m","M","n","N","o","O","p","P","q","Q","r","R","s","S","t","T","u","U","v","V","w","W","x","X","y","Y","z","Z","1","2","3","4","5","6","7","8","9");
		$str = '';
	
		for($i = 1; $i <= $length; ++$i)
		{
			$ch = mt_rand(0, count($set)-1);
			$str .= $set[$ch];
		}
	
		return $str;
	}
	
	public static function clearUrl($url)
	{
    	$url = trim($url);
    	if(substr($url, 0, 5) == 'https') {
			$resultUrl = substr($url, 8, strlen($url)-1);
    	} elseif (substr($url, 0, 4) == 'http') {
			$resultUrl = substr($url, 7, strlen($url)-1);
		} else {
			$resultUrl = $url;
		}
		return $resultUrl;
	}

    public static function parseResponseString($argStr)
    {
        $resultArray = array();
        $token = strtok($argStr, '&');

        while ($token !== false) {
            $tokenArray = explode('=', $token);

            $resultArray[$tokenArray[0]] = urldecode($tokenArray[1]);
            $token = strtok('&');
        }
        return $resultArray;
    }

    public static function getDateDiff($firstDate, $secondDate)
    {
		$minute = 60;
		$hour = 60;
		$day = 60*24;
		$month = 60*24*30;

		$dateDiffInMinutes = abs(round(($firstDate - $secondDate)/($minute)));
		$resultArray = array();

		if ($dateDiffInMinutes < $hour) {
			$resultArray["value"] = $dateDiffInMinutes;
			$resultArray["units"] = "minute";
		} elseif ($dateDiffInMinutes >= $hour && $dateDiffInMinutes < $day) {
			$resultArray["value"] = round($dateDiffInMinutes/$hour);
			$resultArray["units"] = "hour";
		} elseif ($dateDiffInMinutes >= $day && $dateDiffInMinutes < $month) {
			$resultArray["value"] = round($dateDiffInMinutes/($day));
			$resultArray["units"] = "day";
		} elseif ($dateDiffInMinutes >= $month) {
			$resultArray["value"] = round($dateDiffInMinutes/($month));
			$resultArray["units"] = "month";
		}

		if ($resultArray["value"] > 1) {
			$resultArray["units"] = $resultArray["units"] . "s";
		}

		return $resultArray;
	}

    public static function getDateDiffExt($firstDate, $secondDate)
    {
		$minute = 60;
		$hour = 60*60;
		$day = 60*60*24;
		$month = 60*60*24*30;

		$dateDiffInSeconds = abs(round($firstDate - $secondDate));
		$resultArray = array();

        $resultArray['second'] = $dateDiffInSeconds;
        $resultArray['minute'] = round($dateDiffInSeconds/$minute);
        $resultArray['hour'] = round($dateDiffInSeconds/$hour);
        $resultArray['day'] = round($dateDiffInSeconds/$day);
        $resultArray['month'] = round($dateDiffInSeconds/$month);

		return $resultArray;
	}

    public static function getUrlInputWidth($siteUrl)
    {
        $urlLength = strlen($siteUrl) + 7;
        $inputWidth = 620-$urlLength*10;

        return $inputWidth;
    }

    public static function formatHours($hourInt)
    {
        if (strlen((string)$hourInt) == 1) {
            $hourStr = '0'.(string)$hourInt;
        } else {
            $hourStr = (string)$hourInt;
        }
        return $hourStr;
    }

    /**
     * Reformats Zend locale date to front end date format used by JQuery
     * calendar popup.
     */
    public static function reformatDateFormatForCalendar($inDateFormat)
    {
        $shortFormat = str_replace("MMM", "M", $inDateFormat);
        $shortFormat = str_replace("MM", "mm", $shortFormat);
        $shortFormat = str_replace("yyyy", "y", $shortFormat);
        $shortFormat = str_replace("yy", "y", $shortFormat);
        $shortFormat = str_replace("y", "yy", $shortFormat);

        return $shortFormat;
    }

}