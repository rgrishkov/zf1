<?php
class ZendExt_Controllers_WinGrinController extends Zend_Controller_Action
{
    protected function getLocaleDateFormat()
    {
        $locale = Zend_Registry::get('Zend_Locale');
        return Zend_Locale_Data::getContent($locale, 'date', 'medium');
    }

    protected function getLocaleDateTimeFormat()
    {
        $locale = Zend_Registry::get('Zend_Locale');
        return Zend_Locale_Data::getContent($locale, 'date', 'medium') . ' HH:mm:ss';
    }

    protected function getTimeZone()
    {
        return Zend_Registry::get('Timezone');
    }
}
