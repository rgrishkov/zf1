<?php
class ZendExt_Controllers_WinGrinUserController extends ZendExt_Controllers_WinGrinController
{
    protected $campaign = null;
    protected $competition = null;
    protected $site = null;
    protected $isLocked = false;
    protected $user = null;

    public function init()
    {
        $authentication = Zend_Auth::getInstance();

        if ($authentication->hasIdentity() == true) {
            $userEmail = $authentication->getIdentity();

            $usersMapper = new Application_Model_UsersMapper();
            $user = new Application_Model_Users();

            $usersMapper->loadUserByEmail($userEmail, $user);
            $this->user = $user;
            $this->view->assign('loginedUser', $user);

            $localeString = Zend_Locale::getLocaleToTerritory($user->getCountry());

            if ($localeString != null) {
                $locale = new Zend_Locale($localeString);
            } else {
                $locale = new Zend_Locale('en_AU');
            }

            Zend_Registry::set('Zend_Locale', $locale);

            if ($user->getTime_Zone_Name() != null) {
                Zend_Registry::set('Timezone', $user->getTime_Zone_Name());
                date_default_timezone_set($user->getTime_Zone_Name());
            } else {
                Zend_Registry::set('Timezone', 'Australia/Sydney');
                date_default_timezone_set('Australia/Sydney');
            }

            $layout = Zend_Layout::getMvcInstance();
            $layout->setLayout("user_layout");
            $layout->assign('loginedUser', $user);

            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/user_campaign_defaults.ini', 'heading');
            $this->view->assign('heading', $config->heading);

            $this->authenticatedInit($user);

            if ($this->_getParam('site_id', null) != null){
                $userSitesMapper = new Application_Model_UserSitesMapper();
                $isUserSite = $userSitesMapper->isUserSite($userEmail, $this->_getParam('site_id', null));

                if ($isUserSite == false) {
                    throw new Exception('Invalid site_id');
                }
            }

            if ($this->_getParam('competition_id', null) != null){
                $competitionId = $this->_getParam('competition_id', null);

                $competitionMapper = new Application_Model_CampaignTokenCompMapper();
                $isUserCompetition = $competitionMapper->isUserCompetition($userEmail, $competitionId);

                if ($isUserCompetition == false) {
                    throw new Exception('Invalid competition_id');
                }

                $this->setCompetitionControllerData($competitionId);
            }

            $promotionConfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'promotion');

            if ($promotionConfig->allow_promotion_module == 1) {
                $this->view->assign('allowPromotionModule', true);
            } else {
                $this->view->assign('allowPromotionModule', false);
            }

            if ($this->campaign != null && ($this->competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_NEW || $this->competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_INPROGRESS)) {
                $this->view->assign('stepsNavigation', true);
            } else {
                $this->view->assign('stepsNavigation', false);
            }

        } else {
            $exceptionText = 'Your session has timed out, please <a href="'.$this->view->url(array('controller' => 'index'), null, true).'">Login</a>';

            throw new Exception($exceptionText);
        }
    }

    protected function setCompetitionControllerData($competitionId)
    {
        $this->competition = new Application_Model_CampaignTokenComp();
        $competitionMapper = new Application_Model_CampaignTokenCompMapper();
        $competitionMapper->loadCampaignTokenCompById($competitionId, $this->competition);

        $this->campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($this->competition->getCampaign_Id(), $this->campaign);

        $this->site = new Application_Model_Site();
        $siteMapper = new Application_Model_SiteMapper();
        $siteMapper->loadSiteById($this->campaign->getSite_Id(), $this->site);

        $this->isLocked = $competitionMapper->isLocked($this->campaign->getCampaign_Id());
    }

    /**
     * Perform additional tests to see if user allowed to access the current
     * page. If any errors throw exception.
     *
     * @param <type> $user User object for current logged in user
     */
    protected function authenticatedInit($user)
    {
    }

    protected function  showTopErrorBox() {
        $this->view->assign('showTopErrorBox', true);
    }

}
