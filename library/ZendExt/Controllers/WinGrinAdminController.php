<?php
class ZendExt_Controllers_WinGrinAdminController extends ZendExt_Controllers_WinGrinController
{
    public function init()
    {
        $authentication = Zend_Auth::getInstance();

    	if ($authentication->hasIdentity() == true) {
	    	$userEmail = $authentication->getIdentity();
            $action = $this->_getParam('action');

	    	$usersMapper = new Application_Model_UsersMapper();
	    	$user = new Application_Model_Users();

	    	$usersMapper->loadUserByEmail($userEmail, $user);
            $this->view->assign('loginedUser', $user);

            // Get from user configuation.
            $localeString = Zend_Locale::getLocaleToTerritory($user->getCountry());

            if ($localeString != null) {
                $locale = new Zend_Locale($localeString);
            } else {
                $locale = new Zend_Locale('en_AU');
            }

            Zend_Registry::set('Zend_Locale', $locale);

            if ($user->getTime_Zone_Name() != null) {
                Zend_Registry::set('Timezone', $user->getTime_Zone_Name());
                date_default_timezone_set($user->getTime_Zone_Name());
            } else {
                Zend_Registry::set('Timezone', 'Australia/Sydney');
                date_default_timezone_set('Australia/Sydney');
            }

            $layout = Zend_Layout::getMvcInstance();
            $layout->assign('loginedUser', $user);

            $exceptArray = array('generatecsv', 'downloadcsv');

            if ($user->isAdmin() == false && in_array($action, $exceptArray) == false) {
                throw new Exception('', 404);
            }
    	} else {
    		$this->_helper->redirector('', 'index');
    	}
    }

    /**
     * Perform additional tests to see if user allowed to access the current
     * page. If any errors throw exception.
     *
     * @param <type> $user User object for current logged in user
     */
    protected function authenticatedInit($user)
    {
    }

    /**
     * Get the rendering template layout required for current controller. 
     * Defaults to user layout. 
     * 
     * @return <type> Layout type. Can be "user_layout" or "admin_layout".
     */
    protected function getLayoutType()
    {
        return "user_layout";
    }

    protected function getLocaleDateFormat()
    {
        $locale = Zend_Registry::get('Zend_Locale');
        return Zend_Locale_Data::getContent($locale, 'date', 'medium');
    }

    protected function getLocaleDateTimeFormat()
    {
        $locale = Zend_Registry::get('Zend_Locale');
        return Zend_Locale_Data::getContent($locale, 'date', 'medium') . ' HH:mm:ss';
    }

    protected function getTimeZone()
    {
        return Zend_Registry::get('Timezone');
    }

}
