<?php
class ZendExt_UserForms_LookForm extends ZendExt_UserForms_CompetitionForm
{
    private $competitionDateValidator;

	public function init()
	{
		$this->setAction('/user/competitions/look');
        $this->setAttrib('id', 'look_form');
		$this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

        $configUtils = new ZendExt_Utils_ConfigUtils();
        $configUtils->setCompetitionType($this->competition->getCampaign_Comp_Type());

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'competitions/look_form.phtml'))
        ));

        $competitionName = $this->createElement('text', 'competition_name', array('class' => 'span6'));
        $competitionName->setAttrib('maxlength', 50);
        //$competitionName->removeDecorator('HtmlTag');
        $competitionName->setLabel($configUtils->getCustomLabel($competitionName));
        $competitionName->setDescription($configUtils->getCustomDescription($competitionName));
        $competitionName->setRequired(true);
        $competitionName->addValidator('NotEmpty');
        $competitionName->addValidator(new Zend_Validate_StringLength(array('max' => 50)));
        $this->addElement($competitionName);

        $introText = $this->createElement('text', 'intro_text', array('class' => 'span6'));
        $introText->setAttrib('maxlength', 100);
       // $introText->removeDecorator('HtmlTag');
        $introText->setLabel($configUtils->getCustomLabel($introText));
        $introText->setDescription($configUtils->getCustomDescription($introText));
        $introText->setRequired(true);
        $introText->addValidator('NotEmpty');
        $introText->addValidator(new Zend_Validate_StringLength(array('max' => 100)));
        $this->addElement($introText);

        $startedButtonText = $this->createElement('text', 'get_started_button_text', array('class' => 'span6'));
        $startedButtonText->setAttrib('maxlength', 15);
        $startedButtonText->setLabel($configUtils->getCustomLabel($startedButtonText));
        $startedButtonText->setDescription($configUtils->getCustomDescription($startedButtonText));
        $startedButtonText->setRequired(true);
        $startedButtonText->addValidator('NotEmpty');
        $startedButtonText->addValidator(new Zend_Validate_StringLength(array('max' => 15)));
        $this->addElement($startedButtonText);

        $showImage = $this->createElement('checkbox', 'show_image', array('id' => 'show_image', 'onclick' => 'showHideUploadBox(this.checked); '));
        $showImage->removeDecorator('HtmlTag');
        $showImage->removeDecorator('Label');
        $showImage->setRequired(true);
        $showImage->addValidator('NotEmpty');
        $this->addElement($showImage);

        $this->uploadLogoElement = new ZendExt_Elements_UploadCompetitionLogoElement('upload_logo');
        $this->addElement($this->uploadLogoElement);

        $competitionImagesGallery = new ZendExt_Elements_CompetitionImagesGallery('competition_images_gallery');
        $this->addElement($competitionImagesGallery);


        $showCountdownTimer = $this->createElement('checkbox', 'show_countdown_timer', array('id' => 'show_countdown_timer'));
        $showCountdownTimer->removeDecorator('HtmlTag');
        $showCountdownTimer->removeDecorator('Label');
        $showCountdownTimer->setRequired(true);
        $showCountdownTimer->addValidator('NotEmpty');
        $this->addElement($showCountdownTimer);

        $dateValidator = new Zend_Validate_Date();
        $this->competitionDateValidator = new ZendExt_Validators_CompetitionDateValidator();

        $startDateValidatorChain = new Zend_Validate();
        $startDateValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator($dateValidator)->addValidator($this->competitionDateValidator);

        $startDate = $this->createElement('text', 'start_date', array('onchange' => 'recalculateCompetitionLength(); return false;', 'class' => 'span2'));
       // $startDate->removeDecorator('HtmlTag');
        $startDate->removeDecorator('Label');
        $startDate->setRequired(true);
        $startDate->addValidator($startDateValidatorChain);
        $this->addElement($startDate);

        $endDateValidatorChain = new Zend_Validate();
        $endDateValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator($dateValidator)->addValidator($this->competitionDateValidator);

        $endDate = $this->createElement('text', 'end_date', array('onchange' => 'recalculateCompetitionLength(); return false;', 'class' => 'span2'));
       // $endDate->removeDecorator('HtmlTag');
       // $endDate->removeDecorator('Label');
        $endDate->setRequired(true);
        $endDate->addValidator($endDateValidatorChain);
        $this->addElement($endDate);

        $startTimeHours = $this->createElement('select', 'start_time_hours', array('class' => 'timespan'));
        $startTimeHours->removeDecorator('HtmlTag');
        $startTimeHours->removeDecorator('Label');
        $this->addElement($startTimeHours);

        $startTimeMinutes = $this->createElement('select', 'start_time_minutes', array('class' => 'timespan'));
        $startTimeMinutes->removeDecorator('HtmlTag');
        $startTimeMinutes->removeDecorator('Label');
        $this->addElement($startTimeMinutes);

        $endTimeHours = $this->createElement('select', 'end_time_hours', array('class' => 'timespan'));
        $endTimeHours->removeDecorator('HtmlTag');
        $endTimeHours->removeDecorator('Label');
        $this->addElement($endTimeHours);

        $endTimeMinutes = $this->createElement('select', 'end_time_minutes', array('class' => 'timespan'));
        $endTimeMinutes->removeDecorator('HtmlTag');
        $endTimeMinutes->removeDecorator('Label');
        $this->addElement($endTimeMinutes);

        for ($hoursCounter = 0; $hoursCounter<24; $hoursCounter++) {
            $hourStr = ZendExt_Utils_StringUtils::formatHours($hoursCounter);
            $startTimeHours->addMultiOption($hourStr, $hourStr);
            $endTimeHours->addMultiOption($hourStr, $hourStr);
        }

        for ($minutesCounter = 0; $minutesCounter<60; $minutesCounter+=10) {
            $minutesStr = ZendExt_Utils_StringUtils::formatHours($minutesCounter);
            $startTimeMinutes->addMultiOption($minutesStr, $minutesStr);
            $endTimeMinutes->addMultiOption($minutesStr, $minutesStr);
        }

        $urlValidatorChain = new Zend_Validate();
        $urlValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator(new ZendExt_Validators_UrlValidator());

        $triggerUrl = $this->createElement('text', 'trigger_url', array('class' => 'half'));
        $triggerUrl->removeDecorator('HtmlTag');
        $triggerUrl->removeDecorator('Label');
        $triggerUrl->setRequired(true);
        $triggerUrl->addValidator($urlValidatorChain);
        $this->addElement($triggerUrl);

       /* $triggerUrlType = $this->createElement('radio', 'trigger_url_type', array('onchange' => 'allowTriggerUrl(this.value); return false;'));
        $triggerUrlType->removeDecorator('HtmlTag');
        $triggerUrlType->removeDecorator('Label');
        $triggerUrlType->addMultiOption('no_link', 'Stay on current page');
        $triggerUrlType->addMultiOption('homepage', 'Go to homepage');
        $triggerUrlType->addMultiOption('specified_url', 'Go to specified URL');
        $this->addElement($triggerUrlType);*/

        /*$postEntryLink = $this->createElement('text', 'post_entry_link', array('class' => 'half'));
        $postEntryLink->removeDecorator('HtmlTag');
        $postEntryLink->removeDecorator('Label');
        $postEntryLink->removeDecorator('DtDdWrapper');
        $postEntryLink->setRequired(true);
        $postEntryLink->addValidator($urlValidatorChain);
        $this->addElement($postEntryLink);*/

        $primaryHighLightColor = $this->createElement('hidden', 'primary_highlight_color');
        $primaryHighLightColor->setLabel($configUtils->getCustomLabel($primaryHighLightColor));
        $primaryHighLightColor->removeDecorator('Label');
        $this->addElement($primaryHighLightColor);

        $primaryBackgroundColor = $this->createElement('hidden', 'primary_background_color');
        $primaryBackgroundColor->setLabel($configUtils->getCustomLabel($primaryBackgroundColor));
        $primaryBackgroundColor->removeDecorator('Label');
        $this->addElement($primaryBackgroundColor);

        $primaryFontColor = $this->createElement('hidden', 'primary_font_color');
        $primaryFontColor->setLabel($configUtils->getCustomLabel($primaryFontColor));
        $primaryFontColor->removeDecorator('Label');
        $this->addElement($primaryFontColor);

        $primaryBorderColor = $this->createElement('hidden', 'primary_border_color');
        $primaryBorderColor->setLabel($configUtils->getCustomLabel($primaryBorderColor));
        $primaryBorderColor->removeDecorator('Label');
        $this->addElement($primaryBorderColor);

        $primaryButtonColor = $this->createElement('hidden', 'primary_button_color');
        $primaryButtonColor->setLabel($configUtils->getCustomLabel($primaryButtonColor));
        $primaryButtonColor->removeDecorator('Label');
        $this->addElement($primaryButtonColor);

        $primaryButtonFontColor = $this->createElement('hidden', 'primary_button_font_color');
        $primaryButtonFontColor->setLabel($configUtils->getCustomLabel($primaryButtonFontColor));
        $primaryButtonFontColor->removeDecorator('Label');
        $this->addElement($primaryButtonFontColor);

        $primaryFontStyle = $this->createElement('select', 'primary_font_style');
        $primaryFontStyle->setLabel($configUtils->getCustomLabel($primaryFontStyle));
        $primaryFontStyle->setDescription($configUtils->getCustomDescription($primaryFontStyle));
        $primaryFontStyle->addValidator('NotEmpty');
        $this->addElement($primaryFontStyle);

        $primaryHeaderColor = $this->createElement('hidden', 'primary_header_color');
        $this->addElement($primaryHeaderColor);

        $primaryButtonGradient1 = $this->createElement('hidden', 'primary_button_gradient_1');
        $this->addElement($primaryButtonGradient1);

        $primaryButtonGradient2 = $this->createElement('hidden', 'primary_button_gradient_2');
        $this->addElement($primaryButtonGradient2);


        $siteUrl = $this->createElement('hidden', 'site_url');
      //  $siteUrl->removeDecorator('Label');
	  //  $siteUrl->removeDecorator('HtmlTag');
        $this->addElement($siteUrl);

        $competitionId = $this->createElement('hidden', 'competition_id');
      //  $competitionId->removeDecorator('Label');
      // $competitionId->removeDecorator('HtmlTag');
		$this->addElement($competitionId);

        $themeId = $this->createElement('hidden', 'theme_id');
      //  $themeId->removeDecorator('Label');
	 //	$themeId->removeDecorator('HtmlTag');
		$this->addElement($themeId);

        $competitionLength = $this->createElement('hidden', 'competition_length');
      //  $competitionLength->removeDecorator('Label');
      //  $competitionLength->removeDecorator('HtmlTag');
        $this->addElement($competitionLength);

        $selectedTriggerUrlType = $this->createElement('hidden', 'selected_trigger_url_type');
      //  $selectedTriggerUrlType->removeDecorator('Label');
      //  $selectedTriggerUrlType->removeDecorator('HtmlTag');
        $this->addElement($selectedTriggerUrlType);


		
		$submit = $this->createElement('submit', 'save', array('class' => 'btn primary large'));
		$submit->setLabel('Save & Continue');
		$this->addElement($submit);

	}

    public function populate($formData)
	{
        if ($formData['get_started_button_text'] == null) {
            $formData['get_started_button_text'] = 'Enter Now';
        }
        if ($formData['primary_highlight_color'] == null) {
            $formData['primary_highlight_color'] = '#ffffff';
        }
        if ($formData['primary_background_color'] == null) {
            $formData['primary_background_color'] = '#ffffff';
        }
        if ($formData['primary_font_color'] == null) {
            $formData['primary_font_color'] = '#ffffff';
        }
        if ($formData['primary_border_color'] == null) {
            $formData['primary_border_color'] = '#ffffff';
        }
        if ($formData['primary_button_color'] == null) {
            $formData['primary_button_color'] = '#ffffff';
        }
        if ($formData['primary_button_font_color'] == null) {
            $formData['primary_button_font_color'] = '#ffffff';
        }

        $uploadLogoElementElement = $this->getElement('upload_logo');
        $uploadLogoElementElement->populate($formData);

        $selectedTriggerUrlTypeElement = $this->getElement('selected_trigger_url_type');
        $selectedTriggerUrlTypeElement->setValue($formData['trigger_url_type']);

        $this->selectView();

		return parent::populate($formData);
	}

    protected function selectView()
    {
        switch($this->competitionType) {
        case COMPETITION_TYPE_BASIC_COMPETITION:
            $templatePath = 'competitions/types/basic-competition/look_form.phtml';
            break;
        case COMPETITION_TYPE_BASIC_DEAL:
            $templatePath = 'competitions/types/basic-deal/look_form.phtml';
            break;
        case COMPETITION_TYPE_BASIC_SIGNUP_FORM:
            $templatePath = 'competitions/types/basic-signup-form/look_form.phtml';
            break;
        case COMPETITION_TYPE_BASIC_ANNOUNCEMENT:
            $templatePath = 'competitions/types/basic-announcement/look_form.phtml';

            $saveButtonElement = $this->getElement('save');
            $saveButtonElement->setLabel('Save');
            break;
        default:
            $templatePath = 'competitions/types/basic-competition/look_form.phtml';
        }

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => $templatePath))
        ));
    }




    public function isValid($formData)
    {
        $this->competitionDateValidator->setCompetition($this->competition);

        if (isset($formData['start_date'])) {
            $startDateLiveString = $formData['start_date'].' '.$formData['start_time_hours'].':'.$formData['start_time_minutes'].':00';
            $startDate = new Zend_Date($startDateLiveString);
            $startDate->setTimeZone('GMT');
            $this->competitionDateValidator->setStartDate($startDate);
        }

        if (isset($formData['end_date'])) {
            $endDateString = $formData['end_date'].' '.$formData['end_time_hours'].':'.$formData['end_time_minutes'].':00';
            $endDate = new Zend_Date($endDateString);
            $endDate->setTimeZone('GMT');
            $this->competitionDateValidator->setEndDate($endDate);
        }

        /*if ($this->competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_SCHEDULED) {
            $minDays = $this->product->getMin_Days();
            $maxDays = $this->product->getMax_Days();

            $competitionLengthElement = $this->getElement('competition_length');

            $lengthRangeValidator = new Zend_Validate_Between($minDays, $maxDays);
            $competitionLengthElement->addValidator($lengthRangeValidator);
        }

        return parent::isValid($formData);*/
        if ($formData['trigger_url_type'] != 'specified_url') {
            $triggerUrlElement = $this->getElement('trigger_url');
            $triggerUrlElement->removeValidator('NotEmpty');
            $triggerUrlElement->setRequired(false);
        }


        /*if ($formData['post_entry_link_type'] != 'specified_url') {
            $postEntryLinkElement = $this->getElement('post_entry_link');
            $postEntryLinkElement->removeValidator('NotEmpty');
            $postEntryLinkElement->setRequired(false);
        }*/

        return parent::isValid($formData);
    }


    public function setCompetitionId($competitionId)
    {
        $competitionElement = $this->getElement('competition_id');
		$competitionElement->setValue($competitionId);
	}

    public function setCompetition(Application_Model_CampaignTokenComp $competition)
    {
        $this->competition = $competition;
    }

    public function lock()
    {
        $formElements = $this->getElements();
        if ($this->competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_SCHEDULED) {
            //$minDays = $this->product->getMin_Days();
            //$maxDays = $this->product->getMax_Days();

            /*$competitionLengthElement = $this->getElement('competition_length');
            $competitionLengthElement->clearMultiOptions();

            for ($lengthCounter = $minDays; $lengthCounter<=$maxDays; $lengthCounter++) {
                $competitionLengthElement->addMultiOption($lengthCounter, $lengthCounter);
            }*/

        } elseif($this->competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_RUNNING) {
            //$competitionLengthElement = $this->getElement('competition_length');
            //$competitionLengthElement->setAttrib('disabled', 'disabled');

            $this->lockDates();
        } elseif($this->competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_COMPLETED) {

            foreach ($formElements as $element) {
                $element->setAttrib('disabled', 'disabled');
            }

            //$competitionImagesGallery = $this->getElement('competition_images_gallery');
            //$competitionImagesGallery->lock();

            $this->removeElement('save');
        }
    }

    private function lockDates()
    {
        $startDateField = $this->getElement('start_date');
        $startDateField->setAttrib('disabled', 'disabled');
        $startDateField->setRequired(false);
        $startDateField->clearValidators();

        /*$endDateField = $this->getElement('end_date');
        $endDateField->setAttrib('disabled', 'disabled');
        $endDateField->setRequired(false);
        $endDateField->clearValidators();*/

        $startTimeHoursField = $this->getElement('start_time_hours');
        $startTimeHoursField->setAttrib('disabled', 'disabled');

        $startTimeMinutesField = $this->getElement('start_time_minutes');
        $startTimeMinutesField->setAttrib('disabled', 'disabled');

        /*$endTimeHoursField = $this->getElement('end_time_hours');
        $endTimeHoursField->setAttrib('disabled', 'disabled');

        $endTimeMinutesField = $this->getElement('end_time_minutes');
        $endTimeMinutesField->setAttrib('disabled', 'disabled');*/
    }

    public function fillFonts($optionsList)
    {
        $select = $this->getElement('primary_font_style');

        foreach ($optionsList as $id => $title) {
            $select->addMultiOption($id, $title);
        }
    }
}