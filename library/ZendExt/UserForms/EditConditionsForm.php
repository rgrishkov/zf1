<?php
class ZendExt_UserForms_EditConditionsForm extends ZendExt_UserForms_CompetitionForm
{
	public function init()
	{
		$this->setAction('/user/competitions/conditions');
		$this->setMethod('post');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'competitions/conditions_form.phtml'))
        ));

        $conditionSampleId = $this->createElement('select', 'condition_sample_id');
        $conditionSampleId->removeDecorator('HtmlTag');
        $conditionSampleId->removeDecorator('Label');
        $this->addElement($conditionSampleId);

        $previewButton = $this->createElement('button', 'preview_button',
            array(
                'class' => 'btn data-controls-modal',
                'onclick' => 'previewCondition(); return false;',
                'data-controls-modal' => 'preview_condition_popup',
                'data-backdrop' => true,
                'data-keyboard' => true
            ));
		$previewButton->removeDecorator('HtmlTag');
        $previewButton->removeDecorator('Label');
        $previewButton->removeDecorator('DtDdWrapper');
        $previewButton->setLabel('Preview sample T&C');
		$this->addElement($previewButton);

        $tokensValidator = new ZendExt_Validators_ConditionTokensValidator();

        $contentValidatorChain = new Zend_Validate();
        $contentValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator($tokensValidator);

        $conditionContent = $this->createElement('textarea', 'condition_content', array('class' => 'full wysiwyg'));
		$conditionContent->setAttribs(array('rows' => 20, 'cols' => 20));
		$conditionContent->setLabel('Terms and conditions:');
		$conditionContent->setRequired(true);
		$conditionContent->addValidator($contentValidatorChain);
		$this->addElement($conditionContent);

        $conditionId = $this->createElement('hidden', 'condition_id');
        $conditionId->removeDecorator('Label');
		$conditionId->removeDecorator('HtmlTag');
		$this->addElement($conditionId);

        $competitionId = $this->createElement('hidden', 'competition_id');
        $competitionId->removeDecorator('Label');
		$competitionId->removeDecorator('HtmlTag');
		$this->addElement($competitionId);

        $orderStatus = $this->createElement('hidden', 'order_status');
        $orderStatus->removeDecorator('Label');
        $orderStatus->removeDecorator('HtmlTag');
        $this->addElement($orderStatus);
		
		$submit = $this->createElement('submit', 'save', array('class' => 'btn primary large'));
		$submit->setLabel('Save changes');
		$this->addElement($submit);
	}

    public function fillConditionsList($locationsList)
	{
        $select = $this->getElement('condition_sample_id');

		foreach ($locationsList as $id => $title) {
			$select->addMultiOption($id, $title);
		}
	}

    public function setCompetitionId($competitionId)
    {
        $competitionIdField = $this->getElement('competition_id');
		$competitionIdField->setValue($competitionId);
	}

    public function populate($formData)
    {
        $this->selectView();

        return parent::populate($formData);
    }


    protected function selectView()
    {
        switch($this->competitionType) {
            case COMPETITION_TYPE_BASIC_COMPETITION:
                $templatePath = 'competitions/types/basic-competition/conditions_form.phtml';
                break;
            case COMPETITION_TYPE_BASIC_DEAL:
                $templatePath = 'competitions/types/basic-deal/conditions_form.phtml';
                break;
            case COMPETITION_TYPE_BASIC_SIGNUP_FORM:
                $templatePath = 'competitions/types/basic-signup-form/conditions_form.phtml';
                break;
            default:
                $templatePath = 'competitions/types/basic-competition/conditions_form.phtml';
                break;
        }

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => $templatePath))
        ));
    }

    public function lock()
    {
        $formElements = $this->getElements();

        foreach ($formElements as $element) {
            $element->setAttrib('disabled', 'disabled');
        }

        $this->removeElement('save');
    }
}