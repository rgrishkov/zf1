<?php
class ZendExt_UserForms_CampaignQuestionsForm extends ZendExt_UserForms_CompetitionForm
{
	public function init()
	{
		$this->setAction('/user/competitions/questions');
		$this->setMethod('post');

        $configUtils = new ZendExt_Utils_ConfigUtils();
        $configUtils->setCompetitionType($this->competition->getCampaign_Comp_Type());

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'competitions/questions_form.phtml'))
        ));

        $skillQuestion = $this->createElement('text', 'skill_question', array('class' => 'full'));
        $skillQuestion->setAttrib('maxlength', 200);
        $skillQuestion->removeDecorator('HtmlTag');
        $skillQuestion->removeDecorator('Label');
		$skillQuestion->addDecorator('Label');
		$skillQuestion->setLabel($configUtils->getCustomLabel($skillQuestion));
        $skillQuestion->setDescription($configUtils->getCustomDescription($skillQuestion));
		$skillQuestion->setRequired(true);
		$skillQuestion->addValidator('NotEmpty');
        $skillQuestion->addValidator(new Zend_Validate_StringLength(array('max' => 200)));
		$this->addElement($skillQuestion);

        $maxWordsValidatorChain = new Zend_Validate();
        $maxWordsValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator(new Zend_Validate_Digits());

        $maxWords = $this->createElement('text', 'max_words', array('class' => 'span1'));
        $maxWords->removeDecorator('Label');
		$maxWords->addDecorator('Label');
		$maxWords->setLabel($configUtils->getCustomLabel($maxWords));
        $maxWords->setDescription($configUtils->getCustomDescription($maxWords));
        $maxWords->setAttrib('maxlength', 3);
        $maxWords->setAttrib('size', 1);
		$maxWords->setRequired(true);
		$maxWords->addValidator($maxWordsValidatorChain);
		$this->addElement($maxWords);

        $allowMultipleChoiceQuestion = $this->createElement('checkbox', 'allow_multiple_choice_question', array('onchange' => 'allowMultipleChoiceQuestion(); return false;'));
        $allowMultipleChoiceQuestion->removeDecorator('HtmlTag');
        $allowMultipleChoiceQuestion->removeDecorator('Label');
		$allowMultipleChoiceQuestion->setRequired(true);
		$this->addElement($allowMultipleChoiceQuestion);

        $allowSkillQuestion = $this->createElement('checkbox', 'allow_skill_question', array('onchange' => 'allowSkillQuestion(); return false;'));
        $allowSkillQuestion->removeDecorator('HtmlTag');
        $allowSkillQuestion->removeDecorator('Label');
		$allowSkillQuestion->setRequired(true);
		$this->addElement($allowSkillQuestion);

		$question = $this->createElement('text', 'question', array('class' => 'full'));
        $question->setAttrib('maxlength', 100);
        $question->removeDecorator('HtmlTag');
		$question->removeDecorator('Label');
		$question->addDecorator('Label');
        $question->setLabel($configUtils->getCustomLabel($question));
        $question->setDescription($configUtils->getCustomDescription($question));
		$question->setRequired(true); 
		$question->addValidator('NotEmpty');
        $question->addValidator(new Zend_Validate_StringLength(array('max' => 100)));
		$this->addElement($question);
		
		$answer1 = $this->createElement('text', 'answer1', array('class' => 'full'));
        $answer1->setAttrib('maxlength', 50);
		$answer1->removeDecorator('HtmlTag');
		$answer1->removeDecorator('Label');
		$answer1->addDecorator('Label');
		$answer1->setLabel($configUtils->getCustomLabel($answer1));
        $answer1->setDescription($configUtils->getCustomDescription($answer1));
		$answer1->setRequired(true);
		$answer1->addValidator('NotEmpty');
        $answer1->addValidator(new Zend_Validate_StringLength(array('max' => 50)));
		$this->addElement($answer1);
		
		$answer1Correct = $this->createElement('checkbox', 'answer1_correct');
		$answer1Correct->removeDecorator('HtmlTag');
        $answer1Correct->removeDecorator('Label');
		$this->addElement($answer1Correct);

		$answer2 = $this->createElement('text', 'answer2', array('class' => 'full'));
        $answer2->setAttrib('maxlength', 50);
		$answer2->removeDecorator('HtmlTag');
		$answer2->removeDecorator('Label');
		$answer2->setRequired(true);
		$answer2->addValidator('NotEmpty');
        $answer2->addValidator(new Zend_Validate_StringLength(array('max' => 50)));
		$this->addElement($answer2);
		
		$answer2Correct = $this->createElement('checkbox', 'answer2_correct');
		$answer2Correct->removeDecorator('HtmlTag');
		$answer2Correct->removeDecorator('Label');
		$this->addElement($answer2Correct);
		
		$answer3 = $this->createElement('text', 'answer3', array('class' => 'full'));
        $answer3->setAttrib('maxlength', 50);
		$answer3->removeDecorator('HtmlTag');
		$answer3->removeDecorator('Label');
        $answer3->addValidator(new Zend_Validate_StringLength(array('max' => 50)));
		$this->addElement($answer3);
		
		$answer3Correct = $this->createElement('checkbox', 'answer3_correct');
		$answer3Correct->removeDecorator('HtmlTag');
		$answer3Correct->removeDecorator('Label');
		$this->addElement($answer3Correct);

		$answer4 = $this->createElement('text', 'answer4', array('class' => 'full'));
        $answer3->setAttrib('maxlength', 50);
		$answer4->removeDecorator('HtmlTag');
		$answer4->removeDecorator('Label');
        $answer4->addValidator(new Zend_Validate_StringLength(array('max' => 50)));
		$this->addElement($answer4);
		
		$answer4Correct = $this->createElement('checkbox', 'answer4_correct');
		$answer4Correct->removeDecorator('HtmlTag');
		$answer4Correct->removeDecorator('Label');
		$this->addElement($answer4Correct);

        $correctAnswersValidator = new ZendExt_Validators_CorrectAnswersValidator();
		$correctAnswersValidator->addElement(array('answer' => $answer1, 'correct' => $answer1Correct));
		$correctAnswersValidator->addElement(array('answer' => $answer2, 'correct' => $answer2Correct));
		$correctAnswersValidator->addElement(array('answer' => $answer3, 'correct' => $answer3Correct));
		$correctAnswersValidator->addElement(array('answer' => $answer4, 'correct' => $answer4Correct));

        $emptyField = $this->createElement('hidden', 'empty_field');
        $emptyField->removeDecorator('Label');
		$emptyField->removeDecorator('HtmlTag');
        $emptyField->setValue(1);
        $emptyField->addValidator($correctAnswersValidator);
		$this->addElement($emptyField);

        $userDetails = $this->createElement('radio', 'user_details', array('onchange' => 'showHideProvidersLink(this.value); return false;'));
        $userDetails->removeDecorator('HtmlTag');
        $userDetails->removeDecorator('DtDdWrapper');
        $userDetails->removeDecorator('Label');
        $userDetails->addMultiOption('collect_user_details', 'Just collect them - I will download the CSV');
        $userDetails->addMultiOption('send_emails', 'Send the email leads direct to my email list provider');
        $userDetails->setValue('collect_user_details');
        

        $userDetails->setSeparator('<br>');
		$this->addElement($userDetails);




        $primaryHighLightColor = $this->createElement('hidden', 'primary_highlight_color');
        $this->addElement($primaryHighLightColor);

        $primaryBackgroundColor = $this->createElement('hidden', 'primary_background_color');
        $this->addElement($primaryBackgroundColor);

        $primaryFontColor = $this->createElement('hidden', 'primary_font_color');
        $this->addElement($primaryFontColor);

        $primaryBorderColor = $this->createElement('hidden', 'primary_border_color');
        $this->addElement($primaryBorderColor);

        $primaryButtonColor = $this->createElement('hidden', 'primary_button_color');
        $this->addElement($primaryButtonColor);

        $primaryButtonFontColor = $this->createElement('hidden', 'primary_button_font_color');
        $this->addElement($primaryButtonFontColor);

        $fullImagePath = $this->createElement('hidden', 'full_image_path');
        $this->addElement($fullImagePath);

        $showImage = $this->createElement('hidden', 'show_image');
        $this->addElement($showImage);

        $showCountdownTimer = $this->createElement('hidden', 'show_countdown_timer');
        $this->addElement($showCountdownTimer);

        $getStartedButtonText = $this->createElement('hidden', 'get_started_button_text');
        $this->addElement($getStartedButtonText);

        $competitionName = $this->createElement('hidden', 'competition_name');
        $this->addElement($competitionName);

        $introText = $this->createElement('hidden', 'intro_text');
        $this->addElement($introText);

        $primaryHeaderColor = $this->createElement('hidden', 'primary_header_color');
        $this->addElement($primaryHeaderColor);

        $primaryButtonGradient1 = $this->createElement('hidden', 'primary_button_gradient_1');
        $this->addElement($primaryButtonGradient1);

        $primaryButtonGradient2 = $this->createElement('hidden', 'primary_button_gradient_2');
        $this->addElement($primaryButtonGradient2);

        $mailListProviderName = $this->createElement('hidden', 'mail_list_provider_name');
        $this->addElement($mailListProviderName);

        $mailListName = $this->createElement('hidden', 'mail_list_name');
        $this->addElement($mailListName);

		$questionId = $this->createElement('hidden', 'question_id');
        $questionId->removeDecorator('Label');
		$questionId->removeDecorator('HtmlTag');
		$this->addElement($questionId);

        $siteUrl = $this->createElement('hidden', 'site_url');
        $siteUrl->removeDecorator('Label');
		$siteUrl->removeDecorator('HtmlTag');
		$this->addElement($siteUrl);

        $competitionId = $this->createElement('hidden', 'competition_id');
		$competitionId->removeDecorator('Label');
		$competitionId->removeDecorator('HtmlTag');
        $this->addElement($competitionId);
	
		$submit = $this->createElement('submit', 'save', array('class' => 'btn primary large'));
        //$submit->removeDecorator('Label');
        //$submit->removeDecorator('HtmlTag');
       // $submit->removeDecorator('DtDdWrapper');
        $submit->setLabel('Save & Continue');
		$this->addElement($submit);
	}
	
    public function setCompetitionId($competitionId)
    {
        $competitionElement = $this->getElement('competition_id');
		$competitionElement->setValue($competitionId);
	}

    public function populate($formData)
    {
        if ($formData['question'] != null) {
            $allowSkillQuestionElement = $this->getElement('allow_multiple_choice_question');
            $allowSkillQuestionElement->setAttrib('checked', 'checked');
        }

        if ($formData['skill_question'] != null) {
            $allowSkillQuestionElement = $this->getElement('allow_skill_question');
            $allowSkillQuestionElement->setAttrib('checked', 'checked');
        }

        if ($formData['mail_provider_id'] != null && $formData['mail_list_id'] != null) {
            $userDetailsElement = $this->getElement('user_details');
            $userDetailsElement->setValue('send_emails');
        }

        $this->selectView();

        return parent::populate($formData);
    }

    protected function selectView()
    {
        switch($this->competitionType) {
            case COMPETITION_TYPE_BASIC_COMPETITION:
                $templatePath = 'competitions/types/basic-competition/questions_form.phtml';
                break;
            case COMPETITION_TYPE_BASIC_DEAL:
                $templatePath = 'competitions/types/basic-deal/questions_form.phtml';
                break;
            case COMPETITION_TYPE_BASIC_SIGNUP_FORM:
                $templatePath = 'competitions/types/basic-signup-form/questions_form.phtml';
                break;
            default:
                $templatePath = 'competitions/types/basic-competition/questions_form.phtml';
                break;
        }

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => $templatePath))
        ));
    }

    public function isValid($formData)
	{
        if ((bool)$formData['allow_skill_question'] == false) {
            $skillQuestionElement = $this->getElement('skill_question');
            $skillQuestionElement->removeValidator('NotEmpty');
            $skillQuestionElement->setRequired(false);

            $maxWordsElement = $this->getElement('max_words');
            $maxWordsElement->removeValidator('NotEmpty');
            $maxWordsElement->setRequired(false);
        }

        if ((bool)$formData['allow_multiple_choice_question'] == false) {
            $questionElement = $this->getElement('question');
            $questionElement->removeValidator('NotEmpty');
            $questionElement->setRequired(false);

            $emptyFieldElement = $this->getElement('empty_field');
            $emptyFieldElement->removeValidator('ZendExt_Validators_CorrectAnswersValidator');

            $answer1Field = $this->getElement('answer1');
            $answer1Field->removeValidator('NotEmpty');
            $answer1Field->setRequired(false);

            $answer2Field = $this->getElement('answer2');
            $answer2Field->removeValidator('NotEmpty');
            $answer2Field->setRequired(false);
        }

		return parent::isValid($formData);
	}

    public function setSiteUrl($siteUrl)
    {
        /*$triggerUrlField = $this->getElement('trigger_url');
        $httpPrefixDecorator = $triggerUrlField->getDecorator('httpPrefixDecorator');
        $httpPrefixDecorator->setOption('site_url', $siteUrl);

        $inputWidth = ZendExt_Utils_StringUtils::getUrlInputWidth($siteUrl);

        $triggerUrlField->setOptions(array('style' => 'width: '.$inputWidth.'px;'));*/

        $siteUrlField = $this->getElement('site_url');
        $siteUrlField->setValue('http://'.$siteUrl);
    }

    public function lock()
    {
        $formElements = $this->getElements();

        foreach ($formElements as $element) {
            $element->setAttrib('disabled', 'disabled');
        }
        $this->removeElement('save');
    }
}