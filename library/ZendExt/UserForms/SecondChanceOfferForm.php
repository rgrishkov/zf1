<?php
class ZendExt_UserForms_SecondChanceOfferForm extends ZendExt_Forms_Form
{
	public function init()
	{
		$this->setAction('/user/promotion/offer');
		$this->setMethod('post');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'promotion/second_chance_offer_form.phtml'))
        ));

        $allowSecondChanceOffer = $this->createElement('checkbox', 'allow_second_chance_offer', array('onclick' => 'allowSecondChanceOffer();'));
        $allowSecondChanceOffer->removeDecorator('HtmlTag');
        $allowSecondChanceOffer->removeDecorator('Label');
		$this->addElement($allowSecondChanceOffer);

        $offerSummary = $this->createElement('text', 'offer_summary', array('class' => 'full'));
        $offerSummary->setAttrib('maxlength', 200);
		$offerSummary->setLabel('Offer summary:');
		$offerSummary->setRequired(true);
		$offerSummary->addValidator('NotEmpty');
        $offerSummary->addValidator(new Zend_Validate_StringLength(array('max' => 200)));
		$this->addElement($offerSummary);

        $offerDetails = $this->createElement('textarea', 'offer_details', array('class' => 'full wysiwyg'));
		$offerDetails->setAttribs(array('rows' => 10, 'cols' => 20));
		$offerDetails->setLabel('Offer details:');
		$offerDetails->setRequired(true);
		$offerDetails->addValidator('NotEmpty');
		$this->addElement($offerDetails);

        $postDetailsId = $this->createElement('hidden', 'post_details_id');
        $postDetailsId->removeDecorator('Label');
		$postDetailsId->removeDecorator('HtmlTag');
		$this->addElement($postDetailsId);

        $competitionId = $this->createElement('hidden', 'competition_id');
        $competitionId->removeDecorator('Label');
		$competitionId->removeDecorator('HtmlTag');
		$this->addElement($competitionId);
		
		$submit = $this->createElement('submit', 'save', array('class' => 'btn btn-green big'));
		$submit->setLabel('Save & Continue');
		$this->addElement($submit);
	}

    public function setCompetitionId($competitionId)
    {
        $competitionIdField = $this->getElement('competition_id');
		$competitionIdField->setValue($competitionId);
	}

    public function populate($formData)
    {
        if ($formData['offer_summary'] != null || $formData['offer_details'] != null) {
            $allowSecondChanceOfferElement = $this->getElement('allow_second_chance_offer');
            $allowSecondChanceOfferElement->setAttrib('checked', 'checked');
        }

        return parent::populate($formData);
    }

    public function lock()
    {
        $formElements = $this->getElements();

        foreach ($formElements as $element) {
            $element->setAttrib('disabled', 'disabled');
        }

        $this->removeElement('save');
    }
}