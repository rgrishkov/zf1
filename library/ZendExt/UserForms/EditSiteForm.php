<?php
class ZendExt_UserForms_EditSiteForm extends ZendExt_Forms_Form
{
	public function init()
	{
		$this->setAction('/user/sites/edit');
        //$this->setAction('/user/sites/ajax-image-upload');
        $this->setAttrib('id', 'edit_site_form');
		$this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

        $configUtils = new ZendExt_Utils_ConfigUtils();

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'sites/site_form.phtml'))
        ));
		     
		$siteId = $this->createElement('text', 'external_site_id', array('class' => 'half readonly'));
        $siteId->removeDecorator('HtmlTag');
		$siteId->setAttrib('readonly', 'readonly');
		$siteId->setLabel($configUtils->getCustomLabel($siteId));
        $siteId->setDescription($configUtils->getCustomDescription($siteId));
		$siteId->setRequired(true); 
		$siteId->addValidator('NotEmpty'); 
		$this->addElement($siteId); 
		
        $urlValidatorChain = new Zend_Validate();
        $urlValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator(new ZendExt_Validators_UrlValidator());
		
		$siteUrl = $this->createElement('text', 'site_url', array('class' => 'half site_url'));
        $siteUrl->removeDecorator('Label');
        $siteUrl->removeDecorator('HtmlTag');
        $siteUrl->setDescription($configUtils->getCustomDescription($siteUrl));
		$siteUrl->setRequired(true); 
		$siteUrl->addValidator($urlValidatorChain);
		$this->addElement($siteUrl);
		
		$submit = $this->createElement('submit', 'save', array('class' => 'btn primary'));
		$submit->setLabel('Save');
		$this->addElement($submit);
		

	}
	
	public function populate($formData)
	{
        //$uploadLogoElementElement = $this->getElement('upload_logo');
        //$uploadLogoElementElement->populate($formData);

		return parent::populate($formData);
	}
	
	public function isValid($formData)
	{
		//$file = $this->uploadImageForm->getElement('site_logo');
		//$file->setRequired(false);
		//$file->removeValidator('NotEmpty');
		
		return parent::isValid($formData);
	}

    public function lock()
    {
        $formElements = $this->getElements();

        foreach ($formElements as $element) {
            $element->setAttrib('disabled', 'disabled');
        }
        $this->removeElement('save');
    }
}