<?php
class ZendExt_UserForms_PromotionForm extends ZendExt_Forms_Form
{
	public function init()
	{
		$this->setAction('/user/promotion/broadcast');
		$this->setMethod('post');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'promotion/promotion_form.phtml'))
        ));

        $mailPromotion = $this->createElement('checkbox', 'mail_promotion');
        $mailPromotion->removeDecorator('HtmlTag');
        $mailPromotion->removeDecorator('Label');
		$this->addElement($mailPromotion);

        $twitterPromotion = $this->createElement('checkbox', 'twitter_promotion');
        $twitterPromotion->removeDecorator('HtmlTag');
        $twitterPromotion->removeDecorator('Label');
		$this->addElement($twitterPromotion);

        $facebookPromotion = $this->createElement('checkbox', 'facebook_promotion');
        $facebookPromotion->removeDecorator('HtmlTag');
        $facebookPromotion->removeDecorator('Label');
		$this->addElement($facebookPromotion);


        $competitionId = $this->createElement('hidden', 'competition_id');
        $competitionId->removeDecorator('Label');
		$competitionId->removeDecorator('HtmlTag');
		$this->addElement($competitionId);
		
		$submit = $this->createElement('submit', 'save', array('class' => 'btn btn-green big'));
		$submit->setLabel('Save & Continue');
		$this->addElement($submit);
	}

    public function setCompetitionId($competitionId)
    {
        $competitionIdField = $this->getElement('competition_id');
		$competitionIdField->setValue($competitionId);
	}

   /* public function populate($formData)
    {
        if ($formData['post_details_id'] != null) {
            $allowPostDetailsElement = $this->getElement('allow_post_details');
            $allowPostDetailsElement->setAttrib('checked', 'checked');
        }

        return parent::populate($formData);
    }*/

    public function lock()
    {
        $formElements = $this->getElements();

        foreach ($formElements as $element) {
            $element->setAttrib('disabled', 'disabled');
        }

        $this->removeElement('save');
    }
}