<?php
class ZendExt_UserForms_UploadCompetitionImageForm extends ZendExt_Forms_SubForm
{
	public function init()
	{
        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'competitions/upload_image_form.phtml'))
        ));

		//$this->setAction('/user/competitions/edit');
		$this->setMethod('post');
		$this->setAttrib('enctype', 'multipart/form-data');

        //$fileTypeValidator = new Zend_Validate_File_IsImage(array('image/png'));
        //$fileTypeValidator->setMessage('Image must be png file');


        $file = $this->createElement('file', 'image_file');
        $file->removeDecorator('HtmlTag');
        $file->removeDecorator('Label');
        $file->setRequired(true); 
		$file->addValidator('NotEmpty');
		$this->addElement($file);
		
		$submit = $this->createElement('submit', 'upload_image', array('class' => 'btn btn-green big'));
		$submit->setLabel('Upload');
		$this->addElement($submit);
	}
}