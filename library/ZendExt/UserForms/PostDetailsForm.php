<?php
class ZendExt_UserForms_PostDetailsForm extends ZendExt_Forms_Form
{
    private $urlUniqueValidator;

	public function init()
	{
		$this->setAction('/user/promotion/listing');
        $this->setAttrib('id', 'post_details_form');
		$this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'promotion/post_details_form.phtml'))
        ));

        $allowPostDetails = $this->createElement('checkbox', 'allow_post_details', array('onclick' => 'allowPostDetails();'));
        $allowPostDetails->removeDecorator('HtmlTag');
        $allowPostDetails->removeDecorator('Label');
		$this->addElement($allowPostDetails);

        $postName = $this->createElement('text', 'post_name', array('class' => 'half'));
        $postName->setAttrib('maxlength', 200);
		$postName->setLabel('Post Heading:');
		$postName->setRequired(true);
		$postName->addValidator('NotEmpty');
        $postName->addValidator(new Zend_Validate_StringLength(array('max' => 200)));
		$this->addElement($postName);

        $postContent = $this->createElement('textarea', 'post_content', array('class' => 'full wysiwyg'));
		$postContent->setAttribs(array('rows' => 10, 'cols' => 20));
		$postContent->setLabel('Post Text:');
		$postContent->setRequired(true);
		$postContent->addValidator('NotEmpty');
		$this->addElement($postContent);

        $siteName = $this->createElement('text', 'site_name', array('class' => 'half'));
        $siteName->setAttrib('maxlength', 100);
        $siteName->removeDecorator('HtmlTag');
		$siteName->setLabel(ZendExt_Utils_ConfigUtils::getCustomLabel($siteName));
        $siteName->setDescription(ZendExt_Utils_ConfigUtils::getCustomDescription($siteName));
		$siteName->setRequired(true);
		$siteName->addValidator('NotEmpty');
        $siteName->addValidator(new Zend_Validate_StringLength(array('max' => 100)));
		$this->addElement($siteName);

        $urlValidatorChain = new Zend_Validate();
        $this->urlUniqueValidator = new ZendExt_Validators_UrlUniqueValidator();
        $urlValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator(new ZendExt_Validators_UrlValidator())->addValidator($this->urlUniqueValidator);

		$siteUrl = $this->createElement('text', 'site_url', array('class' => 'site_url'));
        $siteUrl->setAttrib('maxlength', 100);
        $siteUrl->removeDecorator('Label');
        $siteUrl->removeDecorator('HtmlTag');
		//$siteUrl->setLabel(ZendExt_Utils_ConfigUtils::getCustomLabel($siteUrl));
        $siteUrl->setDescription(ZendExt_Utils_ConfigUtils::getCustomDescription($siteUrl));
		$siteUrl->setRequired(true);
		$siteUrl->addValidator($urlValidatorChain);
        $siteUrl->addValidator(new Zend_Validate_StringLength(array('max' => 100)));
		$this->addElement($siteUrl);

        $siteBio = $this->createElement('textarea', 'site_bio', array('class' => 'full wysiwyg'));
		$siteBio->setAttribs(array('rows' => 10, 'cols' => 20));
		$siteBio->setLabel('Site Bio:');
		$siteBio->setRequired(true);
		$siteBio->addValidator('NotEmpty');
		$this->addElement($siteBio);

        $this->uploadLogoElement = new ZendExt_Elements_UploadSiteLogoElement('upload_logo');
        $this->addElement($this->uploadLogoElement);

        $postDetailsId = $this->createElement('hidden', 'post_details_id');
        $postDetailsId->removeDecorator('Label');
		$postDetailsId->removeDecorator('HtmlTag');
		$this->addElement($postDetailsId);

        $competitionId = $this->createElement('hidden', 'competition_id');
        $competitionId->removeDecorator('Label');
		$competitionId->removeDecorator('HtmlTag');
		$this->addElement($competitionId);

        $siteId = $this->createElement('hidden', 'site_id');
        $siteId->removeDecorator('Label');
		$siteId->removeDecorator('HtmlTag');
		$this->addElement($siteId);
		
		$submit = $this->createElement('submit', 'save', array('class' => 'btn btn-green big'));
		$submit->setLabel('Save & Continue');
		$this->addElement($submit);
	}

    public function setCurrentUrl($value)
	{
		$this->urlUniqueValidator->setCurrentUrl($value);
	}

    public function setCompetitionId($competitionId)
    {
        $competitionIdField = $this->getElement('competition_id');
		$competitionIdField->setValue($competitionId);
	}

    public function setSiteId($siteId)
    {
        $siteIdField = $this->getElement('site_id');
		$siteIdField->setValue($siteId);
	}

    public function populate($formData)
    {
        if ($formData['post_details_id'] != null) {
            $allowPostDetailsElement = $this->getElement('allow_post_details');
            $allowPostDetailsElement->setAttrib('checked', 'checked');
        }

        $uploadLogoElementElement = $this->getElement('upload_logo');
        $uploadLogoElementElement->populate($formData);

        return parent::populate($formData);
    }

    public function lock()
    {
        $formElements = $this->getElements();

        foreach ($formElements as $element) {
            $element->setAttrib('disabled', 'disabled');
        }

        $this->removeElement('save');
    }
}