<?php
class ZendExt_UserForms_EditUserForm extends ZendExt_Forms_Form
{
    private $currentPassword = null;

	public function init()
	{
		$this->setAction('/user/settings/edit');
		$this->setMethod('post');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'settings/user_form.phtml'))
        ));

        $email = $this->createElement('text', 'email', array('class' => 'half readonly'));
        //$email->removeDecorator('HtmlTag');
        $email->setLabel('Email:');
        //$email->setRequired(true);
        //$email->addValidator('NotEmpty');
        $this->addElement($email);


		$firstName = $this->createElement('text', 'firstname', array('class' => 'half'));
        //$firstName->removeDecorator('HtmlTag');
		$firstName->setLabel('First name:');
		$firstName->setRequired(true); 
		$firstName->addValidator('NotEmpty'); 
		$this->addElement($firstName); 
		
		$surName = $this->createElement('text', 'surname', array('class' => 'half'));
        //$surName->removeDecorator('HtmlTag');
		$surName->setLabel('Surname:'); 
		$surName->setRequired(true); 
		$surName->addValidator('NotEmpty');  
		$this->addElement($surName);

		$phone = $this->createElement('text', 'phone');
        //$phone->removeDecorator('HtmlTag');
		$phone->setLabel('Phone:');
		$this->addElement($phone);

        $country = $this->createElement('select', 'country', array('class' => 'half'));
        //$country->removeDecorator('HtmlTag');
		$country->setRequired(true);
        $country->setLabel('Country:');
		$country->addValidator('NotEmpty');
		$this->addElement($country);

        $timezone = $this->createElement('select', 'timezone', array('class' => 'half'));
        //$timezone->removeDecorator('HtmlTag');
		$timezone->setRequired(true);
        $timezone->setLabel('Time zone:');
		$timezone->addValidator('NotEmpty');
		$this->addElement($timezone);

		$currentPassword = $this->createElement('password', 'current_password', array('class' => 'half'));
        //$currentPassword->removeDecorator('HtmlTag');
		$currentPassword->setLabel('Current Password:');
		$this->addElement($currentPassword);

        $newPassword = $this->createElement('password', 'new_password', array('class' => 'password half'));
        //$newPassword->removeDecorator('HtmlTag');
		$newPassword->setLabel('New Password:');
		$this->addElement($newPassword);

        $confirmValidator = new Zend_Validate_Identical();
		$confirmValidator->setToken('new_password');
		$confirmValidator->setMessage('Please confirm password');

		$confirmPassword = $this->createElement('password', 'confirm_password', array('class' => 'half'));
        //$confirmPassword->removeDecorator('HtmlTag');
		$confirmPassword->setLabel('Confirm New Password:');
		$confirmPassword->addValidator($confirmValidator);
		$this->addElement($confirmPassword);
		
		$submit = $this->createElement('submit', 'save', array('class' => 'btn primary large'));
		$submit->setLabel('Save');
		$this->addElement($submit);
	}
	
	public function allowPasswordValidation() {

        $passwordValidator = new ZendExt_Validators_PasswordIdentityValidator();
		$passwordValidator->setCurrentPassword($this->currentPassword);

        $currentPasswordElement = $this->getElement('current_password');
        $currentPasswordElement->addValidator($passwordValidator);

        $lengthValidator = new Zend_Validate_StringLength(array('min' => 6));
        $lengthValidator->setMessage('Password is less than 6 characters long');

        $passwordValidatorChain = new Zend_Validate();
        $passwordValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator($lengthValidator);

        $newPasswordElement = $this->getElement('new_password');
        $newPasswordElement->addValidator($passwordValidatorChain);

        $confirmPasswordElement = $this->getElement('confirm_password');

		$confirmPasswordElement->setRequired(true);
		$confirmPasswordElement->addValidator('NotEmpty');
	}

    public function setCurrentPassword($currentPassword)
    {
        $this->currentPassword = $currentPassword;

    }

    public function fillTopCountries($optionsList)
	{
		$select = $this->getElement('country');

		foreach ($optionsList as $id => $title) {
			$select->addMultiOption($id, $title);
		}
	}

    public function fillBottomCountries($optionsList)
    {
        $select = $this->getElement('country');

        $optionsArray = array();

        foreach ($optionsList as $id => $title) {
            $optionsArray[$id] = $title;
        }

        $select->addMultiOptions(array('' => $optionsArray));

    }

    public function fillTimezones($optionsList)
	{
		$select = $this->getElement('timezone');

		foreach ($optionsList as $id => $title) {
			$select->addMultiOption($id, $title);
		}
	}
}