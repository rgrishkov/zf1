<?php
class ZendExt_MailListsProviders_MailProvidersManager {

    public function getAvailableProviders()
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/mail_lists_providers.ini');

        $providersList = array();

        foreach($config->providers as $providerId => $providerObj) {
            $providerClassName = $providerObj->class_name;

            if (class_exists($providerClassName)) {
                $provider = new $providerClassName;
                $providersList[] = $provider;
            }
        }

        return $providersList;
    }

    public function getProvidersForSelect()
    {
        $providersList = $this->getAvailableProviders();

        $selectEntries = array();
        foreach($providersList as $provider) {
            $entry = array();

            $entry['id'] = $provider->getId();
            $entry['title'] = $provider->getTitle();

            $selectEntries[] = $entry;
        }

        return $selectEntries;
    }

    public function getProviderById($providerId)
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/mail_lists_providers.ini');

        $provider = null;

        if (isset($config->providers->$providerId)) {
            $providerClassName = $config->providers->$providerId->class_name;

            if (class_exists($providerClassName)) {
                $provider = new $providerClassName;
            }
        }

        return $provider;
    }
}
