<?php
require_once 'MCAPI.class.php';

class ZendExt_MailListsProviders_MailChimp_MailChimpFacade extends ZendExt_MailListsProviders_WinGrinMailIntegration implements ZendExt_MailListsProviders_MailProviderInterface {

    private $id = 'mailchimp';

    private $title = 'MailChimp';

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getAPIConnectionPanel($request = null) {
        $view = new Zend_View();
        $view->setScriptPath(realpath(dirname(__FILE__)).'/views');

        if (isset($request['api_key']) && $request['api_key'] != null) {
            $apiKeyField = $view->formPassword('api_key', $request['api_key'], array('id' => 'api_key', 'renderPassword' => true));
        } else {
            $apiKeyField = $view->formText('api_key', null, array('id' => 'api_key'));
        }
        $view->assign('apiKeyField', $apiKeyField);

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/mail_lists_providers.ini');
        $view->assign('logoUrl', $config->providers->mailchimp->logo_url);

        return $view->render('connection_panel.phtml');
    }

    public function getConfigurationPanel($request) {
        $view = new Zend_View();
        $view->setScriptPath(realpath(dirname(__FILE__)).'/views');

        $mailLists = $this->getAvailableLists($request);
        $view->assign('mailLists', $mailLists);
        $view->assign('apiKey', $request['api_key']);

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/mail_lists_providers.ini');
        $view->assign('logoUrl', $config->providers->mailchimp->logo_url);

        if (isset($request['mail_list_id']) && $request['mail_list_id'] != null) {
            $view->assign('mailListId', $request['mail_list_id']);
        } else {
            $view->assign('mailListId', null);
        }

        return $view->render('configuration_panel.phtml');
    }

    public function getAvailableLists($request)
    {
        $api = new MCAPI($request['api_key']);

        $result = $api->lists();

        $mailLists = array();

        if ($api->errorCode == '') {
            foreach($result['data'] as $item) {
                $mailLists[$item['id']] = $item['name'];
            }

            return $mailLists;
        } else {
            return false;
        }
    }


    public function validateConnection($request)
    {
        $api = new MCAPI($request['api_key']);

        $result = $api->ping();

        if ($api->errorCode == '') {
            return true;
        } else {
            return false;
        }
    }

    public function getListId($request)
    {

    }

    public function sendToList($request)
    {
        $api = new MCAPI($request['api_key']);
        $campaignsArray = $api->campaigns(array('list_id' => $request['list_id'], 'status' => 'save'));

        foreach($campaignsArray['data'] as $campaignData) {
            //if ($campaignData['status'] != 'sent') {
                $result = $api->campaignSendNow($campaignData['id']);
                //Zend_Debug::dump($api->errorMessage);
            //}
        }
    }

    public function addToList($email, $firstName, $lastName, $request)
    {
        $params = array('FNAME'=>$firstName, 'LNAME'=>$lastName);

        $api = new MCAPI($request['api_key']);
        $api->listSubscribe($request['list_id'], $email, $params);
    }

    public function storeCredentials($siteId, $request)
    {
        parent::storeCredentials($this->getId(), $siteId, $request);
    }

    public function getCredentials($siteId)
    {
        return parent::getCredentials($this->getId(), $siteId);
    }
}

?>