<?php
require_once 'ConstantContact.php';

class ZendExt_MailListsProviders_ConstantContact_ConstantContactFacade extends ZendExt_MailListsProviders_WinGrinMailIntegration implements ZendExt_MailListsProviders_MailProviderInterface {

    private $id = 'constant_contact';

    private $title = 'ConstantContact';

    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getAPIConnectionPanel($request = null) {
        $view = new Zend_View();
        $view->setScriptPath(realpath(dirname(__FILE__)).'/views');

        if (isset($request['api_key']) && $request['api_key'] != null) {
            $apiKeyField = $view->formPassword('api_key', $request['api_key'], array('id' => 'api_key', 'renderPassword' => true));
        } else {
            $apiKeyField = $view->formText('api_key', null, array('id' => 'api_key'));
        }
        $view->assign('apiKeyField', $apiKeyField);

        if (isset($request['username']) && $request['username'] != null) {
            $usernameField = $view->formText('username', $request['username'], array('id' => 'username', 'renderPassword' => true));
        } else {
            $usernameField = $view->formText('username', null, array('id' => 'username'));
        }
        $view->assign('usernameField', $usernameField);

        if (isset($request['password']) && $request['password'] != null) {
            $passwordField = $view->formPassword('password', $request['password'], array('id' => 'password', 'renderPassword' => true));
        } else {
            $passwordField = $view->formPassword('password', null, array('id' => 'password', 'renderPassword' => true));
        }
        $view->assign('passwordField', $passwordField);

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/mail_lists_providers.ini');
        $view->assign('logoUrl', $config->providers->constant_contact->logo_url);

        return $view->render('connection_panel.phtml');
    }

    public function getConfigurationPanel($request) {
        $view = new Zend_View();
        $view->setScriptPath(realpath(dirname(__FILE__)).'/views');

        $mailLists = $this->getAvailableLists($request);
        $view->assign('mailLists', $mailLists);
        $view->assign('apiKey', $request['api_key']);
        $view->assign('username', $request['username']);
        $view->assign('password', $request['password']);

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/mail_lists_providers.ini');
        $view->assign('logoUrl', $config->providers->constant_contact->logo_url);

        if (isset($request['mail_list_id']) && $request['mail_list_id'] != null) {
            $view->assign('mailListId', $request['mail_list_id']);
        } else {
            $view->assign('mailListId', null);
        }

        return $view->render('configuration_panel.phtml');
    }

    public function getAvailableLists($request)
    {
        $constantContact = new ConstantContact('basic', $request['api_key'], $request['username'], $request['password']);
        $mailLists = $constantContact->getLists();

        $mailListsArray = array();

        if ($mailLists) {

            foreach($mailLists['lists'] as $list){
                $mailListsArray[$list->id] = $list->name;
            }

            return $mailListsArray;
        } else {
            return false;
        }
    }


    public function validateConnection($request)
    {
        $testRequest = new CTCTRequest('basic', $request['api_key'], $request['username'], $request['password']);
        $listCollection = new ListsCollection($testRequest);

        $requestResult = $testRequest->makeRequest($listCollection->uri, 'GET');

        if ($requestResult['error'] == '') {
            return true;
        } else {
            return false;
        }
    }

    public function getListId($request)
    {

    }

    public function sendToList($request)
    {
        //$constantContact = new ConstantContact('basic', $request['api_key'], $request['username'], $request['password']);
        //$constantContact->get
    }

    public function addToList($email, $firstName, $lastName, $request)
    {
        $constantContact = new ConstantContact('basic', $request['api_key'], $request['username'], $request['password']);

        $contactSearchResult = $constantContact->searchContactsByEmail($email);

        if ($contactSearchResult == false) {
            $contact = new Contact();
            $contact->emailAddress = $email;
            $contact->firstName = $firstName;
            $contact->lastName = $lastName;
            $contact->lists = array($request['list_id']);

            $newContact = $constantContact->addContact($contact);
        }
    }

    public function storeCredentials($siteId, $request)
    {
        parent::storeCredentials($this->getId(), $siteId, $request);
    }

    public function getCredentials($siteId)
    {
        return parent::getCredentials($this->getId(), $siteId);
    }
}

?>