<?php
class ZendExt_MailListsProviders_WinGrinMailIntegration {

    public function storeCredentials($providerId, $siteId, $request)
    {
        $siteMailListIntegration = new Application_Model_SiteMailListIntegration();
        $siteMailListIntegrationMapper = new Application_Model_SiteMailListIntegrationMapper();

        $siteMailListIntegrationMapper->loadBySiteId($siteId, $siteMailListIntegration);

        $siteMailListIntegration->setSite_Id($siteId);
        $siteMailListIntegration->setProvider_Id($providerId);
        $siteMailListIntegration->setApi_Key($request['api_key']);

        if (isset($request['username'])) {
            $siteMailListIntegration->setUsername($request['username']);
        }

        if (isset($request['password'])) {
            $siteMailListIntegration->setPassword($request['password']);
        }

        $siteMailListIntegrationMapper->save($siteMailListIntegration);
    }

    public function getCredentials($providerId, $siteId)
    {
        $siteMailListIntegration = new Application_Model_SiteMailListIntegration();
        $siteMailListIntegrationMapper = new Application_Model_SiteMailListIntegrationMapper();

        $loadResult = $siteMailListIntegrationMapper->loadBySiteId($siteId, $siteMailListIntegration);

        if ($loadResult != false) {
            return $siteMailListIntegration;
        } else {
            return null;
        }

    }

}
