<?php
interface ZendExt_MailListsProviders_MailProviderInterface {

    public function getId();

    public function getTitle();

    public function getAPIConnectionPanel($request);

    public function getConfigurationPanel($request);

    public function getAvailableLists($request);

    public function storeCredentials($siteId, $request);

    public function getCredentials($siteId);

    public function validateConnection($request);

    public function getListId($request);

    public function sendToList($request);

    public function addToList($email, $firstName, $lastName, $request);

}
