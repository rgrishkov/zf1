<?php
/**
 * Class for full size image
 * 
 */
class ZendExt_Image_FullSizeImage extends ZendExt_Image_Image {
	
	const DEFAULT_QUALITY = 50;	
	
	/**
	 * Sett image name
	 * 
	 * @param string $fullPath
	 */
	public function setName($fullPath) {
		$buffer = explode("/", $fullPath);
		parent::setName($buffer[sizeof($buffer) - 1]);
	}
	
	/**
	 * Set path to image
	 *
	 * @param string $fullPath
	 * @param string @name
	 */
	public function setPath($fullPath, $name) {
		$path = preg_replace('/' . $name . '/', "", $fullPath);
		parent::setPath($path);
	}	
	
	/**
	 * Set image width
	 *
	 * @param gd $identifier
	 */	
	public function setWidth($identifier) {
		$width = imageSx($identifier);
		parent::setWidth($width);		
	}
	
	/**
	 * Set image height
	 *
	 * @param gd $identifier
	 */	
	public function setHeight($identifier) {
		$height = imageSy($identifier);
		parent::setHeight($height);
	}
	
	/**
	 * Constructor
	 * 
	 * @param string $fullPath
	 * @return FullSizeImage
	 */
	public function __construct($fullPath) {
		$this->setName($fullPath);
		$this->setPath($fullPath, $this->getName());
		$this->setQuality(self::DEFAULT_QUALITY);
		$identifier = $this->createIdentifier($this); 		
		$this->setIdentifier($identifier);
		$this->setWidth($identifier);
		$this->setHeight($identifier);		
	}

	/**
	 * Destructor
	 * 
	 */
	public function __destruct() {
		parent::destroy($this);
	}
	
	/**
	 * Create image identifier
	 *
	 * @param Image $image
	 * @see Image
	 */	
	private function createIdentifier($image) {
		$fileExtension = $this->getFileExtension($image->getName());
		$fullPath = $image->getPath() . $image->getName();
		switch($fileExtension) {
			case "gif":
				$identifier = imageCreateFromGif($fullPath);
				break;
			case "png":
				$identifier = imageCreateFromPng($fullPath);
				break;
			case "wbmp":
				$identifier = imageCreateFromWbmp($fullPath);
				break;
			case "jpg":
			case "jpeg":
				$identifier = imageCreateFromJpeg($fullPath);
				break;				
			default:
				$identifier = false;
				break;
		}
		return $identifier;				
	}
	
	/**
	 * Get file extension
	 * 
	 * @param string $name - file name
	 * @return string 
	 */
	private function getFileExtension($name) {
		$extension = "";
		if ($name != "") {
			$buffer = explode(".", $name);
			$extension = strtolower($buffer[sizeof($buffer) - 1]);
		}
		return $extension;	
	}		
}
?>