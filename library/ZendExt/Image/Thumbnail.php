<?php
/**
 * Class for work with thumbnails
 * 
 */
class ZendExt_Image_Thumbnail extends ZendExt_Image_Image {

	//const PATH = "imgs/thumbnails/";
	const DEFAULT_QUALITY = 100;
	const DEFAULT_BACKGROUND_COLOR = "91ff00";

	/**
	 * FullSizeImage object
	 * 
	 * @var  Image 
	 */
	private $fullSizeImage;
	
	/**
	 * Background color 
	 * 
	 * @var string - color in HEX
	 */
	private $backgroundColor;

	/**
	 * Set thumbnail name
	 * 
	 * @param string $value
	 */
	public function setName($value) {
		//$name = $value . time() . ".png";
		parent::setName($value);
	}

	/**
	 * Set path to image
	 *
	 */
	public function setPath($path) {
		//$config = GlobalConfig::getInstance();
		//$path = $config->root_dir . Thumbnail::PATH;
		//$path = THUMBNAILS_PATH;
		parent::setPath($path);
	}	
	
	/**
	 * Set thumbnail quality
	 * 
	 * @param int $value
	 */
	public function setQuality($value) {
		if ($value == "") {
			$quality = self::DEFAULT_QUALITY;
		} else {
			$quality = $value;			
		}
		parent::setQuality($quality);
	}
	
	/**
	 * Set FullSizeImage object
	 * 
	 * @param Image
	 */
	public function setFullSizeImage($image) {
		$this->fullSizeImage = $image;
	}
	
	/**
	 * Get FullSizeImage object
	 * 
	 * @return Image
	 */
	public function getFullSizeImage() {
		return $this->fullSizeImage;
	}
	
	/**
	 * Get background color
	 * 
	 * @return string - color in HEX
	 */
	public function getBackgroundColor() {
		 return $this->backgroundColor;
	}
	
	/**
	 * Set background color
	 * 
	 * @param string $hexColor - color in HEX[FFFFFF]
	 */
	public function setBackgroundColor($hexColor) {
		if ($hexColor == "") { 
			$this->backgroundColor = self::DEFAULT_BACKGROUND_COLOR; 
		} else {
			$this->backgroundColor = $hexColor;
		}
	}

	/**
	 * Constructor
	 * 
	 * @param Image $fullSizeImage
	 * @param int $width
	 * @param int $height
	 * @param int $quality
	 * @param  string $backgroundColor  
	 * @return Thumbnail
	 */
		
	public function __construct($fullSizeImage, $width, $height, $quality = "", $backgroundColor = "") {
		$this->setFullSizeImage($fullSizeImage);
		$this->setName($fullSizeImage->getName());
		//$this->setPath();
		$this->setWidth($width);
		$this->setHeight($height);
		$this->setQuality($quality);
		$this->setBackgroundColor($backgroundColor);
		$this->setIdentifier($this->createIdentifier($this));
	}
	
	/**
	 * Destructor
	 * 
	 */
	public function __destruct() {
		//$this->destroy($this);	
	}
	
	/**
	 * Create thumbnail
	 * 
	 */
	public function create() {
		$this->display();
	}
	
	public function save() {
		$this->resize($this->getFullSizeImage(), $this);
		//parent::save($this->getPath(), $this->getFullSizeImage()->getName());
        parent::save($this->getPath(), $this->getName());
	}	

	/**
	 * Destroy image
	 * 
	 * @param Image $image
	 */
	public function destroy($image) {
		parent::destroy($image);
		$fullPath = $image->getPath() . $image->getName();
		if (file_exists($fullPath)) {
			unlink($fullPath);
		}		
	}
			
	/**
	 * Resize image
	 *
	 * @param Image $fullSizeImage
	 * @param Image $thumbnailImage
	 */
	public function resize($fullSizeImage, $thumbnailImage) {
		$sourceWidth = $fullSizeImage->getWidth();
		$sourceHeight = $fullSizeImage->getHeight();
		$thumbnailWidth = $thumbnailImage->getWidth();
		$thumbnailHeight = $thumbnailImage->getHeight();

        $sourceProportion = $sourceWidth / $sourceHeight;
        $thumbnailProportion = $thumbnailWidth / $thumbnailHeight;


        if ($sourceHeight > $thumbnailHeight ||$sourceWidth > $thumbnailWidth) {
            if($sourceProportion > $thumbnailProportion) {
                $destinationWidth = $thumbnailWidth;
                $destinationHeight = $destinationWidth / $sourceProportion;
                $destinationX = 0;
                $destinationY = ($thumbnailHeight - $destinationHeight) / 2;
            } elseif ($thumbnailProportion > $sourceProportion) {
                $destinationHeight = $thumbnailHeight;
                $destinationWidth = $destinationHeight / (1 / $sourceProportion);
                $destinationX = ($thumbnailWidth - $destinationWidth) / 2;
                $destinationY = 0;
            } else {
                $destinationWidth = $thumbnailWidth;
                $destinationHeight = $thumbnailHeight;
                $destinationX = 0;
                $destinationY = 0;
            }

            imageCopyResized($thumbnailImage->getIdentifier(), $fullSizeImage->getIdentifier(), $destinationX,
                $destinationY, 0, 0, $destinationWidth, $destinationHeight, $sourceWidth, $sourceHeight);

        } else {
            $this->imagecopymerge_alpha($thumbnailImage->getIdentifier(), $fullSizeImage->getIdentifier(), 0, 0, 0, 0, $thumbnailWidth, $thumbnailHeight, 100);
        }
	}

    public function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
        if(!isset($pct)){
            return false;
        }
        $pct /= 100;
        // Get image width and height
        $w = imagesx( $src_im );
        $h = imagesy( $src_im );
        // Turn alpha blending off
        imagealphablending( $src_im, false );
        // Find the most opaque pixel in the image (the one with the smallest alpha value)
        $minalpha = 127;
        for( $x = 0; $x < $w; $x++ )
        for( $y = 0; $y < $h; $y++ ){
            $alpha = ( imagecolorat( $src_im, $x, $y ) >> 24 ) & 0xFF;
            if( $alpha < $minalpha ){
                $minalpha = $alpha;
            }
        }
        //loop through image pixels and modify alpha for each
        for( $x = 0; $x < $w; $x++ ){
            for( $y = 0; $y < $h; $y++ ){
                //get current alpha value (represents the TANSPARENCY!)
                $colorxy = imagecolorat( $src_im, $x, $y );
                $alpha = ( $colorxy >> 24 ) & 0xFF;
                //calculate new alpha
                if( $minalpha !== 127 ){
                    $alpha = 127 + 127 * $pct * ( $alpha - 127 ) / ( 127 - $minalpha );
                } else {
                    $alpha += 127 * $pct;
                }
                //get the color index with new alpha
                $alphacolorxy = imagecolorallocatealpha( $src_im, ( $colorxy >> 16 ) & 0xFF, ( $colorxy >> 8 ) & 0xFF, $colorxy & 0xFF, $alpha );
                //set pixel with the new color + opacity
                if( !imagesetpixel( $src_im, $x, $y, $alphacolorxy ) ){
                    return false;
                }
            }
        }
        // The image copy
        imagecopy($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h);
    }
	
	/**
	 * Create identifier
	 *
	 * @param Thumbnail $image
	 * @return gd
	 */
	private function createIdentifier($image) {
		$backgroundColor = $image->getBackgroundColor();

        //::dump($backgroundColor);
        //die();

        $red = hexdec(substr($backgroundColor, 0, 2));
		$green = hexdec(substr($backgroundColor, 2, 2));	
		$blue = hexdec(substr($backgroundColor, 4, 2));

		$identifier = imagecreatetruecolor($image->getWidth(), $image->getHeight());

        $color = imagecolorallocate($identifier, 0, 0, 0);
        //$color = imagecolorallocate($identifier, 255, 255, 255);
        //$color = imageColorAllocate($identifier, $red, $green, $blue);
        imagecolortransparent($identifier, $color);

		return $identifier;
	}	
}
?>