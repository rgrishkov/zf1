<?php
abstract class ZendExt_Image_Image {
	
	/**
	 * Image name
	 *
	 * @var string
	 */
	private $name;

	/**
	 * Path to image
	 *
	 * @var string
	 */
	private $path;	
	
	/**
	 * Image width;
	 *
	 * @var int
	 */
	private $width;
	
	/**
	 * Image height
	 *
	 * @var int
	 */
	private $height;
	
	/**
	 * Image quality
	 *
	 * @var int
	 */
	private $quality;
	
	/**
	 * Image identifier
	 *
	 * @var gd
	 */
	private $identifier;

	/**
	 * Set image name
	 *
	 * @param string $value
	 */
	public function setName($value) {
		$this->name = $value;
	}
	
	/**
	 * Get image name
	 *
	 * @return string 
	 */	
	public function getName() {
		return $this->name;		
	}

	/**
	 * Set path to image
	 *
	 * @param string $value
	 */
	public function setPath($value) {
		$this->path = $value;
	}
	
	/**
	 * Get path to image
	 *
	 * @return string
	 */	
	public function getPath() {
		return $this->path;		
	}
	
	/**
	 * Set image width
	 *
	 * @param int $value
	 */	
	public function setWidth($value) {
		$this->width = $value;
	}
	
	/**
	 * Get image width
	 *
	 * @return int
	 */	
	public function getWidth() {
		return $this->width;		
	}

	/**
	 * Set image height
	 *
	 * @param int $value
	 */	
	public function setHeight($value) {
		$this->height = $value;
	}
	
	/**
	 * Get image height
	 *
	 * @return int
	 */	
	public function getHeight() {
		return $this->height;		
	}

	/**
	 * Set image quality
	 *
	 * @param int $value 
	 */	
	public function setQuality($value) {
		$this->quality = $value;
	}

	/**
	 * Get image quality
	 *
	 * @return int
	 */	
	public function getQuality() {
		return $this->quality;
	}

	/**
	 * Set image identifier
	 *
	 * @param gd $image
	 */	
	public function setIdentifier($image) {
		$this->identifier = $image;
	}
	
	/**
	 * Get image identifier
	 *
	 * @return gd  
	 */
	public function getIdentifier() {
		return $this->identifier;
	}
		
	/**
	 * Create image identifier
	 * 
	 * @return mixed:
	 * gd  - image identifier if image load successful
	 * bool - false if loading process failure
	 */
	private function createIdentifier() {}
	
	/**
	 * Display image
	 *
	 */
	public function display() {
		$fullPath = "";
		if (is_dir($this->getPath())) {
			$fullPath = $this->getPath() . $this->getName();		
			imageJpeg($this->getIdentifier(), $fullPath, $this->getQuality());
		}
		if (file_exists($fullPath)) {		
			header("Content-Type:image/jpeg");
			readfile($fullPath);
		}				
	}

	/**
	 * Save image
	 *
	 * @param string $path
	 * @param string $fileName
	 */
	public function save($path, $fileName) {
		if (is_dir($path)) {
			$fullPath = $path.'/'.$fileName;	
            imagepng($this->getIdentifier(), $fullPath, 0);
            imagedestroy($this->getIdentifier());
		}
	}	
	
	/**
	 * Destroy image
	 * 
	 * @param Image $image
	 */
	public function destroy($image) {
		imagedestroy($image->getIdentifier());						
	}
}
?>