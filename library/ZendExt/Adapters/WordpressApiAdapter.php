<?php
class ZendExt_Adapters_WordpressApiAdapter {

	public function newPost(Application_Model_CampaignPostDetails $postDetails, Application_Model_Campaign $campaign, Application_Model_Site $vendorSite, Application_Model_MediaFile $siteLogo, $campaignLocations)
	{   
        $config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/sites.xml');
		$publishResult = array();
        $configuredSiteCount = 0;
        
		foreach ($config->sites->site as $site) {

			if ($site->url != '') {

                $configuredSiteCount++;
                
				if ($this->checkLocations($site->locations->location, $campaignLocations) != false) {
					$client = new IXR_Client($site->url.'/xmlrpc.php');
		
					$contentStruct = array();
					$contentStruct['title'] = $postDetails->getPost_Name();
					$contentStruct['description'] = $postDetails->getPost_Content();
					$contentStruct['custom_fields'] = array(
						array(
							'key' => 'Description',
							'value' => $postDetails->getPost_Description()
						),
						array(
							'key' => 'EndDate',
							'value' => $campaign->getCampaign_Live_End_Date()
						),
						array(
							'key' => 'Fineprint',
							'value' => $postDetails->getPost_Conditions()
						),
						array(
							'key' => 'Keywords',
							'value' => $postDetails->getPost_Keywords()
						),
						array(
							'key' => 'Promo URL',
							'value' => $campaign->getSite_Url()
						),
						array(
							'key' => 'StartDate',
							'value' => $campaign->getCampaign_Live_Start_Date()
						),
						array(
							'key' => 'Title',
							'value' => $postDetails->getPost_Title()
						),
						array(
							'key' => 'Vendor Name',
							'value' => $vendorSite->getSite_Name(),
						),
						array(
							'key' => 'Vendor Logo',
							'value' => $siteLogo->getMedia_Location(),
						),
						array( 
							'key' => 'Vendor Text',
							'value' => $vendorSite->getSite_Bio(),
						),
						array( 
							'key' => 'extraon',
							'value' => $postDetails->getShow_Post_Features(),
						),
						array( 
							'key' => 'extra1',
							'value' => $postDetails->getFeature_Buddy_Deal(),
						),
						array( 
							'key' => 'extra2',
							'value' => $postDetails->getFeature_Skill_Question(),
						)
					);
					
					$contentStruct['mt_taxonomy'] = array(
						array(
							'term_name' => $postDetails->getPromotion_Type(),
							'taxonomy' => 'promo'
						)
					);
					
					/*Zend_Debug::dump($contentStruct);
					die();*/

					$result = $client->query("metaWeblog.newPost", 0, $site->login, $site->password, $contentStruct, (int)$postDetails->getIs_Live());

					if ($result != false) {
						$postId = $client->message->params[0];
						$publishResult[] = array('site_url' => $site->url, 'post_id' => $postId);
					} else {
						throw new Exception("Error code [" . $client->getErrorCode() . "] Error message [" . $client->getErrorMessage() . "] Fault string [" . $client->message->faultString . "]");
					}
				} else {
					throw new Exception('There are no sites for this locations');
				}
			}
		}

        if ($configuredSiteCount == 0)
        {
            throw new Exception('There are no valid sites configured to post to.');
        }

		return $publishResult;
	}
	
	public function updatePost(Application_Model_CampaignPostDetails $postDetails, Application_Model_Campaign $campaign, Application_Model_Site $vendorSite, Application_Model_MediaFile $siteLogo, $campaignLocations)
	{
		$config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/sites.xml');
		$publishResult = array();
		
		foreach ($config->sites->site as $site) {
			if ($site->url != '') {
				if ($this->checkLocations($site->locations->location, $campaignLocations) != false) {

					$client = new IXR_Client($site->url.'/xmlrpc.php');
		
					//$contentStruct = $this->getContentStruct($postDetails, $campaign, $vendorSite, $siteLogo, $campaignLocations);
					$customFields = $this->getCustomFields($site, $postDetails->getPost_Id());
					
					foreach ($customFields as &$field) {
						if ($field['key'] == 'Description') {
							$field['value'] = $postDetails->getPost_Description();
						}
						if ($field['key'] == 'EndDate') {
							$field['value'] = $campaign->getCampaign_Live_End_Date();
						}
						if ($field['key'] == 'Fineprint') {
							$field['value'] = $postDetails->getPost_Conditions();
						}
						if ($field['key'] == 'Keywords') {
							$field['value'] = $postDetails->getPost_Keywords();
						}
						if ($field['key'] == 'Promo URL') {
							$field['value'] = $campaign->getSite_Url();
						}
						if ($field['key'] == 'StartDate') {
							$field['value'] = $campaign->getCampaign_Live_Start_Date();
						}
						if ($field['key'] == 'Title') {
							$field['value'] = $postDetails->getPost_Title();
						}
						if ($field['key'] == 'Vendor Name') {
							$field['value'] = $vendorSite->getSite_Name();
						}
						if ($field['key'] == 'Vendor Logo') {
							$field['value'] = $siteLogo->getMedia_Location();
						}
						if ($field['key'] == 'Vendor Text') {
							$field['value'] = $vendorSite->getSite_Bio();
						}
						if ($field['key'] == 'extraon') {
							$field['value'] = $postDetails->getShow_Post_Features();
						}
						if ($field['key'] == 'extra1') {
							$field['value'] = $postDetails->getFeature_Buddy_Deal();
						}
						if ($field['key'] == 'extra2') {
							$field['value'] = $postDetails->getFeature_Skill_Question();
						}
					} 
					
				  	$contentStruct = array();
					$contentStruct['title'] = $postDetails->getPost_Name();
					$contentStruct['description'] = $postDetails->getPost_Content();
					$contentStruct['custom_fields'] = $customFields;
					
					$contentStruct['mt_taxonomy'] = array(
						array(
							'term_name' => $postDetails->getPromotion_Type(),
							'taxonomy' => 'promo'
						)
					);
					
					$result = $client->query("metaWeblog.editPost", $postDetails->getPost_Id(), $site->login, $site->password, $contentStruct, (int)$postDetails->getIs_Live());
					
					if ($result != false) {
						$postId = $client->message->params[0];
						$publishResult[] = array('site_url' => $site->url, 'post_id' => $postId);
					} else {
						throw new Exception($client->message->faultString);
					}	
				}
			}
		}
		return $publishResult;
	}
	
	protected function getCustomFields($site, $postId)
	{
		$client = new IXR_Client($site->url.'/xmlrpc.php');
		
		$result = $client->query("metaWeblog.getPost", $postId, $site->login, $site->password);
		return $client->message->params[0]['custom_fields'];
	}
	
	protected function checkLocations($siteLocations, $campaignLocation)
	{
		$siteLocationsArray = array();
		$campaignLocationsArray = array();

		foreach ($siteLocations as $location) {
			$siteLocationsArray[] = $location;
		}
		
		foreach ($campaignLocation as $location) {
			$campaignLocationsArray[] = $location->getLocation_City_Id();
		}
		
		$intersection = array_intersect($siteLocationsArray, $campaignLocationsArray);
		if (count($intersection) > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public function publishWinners(Application_Model_CampaignPostDetails $postDetails, $competitionsList, $campaignLocations) {
		$config = new Zend_Config_Xml(APPLICATION_PATH . '/configs/sites.xml');
		$publishResult = array();
		
		foreach ($config->sites->site as $site) {
			if ($site->url != '') {
				if ($this->checkLocations($site->locations->location, $campaignLocations) != false) {

					$winnersList = '';
					foreach ($competitionsList as $competition) {
						if ($competition->getWinner_Id() != null && $competition->getWinner_Id() != '') {
							$winnersList .= $competition->getWinner_Id().',';
						}	
					}
					
					if ($winnersList == '') {
						throw new Exception('Winners list cant be empty');
					} else {
						$winnersList = substr($winnersList, 0, -1);
					}
					
					$client = new IXR_Client($site->url.'/xmlrpc.php');
					
					$winnersField = array(
						'key' => 'Winners',
						'value' => $winnersList
					);
					
					$customFields = $this->getCustomFields($site, $postDetails->getPost_Id());
					foreach ($customFields as $field) {
						if ($field['key'] == 'Winners') {
							$winnersField = $field;
							$winnersField['value'] = $winnersList;
						}
					} 
					
					//Zend_Debug::dump($winnersList);
					//die();
		
				  	$contentStruct = array();
					$contentStruct['title'] = $postDetails->getPost_Name();
					$contentStruct['description'] = $postDetails->getPost_Content();
					$contentStruct['custom_fields'] = array($winnersField);
					
					$result = $client->query("metaWeblog.editPost", $postDetails->getPost_Id(), $site->login, $site->password, $contentStruct, (int)$postDetails->getIs_Live());
					
					if ($result != false) {
						$postId = $client->message->params[0];
						$publishResult[] = array('site_url' => $site->url, 'post_id' => $postId);
					} else {
						throw new Exception($client->message->faultString);
					}	
				}
			}
		}
		return $publishResult;
	}
	
}

?>