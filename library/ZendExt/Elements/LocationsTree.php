<?php
class ZendExt_Elements_LocationsTree extends Zend_Form_Element_Xhtml
{
	private $locationsTree;
	
	private $selectedCitiesList = array();
	private $selectedRegionsList = array();
	private $selectedCountriesList = array();
	
	private $isError = false;
	
	public function render() 
	{
		//Zend_Debug::dump($this->campaignLocations);
		$result = '<p><label>Locations:</label><br />';
        		
		if ($this->isError == true) {
			$result .= '<ul class="errors"><li>Please select city</li></ul>';
		}
        $result .= '<div class="listbox"><ul id="tree" class="treeview">';
		
		foreach ($this->locationsTree as $country) {
			$result .= '
				<li class="expandable">
	            	<div class="hitarea expandable-hitarea "></div>'.$country['id'].' - '.$country['name'];

			if (in_array($country['id'], $this->selectedCountriesList)) {
				$result .= '<ul>';
			} else {
				$result .= '<ul style="display: none;">';
			}
			
			if (count($country['regions']) > 0) {
				
				foreach ($country['regions'] as $region) {
					
					$result .= '<li><div class="hitarea expandable-hitarea "></div>'.$region['id'].' - '.$region['name'];
					
					if (count($region['cities']) > 0) {
						
						if (in_array($region['id'], $this->selectedRegionsList)) {
							$result .= '<ul>';
						} else {
							$result .= '<ul style="display: none;">';
						}
							
						foreach ($region['cities'] as $city) {
							if (in_array($city['id'], $this->selectedCitiesList)) {
								$result .= '<li>'.$city['id'].' - '.$city['name'].' <input type="checkbox" value="'.$city['id'].'" name="location_cities_list[]" checked="checked" /></li>';
							} else {
								$result .= '<li>'.$city['id'].' - '.$city['name'].' <input type="checkbox" value="'.$city['id'].'" name="location_cities_list[]" /></li>';
							}
						}
						$result .= '</ul>';
					}
					$result .= '</li>';
				}
			}
			
			if (count($country['cities_without_region']) > 0) {
				foreach ($country['cities_without_region'] as $city) {
					if (in_array($city['id'], $this->selectedCitiesList)) {
						$result .= '<li>'.$city['id'].' - '.$city['name'].' <input type="checkbox" value="'.$city['id'].'" name="location_cities_list[]" checked="checked" /></li>';
					} else {
						$result .= '<li>'.$city['id'].' - '.$city['name'].' <input type="checkbox" value="'.$city['id'].'" name="location_cities_list[]" /></li>';
					}	
				}
			}
			$result .= '</ul></li>';
		}
		
		$result .= '</ul></div></p>';
		
		return $result;
	}
	
	public function isValid($value)
	{
		if ($value == null) {
			$this->isError = true;
			return false;
		} else {
			return true;
		}
	}
	
	public function fillLocaionsTree($locationsTree)
	{
		$this->locationsTree = $locationsTree;
	}
	
	public function populate($formData)
	{
		
		if (isset($formData['campaign_locations'])) {
			
			foreach ($formData['campaign_locations'] as $location) {
				$this->selectedCitiesList[] = $location->getLocation_City_Id();
				$this->selectedRegionsList[] = $location->getLocation_Region_Id();
				$this->selectedCountriesList[] = $location->getLocation_Country_Id();
			}
		}
	}
}

?>