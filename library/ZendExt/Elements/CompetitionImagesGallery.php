<?php
class ZendExt_Elements_CompetitionImagesGallery extends Zend_Form_Element_Xhtml
{
    private $mediaFilesList = array();

    public function render()
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');

        $galleryPath = $config->image->competition_gallery_folder;
        $galleryUrl = $config->image->gallery_img_prefix;


        $result = '<div id="gallery_popup" class="modal hide fade">
                    <div class="modal-header">
                        <a href="#" class="close">&times;</a>
                        <h3>Choose an image from Gallery</h3>
                    </div>
                    <div class="modal-body">
                        <table>
                            <tr>
                                <td>
                                    <div id="thumbs" class="navigation">
                                        <ul class="thumbs noscript">';

        if ($handle = opendir($galleryPath)) {

            while (($fileName = readdir($handle)) != false) {

                if ($fileName != "." && $fileName != ".." && is_file($galleryPath.'/'.$fileName)) {

                    $result .= '<li style="opacity: 1; " class="">
                                    <a class="thumb" href="#leaf" title="" rel="history" id="'.$fileName.'">
                                        <img src="'.$galleryUrl.'/'.$fileName.'" onclick="chooseImage(\''.$fileName.'\'); return false;">
                                    </a>
                                </li>';

                }
            }
            closedir($handle);
        }

        $result .= '
                            </ul>
                        </div>
                        </td></tr>
                    </table>
                </div>
                <div class="modal-footer">
                      <a href="#" class="btn secondary close">Close</a>
                    </div>
            </div>';

        return $result;
    }
}

?>