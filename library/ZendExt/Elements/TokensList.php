<?php
class ZendExt_Elements_TokensList extends Zend_Form_Element_Xhtml
{
	
	private $itemsList = array(); 
	private $selectedItemId = null;
	
	public function render() 
	{
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
		
		$result = '<p>
			<label for="token_id">Token Type:</label><br/>
			<select name="token_id" id="token_id">
				<option value="">--Please select--</option>';
		
		foreach ($this->itemsList as $item) {
			
			if ($this->selectedItemId != null && $this->selectedItemId == $item['value']) { 
				$result .= '<option value="'.$item['value'].'" title="'.$config->development->image->img_prefix.$item['image'].'" selected="selected">'.$item['title'].'</option>';
			} else {
				$result .= '<option value="'.$item['value'].'" title="'.$config->development->image->img_prefix.$item['image'].'">'.$item['title'].'</option>';
			}	
		}
		
		$result .= '</select></p>';
		return $result;
	}
	
	public function addItem($item)
	{
		$this->itemsList[] = $item;
	}
	
	public function setSelectedItemId($value)
	{
		$this->selectedItemId = $value;
	}
	
	/*public function isValid($value)
	{
		return true;
	}*/
}

?>