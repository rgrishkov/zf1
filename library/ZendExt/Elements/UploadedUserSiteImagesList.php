<?php
class ZendExt_Elements_UploadedUserSiteImagesList extends Zend_Form_Element_Xhtml
{
    private $mediaFilesList = null;
	private $ownerId;
	private $ownerExternalId;
    private $isLocked = false;


	public function setMediaFilesList($value)
	{
		$this->mediaFilesList = $value;
	}

	public function setOwnerId($value)
	{
		$this->ownerId = $value;
	}

	public function setOwnerExternalId($value)
	{
		$this->ownerExternalId = $value;
	}

    public function render()
	{
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
		$result = '<table class="full no-style no-border">';

		if ($this->mediaFilesList == null) {
			$result .= '
				<tr>
					<td style="vertical-align: top;"><img src="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/img/preview-not-available.gif" alt="" /></td>
				</tr>
			';
		} else {
			foreach ($this->mediaFilesList as $mediaFile) {
				$imagePath = $config->development->image->img_prefix.$mediaFile->getMedia_Location();
				$imageName = $mediaFile->getMedia_Name();
				$thumbnailPath = str_replace('/images/', '/thumbnails/', $imagePath);

				$realPath = UPLOAD_PATH.'/sites/'.$this->ownerExternalId.'/images/'.$imageName;
				$realThumbnailPath = UPLOAD_PATH.'/sites/'.$this->ownerExternalId.'/thumbnails/'.$imageName;

				$imageId = time();

				$result .=	'<tr>';
				if (file_exists($realPath) == true && file_exists($realThumbnailPath) == true) {
					$imageSize = round(filesize($realPath)/1024);
					$result .=	'
						<td style="vertical-align: top;">
							 <a href="'.$imagePath.'" class="lightbox" id="img_'.$imageId.'" style="border: 0 none;"><img src="'.$thumbnailPath.'" class="thumbnail" alt="" /></a>
						</td>
						<td style="vertical-align: top;">
							<b class="big">'.$imageName.'</b><br/>
							<small><b>'.$imageSize.'kB</b></small>';
                    if ($this->isLocked != true) {
                        $result .=	' <a href="'.$this->getView()->url(array('module' => 'user', 'controller' => 'sites', 'action' => 'deleteimage', 'owner_id' => $this->ownerId, 'image_name' => $imageName), null, true).'">remove</a> &middot; <a href="#" onclick="$(\'#img_'.$imageId.'\').click(); return false;">show</a></td>';
                    } else {
                        $result .=	' <a href="#" onclick="$(\'#img_'.$imageId.'\').click(); return false;">show</a></td>';
                    }
				} else {
					$result .=	'
						<td style="vertical-align: top;">
							 <img src="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/img/preview-not-available.gif" alt="" />
						</td>
						<td style="vertical-align: top;">
							<b class="big">'.$imageName.'</b><br/>
							<small><b></b></small>';
                    if ($this->isLocked != true) {
					    $result .=	' <a href="'.$this->getView()->url(array('module' => 'user', 'controller' => 'sites', 'action' => 'deleteimage', 'owner_id' => $this->ownerId, 'image_name' => $imageName), null, true).'">remove</a></td>';
                    }
				}

                $result .= '</tr>';
			}
		}
		$result .= '</table>';

		return $result;
	}

    public function lock()
    {
        $this->isLocked = true;
    }

	public function populate($formData)
	{
		if (isset($formData['media_files_list'])) {
			$this->setMediaFilesList($formData['media_files_list']);
		}

		$this->setOwnerId($formData['competition_id']);
		$this->setOwnerExternalId($formData['external_site_id']);
	}
}

?>