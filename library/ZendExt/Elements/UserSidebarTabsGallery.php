<?php
class ZendExt_Elements_UserSidebarTabsGallery extends Zend_Form_Element_Xhtml
{
    private $mediaFilesList = array();
    private $sidebarTabImage = null;
    private $uploadErrorMessageText = '';
    private $isLocked = false;

    const  MINHEIGHT =  143;
    const  MAXHEIGHT =  147;
    const  MINWIDTH =  48;
    const  MAXWIDTH =  52;

    public function setMediaFilesList($value)
	{
		$this->mediaFilesList = $value;
	}

    public function setSidebarTabImage($value)
	{
		$this->sidebarTabImage = $value;
	}

    public function getErrorMessageText()
    {
        return $this->uploadErrorMessageText;
    }


	public function render() 
	{
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');
        $result = $this->getView()->formHidden('sidebar_tab_image_id', null, array('id' => 'sidebar_tab_image_id'));
        $result .= '<table class="full no-style no-border"><tr><td valign="top" style="vertical-align: top;" width="40%">';

        if ($this->sidebarTabImage != null) {
            $result .= '
                <div id="loaded_image_container" class="loaded_image_container"><img src="'.$config->image->uploaded_img_prefix.$this->sidebarTabImage->getMedia_Location().'"></div>
                <div id="gallery_image_container" style="display: none;">';
            $galleryLinkClass = '';
        } else {
            $result .= '
                <div id="loaded_image_container" class="loaded_image_container" style="display: none;"><img src="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/img/empty-image.jpg"></div>
                <div id="gallery_image_container">';
            $galleryLinkClass = 'inactive';
        }

        if ($this->isLocked == false) {
            $result .= '
                    <div id="gallery_tabs" class="content-competitions">
                        <div id="controls_tabs" class="controls-competitions"></div>
                        <div class="slideshow-container-competitions">
                            <div id="loading_tabs" class="loader-competitions"></div>
                            <div id="slideshow_tabs" class="slideshow-competitions"></div>
                        </div>
                        <div id="caption_tabs" class="caption-container"></div>
                    </div>
                </div>
                <a href="#" onclick="showHideGallery(); return false;" id="gallery_link" class="'.$galleryLinkClass.'">Choose other image</a>&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="showHideUploadForm(); return false;" id="upload_link">Upload image</a>
            </td>
            <td valign="top" style="vertical-align: top;" width="60%">';

            if ($this->uploadErrorMessageText != '') {
                $result .= '<div id="upload_form_container" class="upload_form_container">';
            } else {
                $result .= '<div id="upload_form_container" class="upload_form_container" style="display: none;">';
            }

            $result .= '
                <table class="full no-style no-border">
                    <tr>
                        <td><label for="image_file">Upload Image:</label></td>
                    </tr>
                    <tr>
                        <td>
                            Image must be PNG, JPG or GIF file 145px high by 50px wide<br>';

            if ($this->uploadErrorMessageText != '') {
                $result .= '<input type="file" name="image_file" id="image_file">
                            <ul class="errors"><li>'.$this->uploadErrorMessageText.'</li></ul>';
            } else {
                $result .= '<input type="file" name="image_file" id="image_file">
                            <ul class="errors"></ul>';
            }

            $result .= '
                        </td>
                    </tr>
                    <tr>
                        <td><input type="button" value="Upload" name="upload_image" id="upload_image" class="btn btn-green big" onclick="uploadImage(); return false;"></td>
                    </tr>
                </table>
            </div>';

            if ($this->sidebarTabImage != null) {
                $result .= '<div id="gallery_thumbs_container" style="display: none;">';
            } else {
                $result .= '<div id="gallery_thumbs_container">';
            }

            $result .= '<div id="thumbs_tabs" class="navigation_tabs"><ul class="thumbs noscript">';

            foreach($this->mediaFilesList as $mediaFile) {

                $imagePath = $config->image->img_prefix.$mediaFile->getMedia_Location();
                $realPath = UPLOAD_PATH.$mediaFile->getMedia_Location();

                if (file_exists($realPath) == true) {
                    $result .= '
                        <li>
                            <a class="thumb" name="'.$mediaFile->getMedia_Id().'" href="'.$imagePath.'" title="" id="'.$mediaFile->getMedia_Id().'">
                                <img src="'.$imagePath.'" alt="" />
                            </a>
                        </li>';
                } else {
                    $result .= '
                        <li>
                            <a class="thumb" name="'.$mediaFile->getMedia_Id().'" href="'.$imagePath.'" title="">
                                <img src="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/img/preview-not-available.gif" alt="" />
                            </a>
                        </li>';
                }
            }

            $result .= '</ul></div></div></td></tr></table>';
        } else {
            $result .= '</div></td></tr></table>';
        }

		return $result;
	}

    public function populate($formData)
	{
		if (isset($formData['media_files_list'])) {
			$this->setMediaFilesList($formData['media_files_list']);
		}

        if (isset($formData['sidebar_tab_image'])) {
		    $this->setSidebarTabImage($formData['sidebar_tab_image']);
        }
	}

    public function isValidAjax()
    {
        $upload = new Zend_File_Transfer_Adapter_Http();
        $fileInfo = $upload->getFileInfo();

        if ($fileInfo["image_file"]['size'] == null) {
            $this->uploadErrorMessageText = 'Value is required and can&rsquo;t be empty';
            $isValid = false;
        } else {
            $imageValidatorChain = new Zend_Validate();

            $imageTypeValidator = new Zend_Validate_File_MimeType(array('image/png', 'image/jpeg', 'image/gif'));
            $imageTypeValidator->setMessage('Image must be PNG, JPG or GIF file 145px high by 50px wide');


            $imageSizeValidator = new Zend_Validate_File_ImageSize(
                array('minheight' => self::MINHEIGHT, 'maxheight' => self::MAXHEIGHT, 'minwidth' => self::MINWIDTH, 'maxwidth' => self::MAXWIDTH)
            );

            $imageValidatorChain->addValidator($imageTypeValidator)->addValidator($imageSizeValidator);
            $upload->addValidator($imageValidatorChain);

            if ($upload->isValid() == false) {

                $errorMessages = array_values($upload->getMessages());
                if (count($errorMessages) != 0) {
                    $this->uploadErrorMessageText = $errorMessages[0];
                }

                $isValid = false;
            } else {
                $isValid = true;
            }
        }

        return $isValid;
    }

    public function lock()
    {
        $this->isLocked = true;
    }
}

?>