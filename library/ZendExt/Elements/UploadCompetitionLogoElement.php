<?php
class ZendExt_Elements_UploadCompetitionLogoElement extends Zend_Form_Element_Xhtml
{
    private $uploadErrorMessageText = '';
    private $mediaFile = null;

    public function getErrorMessageText()
    {
        return $this->uploadErrorMessageText;
    }

    public function setMediaFile($value)
    {
        $this->mediaFile = $value;
    }

	public function render()
	{
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');

        if ($this->mediaFile != null) {
            $imagePath = $config->image->uploaded_img_prefix.$this->mediaFile->getMedia_Location();
        } else {
            $imagePath = '';
        }

		$result = '
		    <div class="uploadimage">
                <div id="success_message_container"></div>
                <input type="file" name="image_file" id="image_file" onchange="uploadImage(); return false;">
                <input type="hidden" name="full_image_path" id="full_image_path" value="'.$imagePath.'">
                <ul class="errors"></ul>
            </div>';

		return $result;
	}

    public function populate($formData)
    {
        if (isset($formData['media_file'])) {
            $this->setMediaFile($formData['media_file']);
        }
    }

    public function isValidAjax()
    {
        $upload = new Zend_File_Transfer_Adapter_Http();
        $fileInfo = $upload->getFileInfo();

        if ($fileInfo["image_file"]['size'] == null) {
            $this->uploadErrorMessageText = 'Value is required and can&rsquo;t be empty';
            $isValid = false;
        } else {
            $imageValidatorChain = new Zend_Validate();

            $imageTypeValidator = new Zend_Validate_File_MimeType(array('image/png', 'image/jpeg', 'image/gif'));
            $imageTypeValidator->setMessage('Image must be PNG, JPG or GIF file');

            $imageValidatorChain->addValidator($imageTypeValidator);
            $upload->addValidator($imageValidatorChain);

            if ($upload->isValid() == false) {

                $errorMessages = array_values($upload->getMessages());
                if (count($errorMessages) != 0) {
                    $this->uploadErrorMessageText = $errorMessages[0];
                }

                $isValid = false;
            } else {
                $isValid = true;
            }
        }

        return $isValid;
    }

}

?>