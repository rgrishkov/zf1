<?php
class ZendExt_Elements_UploadedSiteImagesList extends Zend_Form_Element_Xhtml
{
	private $mediaFilesList = null;
	private $ownerId;
	private $ownerExternalId;
	private $imageTypesList = array();
	private $selectedImageTypesList = null;
	private $wrongTypeImages = array();
	
	
	public function setMediaFilesList($value)
	{
		$this->mediaFilesList = $value;	
	}
	
	public function setOwnerId($value)
	{
		$this->ownerId = $value;
	}
	
	public function setOwnerExternalId($value)
	{
		$this->ownerExternalId = $value;
	}

    public function setSelectedImageTypesList($value)
	{
		$this->selectedImageTypesList = $value;
	}
	
	public function render() 
	{
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');
		$result = '<table class="full no-style no-border">';
		
		if ($this->mediaFilesList == null) {
			$result .= '
				<tr>
					<td style="vertical-align: top;"><img src="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/img/preview-not-available.gif" alt="" /></td>
				</tr>
			';
		} else {
			foreach ($this->mediaFilesList as $mediaFile) {
				$imagePath = $config->image->uploaded_img_prefix.$mediaFile->getMedia_Location();
				$imageName = $mediaFile->getMedia_Name();
				$thumbnailPath = str_replace('/images/', '/thumbnails/', $imagePath);
				
				$realPath = $config->upload_path->sites.'/'.$this->ownerExternalId.'/images/'.$imageName;
				$realThumbnailPath = $config->upload_path->sites.'/'.$this->ownerExternalId.'/thumbnails/'.$imageName;

				$imageId = time();
				
				$result .=	'<tr>';
				if (file_exists($realPath) == true && file_exists($realThumbnailPath) == true) {
					$imageSize = round(filesize($realPath)/1024);
					$result .=	'
						<td style="vertical-align: top;">
							 <a href="'.$imagePath.'" class="lightbox" id="img_'.$imageId.'" style="border: 0 none;"><img src="'.$thumbnailPath.'" class="thumbnail" alt="" /></a>
						</td>
						<td style="vertical-align: top;">
							<b class="big">'.$imageName.'</b><br/>
							<small><b>'.$imageSize.'kB</b></small>
							<a href="'.$this->getView()->url(array('controller' => 'sites', 'action' => 'deleteimage', 'owner_id' => $this->ownerId, 'image_name' => $imageName), null, true).'">remove</a> &middot; <a href="#" onclick="$(\'#img_'.$imageId.'\').click(); return false;">show</a>
						</td>';
				} else {
					$result .=	'
						<td style="vertical-align: top;">
							 <img src="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/img/preview-not-available.gif" alt="" />
						</td>
						<td style="vertical-align: top;">
							<b class="big">'.$imageName.'</b><br/>
							<small><b></b></small>
							<a href="'.$this->getView()->url(array('controller' => 'sites', 'action' => 'deleteimage', 'owner_id' => $this->ownerId, 'image_name' => $imageName), null, true).'">remove</a>
						</td>';
				}
				$result .=	'	
					<td style="vertical-align: top;">
						<label>Type:</label>
						<select name="images_types_list['.$imageName.']">';
							foreach ($this->imageTypesList as $value => $title) {
								if ($this->selectedImageTypesList != null && $value == $this->selectedImageTypesList[$imageName]) {
									$result .=	'<option value="'.$value.'" selected="selected">'.$title.'</option>';
									$selectedTitle = $title;
								} elseif ($this->selectedImageTypesList == null && $value == $mediaFile->getMedia_Type()) { 
									$result .=	'<option value="'.$value.'" selected="selected">'.$title.'</option>';
									$selectedTitle = $title;
								} else {
									$result .=	'<option value="'.$value.'">'.$title.'</option>';
								}	
							}
				$result .='</select>';
				if (in_array($imageName, $this->wrongTypeImages) == true) {
					$result .='<div>You can have only one '.$selectedTitle.'</div>';
				}
				
				$result .='</td></tr>';
			}
		}	
		$result .= '</table>';
		
		return $result;
	}
	
	public function populate($formData)
	{
		if (isset($formData['media_files_list'])) {
			$this->setMediaFilesList($formData['media_files_list']);
		}
		
		if(isset($formData['images_types_list'])) {
			$this->selectedImageTypesList = $formData['images_types_list'];
		}
		
		$this->setOwnerId($formData['site_id']);
		$this->setOwnerExternalId($formData['external_site_id']);
		
		
	}
	
	public function fillImageTypes($optionsList)
	{
		$this->imageTypesList = $optionsList;
	}
	
	public function isValid($imageTypesList)
	{ 	
		if ($imageTypesList != null) {
			$isValid = true;
			$searchResult = array_keys($imageTypesList, MEDIA_TYPE_LOGO);
			if (count($searchResult) > 1) {
				for ($i = 1; $i<(count($searchResult)); $i++) {
					$this->wrongTypeImages[] = $searchResult[$i];
				}
				
				$isValid = false;
			}
			
			return $isValid;
		} else {
			return true;
		}
	}
}

?>