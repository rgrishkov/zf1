<?php
class ZendExt_Elements_PublishWinnersBox extends Zend_Form_Element_Xhtml
{
	private $publishUrl = null;
	private $publishWinnersDate = null;
	private $postId = null;
	
	public function render() 
	{
		if ($this->publishWinnersDate != null && $this->postId != null) {
			$date = new Zend_Date($this->publishWinnersDate);
			$date->setLocale(Zend_Locale::ZFDEFAULT);
    		$publishDate = $date->toString('MMM dd, YYYY @ HH:mm');
    		$status = 'Published';
    		$buttonValue = 'Update';
		} else {
			$publishDate = '';
			$status = '';
			$buttonValue = 'Publish';
		}
		
		$result = '
                <div class="content-box corners content-box-opened">
                    <header>
                        <h3>Publish</h3>
                    </header>
                    <section>
                        <p>
                            Status: <b>'.$status.'</b>
                        </p>
                        <p>
                            Published on: <b>'.$publishDate.'</b>
                        </p>
                        <p>
                            <input type="button" class="btn btn-blue big" value="'.$buttonValue.'" onclick="publishPost();" >
                        </p>
                    </section>
                </div>
				<script type="text/javascript">
					function publishPost() {
						var isLiveStr = $("#is_live").attr("checked");
						
						if (isLiveStr == true) {
							var isLive = 1;
						} else {
							var isLive = 0;
						}
						document.location.href = "'.$this->publishUrl.'/is_live/"+isLive;
					}
				</script>
		';
		return $result;
	}
	
	public function populate($formData)
	{
		$this->publishUrl = $this->getView()->url(array('controller' => 'campaigns', 'action' => 'publishwinners', 'id' => $formData['campaign_id']));
		$this->publishWinnersDate = $formData['publish_winners_date'];
		$this->postId = $formData['post_id'];
	}
	
}

?>