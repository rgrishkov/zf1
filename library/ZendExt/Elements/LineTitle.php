<?php
class ZendExt_Elements_LineTitle extends Zend_Form_Element_Xhtml
{
	
	private $title = '';
    private $helpText = null;
	
	public function render() 
	{
		$result = '<h4>'.$this->getTitle().'</h4><hr/>';
        if ($this->getHelpText() != null) {
            $result .= $this->getHelpText().'<br><br>';
        }

		return $result;
	}
	
	public function setTitle($value)
	{
		$this->title = $value;
	}
	
	public function getTitle() {
		return $this->title;
	}

    public function setHelpText($value)
	{
		$this->helpText = $value;
	}

	public function getHelpText() {
		return $this->helpText;
	}
}

?>