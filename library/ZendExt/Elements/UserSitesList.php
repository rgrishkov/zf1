<?php
class ZendExt_Elements_UserSitesList extends Zend_Form_Element_Xhtml
{
	
	private $sitesList; 
	
	public function render() 
	{
		$sitesList = $this->getSitesList();
		
		$result = '
			<div>
				<label for="sites" class="fl mr">Sites:</label>
			</div><br>
			<div >
			<table id="sites_list_container">
		';
		if ($sitesList != null) {
			foreach ($sitesList as $site) {
				$result .= '
				<tr id="site_item_'.$site->site_id.'">
					<td class="site-list-td" width="300px">
						'.$site->site_url.'
						<input type="hidden" name="sites[]" value="'.$site->site_id.'">
					</td>
					<td  class="site-list-td" width="16px">
						<a href="#" onclick="removeSite('.$site->site_id.'); return false;"><span class="icon icon-cancel fl mr">&nbsp;</span></a>
					</td>
				</tr>'; 	
			}
		}

		$result .= '</table>
			<br/>
			<div>
				<a class="btn ui-state-default ui-corner-all" id="dialog_link">Search</a>
			</div>
			<div>';
		
		$result .= '<br/>
			<div id="dialog">
            	<p>
                	<label for="site">Site:</label>
                	<input type="text" id="site_for_search" name="site_for_search" />
                	<a class="btn" onclick="searchSite(); return false;">Search</a>
               	</p>
                <table class="stylized full" id="sites_list_table">
                </table>
           	</div>
		
		';
		
		$result .= '</div><br>';
		return $result;
	}
	
	public function setSitesList($sitesList)
	{
		$this->sitesList = $sitesList;	
	}
	
	public function getSitesList() {
		return $this->sitesList;
	}
}

?>