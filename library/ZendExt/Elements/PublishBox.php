<?php
class ZendExt_Elements_PublishBox extends Zend_Form_Element_Xhtml
{
	private $publishUrl = null;
	private $publishDate = null;
	private $postId = null;
	private $isLive = false;
	
	public function render() 
	{
		if ($this->publishDate != null && $this->postId != null) {
			$date = new Zend_Date($this->publishDate);
			$date->setLocale(Zend_Locale::ZFDEFAULT);
    		$publishDate = $date->toString('MMM dd, YYYY @ HH:mm');
    		$status = 'Published';
    		$buttonValue = 'Update';
		} else {
			$publishDate = '';
			$status = '';
			$buttonValue = 'Publish';
		}
		
		$result = '
					<div class="content-box corners content-box-opened">
						<header>
							<h3>Publish</h3>
						</header>
						<section>
							<p>
								Status: <b>'.$status.'</b>
							</p>
							<p>
								Published on: <b>'.$publishDate.'</b>
							</p>
							<p>';
		if ($this->isLive == true) { 						
			$result .= 'Live: <input type="checkbox" name="is_live" id="is_live" checked="checked">';
		} else {
			$result .= 'Live: <input type="checkbox" name="is_live" id="is_live">';
		}	
		$result .= '</p>
					<p>
						<input type="button" class="btn btn-blue big" value="'.$buttonValue.'" onclick="publishPost();" >
					</p>
				</section>
			</div>
		<script type="text/javascript">
			function publishPost() {
				var isLiveStr = $("#is_live").attr("checked");
				
				if (isLiveStr == true) {
					var isLive = 1;
				} else {
					var isLive = 0;
				}
				document.location.href = "'.$this->publishUrl.'/is_live/"+isLive;
			}
		</script>
		';
		return $result;
	}
	
	public function populate($formData)
	{
		$this->publishUrl = $this->getView()->url(array('controller' => 'campaigns', 'action' => 'publish', 'id' => $formData['campaign_id']));
		$this->publishDate = $formData['publish_date'];
		$this->postId = $formData['post_id'];
		$this->isLive = (bool)$formData['is_live'];
	}
	
}

?>