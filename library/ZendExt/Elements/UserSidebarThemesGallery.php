<?php
class ZendExt_Elements_UserSidebarThemesGallery extends Zend_Form_Element_Xhtml
{
    private $themesList = array();
    private $isLocked = false;

	public function render() 
	{
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');

        $result = '
            <table class="full no-style no-border">
            <tr>
                <td valign="top">
                    <div id="thumbs" class="navigation">
                        <ul class="thumbs noscript">';

        foreach($this->themesList as $theme) {
            $imagePath = $config->development->image->img_prefix.$theme->getBackground_Location();
            $thumbnailPath = str_replace('/images/', '/thumbnails/', $imagePath);

            $pathInfo = pathinfo($imagePath);
            $imageName = $pathInfo['basename'];

            $realPath = UPLOAD_PATH.'/themes/'.$theme->getExternal_Theme_Id().'/images/'.$imageName;
		    $realThumbnailPath = UPLOAD_PATH.'/themes/'.$theme->getExternal_Theme_Id().'/thumbnails/'.$imageName;

            if (file_exists($realPath) == true && file_exists($realThumbnailPath) == true) {
                $result .= '
                    <li>
                        <a class="thumb" name="'.$theme->getExternal_Theme_Id().'" href="'.$imagePath.'" title="'.$theme->getTheme_Name().'" id="'.$theme->getTheme_Id().'">
                            <img src="'.$thumbnailPath.'" alt="'.$theme->getTheme_Name().'" />
                        </a>
                    </li>';
            } else {
                $result .= '
                    <li>
                        <a class="thumb" name="'.$theme->getExternal_Theme_Id().'" href="'.$imagePath.'" title="'.$theme->getTheme_Name().'">
                            <img src="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/img/preview-not-available.gif" alt="" />
                        </a>
                    </li>';
            }

        }

        $result .= '
                    </ul>
                </div>
            </td>
            <td valign="top" style="vertical-align: top;">
                <div id="gallery" class="content">
                    <div id="controls" class="controls"></div>
                    <div class="slideshow-container">
                        <div id="loading" class="loader"></div>
                        <div id="slideshow" class="slideshow"></div>
                    </div>
                    <div id="caption" class="caption-container"></div>

                </div>';

        $result .= '</td></tr></table>';

		return $result;
	}

    public function lock()
    {
        $this->isLocked = true;
    }

    public function populate($formData) {
        $this->themesList = $formData['themes_list'];
    }
}

?>