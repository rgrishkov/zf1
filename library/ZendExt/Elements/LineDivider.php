<?php
class ZendExt_Elements_LineDivider extends Zend_Form_Element_Xhtml
{
	
	private $title = ''; 
	
	public function render() 
	{
		$result = '<fieldset><legend>'.$this->getTitle().'</legend></fieldset>
		';
		return $result;
	}
	
	public function setTitle($title)
	{
		$this->title = $title;	
	}
	
	public function getTitle() {
		return $this->title;
	}
}

?>