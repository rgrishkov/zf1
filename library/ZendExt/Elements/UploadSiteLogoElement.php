<?php
class ZendExt_Elements_UploadSiteLogoElement extends Zend_Form_Element_Xhtml
{
	
	private $mediaFile = null;
	private $ownerId;
	private $ownerExternalId;
    private $uploadErrorMessageText = '';

    public function getErrorMessageText()
    {
        return $this->uploadErrorMessageText;
    }

	public function setMediaFile($value)
	{
		$this->mediaFile = $value;
	}
	
	public function setOwnerId($value)
	{
		$this->ownerId = $value;
	}
	
	public function setOwnerExternalId($value)
	{
		$this->ownerExternalId = $value;
	}
	
	public function render() 
	{
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');
		$result = '<table class="full no-style no-border"><tr>';

		if ($this->mediaFile == null) {
			$result .= '
			    <td style="vertical-align: top;" id="loaded_image_container"><img src="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/img/preview-not-available.gif" alt="" /></td>
			    <td style="vertical-align: top;" id="image_data_container"></td>';
		} else {
            $imagePath = $config->image->uploaded_img_prefix.$this->mediaFile->getMedia_Location();

            $imageName = $this->mediaFile->getMedia_Name();
            $thumbnailPath = str_replace('/images/', '/thumbnails/', $imagePath);

            $realPath = $config->upload_path->sites.'/'.$this->ownerExternalId.'/images/'.$imageName;
            $realThumbnailPath = $config->upload_path->sites.'/'.$this->ownerExternalId.'/thumbnails/'.$imageName;

            $imageId = time();

            if (file_exists($realPath) == true && file_exists($realThumbnailPath) == true) {
                $imageSize = round(filesize($realPath)/1024);

                $result .= '
                    <td style="vertical-align: top;" id="loaded_image_container">
                         <a href="'.$imagePath.'" class="lightbox" id="img_'.$imageId.'" style="border: 0 none;"><img src="'.$thumbnailPath.'" alt="" class="thumbnail"/></a>
                    </td>
                    <td style="vertical-align: top;" id="image_data_container">
                        <b class="big">'.$imageName.'</b><br/>
                        <small><b>'.$imageSize.'kB</b></small>
                        <a href="#" onclick="deleteImage(\''.$this->ownerId.'\', \''.$imageName.'\'); return false;">remove</a> &middot; <a href="#" onclick="$(\'#img_'.$imageId.'\').click(); return false;">show</a>
                    </td>';
            } else {
                $result .= '
                    <td style="vertical-align: top;" id="loaded_image_container">
                        <img src="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/img/preview-not-available.gif" alt="" />
                    </td>
                    <td style="vertical-align: top;" id="image_data_container">
                        <b class="big">'.$imageName.'</b><br/>
                        <small></small>
                        <a href="#" onclick="deleteImage(\''.$this->ownerId.'\', \''.$imageName.'\'); return false;">remove</a>
                    </td>';
			}
		}
        $result .= '
            <td style="vertical-align: top;">
                <input type="file" name="image_file" id="image_file">
                <ul class="errors"></ul>
                <input type="button" value="Upload" name="upload_image" id="upload_image" class="btn btn-green big" onclick="uploadImage(); return false;">
            </td></tr>';
		$result .= '</table>';
		
		return $result;
	}
	
	public function populate($formData)
	{
		if (isset($formData['media_file'])) {
            $this->setMediaFile($formData['media_file']);
        }

		$this->setOwnerId($formData['site_id']);
		$this->setOwnerExternalId($formData['external_site_id']);
	}

    public function isValidAjax()
    {
        $upload = new Zend_File_Transfer_Adapter_Http();
        $fileInfo = $upload->getFileInfo();

        if ($fileInfo["image_file"]['size'] == null) {
            $this->uploadErrorMessageText = 'Value is required and can&rsquo;t be empty';
            $isValid = false;
        } else {
            $imageValidatorChain = new Zend_Validate();

            $imageTypeValidator = new Zend_Validate_File_MimeType(array('image/png', 'image/jpeg', 'image/gif'));
            $imageTypeValidator->setMessage('Image must be PNG, JPG or GIF file');

            $imageValidatorChain->addValidator($imageTypeValidator);
            $upload->addValidator($imageValidatorChain);

            if ($upload->isValid() == false) {

                $errorMessages = array_values($upload->getMessages());
                if (count($errorMessages) != 0) {
                    $this->uploadErrorMessageText = $errorMessages[0];
                }

                $isValid = false;
            } else {
                $isValid = true;
            }
        }

        return $isValid;
    }

}

?>