<?php
class ZendExt_Decorators_HttpPrefixDecorator extends Zend_Form_Decorator_Abstract
{
	
	public function render($content) 
	{
		if (isset($this->_options['site_url']) && $this->_options['site_url'] != null) {
            $prefix = 'http://'.$this->_options['site_url'];
        } else {
            $prefix = 'http://';
        }

        $result = '<span class="http_label">'.$prefix.'</span>'.$content;

		return $result;
	}
}

?>