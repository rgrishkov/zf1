<?php
class ZendExt_Decorators_ImagesListDecorator extends Zend_Form_Decorator_Abstract
{
	
	public function render($content) 
	{
		$form = $this->getElement();
		$imageNamesList = $form->getElements();
		
		$result = '
			<div class="colgroup leading">
				<div class="width1 column first ta-center">';
			
			if ($imageNamesList == null) {
				$result .=	'<img src="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/img/preview-not-available.gif" alt="" />';
			} else {
				foreach ($imageNamesList as $imageName) {
					$result .=	'<img src="'.Zend_Controller_Front::getInstance()->getBaseUrl().'/upload/thumbnails/'. $imageName->getValue().'" alt="" />';
				}
			}	
			
			$result .=	'
				</div>
				<div class="width4 column">
					<a href="#"><b class="big">'.$imageNamesList.'</b></a><br/>
					<!-- <small><b>534kB</b> |  Author: <b>UITemplates.com</b> |  Tags: <a href="#">logo</a></small><br/>
					<a href="#">remove</a> &middot; <a href="#">show</a> &middot; <a href="#">edit</a>-->
				</div>
			</div>';
			return $result;
	}
}

?>