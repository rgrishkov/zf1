<?php
class ZendExt_Decorators_TableDecorator extends Zend_Form_Decorator_Abstract
{
	private $preString = '';
	private $postString = '';
	private $cellAtribs = array();
	
	public function startTable($atrib = 'full no-style no-border') 
	{
		$this->preString .= '<table class="'.$atrib.'">';
	}
	
	public function setPreString($arg)
	{
		$this->preString = $arg;		
	}
	
	public function setPostString($arg)
	{
		$this->postString = $arg;		
	}
	
	public function startRow() 
	{
		$this->preString .= '<tr>';
	}
	
	public function endRow() 
	{
		$this->postString .= '</tr>';
	}
	
	public function endTable() 
	{
		$this->postString .= '</table>';
	}
	
	public function setTableTitles($titles) 
	{
		$this->preString .= '<tr>';
		 
		foreach ($titles as $title) {
			$this->preString .= '<td><label>'.$title.'</label></td>';
		}
		
		$this->preString .= '</tr>'; 
	}
	
	public function setCellAttribs($atribs)
	{
		$this->cellAtribs = $atribs;
	}
	
	public function render($content) 
	{
		$atribs = '';
		if (count($this->cellAtribs) > 0) {
			foreach ($this->cellAtribs as $atribName => $atribValue) {
				$atribs .= $atribName.'="'.$atribValue.'"';
			}
		}
		
		$result = $this->preString.'<td '.$atribs.' >'.$content.'</td>'.$this->postString;
		return $result;
	}
}

?>