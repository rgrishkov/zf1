<?php
class ZendExt_Decorators_ContentBoxDecorator extends Zend_Form_Decorator_Abstract
{
	
	public function render($content) 
	{
		$header = $this->_options['header'];
		$label = $this->_options['label'];
		
		$result = '
			<div class="gray-small">
				<div class="content-box corners">
					<header>
						<h3>'.$header.'</h3>
					</header>
					<section>
						<p>
							<label for="primary-role">'.$label.'</label>
							'.$content.'
						</p>
					</section>
				</div>
			</div>		
		';
		return $result;
	}
}

?>