<?php
class ZendExt_Facades_MessagesFacade {

    public function sendSystemMessage(Application_Model_Users $user, $messageId)
    {
        $messageTemplate = new Application_Model_MessageTemplate();
        $templateMapper = new Application_Model_MessageTemplatesMapper();

        if (!$templateMapper->loadMessageById($messageId, $messageTemplate)) {
            throw new Exception('Invalid message id');
        }

        $userMessage = new Application_Model_UserMessage();
        $messageMapper = new Application_Model_UserMessageMapper();

        $userMessage->setUserEmail($user->getEmail());
        $userMessage->setSubject($messageTemplate->getSubject());
        $userMessage->setBody($messageTemplate->getMessage_Template());
        $userMessage->setFrom('Administration');

        $messageMapper->save($userMessage);


        if ($messageTemplate->getEmail_Template() != null) {


            $templateFileName = $messageTemplate->getEmail_Template();

            if (file_exists(EMAIL_TEMPLATES_PATH.'/'.$templateFileName) == false) {
                throw new Exception('Invalid template file name');
            }

            $handle = fopen(EMAIL_TEMPLATES_PATH.'/'.$templateFileName, "r");
            $content = '';
            while (!feof($handle)) {
              $content .= fread($handle, 8192);
            }
            fclose($handle);

            $content = str_replace('[FIRSTNAME]', $user->getFirstname(), $content);
            $content = str_replace('[LASTNAME]', $user->getSurname(), $content);
            $content = str_replace('[EMAIL]', $user->getEmail(), $content);

            if ($messageTemplate->getSend_Email() == true) {
                $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'emails');

                $mail = new Zend_Mail();
                $mail->setBodyHtml($content);
                $mail->setFrom($config->from->email, $config->from->name);
                $mail->addTo($user->getEmail(), $user->getFirstname().' '.$user->getSurname());
                $mail->setSubject($messageTemplate->getSubject());

                ZendExt_Facades_MessagesFacade::sendEmail($mail);
            }
        }
    }

    public function sendPasswordRecoveryEmail(Application_Model_Users $user, $passwordRecoveryUrl, $passwordRecoveryKey)
    {
        $messageTemplate = new Application_Model_MessageTemplate();
        $templateMapper = new Application_Model_MessageTemplatesMapper();
        $templateMapper->loadMessageById('PASSWORD_RECOVERY', $messageTemplate);

        $templateFileName = $messageTemplate->getEmail_Template();

        if (file_exists(EMAIL_TEMPLATES_PATH.'/'.$templateFileName) == false) {
            throw new Exception('Invalid template file name');
        }

        $handle = fopen(EMAIL_TEMPLATES_PATH.'/'.$templateFileName, "r");
        $content = '';
        while (!feof($handle)) {
          $content .= fread($handle, 8192);
        }
        fclose($handle);

        $content = str_replace('[FIRSTNAME]', $user->getFirstname(), $content);
        $content = str_replace('[LASTNAME]', $user->getSurname(), $content);
        $content = str_replace('[EMAIL]', $user->getEmail(), $content);

        $content = str_replace('[RECOVERY_URL]', $passwordRecoveryUrl, $content);
        $content = str_replace('[RECOVERY_KEY]', $passwordRecoveryKey, $content);

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'emails');

        $mail = new Zend_Mail();
        $mail->setBodyHtml($content);
        $mail->setFrom($config->from->email, $config->from->name);
        $mail->addTo($user->getEmail(), $user->getFirstname().' '.$user->getSurname());
        $mail->setSubject('Password recovery');

        ZendExt_Facades_MessagesFacade::sendEmail($mail);
    }

    public static function sendEmail(Zend_Mail $mail)
    {
        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'emails');

        if ($config->need_smtp_auth == 1) {

            $transportConfig = array(
                'auth' => 'login',
                'port' => $config->smtp->port,
                'username' => $config->smtp->login,
                'password' => $config->smtp->password
            );

            $transport = new Zend_Mail_Transport_Smtp($config->smtp->server, $transportConfig);
            $mail->send($transport);
        } else {
            $mail->send();
        }
    }
}
