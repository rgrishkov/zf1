<?php
class ZendExt_Facades_UsersFacade {
	
	public function saveUser(Application_Model_Users $user)
	{
		$userMapper = new Application_Model_UsersMapper();
        $userMapper->save($user);
	}
	
	public function deleteUser(Application_Model_Users $user)
	{
		$userMapper = new Application_Model_UsersMapper();
		$userMapper->deleteUserByEmail($user->getEmail());
	}
}

?>