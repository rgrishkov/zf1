<?php
class ZendExt_Facades_CompetitionsFacade {

    public static function changeCampaignStatus(Application_Model_CampaignTokenComp $competition, $status) {
        $competitionMapper = new Application_Model_CampaignTokenCompMapper();

        $competition->setCampaign_Comp_Status($status);
        $competitionMapper->save($competition);
    }

    public static function setInprogressStatus(Application_Model_CampaignTokenComp $competition) {
        if ($competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_NEW) {
            self::changeCampaignStatus($competition, CAMPAIGN_STATUS_INPROGRESS);
        }
    }

    public static function pauseCompetition(Application_Model_CampaignTokenComp $competition) {
        if ($competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_RUNNING) {
            self::changeCampaignStatus($competition, CAMPAIGN_STATUS_PAUSED);
        }
    }

    public static function startCompetition(Application_Model_CampaignTokenComp $competition) {
       if ($competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_INPROGRESS || $competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_PAUSED) {
           self::changeCampaignStatus($competition, CAMPAIGN_STATUS_RUNNING);
       }
   }
}
