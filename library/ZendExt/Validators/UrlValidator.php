<?php
class ZendExt_Validators_UrlValidator extends Zend_Validate_Abstract {

	const MSG_ERROR = 'msgError';
	
	protected $_messageTemplates = array(self::MSG_ERROR => 'Url %value% is not valid');
	
	public function isValid($value) 
	{
	
		$this->_value = $value;
		$parsedUrl = parse_url($value);

		if (!isset($parsedUrl['scheme'])) {
            $url = ltrim($value, '/');
			$url = 'http://'.$url;
		} else {
			$url = $value;
		}

		if (Zend_Uri::check($url)) {
			return true;
		} else {
			$this->_error(self::MSG_ERROR);
			return false;
		}
	}

}

?>