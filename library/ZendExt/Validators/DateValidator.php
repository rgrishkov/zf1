<?php
class ZendExt_Validators_DateValidator extends Zend_Validate_Abstract {

	const MSG_ERROR = 'msgError';
	
	private $laterDate = null;
	private $earlierDate = null;
	private $time = null;
	
	protected $_messageTemplates = array(self::MSG_ERROR => '');
	
	public function isValid($value) 
	{
		$this->_value = $value;
		$isValid = false;

		if ($this->time != null) {
			$value = $value.' '.$this->time;
		}

		$validatingDate = new Zend_Date($value);
		
		if ($this->earlierDate != null) {
			if ($validatingDate->isLater($this->earlierDate) == true) {
				$isValid = true;
			} else {
				$isValid = false;
			}
		} 
		
		if ($this->laterDate != null) {
			if ($validatingDate->isEarlier($this->laterDate) == true) {
				$isValid = true;
			} else {
				$isValid = false;
			}
		}
		
		if ($isValid == true) {
			return true;
		} else {
			$this->_error(self::MSG_ERROR);
			return false;
		}
	}
	
	public function setLaterDate($value)
	{
		$this->laterDate = new Zend_Date($value);
	}
	
	public function setEarlierDate($value)
	{
		$this->earlierDate = new Zend_Date($value);
	}
	
	public function setTime($value)
	{
		$this->time = $value;	
	}

}

?>