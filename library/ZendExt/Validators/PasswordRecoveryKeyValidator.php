<?php
class ZendExt_Validators_PasswordRecoveryKeyValidator extends Zend_Validate_Abstract {

	const MSG_ERROR = 'msgError';
	
	protected $_messageTemplates = array(self::MSG_ERROR => 'Unknown or invalid key. Only the last key requested is valid and expires after one hour.');
	
	public function isValid($value)
	{
		
		$this->_setValue($value);
		
		$user = new Application_Model_Users();
		$userMapper = new Application_Model_UsersMapper();
		
        $loadResult = $userMapper->loadUserByRecoveryKey($value, $user);

        if ($loadResult == true) {
            return true;
        } else {
            $this->_error(self::MSG_ERROR);
            return false;
        }

	}
}

?>