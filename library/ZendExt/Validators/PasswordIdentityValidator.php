<?php
class ZendExt_Validators_PasswordIdentityValidator extends Zend_Validate_Abstract {

	const MSG_ERROR = 'msgError';
	
	protected $_messageTemplates = array(self::MSG_ERROR => 'Please enter current password');

    protected $currentPassword = null;
	
	public function isValid($value) 
	{
		if (md5($value) == $this->currentPassword) {
			return true;
		} else {
			$this->_error(self::MSG_ERROR);
			return false;
		}
	}

    public function setCurrentPassword($currentPassword)
    {
        $this->currentPassword = $currentPassword;
    }

}

?>