<?php
class ZendExt_Validators_ConditionTokensValidator extends Zend_Validate_Abstract {

	const MSG_ERROR = 'msgError';
	
	protected $_messageTemplates = array(self::MSG_ERROR => 'Please replace all [TOKENS]');
	
	public function isValid($value) 
	{
		$this->_value = $value;

	    $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/condition_tokens.ini');

        foreach($config as $token) {
            Zend_Debug::dump(strpos($value,$token));
            if (strpos($value,$token) != false) {
                $this->_error(self::MSG_ERROR);
                return false;
            }

        }
        return true;
	}

}

?>