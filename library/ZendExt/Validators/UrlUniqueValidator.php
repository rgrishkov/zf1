<?php
class ZendExt_Validators_UrlUniqueValidator extends Zend_Validate_Abstract {

	const MSG_ERROR = 'msgError';
	
	private $currentUrl = null;
	
	protected $_messageTemplates = array(self::MSG_ERROR => 'Url %value% already exist');
	
	public function isValid($value) 
	{
		
		$this->_setValue($value);
		
		if ($value != $this->currentUrl) {
			$site = new Application_Model_Site();
			$siteMapper = new Application_Model_SiteMapper();
			$result = $siteMapper->loadSiteByUrl($value, $site);
			
			if ($result == false) {
				return true;
			} else {
				$this->_error(self::MSG_ERROR);
				return false;
			}
			
		} else {
			return true;
		}
	}
	
	public function setCurrentUrl($value)
	{
		$this->currentUrl = $value;		
	}
	
	public function getCurrentUrl()
	{
		return $this->currentUrl;
	}

}

?>