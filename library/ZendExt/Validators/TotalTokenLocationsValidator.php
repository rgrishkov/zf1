<?php
class ZendExt_Validators_TotalTokenLocationsValidator extends Zend_Validate_Abstract {

	const MSG_ERROR = 'msgError';

    const MAX_LOCATIONS_DEFAULT = 2;

    const MAX_LOCATIONS_URL = 4;

    protected $campaignId;

    protected $type;
	
	protected $_messageTemplates = array(self::MSG_ERROR => 'You can enter maximum %value%');

	public function isValid($value)
	{
        $tokenLocationsMapper = new Application_Model_CampaignTokenLocationMapper();
        $totalTokenLocations = $tokenLocationsMapper->getTotalLocationsByCampaignId($this->getCampaignId(), $this->getType());

        if ($this->getType() == LOCATION_TYPE_DEFAULT) {
            if ($totalTokenLocations >= self::MAX_LOCATIONS_DEFAULT) {
                $this->_setValue(self::MAX_LOCATIONS_DEFAULT.' Default locations');
                $this->_error(self::MSG_ERROR);
                return false;
            }
        } elseif($this->getType() == LOCATION_TYPE_URL) {
            if ($totalTokenLocations >= self::MAX_LOCATIONS_URL) {
                $this->_setValue(self::MAX_LOCATIONS_URL.' Page URL locations');
                $this->_error(self::MSG_ERROR);
                return false;
            }
        }

        return true;
	}

    public function getCampaignId() {
        return $this->campaignId;
    }

    public function setCampaignId($value) {
        $this->campaignId = $value;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($value) {
        $this->type = $value;
    }
}

?>