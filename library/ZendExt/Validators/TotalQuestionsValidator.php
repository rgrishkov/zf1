<?php
class ZendExt_Validators_TotalQuestionsValidator extends Zend_Validate_Abstract {

	const MSG_ERROR = 'msgError';

    const MAX_QUESTIONS = 4;

    protected $campaignId;
	
	protected $_messageTemplates = array(self::MSG_ERROR => 'You can enter maximum %value%');
	
	public function isValid($value)
	{
        $this->_setValue(self::MAX_QUESTIONS.' questions');

        $questionMapper = new Application_Model_CampaignQuestionMapper();
        $totalQuestions = $questionMapper->getTotalQuestionsByCampaignId($this->getCampaignId());

        if ($totalQuestions < self::MAX_QUESTIONS) {
            return true;
		} else {
			$this->_error(self::MSG_ERROR);
			return false;
		 }
	}

    public function getCampaignId() {
        return $this->campaignId;
    }

    public function setCampaignId($value) {
        $this->campaignId = $value;
    }
}

?>