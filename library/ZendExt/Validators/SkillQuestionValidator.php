<?php
class ZendExt_Validators_SkillQuestionValidator extends Zend_Validate_Abstract {

	const MSG_ERROR = 'msgError';
	
	protected $_messageTemplates = array(self::MSG_ERROR => 'Skill question can contain maximum %value% words');
	
	public function isValid($value) 
	{
	    $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/user_campaign_defaults.ini', 'competition');

        $skillQuestionMaxWords = $config->question_max_words;
        $skillQuestionTotalWords = str_word_count($value, 0);

        if ($skillQuestionTotalWords > $skillQuestionMaxWords) {
            $this->_value = $skillQuestionMaxWords;
            $this->_error(self::MSG_ERROR);
            return false;
        } else {
            return true;
        }
	}

}

?>