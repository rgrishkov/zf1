<?php
class ZendExt_Validators_EmailUniqueValidator extends Zend_Validate_Abstract {

	const MSG_ERROR = 'msgError';
	
	protected $_messageTemplates = array(self::MSG_ERROR => 'Email Adress %value% is not unique');
	
	private $oldEmail = null;
	
	public function isValid($value) 
	{
		
		$this->_setValue($value);
		
		$user = new Application_Model_Users();
		$userMapper = new Application_Model_UsersMapper();

		if ($value != $this->getOldEmail()) {
			$loadResult = $userMapper->loadUserByEmail($value, $user);
			
			if ($loadResult == true) {
				$this->_error(self::MSG_ERROR);
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
	
	public function setOldEmail($email) 
	{
		$this->oldEmail = $email;
	}
	
	public function getOldEmail() 
	{
		return $this->oldEmail;
	}
}

?>