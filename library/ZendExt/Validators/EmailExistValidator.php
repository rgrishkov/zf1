<?php
class ZendExt_Validators_EmailExistValidator extends Zend_Validate_Abstract {

	const MSG_ERROR = 'msgError';
	
	protected $_messageTemplates = array(self::MSG_ERROR => 'Unknown user account');
	
	public function isValid($value)
	{
		
		$this->_setValue($value);
		
		$user = new Application_Model_Users();
		$userMapper = new Application_Model_UsersMapper();
		
			$loadResult = $userMapper->loadUserByEmail($value, $user);
			
			if ($loadResult == true) {
                return true;
			} else {
                $this->_error(self::MSG_ERROR);
				return false;
			}

	}
}

?>