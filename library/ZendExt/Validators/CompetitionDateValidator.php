<?php
class ZendExt_Validators_CompetitionDateValidator extends Zend_Validate_Abstract {

	const MSG_ERROR_CAMPAIGNS = 'msgErrorCampaigns';
    const MSG_ERROR_DATE = 'msgErrorDate';
    const MSG_ERROR_END_DATE_EARLIER = 'msgErrorDateEarlier';
    const MSG_ERROR_EQUALS = 'msgErrorEquals';
	
	protected $_messageTemplates = array(
        self::MSG_ERROR_CAMPAIGNS => 'You already have competitions for this dates',
        self::MSG_ERROR_DATE => 'Selected date is earlier then current date',
        self::MSG_ERROR_END_DATE_EARLIER => 'End date is earlier then start date',
        self::MSG_ERROR_EQUALS => 'End date later then start date'
    );
    protected $competition = null;
    protected $campaignId = null;
    protected $startDate;
    protected $endDate;
	
	public function isValid($value)
	{
		$this->_setValue($value);

        $competitionMapper = new Application_Model_CampaignTokenCompMapper();
        $currentDateString = $competitionMapper->getCurrentDate();

        $currentDate = new Zend_Date($currentDateString, 'YYYY-MM-DD');
        $currentDate->setTimeZone('GMT');

        $validatingDate = new Zend_Date($value);
        $validatingDate->setTimeZone('GMT');
        //Zend_Debug::dump($this->startDate);
        //die();

        /*Zend_Debug::dump($validatingDate->__toString());
        Zend_Debug::dump($this->endDate->__toString());

        die();*/
        if ($this->competition->getCampaign_Comp_Status() != CAMPAIGN_STATUS_RUNNING) {


            if ($validatingDate->isEarlier($currentDate)) {
                $this->_error(self::MSG_ERROR_DATE);
                return false;
            }

            if ($this->endDate->isEarlier($this->startDate)) {
               $this->_error(self::MSG_ERROR_END_DATE_EARLIER);
               return false;
            }

            if ($this->endDate->equals($this->startDate)) {
               $this->_error(self::MSG_ERROR_EQUALS);
               return false;
            }

        }



        $competitionId = $this->competition->getCampaign_Comp_Id();

        $competition = new Application_Model_CampaignTokenComp();
        $competitionMapper = new Application_Model_CampaignTokenCompMapper();
        $competitionMapper->loadCampaignTokenCompById($competitionId, $competition);

        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($competition->getCampaign_Id(), $campaign);

        $haveCampaigns = $competitionMapper->haveThisDatesCompetitions($competitionId, $campaign->getSite_Id(), $this->startDate, $this->endDate);

        if ($haveCampaigns == true) {
            $this->_error(self::MSG_ERROR_CAMPAIGNS);
            return false;
        }

        return true;
	}

    public function setCompetition($value)
    {
        $this->competition = $value;
    }

    public function setCampaignId($value)
    {
        $this->campaignId = $value;
    }


    public function setStartDate($value)
    {
        $this->startDate = $value;
    }

    public function setEndDate($value)
    {
        $this->endDate = $value;
    }
}

?>