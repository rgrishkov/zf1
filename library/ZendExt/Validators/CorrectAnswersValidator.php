<?php
class ZendExt_Validators_CorrectAnswersValidator extends Zend_Validate_Abstract {
	
	private $elements = array();
	
	const MSG_ERROR = 'msgError';
	
	protected $_messageTemplates = array(self::MSG_ERROR => 'At least one answer must be correct');

    public function isValid($value)
    {
    	$elements = $this->getElements();
    	$correctAnswers = array(); 
    	
    	foreach($elements as $element) {
    		if ($element['answer']->getValue() != '' && (int) $element['correct']->getValue() != 0) {
    			$correctAnswers[] = $element;
    		} 
    	} 
    	
    	if (count($correctAnswers) == 0) {
    		$this->_error(self::MSG_ERROR);
    		return false;
    	} else {
    		return true;
    	}
    }
    
    public function addElement($element) 
    {
    	$this->elements[] = $element;
    }
    
    public function getElements() 
    {
    	return $this->elements;
    }
}

?>