<?php
class ZendExt_Forms_ContactForm extends ZendExt_Forms_Form
{
	
	public function init()
	{
		$this->setAction('/help/contact');
		$this->setMethod('post');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'emails');

        $department = $this->createElement('select', 'department');
		$department->setLabel('Department:');
		$department->setRequired(true);
		$department->addValidator('NotEmpty');
		$department->addMultiOption($config->department->support, 'Support');
        $department->addMultiOption($config->department->billing, 'Billing');
		$this->addElement($department);

		$subject = $this->createElement('text', 'subject', array('class' => 'half'));
		$subject->setLabel('Subject:');
		$subject->setRequired(true);
		$subject->addValidator('NotEmpty');
		$this->addElement($subject);

        $message = $this->createElement('textarea', 'message', array('class' => 'full wysiwyg', 'id' => 'message'));
		$message->setAttribs(array('rows' => 10, 'cols' => 20));
		$message->removeDecorator('Label');
		$message->removeDecorator('HtmlTag');
        $message->addDecorator('Label');
		$message->setLabel('Message:');
		$this->addElement($message);

        $name = $this->createElement('text', 'name', array('class' => 'half'));
		$name->setLabel('Your name:');
		$name->setRequired(true);
		$name->addValidator('NotEmpty');
		$this->addElement($name);

        $email = $this->createElement('text', 'email', array('class' => 'half'));
		$email->setLabel('Your email:');
		$email->setRequired(true);
        $email->addValidator('EmailAddress');
		$email->addValidator('NotEmpty');
		$this->addElement($email);

		$submit = $this->createElement('submit', 'add', array('class' => 'btn btn-green big'));
		$submit->setLabel('Send');
		$this->addElement($submit);

	}
	
	public function setName($value)
	{
        $name = $this->getElement('name');
		$name->setValue($value);
	}

    public function setEmail($value)
    {
        $email = $this->getElement('email');
		$email->setValue($value);
    }

}