<?php
class ZendExt_Forms_RegistrationForm extends ZendExt_Forms_Form
{
	public function init()
	{
		$this->setAction('/index/registration');
		$this->setMethod('post');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'index/registration_form.phtml'))
        ));

        /*$siteName = $this->createElement('text', 'site_name', array('class' => 'full'));
		$siteName->setRequired(true);
        $siteName->removeDecorator('Label');
		$siteName->addValidator('NotEmpty');
        $this->addElement($siteName);*/

        $urlValidatorChain = new Zend_Validate();
        $urlValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator(new ZendExt_Validators_UrlValidator());

		$siteUrl = $this->createElement('text', 'site_url');
		$siteUrl->setRequired(true);
        $siteUrl->removeDecorator('Label');
		$siteUrl->addValidator($urlValidatorChain);
        $this->addElement($siteUrl);

		$firstName = $this->createElement('text', 'firstname');
		$firstName->setRequired(true);
        $firstName->removeDecorator('Label');
		$firstName->addValidator('NotEmpty');
		$this->addElement($firstName);

		$surName = $this->createElement('text', 'surname');
		$surName->setRequired(true);
        $surName->removeDecorator('Label');
		$surName->addValidator('NotEmpty');
		$this->addElement($surName);

        $emailValidatorChain = new Zend_Validate();
        $emailValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator(new Zend_Validate_EmailAddress())->addValidator(new ZendExt_Validators_EmailUniqueValidator());

		$email = $this->createElement('text', 'email');
		$email->setRequired(true);
        $email->removeDecorator('Label');
        $email->addValidator($emailValidatorChain);
		$this->addElement($email);

        $country = $this->createElement('select', 'country', array('style' => 'width:219px;'));
        $country->removeDecorator('Label');
		$country->setRequired(true);
        $country->setLabel('Country:');
		$country->addValidator('NotEmpty');
		$this->addElement($country);

        $timezone = $this->createElement('select', 'timezone', array('style' => 'width:219px;'));
        $timezone->removeDecorator('Label');
		$timezone->setRequired(true);
        $timezone->setLabel('Time zone:');
		$timezone->addValidator('NotEmpty');
		$this->addElement($timezone);

        $lengthValidator = new Zend_Validate_StringLength(array('min' => 6));
        $lengthValidator->setMessage('Password is less than 6 characters long');

        $passwordValidatorChain = new Zend_Validate();
        $passwordValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator($lengthValidator);

		$password = $this->createElement('password', 'password', array('class' => 'password'));
        $password->setRequired(true);
        $password->removeDecorator('Label');
        $password->addValidator($passwordValidatorChain);
		$this->addElement($password);

        $confirmValidator = new Zend_Validate_Identical();
		$confirmValidator->setToken('password');
		$confirmValidator->setMessage('Please confirm password');

		$confirmPassword = $this->createElement('password', 'confirm_password');
		$confirmPassword->addValidator($confirmValidator);
        $confirmPassword->setRequired(true);
        $confirmPassword->removeDecorator('Label');
		$this->addElement($confirmPassword);

		$submit = $this->createElement('submit', 'submit', array('class' => 'btn large primary'));
		$submit->setLabel('Signup Now');
		$this->addElement($submit);
	}

    public function fillCountries($optionsList)
	{
		$select = $this->getElement('country');

		foreach ($optionsList as $id => $title) {
			$select->addMultiOption($id, $title);
		}
	}

    public function fillTimezones($optionsList)
	{
		$select = $this->getElement('timezone');

		foreach ($optionsList as $id => $title) {
			$select->addMultiOption($id, $title);
		}
    }

}