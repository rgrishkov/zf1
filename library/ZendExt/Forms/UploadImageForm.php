<?php
class ZendExt_Forms_UploadImageForm extends ZendExt_Forms_SubForm
{
	public function init()
	{
		//$this->setAction('/sites/add');
		$this->setMethod('post');
		$this->setAttrib('enctype', 'multipart/form-data');
		
		$lineTitle = new ZendExt_Elements_LineTitle('line_title');
		$lineTitle->setTitle('Logo & Images');
		$this->addElement($lineTitle);
		     
		$tableDecorator = new ZendExt_Decorators_TableDecorator();
		$tableDecorator->startTable('half no-style no-border');
		$tableDecorator->startRow();
		
        $file = $this->createElement('file', 'site_logo');
        $file->setRequired(true); 
		$file->addValidator('NotEmpty');
		$file->addDecorator($tableDecorator); 
		$this->addElement($file);
		
		$tableDecorator = new ZendExt_Decorators_TableDecorator();
		$tableDecorator->endRow();
		$tableDecorator->endTable();
		$submit = $this->createElement('submit', 'upload_image', array('class' => 'btn btn-green big'));
		$submit->addDecorator($tableDecorator); 

		$submit->setLabel('Upload');
		$this->addElement($submit);
	}

    public function setTitle($value)
    {
        $lineTitle = $this->getElement('line_title');
        $lineTitle->setTitle($value);
    }
}