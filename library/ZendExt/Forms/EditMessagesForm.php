<?php
class ZendExt_Forms_EditMessagesForm extends ZendExt_Forms_Form
{
	public function init() 
	{
		$this->setAction('/settings/messages');
		$this->setMethod('post');
		
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/messages.ini');
		
		$subform = new ZendExt_Forms_SubForm();
		$subform->removeDecorator('HtmlTag');
		$subform->removeDecorator('DtDdWrapper');
		
		foreach ($config->message_type as $messageType => $messageTitle) {
			$message = $this->createElement('text', $messageType, array('class' => 'full'));
			$message->setLabel($messageTitle.':');
			$message->setRequired(true); 
			$message->addValidator('NotEmpty'); 
			$subform->addElement($message);   
		} 
		
		$this->addSubForm($subform, 'messages');
		 
		$submit = $this->createElement('submit', 'add', array('class' => 'btn btn-green big'));
		$submit->setLabel('Update');
		$this->addElement($submit);
	}

}