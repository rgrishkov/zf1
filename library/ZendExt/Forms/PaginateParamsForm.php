<?php
class ZendExt_Forms_PaginateParamsForm extends ZendExt_Forms_Form {
	
	public function init() 
	{
		//$this->setAction('/users/index');
		$this->setMethod('get');
		     
		$perPage = $this->createElement('select', 'per_page');
		$perPage->addMultiOption(10, 10);
		$perPage->addMultiOption(25, 25);
		$perPage->addMultiOption(50, 50);
		$perPage->addMultiOption(100, 100);
		
		$perPage->removeDecorator('HtmlTag');
		
		$perPage->addDecorator('Label');
		$perPage->setLabel('Show entries:');
		
		$perPage->setAttrib('onchange', 'this.form.submit();');
		
		$this->addElement($perPage);
		
	}
}

?>