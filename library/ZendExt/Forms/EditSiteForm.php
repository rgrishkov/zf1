<?php
class ZendExt_Forms_EditSiteForm extends ZendExt_Forms_Form
{
	private $uploadImageForm;
	private $imageNamesForm;
	private $uploadedImagesListElement;
	
	public function init() 
	{
		$this->setAction('/sites/edit');
		$this->setMethod('post');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'sites/edit_site_form.phtml'))
        ));
		     
		$siteId = $this->createElement('text', 'external_site_id', array('class' => 'half readonly'));
        $siteId->removeDecorator('HtmlTag');
		$siteId->setAttrib('readonly', 'readonly');
		$siteId->setLabel('Site Id:');
		$siteId->setRequired(true); 
		$siteId->addValidator('NotEmpty'); 
		$this->addElement($siteId); 
		
		$siteName = $this->createElement('text', 'site_name', array('class' => 'half'));
        $siteName->removeDecorator('HtmlTag');
		$siteName->setLabel('Site Name:');
		$siteName->setRequired(true); 
		$siteName->addValidator('NotEmpty'); 
		$this->addElement($siteName);

        $urlValidatorChain = new Zend_Validate();
        $urlValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator(new ZendExt_Validators_UrlValidator());

		$siteUrl = $this->createElement('text', 'site_url', array('class' => 'site_url'));
		$siteUrl->removeDecorator('Label');
        $siteUrl->removeDecorator('HtmlTag');
		$siteUrl->setRequired(true); 
		$siteUrl->addValidator($urlValidatorChain);
		$siteUrl->setDescription('e.g. www.example.com');
		$this->addElement($siteUrl);
		
		$this->uploadImageForm = new ZendExt_Forms_UploadImageForm();
		$this->addSubForm($this->uploadImageForm, 'upload_form');
		
		$this->uploadedImagesListElement = new ZendExt_Elements_UploadedSiteImagesList('images_types_list');
		$this->addElement($this->uploadedImagesListElement);
		
		$siteBio = $this->createElement('textarea', 'site_bio', array('class' => 'full wysiwyg', 'id' => 'bio'));
        $siteBio->removeDecorator('HtmlTag');
		$siteBio->setAttribs(array('rows' => 10, 'cols' => 20));
		$siteBio->removeDecorator('Label');
		$siteBio->removeDecorator('HtmlTag');
		$siteBio->addDecorator('Label');
		$siteBio->setLabel('Site Bio:');
		$this->addElement($siteBio);
		
		$submit = $this->createElement('submit', 'save', array('class' => 'btn btn-green big'));
		$submit->setLabel('Save');
		$this->addElement($submit);
		
		$siteId = $this->createElement('hidden', 'site_id');
		$siteId->removeDecorator('Label');
		$siteId->removeDecorator('HtmlTag');
        $this->addElement($siteId);

        $competitionId = $this->createElement('hidden', 'competition_id');
		$competitionId->removeDecorator('Label');
		$competitionId->removeDecorator('HtmlTag');
        $this->addElement($competitionId);
	}
	
	public function fillImageTypes($optionsList)
	{
		$this->uploadedImagesListElement->fillImageTypes($optionsList);
	}
	
	public function populate($formData)
	{
		$this->uploadedImagesListElement->populate($formData);
		$this->uploadImageForm->populate($formData);
		return parent::populate($formData);
	}
	
	public function isValid($formData)
	{
		$file = $this->uploadImageForm->getElement('site_logo');
		$file->setRequired(false);
		$file->removeValidator('NotEmpty');
		
		return parent::isValid($formData);
	}
}