<?php
class ZendExt_Forms_SearchUsersForm extends ZendExt_Forms_Form
{
	public function init() 
	{
		$this->setAction('/users');
		$this->setMethod('post');
		     
		$firstName = $this->createElement('text', 'site', array('class' => 'half'));
		$firstName->setLabel('Site:');
		$this->addElement($firstName); 
		
		$email = $this->createElement('text', 'email', array('class' => 'half'));
		$email->setLabel('Email:');
		$email->setDescription('e.g. example@example.com');
		$email->addValidator('EmailAddress');   
		$this->addElement($email);
		
		$phone = $this->createElement('text', 'name', array('class' => 'half'));
		$phone->setLabel('Name:');
		$this->addElement($phone);
		
		
		$submit = $this->createElement('submit', 'add', array('class' => 'btn btn-green big'));
		$submit->setLabel('Search');
		$this->addElement($submit);
	}
}