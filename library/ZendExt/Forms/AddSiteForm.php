<?php
class ZendExt_Forms_AddSiteForm extends ZendExt_Forms_Form
{
	
	public function init()
	{
		$this->setAction('/sites/add');
		$this->setMethod('post');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'sites/add_site_form.phtml'))
        ));

		$externalIdField = $this->createElement('text', 'external_site_id', array('class' => 'half readonly'));
        $externalIdField->removeDecorator('HtmlTag');
		$externalIdField->setAttrib('readonly', 'readonly');
		$externalIdField->setLabel('Site Id:');
		$externalIdField->setRequired(true);
		$externalIdField->addValidator('NotEmpty');
		$this->addElement($externalIdField);

		$siteName = $this->createElement('text', 'site_name', array('class' => 'half'));
        $siteName->removeDecorator('HtmlTag');
		$siteName->setLabel('Site Name:');
		$siteName->setRequired(true);
		$siteName->addValidator('NotEmpty');
		$this->addElement($siteName);

        $urlValidatorChain = new Zend_Validate();
        $urlValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator(new ZendExt_Validators_UrlValidator());

        $siteUrl = $this->createElement('text', 'site_url', array('class' => 'site_url'));
        $siteUrl->removeDecorator('Label');
        $siteUrl->removeDecorator('HtmlTag');
		$siteUrl->setRequired(true);
		$siteUrl->addValidator($urlValidatorChain);
		$siteUrl->setDescription('e.g. www.example.com');
		
		$this->addElement($siteUrl);
		
		$siteBio = $this->createElement('textarea', 'site_bio', array('class' => 'full wysiwyg', 'id' => 'bio'));
        $siteBio->removeDecorator('HtmlTag');
		$siteBio->setAttribs(array('rows' => 10, 'cols' => 20));
		$siteBio->removeDecorator('Label');
		$siteBio->removeDecorator('HtmlTag');
		
		$siteBio->addDecorator('Label');
		$siteBio->setLabel('Site Bio:');
		
		$this->addElement($siteBio);
		
		$submit = $this->createElement('submit', 'save', array('class' => 'btn btn-green big'));
		$submit->setLabel('Add');
		$this->addElement($submit);

	}
	
	public function setExternalId($externalId) 
	{
        $externalIdField = $this->getElement('external_site_id');
		$externalIdField->setValue($externalId);
	}

}