<?php
class ZendExt_Forms_Form extends Zend_Form
{
	public function setAction($action)
	{
	    $url = rtrim(Zend_Controller_Front::getInstance()->getBaseUrl(), '/') .$action;
	    parent::setAction($url);
	}
}