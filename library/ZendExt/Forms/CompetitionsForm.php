<?php
class ZendExt_Forms_CompetitionsForm extends ZendExt_Forms_Form
{
    private $competitionDateValidator;

	public function init()
	{
		$this->setAction('/campaigns/competitions');
		$this->setMethod('post');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'campaigns/competitions_form.phtml'))
        ));

		$competitionName = $this->createElement('text', 'competition_name');
        $competitionName->removeDecorator('Label');
		$competitionName->setRequired(true);
		$competitionName->addValidator('NotEmpty');
		$this->addElement($competitionName);

        $status = $this->createElement('select', 'campaign_comp_status');
		$status->removeDecorator('Label');
		$status->setRequired(true);
		$status->addValidator('NotEmpty');
		$status->addMultiOption(null, '--None--');
		$this->addElement($status);

        $httpPrefixDecorator = new ZendExt_Decorators_HttpPrefixDecorator();

        $triggerUrl = $this->createElement('text', 'trigger_url', array('class' => 'trigger_url'));
        $triggerUrl->setValue('/');
        $triggerUrl->removeDecorator('HtmlTag');
        $triggerUrl->removeDecorator('Label');
        $triggerUrl->addDecorator($httpPrefixDecorator);
        $urlValidator = new ZendExt_Validators_UrlValidator();
		$triggerUrl->addValidator($urlValidator);
		$this->addElement($triggerUrl);

		$requiresSkillQuestion = $this->createElement('checkbox', 'requires_skill_question');
		$requiresSkillQuestion->removeDecorator('HtmlTag');
		$requiresSkillQuestion->removeDecorator('Label');
		$this->addElement($requiresSkillQuestion);
		
		$skillQuestion = $this->createElement('text', 'skill_question', array('class' => 'full'));
        $skillQuestion->removeDecorator('Label');
		$this->addElement($skillQuestion);
		
		$maxWords = $this->createElement('text', 'max_words');
        $maxWords->removeDecorator('Label');
		$maxWords->setAttrib('maxlength', 3);
        $maxWords->addValidator('Digits');
		$this->addElement($maxWords);

        $numSecondarySidebarImages = $this->createElement('text', 'num_secondary_sidebar_images');
        $numSecondarySidebarImages->removeDecorator('Label');
		$numSecondarySidebarImages->setAttrib('maxlength', 2);
        $numSecondarySidebarImages->addValidator('NotEmpty');
        $numSecondarySidebarImages->addValidator('Digits');
		$this->addElement($numSecondarySidebarImages);
		
		$introText = $this->createElement('text', 'intro_text');
        $introText->removeDecorator('Label');
        $introText->setRequired(true);
        $introText->addValidator('NotEmpty');
		$this->addElement($introText);

        $dateValidator = new Zend_Validate_Date();
        $this->competitionDateValidator = new ZendExt_Validators_CompetitionDateValidator();

        $dateValidatorChain = new Zend_Validate();
        $dateValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator($dateValidator)->addValidator($this->competitionDateValidator);

		$startDate = $this->createElement('text', 'start_date');
        $startDate->removeDecorator('HtmlTag');
        $startDate->removeDecorator('Label');
		$startDate->setRequired(true);
		$startDate->addValidator($dateValidatorChain);
		$this->addElement($startDate);

        //$timeValidatorChain = new Zend_Validate();
        //$timeValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator($timeValidator);

        $competitionLength = $this->createElement('select', 'competition_length');
        $competitionLength->removeDecorator('HtmlTag');
        $competitionLength->removeDecorator('Label');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/user_campaign_defaults.ini', 'competition');

        for ($lengthCounter = 1; $lengthCounter<=$config->length_days; $lengthCounter++) {
            $competitionLength->addMultiOption($lengthCounter, $lengthCounter);
        }

        $this->addElement($competitionLength);

        $startTimeHours = $this->createElement('select', 'start_time_hours');
        $startTimeHours->removeDecorator('HtmlTag');
        $startTimeHours->removeDecorator('Label');
        $this->addElement($startTimeHours);

        $startTimeMinutes = $this->createElement('select', 'start_time_minutes');
        $startTimeMinutes->removeDecorator('HtmlTag');
        $startTimeMinutes->removeDecorator('Label');
        $this->addElement($startTimeMinutes);

        $endTimeHours = $this->createElement('select', 'end_time_hours');
        $endTimeHours->removeDecorator('HtmlTag');
        $endTimeHours->removeDecorator('Label');
        $this->addElement($endTimeHours);

        $endTimeMinutes = $this->createElement('select', 'end_time_minutes');
        $endTimeMinutes->removeDecorator('HtmlTag');
        $endTimeMinutes->removeDecorator('Label');
        $this->addElement($endTimeMinutes);

        for ($hoursCounter = 0; $hoursCounter<24; $hoursCounter++) {
            $hourStr = ZendExt_Utils_StringUtils::formatHours($hoursCounter);
            $startTimeHours->addMultiOption($hourStr, $hourStr);
            $endTimeHours->addMultiOption($hourStr, $hourStr);
        }

        for ($minutesCounter = 0; $minutesCounter<60; $minutesCounter+=10) {
            $minutesStr = ZendExt_Utils_StringUtils::formatHours($minutesCounter);
            $startTimeMinutes->addMultiOption($minutesStr, $minutesStr);
            $endTimeMinutes->addMultiOption($minutesStr, $minutesStr);
        }
		
		$postDetailsId = $this->createElement('hidden', 'campaign_comp_id');
        $postDetailsId->removeDecorator('Label');
		$postDetailsId->removeDecorator('HtmlTag');
		$this->addElement($postDetailsId);
		
		$campaignId = $this->createElement('hidden', 'campaign_id');
        $campaignId->removeDecorator('Label');
		$campaignId->removeDecorator('HtmlTag');
		$this->addElement($campaignId);
		
		$submit = $this->createElement('submit', 'save', array('class' => 'btn btn-green big'));
		$submit->setLabel('Save');
		$this->addElement($submit);
	}
	
	public function setCampaignId($campaignId) 
	{
        $campaignIdField = $this->getElement('campaign_id');
	    $campaignIdField->setValue($campaignId);
	}

    public function fillCampaignStatus($optionsList)
	{
		$select = $this->getElement('campaign_comp_status');

		foreach ($optionsList as $id => $title) {
			$select->addMultiOption($id, $title);
		}
	}

    public function setSiteUrl($siteUrl)
    {
        $triggerUrlField = $this->getElement('trigger_url');
        $httpPrefixDecorator = $triggerUrlField->getDecorator('httpPrefixDecorator');
        $httpPrefixDecorator->setOption('site_url', $siteUrl);

        $inputWidth = ZendExt_Utils_StringUtils::getUrlInputWidth($siteUrl);

        $triggerUrlField->setOptions(array('style' => 'width: '.$inputWidth.'px;'));
    }
	
	public function isValid($formData) 
	{
        if (isset($formData['start_date'])) {
            $this->competitionDateValidator->setCompetitionLength($formData['competition_length']);
            $this->competitionDateValidator->setCompetitionId($formData['campaign_comp_id']);
            $this->competitionDateValidator->setCampaignId($formData['campaign_id']);

            //$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/user_campaign_defaults.ini', 'competition');

            $startDateString = $formData['start_date'].' '.$formData['start_time_hours'].':'.$formData['start_time_minutes'].':00';
            $startDate = new Zend_Date($startDateString);
            //$startDate->subDay($config->pre_days);
            $startDate->setTimeZone('GMT');
            $this->competitionDateValidator->setStartDate($startDate->toString(Zend_Date::ISO_8601));

            $endDateString = $formData['start_date'].' '.$formData['end_time_hours'].':'.$formData['end_time_minutes'].':00';
            $endDate = new Zend_Date($endDateString);
            $endDate->addDay($formData['competition_length']);
            //$endDate->addDay($config->post_days);
            $endDate->setTimeZone('GMT');
            $this->competitionDateValidator->setEndDate($endDate->toString(Zend_Date::ISO_8601));
        }


        $maxWordsValidatorChain = new Zend_Validate();
		$maxWordsField = $this->getElement('max_words');

		if ((bool)$formData['requires_skill_question'] == true) {
            $skillQuestionField = $this->getElement('skill_question');

			$skillQuestionField->setRequired(true);
			$skillQuestionField->addValidator('NotEmpty');

            $maxWordsField->setRequired(true);
            $maxWordsValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator(new Zend_Validate_Digits())->addValidator(new Zend_Validate_Between(1, 500));
		} else {
            $maxWordsValidatorChain->addValidator(new Zend_Validate_Digits())->addValidator(new Zend_Validate_Between(1, 500));
        }

        $maxWordsField->addValidator($maxWordsValidatorChain);

		return parent::isValid($formData);
	}
	
}