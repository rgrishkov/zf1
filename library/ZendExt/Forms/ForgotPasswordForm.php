<?php
class ZendExt_Forms_ForgotPasswordForm extends ZendExt_Forms_Form
{
	public function init()
	{
		$this->setAction('/index/forgotpassword');
		$this->setMethod('post');

        $emailExistValidator = new ZendExt_Validators_EmailExistValidator();

		$email = $this->createElement('text', 'email', array('class' => 'half'));
		$email->setLabel('Email:');
		$email->setRequired(true);
		$email->addValidator('NotEmpty');
		$email->addValidator('EmailAddress');
        $email->addValidator($emailExistValidator);
		$this->addElement($email);

		$submit = $this->createElement('submit', 'submit', array('class' => 'btn large primary'));
		$submit->setLabel('Submit');
		$this->addElement($submit);
	}

}