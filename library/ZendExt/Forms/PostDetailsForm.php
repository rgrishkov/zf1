<?php
class ZendExt_Forms_PostDetailsForm extends ZendExt_Forms_Form
{
	private $uploadedImagesListElement;
	private $uploadImageForm;
	private $publishBox;
	
	public function init() 
	{
		$this->setAction('/campaigns/details');
		$this->setMethod('post');
		     
		$lineTitle = new ZendExt_Elements_LineTitle('line_title1');
		$lineTitle->setTitle('META');
		$this->addElement($lineTitle);

		$postTitle = $this->createElement('text', 'post_title', array('class' => 'half'));
		$postTitle->setLabel('Post Title:');
		$postTitle->setRequired(true); 
		$postTitle->addValidator('NotEmpty');
		$this->addElement($postTitle);
		
		$postDescription = $this->createElement('text', 'post_description', array('class' => 'half'));
		$postDescription->setLabel('Post Description:');
		$postDescription->setRequired(true); 
		$postDescription->addValidator('NotEmpty'); 
		$this->addElement($postDescription);
		
		$postKeywords = $this->createElement('text', 'post_keywords', array('class' => 'half'));
		$postKeywords->setLabel('Keywords:');
		$postKeywords->setRequired(true); 
		$postKeywords->addValidator('NotEmpty'); 
		$this->addElement($postKeywords);
		
		$lineTitle = new ZendExt_Elements_LineTitle('line_title2');
		$lineTitle->setTitle('POST');
		$this->addElement($lineTitle);
		
		$postName = $this->createElement('text', 'post_name', array('class' => 'half'));
		$postName->setLabel('Post Heading:');
		$postName->setRequired(true); 
		$postName->addValidator('NotEmpty'); 
		$this->addElement($postName);
		
		$promotionType = $this->createElement('select', 'promotion_type');
		$promotionType->setLabel('Promotion type:');
		$promotionType->setRequired(true); 
		$promotionType->addValidator('NotEmpty');
		$promotionType->addMultiOption(null, '--Please select--');
		$promotionType->addMultiOption('big promo', 'Big promo');
		$promotionType->addMultiOption('mini promo', 'Mini promo');
        $promotionType->addMultiOption('user promo', 'User promo');
		$this->addElement($promotionType);
		
		$postContent = $this->createElement('textarea', 'post_content', array('class' => 'full wysiwyg'));
		$postContent->setAttribs(array('rows' => 10, 'cols' => 20));
		$postContent->setLabel('Post Text:');
		$postContent->setRequired(true); 
		$postContent->addValidator('NotEmpty'); 
		$this->addElement($postContent);
		
		$postExerpt = $this->createElement('textarea', 'post_exerpt', array('class' => 'full wysiwyg'));
		$postExerpt->setAttribs(array('rows' => 10, 'cols' => 20));
		$postExerpt->setLabel('Mini Text:');
		$postExerpt->setRequired(true); 
		$postExerpt->addValidator('NotEmpty'); 
		$this->addElement($postExerpt);
		
		$postConditions = $this->createElement('textarea', 'post_conditions', array('class' => 'full wysiwyg'));
		$postConditions->setAttribs(array('rows' => 10, 'cols' => 20));
		$postConditions->setLabel('Conditions:');
		$postConditions->setRequired(true); 
		$postConditions->addValidator('NotEmpty'); 
		$this->addElement($postConditions);
		
		$lineTitle = new ZendExt_Elements_LineTitle('line_title3');
		$lineTitle->setTitle('Deal Features');
		$this->addElement($lineTitle);
		
		$showPostFeatures = $this->createElement('checkbox', 'show_post_features');
		$showPostFeatures->removeDecorator('HtmlTag');
		$showPostFeatures->removeDecorator('Label');
		$showPostFeatures->addDecorator('Label'); 
		$showPostFeatures->setLabel('Show Deal Features:');
		$showPostFeatures->addDecorator(new Zend_Form_Decorator_HtmlTag('br'));
		$showPostFeatures->addDecorator(new Zend_Form_Decorator_HtmlTag('br'));
		$this->addElement($showPostFeatures);
		
		$featureBuddyDeal = $this->createElement('checkbox', 'feature_buddy_deal');
		$featureBuddyDeal->removeDecorator('HtmlTag');
		$featureBuddyDeal->removeDecorator('Label');
		$featureBuddyDeal->addDecorator('Label');
		$featureBuddyDeal->setLabel('Buddy Deal:');
		$featureBuddyDeal->addDecorator(new Zend_Form_Decorator_HtmlTag('br'));
		$featureBuddyDeal->addDecorator(new Zend_Form_Decorator_HtmlTag('br'));
		$this->addElement($featureBuddyDeal);
		
		$featureSkillQuestion = $this->createElement('checkbox', 'feature_skill_question');
		$featureSkillQuestion->removeDecorator('HtmlTag');
		$featureSkillQuestion->removeDecorator('Label');
		$featureSkillQuestion->addDecorator('Label'); 
		$featureSkillQuestion->setLabel('Skill based question:');
		$this->addElement($featureSkillQuestion);

		$this->uploadImageForm = new ZendExt_Forms_UploadImageForm();
		$this->addSubForm($this->uploadImageForm, 'upload_form');
		
		$this->uploadedImagesListElement = new ZendExt_Elements_UploadedCampaignImagesList('images_types_list');
		$this->addElement($this->uploadedImagesListElement);
		
		$postDetailsId = $this->createElement('hidden', 'post_details_id');
		$postDetailsId->removeDecorator('Label');
		$postDetailsId->removeDecorator('HtmlTag');
		$this->addElement($postDetailsId);
		
		$campaignId = $this->createElement('hidden', 'campaign_id');
		$campaignId->removeDecorator('Label');
		$campaignId->removeDecorator('HtmlTag');
		$this->addElement($campaignId);
		
		$externalCampaignId = $this->createElement('hidden', 'external_campaign_id');
		$externalCampaignId->removeDecorator('Label');
		$externalCampaignId->removeDecorator('HtmlTag');
		$this->addElement($externalCampaignId);
		
		$publishDate = $this->createElement('hidden', 'publish_date');
		$publishDate->removeDecorator('Label');
		$publishDate->removeDecorator('HtmlTag');
		$this->addElement($publishDate);
		
		$postId = $this->createElement('hidden', 'post_id');
		$postId->removeDecorator('Label');
		$postId->removeDecorator('HtmlTag');
		$this->addElement($postId);
		
		$isLive = $this->createElement('hidden', 'is_live');
		$isLive->removeDecorator('Label');
		$isLive->removeDecorator('HtmlTag');
		$this->addElement($isLive);
		
		$submit = $this->createElement('submit', 'add', array('class' => 'btn btn-green big'));
		$submit->setLabel('Update');
		$this->addElement($submit);
	}
	
	public function fillImageTypes($optionsList)
	{
		$this->uploadedImagesListElement->fillImageTypes($optionsList);
	}
	
	public function populate($formData)
	{
		$this->uploadedImagesListElement->populate($formData);
		$this->uploadImageForm->populate($formData);

		return parent::populate($formData);
	}
	
	public function isValid($formData)
	{
		$file = $this->uploadImageForm->getElement('site_logo');
		$file->setRequired(false);
		$file->removeValidator('NotEmpty');
		
		return parent::isValid($formData);
	}
	
}