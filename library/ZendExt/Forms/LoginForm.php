<?php
class ZendExt_Forms_LoginForm extends ZendExt_Forms_Form
{
	public function init() 
	{
		$this->setAction('/index');
		$this->setMethod('post');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'index/login_form.phtml'))
        ));
		
		$email = $this->createElement('text', 'email', array('class' => 'half'));
		$email->setLabel('Email:');
		$email->setRequired(true); 
		$email->addValidator('NotEmpty'); 
		$email->addValidator('EmailAddress');  
		$this->addElement($email);
		
		$password = $this->createElement('password', 'password', array('class' => 'half'));
		$password->setLabel('Password:');
		$password->setRequired(true); 
		$password->addValidator('NotEmpty');
		$this->addElement($password);
		
		$submit = $this->createElement('submit', 'submit', array('class' => 'btn large primary'));
        $submit ->removeDecorator('HtmlTag');
        $submit ->removeDecorator('DtDdWrapper');
		$submit->setLabel('Login');
		$this->addElement($submit);
	}
	
	public function setWrongLoginError()
	{
		$password = $this->getElement('password');
		$password->setErrors(array('Wrong Email or Password'));
	}
}