<?php
class ZendExt_Forms_TestingForm extends ZendExt_Forms_Form
{
    private $campaignIdField;

	public function init()
	{
		$this->setAction('/campaigns/testing');
		$this->setMethod('post');
		     
        $ip = $this->createElement('text', 'ip');
		$ip->setLabel('IP:');
		$ip->setRequired(true);
		$ip->addValidator('NotEmpty');

        $regexValidator = new Zend_Validate_Regex(array('pattern' => '/^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$/'));
        $regexValidator->setMessage('Please insert correct IP');
        $ip->addValidator($regexValidator);
        $this->addElement($ip);

        $this->campaignIdField = $this->createElement('hidden', 'campaign_id');
		$this->addElement($this->campaignIdField);
		
		$submit = $this->createElement('submit', 'add', array('class' => 'btn btn-green big'));
		$submit->setLabel('Add');
		$this->addElement($submit);
	}

    public function setCampaignId($campaignId)
	{
		$this->campaignIdField->setValue($campaignId);
	}

}