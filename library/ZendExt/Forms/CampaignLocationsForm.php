<?php
class ZendExt_Forms_CampaignLocationsForm extends ZendExt_Forms_Form
{
	private $groupsSelect;

    private $checkTotalLocations = false;
	
	public function init() 
	{
		$this->setAction('/campaigns/locations');
		$this->setMethod('post');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'campaigns/locations_form.phtml'))
        ));
		
		$typeSelect = $this->createElement('select', 'type');
        $typeSelect->removeDecorator('HtmlTag');
		$typeSelect->setLabel('Type:');
		$typeSelect->addMultiOption('default', 'Default');
		$typeSelect->addMultiOption('regular_expression', 'Regular Expression');
		$typeSelect->addMultiOption('page_url', 'Page URL');
		$typeSelect->setRequired(true); 
		$typeSelect->addValidator('NotEmpty');
		$typeSelect->setAttrib('onload', 'changeType(); return false;');
		$typeSelect->setAttrib('onchange', 'changeType(); return false;');
		$this->addElement($typeSelect);
		
		$regularExpression = $this->createElement('text', 'regular_expression', array('class' => 'half'));
        $regularExpression->removeDecorator('HtmlTag');
		$regularExpression->setLabel('Regular Expression:');
		$regularExpression->setRequired(true); 
		$regularExpression->addValidator('NotEmpty');
		$regularExpression->setDescription('Enclose regular expression with /<regex>/ and escape / correctly with \ /');
		$this->addElement($regularExpression);
		
		$locationUrl = $this->createElement('text', 'page_url', array('class' => 'site_url'));
        $locationUrl->removeDecorator('Label');
        $locationUrl->removeDecorator('HtmlTag');
		$locationUrl->setLabel('Page url:');
		$locationUrl->setRequired(true); 
		$locationUrl->addValidator('NotEmpty');
		
		$urlValidator = new ZendExt_Validators_UrlValidator();
		$locationUrl->addValidator($urlValidator);
		$this->addElement($locationUrl);
		
		$matchText = $this->createElement('text', 'match_text', array('class' => 'half'));
        $matchText->removeDecorator('HtmlTag');
		$matchText->setLabel('Match text:');
		$matchText->setRequired(true); 
		$matchText->addValidator('NotEmpty');
		$this->addElement($matchText);
		
		/*$this->groupsSelect = $this->createElement('multiselect', 'group_id[]', array('class' => 'half list'));
		$this->groupsSelect->setRequired(true); 
		$this->groupsSelect->addValidator('NotEmpty');
		$this->groupsSelect->setLabel('Groups:');
		$this->addElement($this->groupsSelect);*/
		
		$campaignId = $this->createElement('hidden', 'campaign_id');
        $campaignId->removeDecorator('Label');
		$campaignId->removeDecorator('HtmlTag');
		$this->addElement($campaignId);
		
		$locationId = $this->createElement('hidden', 'location_id');
        $locationId->removeDecorator('Label');
		$locationId->removeDecorator('HtmlTag');
		$this->addElement($locationId);

		$submit = $this->createElement('submit', 'save', array('class' => 'btn btn-green big'));
		$submit->setLabel('Save');
		
		$this->addElement($submit);
	}
	
	public function setSiteId($siteId)
    {
        $siteIdField = $this->getElement('site_id');
		$siteIdField->setValue($siteId);
	}

    public function setCampaignId($campaignId)
    {
        $campaignIdField = $this->getElement('campaign_id');
		$campaignIdField->setValue($campaignId);
	}
	
	/*public function populateGroups($groupsList)
    {
		foreach ($groupsList as $group){
			$this->groupsSelect->addMultiOption($group->getSite_Token_Location_Group_Id(), $group->getSite_Token_Location_Group_Name());
		}
		
	}*/

    public function setCheckTotalLocations($value)
    {
        $this->checkTotalLocations = $value;
    }

	public function isValid($formData)
	{
        if ($this->checkTotalLocations == true) {

            $siteIdField = $this->getElement('site_id');

            $totalLocationsValidator = new ZendExt_Validators_TotalTokenLocationsValidator();
            $totalLocationsValidator->setSiteId($siteIdField->getValue());
            $totalLocationsValidator->setType($formData['type']);

            $mathTextElement = $this->getElement('match_text');
            $mathTextElement->addValidator($totalLocationsValidator);
        }

		if ($formData['type'] == 'default') {
			$regularExpression = $this->getElement('regular_expression');
			$regularExpression->removeValidator('NotEmpty');
			$regularExpression->setRequired(false);
			
			$pageUrl = $this->getElement('page_url');
			$pageUrl->removeValidator('NotEmpty');
			$pageUrl->setRequired(false);
		} elseif($formData['type'] == 'regular_expression') {
			$pageUrl = $this->getElement('page_url');
			$pageUrl->removeValidator('NotEmpty');
			$pageUrl->setRequired(false);
		} elseif($formData['type'] == 'page_url') {
			$regularExpression = $this->getElement('regular_expression');
			$regularExpression->removeValidator('NotEmpty');
			$regularExpression->setRequired(false);
		}
		
		return parent::isValid($formData);
	}
}