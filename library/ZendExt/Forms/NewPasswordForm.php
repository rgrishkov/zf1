<?php
class ZendExt_Forms_NewPasswordForm extends ZendExt_Forms_Form
{
	public function init()
	{
		$this->setAction('/index/newpassword');
		$this->setMethod('post');

        $lengthValidator = new Zend_Validate_StringLength(array('min' => 6));
        $lengthValidator->setMessage('Password is less than 6 characters long');

        $passwordValidatorChain = new Zend_Validate();
        $passwordValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator($lengthValidator);

        $password = $this->createElement('password', 'password', array('class' => 'password half'));
        $password->setLabel('Password:');
        $password->setRequired(true);
        $password->addValidator($passwordValidatorChain);
		$this->addElement($password);

        $confirmValidator = new Zend_Validate_Identical();
		$confirmValidator->setToken('password');
		$confirmValidator->setMessage('Please confirm password');

		$confirmPassword = $this->createElement('password', 'confirm_password', array('class' => 'half'));
        $confirmPassword->setLabel('Confirm password:');
		$confirmPassword->addValidator($confirmValidator);
        $confirmPassword->setRequired(true);
        $confirmPassword->addValidator('NotEmpty');
		$this->addElement($confirmPassword);

        $recoveryKey = $this->createElement('hidden', 'recovery_key');
        $recoveryKey->removeDecorator('Label');
		$recoveryKey->removeDecorator('HtmlTag');
		$this->addElement($recoveryKey);

		$submit = $this->createElement('submit', 'submit', array('class' => 'btn large primary'));
		$submit->setLabel('Submit');
		$this->addElement($submit);
	}

    public function setRecoveryKey($recoveryKey)
    {
        $recoveryKeyField = $this->getElement('recovery_key');
		$recoveryKeyField->setValue($recoveryKey);
	}

}