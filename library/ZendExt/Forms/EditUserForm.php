<?php
class ZendExt_Forms_EditUserForm extends ZendExt_Forms_Form
{
	private $emailUniqueValidator;
	
	public function init()
	{
		$this->setAction('/users/edit');
		$this->setMethod('post');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'users/user_form.phtml'))
        ));
		     
		$firstName = $this->createElement('text', 'firstname', array('class' => 'half'));
        $firstName->removeDecorator('HtmlTag');
		$firstName->setLabel('First name:');
		$firstName->setRequired(true); 
		$firstName->addValidator('NotEmpty'); 
		$this->addElement($firstName); 
		
		$surName = $this->createElement('text', 'surname', array('class' => 'half'));
        $surName->removeDecorator('HtmlTag');
		$surName->setLabel('Surname:'); 
		$surName->setRequired(true); 
		$surName->addValidator('NotEmpty');  
		$this->addElement($surName);

        $this->emailUniqueValidator = new ZendExt_Validators_EmailUniqueValidator();
        $emailValidatorChain = new Zend_Validate();
        $emailValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator(new Zend_Validate_EmailAddress())->addValidator($this->emailUniqueValidator);

		$email = $this->createElement('text', 'email', array('class' => 'half'));
        $email->removeDecorator('HtmlTag');
		$email->setLabel('Email:');
		$email->setRequired(true); 
		$email->addValidator($emailValidatorChain);
		$this->addElement($email);
		
		$phone = $this->createElement('text', 'phone');
        $phone->removeDecorator('HtmlTag');
		$phone->setLabel('Phone:');
		$this->addElement($phone);

        $country = $this->createElement('select', 'country', array('class' => 'half'));
        $country->removeDecorator('HtmlTag');
		$country->setRequired(true);
        $country->setLabel('Country:');
		$country->addValidator('NotEmpty');
		$this->addElement($country);

        $timezone = $this->createElement('select', 'timezone', array('class' => 'half'));
        $timezone->removeDecorator('HtmlTag');
		$timezone->setRequired(true);
        $timezone->setLabel('Time zone:');
		$timezone->addValidator('NotEmpty');
		$this->addElement($timezone);

		$password = $this->createElement('password', 'password', array('class' => 'password full'));
        $password->removeDecorator('HtmlTag');
		$password->setLabel('New Password:');
		$this->addElement($password);

        $confirmValidator = new Zend_Validate_Identical();
		$confirmValidator->setToken('password');
		$confirmValidator->setMessage('Please confirm password');

		$confirmPassword = $this->createElement('password', 'confirm_password', array('class' => 'full'));
        $confirmPassword->removeDecorator('HtmlTag');
		$confirmPassword->setLabel('Confirm New Password:');
		$confirmPassword->addValidator($confirmValidator);
		$this->addElement($confirmPassword);
		
		$userSitesList = new ZendExt_Elements_UserSitesList('sites_list');
		$this->addElement($userSitesList);
		
		$active = $this->createElement('checkbox', 'active');
		$active->removeDecorator('Label');
		$active->removeDecorator('HtmlTag');
		$active->addDecorator('Label');
		$active->setLabel('Active:');
		$this->addElement($active);
		
		$rolesMapper = new Application_Model_UserRoleMapper();
		$rolesList = $rolesMapper->getAllForSelect();
		
		$primaryRole = $this->createElement('select', 'primary_role');
		$primaryRole->addMultiOption(null, '--None--');
		$primaryRole->addMultiOptions($rolesList);
		$primaryRole->removeDecorator('HtmlTag');
		$primaryRole->removeDecorator('Label');
		$primaryRole->setRequired(true); 
		$primaryRole->addValidator('NotEmpty');
		$this->addElement($primaryRole);

        $paymentType = $this->createElement('select', 'payment_type');
		$paymentType->removeDecorator('HtmlTag');
		$paymentType->removeDecorator('Label');
		$paymentType->setRequired(true);
		$paymentType->addValidator('NotEmpty');
		$this->addElement($paymentType);
		
		$oldEmail = $this->createElement('hidden', 'old_email');
		$oldEmail->setRequired(true); 
		$oldEmail->addValidator('NotEmpty'); 
		$oldEmail->addValidator('EmailAddress');
        $oldEmail->removeDecorator('Label');
		$oldEmail->removeDecorator('HtmlTag');
		$this->addElement($oldEmail);
		
		$submit = $this->createElement('submit', 'save', array('class' => 'btn btn-green big'));
		$submit->setLabel('Save');
		$this->addElement($submit);
		
		//$testElemet = new Zend_Form_Decorator_HtmlTag("")
		//$this->removeDecorator('DtDdWrapper');
	}
	
	public function setOldEmail($email) 
	{
		$this->emailUniqueValidator->setOldEmail($email);
	}
	
	public function setSitesList($sitesList)
	{
        $userSitesListElement = $this->getElement('sites_list');
		$userSitesListElement->setSitesList($sitesList);
	}
	
	public function allowPasswordConfirmation() {

        $lengthValidator = new Zend_Validate_StringLength(array('min' => 6));
        $lengthValidator->setMessage('Password is less than 6 characters long');

        $passwordValidatorChain = new Zend_Validate();
        $passwordValidatorChain->addValidator(new Zend_Validate_NotEmpty())->addValidator($lengthValidator);

        $passwordElement = $this->getElement('password');
        $passwordElement->addValidator($passwordValidatorChain);

        $confirmPasswordElement = $this->getElement('confirm_password');

		$confirmPasswordElement->setRequired(true);
		$confirmPasswordElement->addValidator('NotEmpty');
	}

    public function fillCountries($optionsList)
	{
		$select = $this->getElement('country');

		foreach ($optionsList as $id => $title) {
			$select->addMultiOption($id, $title);
		}
	}

    public function fillPaymentTypes($optionsList)
	{
		$select = $this->getElement('payment_type');

		foreach ($optionsList as $id => $title) {
			$select->addMultiOption($id, $title);
		}
	}

    public function fillTimezones($optionsList)
	{
		$select = $this->getElement('timezone');

		foreach ($optionsList as $id => $title) {
			$select->addMultiOption($id, $title);
		}
	}
}