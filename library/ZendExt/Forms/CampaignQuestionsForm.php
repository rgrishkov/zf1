<?php
class ZendExt_Forms_CampaignQuestionsForm extends ZendExt_Forms_Form
{
	public function init()
	{
		$this->setAction('/campaigns/questions');
		$this->setMethod('post');

        $this->setDecorators(array(
            array('ViewScript', array('viewScript' => 'campaigns/questions_form.phtml'))
        ));

        $competitionId = $this->createElement('select', 'campaign_comp_id');
		$competitionId->removeDecorator('HtmlTag');
		$competitionId->removeDecorator('Label');
		$competitionId->setRequired(true);
		$this->addElement($competitionId);
		
		$questionType = $this->createElement('select', 'question_type');
		$questionType->addMultiOption('m_choise', 'Multiple Choice');
		$questionType->removeDecorator('HtmlTag');
		$questionType->removeDecorator('Label');
		$questionType->setRequired(true);
		$this->addElement($questionType);

		$question = $this->createElement('text', 'question', array('class' => 'full'));
		$question->setLabel('Question:');
		$question->setRequired(true); 
		$question->addValidator('NotEmpty');
		$this->addElement($question);
		
		$answer1 = $this->createElement('text', 'answer1', array('class' => 'full'));
		$answer1->removeDecorator('HtmlTag');
		$answer1->removeDecorator('Label');
		$answer1->addDecorator('Label');
		$answer1->setLabel('Answers');
		$answer1->setRequired(true);
		$answer1->addValidator('NotEmpty');
		$this->addElement($answer1);
		
		$tableDecorator = new ZendExt_Decorators_TableDecorator();
		$tableDecorator->endRow();
		
		$answer1Correct = $this->createElement('checkbox', 'answer1_correct');
		$answer1Correct->removeDecorator('HtmlTag');
        $answer1Correct->removeDecorator('Label');
		$this->addElement($answer1Correct);
		
		$answer2 = $this->createElement('text', 'answer2', array('class' => 'full'));
		$answer2->removeDecorator('HtmlTag');
		$answer2->removeDecorator('Label');
		$answer2->setRequired(true);
		$answer2->addValidator('NotEmpty');
		$this->addElement($answer2);
		
		$answer2Correct = $this->createElement('checkbox', 'answer2_correct');
		$answer2Correct->removeDecorator('HtmlTag');
		$answer2Correct->removeDecorator('Label');
		$this->addElement($answer2Correct);
		
		$answer3 = $this->createElement('text', 'answer3', array('class' => 'full'));
		$answer3->removeDecorator('HtmlTag');
		$answer3->removeDecorator('Label');
		$this->addElement($answer3);
		
		$answer3Correct = $this->createElement('checkbox', 'answer3_correct');
		$answer3Correct->removeDecorator('HtmlTag');
		$answer3Correct->removeDecorator('Label');
		$this->addElement($answer3Correct);
		
		$answer4 = $this->createElement('text', 'answer4', array('class' => 'full'));
		$answer4->removeDecorator('HtmlTag');
		$answer4->removeDecorator('Label');
		$this->addElement($answer4);
		
		$answer4Correct = $this->createElement('checkbox', 'answer4_correct');
		$answer4Correct->removeDecorator('HtmlTag');
		$answer4Correct->removeDecorator('Label');
		$this->addElement($answer4Correct);

        $correctAnswersValidator = new ZendExt_Validators_CorrectAnswersValidator();
		$correctAnswersValidator->addElement(array('answer' => $answer1, 'correct' => $answer1Correct));
		$correctAnswersValidator->addElement(array('answer' => $answer2, 'correct' => $answer2Correct));
		$correctAnswersValidator->addElement(array('answer' => $answer3, 'correct' => $answer3Correct));
		$correctAnswersValidator->addElement(array('answer' => $answer4, 'correct' => $answer4Correct));

        $emptyField = $this->createElement('hidden', 'empty_field');
        $emptyField->removeDecorator('Label');
		$emptyField->removeDecorator('HtmlTag');
        $emptyField->setValue(1);
        $emptyField->addValidator($correctAnswersValidator);
		$this->addElement($emptyField);
		
		$questionHint = $this->createElement('text', 'question_hint', array('class' => 'full'));
		$questionHint->setLabel('Question hint:');
		$questionHint->setRequired(true);
		$questionHint->addValidator('NotEmpty');
		$this->addElement($questionHint);

		$questionId = $this->createElement('hidden', 'question_id');
        $questionId->removeDecorator('Label');
		$questionId->removeDecorator('HtmlTag');
		$this->addElement($questionId);

        $campaignId = $this->createElement('hidden', 'campaign_id');
        $campaignId->removeDecorator('Label');
		$campaignId->removeDecorator('HtmlTag');
		$this->addElement($campaignId);

        /*$competitionId = $this->createElement('hidden', 'competition_id');
        $competitionId->removeDecorator('Label');
		$competitionId->removeDecorator('HtmlTag');
		$this->addElement($competitionId);*/

		$submit = $this->createElement('submit', 'save', array('class' => 'btn btn-green big'));
		$submit->setLabel('Save');
		$this->addElement($submit);
	}


	
	public function setCampaignId($campaignId)
    {
        $campaignIdField = $this->getElement('campaign_id');
		$campaignIdField->setValue($campaignId);
	}

    /*public function setCompetitionId($competitionId)
    {
        $campaignIdField = $this->getElement('campaign_comp_id');
		$campaignIdField->setValue($competitionId);
	}*/

    public function populateCompetitionsList($competitionsList)
	{
        $competitionIdField = $this->getElement('campaign_comp_id');
		foreach ($competitionsList as $competition) {
			$competitionIdField->addMultiOption($competition->getCampaign_Comp_Id(), $competition->getCampaign_Comp_Name());
		}
	}
}