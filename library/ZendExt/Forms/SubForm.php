<?php
class ZendExt_Forms_SubForm extends Zend_Form_SubForm
{
	public function setAction($action)
	{
	    $url = rtrim(Zend_Controller_Front::getInstance()->getBaseUrl(), '/') .$action;
	    parent::setAction($url);
	}
}