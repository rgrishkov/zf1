<?php
class ZendExt_Forms_PasswordRecoveryForm extends ZendExt_Forms_Form
{
	public function init()
	{
		$this->setAction('/index/recovery');
		$this->setMethod('post');

        $recoveryKeyValidator = new ZendExt_Validators_PasswordRecoveryKeyValidator();

		$recoveryKey = $this->createElement('text', 'recovery_key', array('class' => 'half'));
		$recoveryKey->setLabel('Recovery key:');
		$recoveryKey->setRequired(true);
		$recoveryKey->addValidator('NotEmpty');
        $recoveryKey->addValidator($recoveryKeyValidator);
		$this->addElement($recoveryKey);

		$submit = $this->createElement('submit', 'submit', array('class' => 'btn large primary'));
		$submit->setLabel('Submit');
		$this->addElement($submit);
	}

}