<?php
class ZendExt_Forms_AddCampaignForm extends ZendExt_Forms_Form
{
	public function init()
	{
		$this->setAction('/campaigns/add');
		$this->setMethod('post');
		     
		$lineTitle = new ZendExt_Elements_LineTitle('line_title1');
		$lineTitle->setTitle('Campaign');
		$this->addElement($lineTitle);

		$externalId = $this->createElement('text', 'external_campaign_id', array('class' => 'half readonly'));
		$externalId->setAttrib('readonly', 'readonly');
		$externalId->setLabel('Campaign Id:');
		$externalId->setRequired(true);
		$externalId->addValidator('NotEmpty');
		$this->addElement($externalId);

		$sitesList = $this->createElement('select', 'site_id');
		$sitesList->setLabel('Site:');
		$sitesList->setRequired(true);
		$sitesList->addValidator('NotEmpty');
		$this->addElement($sitesList);

		$campaignName = $this->createElement('text', 'campaign_name', array('class' => 'half'));
		$campaignName->setLabel('Campaign Name:');
		$campaignName->setRequired(true);
		$campaignName->addValidator('NotEmpty');
		$this->addElement($campaignName);

		$locationsTre = new ZendExt_Elements_LocationsTree('location_cities_list');
		$locationsTre->removeDecorator('HtmlTag');
		$locationsTre->removeDecorator('Label');
		$this->addElement($locationsTre);
		
		$submit = $this->createElement('submit', 'add', array('class' => 'btn btn-green big'));
		$submit->setLabel('Add');
		$this->addElement($submit);
	}
	
	public function setExternalId($externalId) 
	{
        $externalIdField = $this->getElement('external_campaign_id');
		$externalIdField->setValue($externalId);
	}
	
	public function fillSitesList($sitesList)
	{
        $sitesListElement = $this->getElement('site_id');

		foreach ($sitesList as $site) {
			$sitesListElement->addMultiOption($site->getSite_Id(), $site->getSite_Url());
		}	
	}
	
	public function fillLocationsTree($locationsTree)
	{
        $locationsTreeElement = $this->getElement('location_cities_list');
		$locationsTreeElement->fillLocaionsTree($locationsTree);
	}
	
	public function populate($formData)
	{
		$locationsTreeElement = $this->getElement('location_cities_list');
        $locationsTreeElement->populate($formData);
		return parent::populate($formData);
	}
}