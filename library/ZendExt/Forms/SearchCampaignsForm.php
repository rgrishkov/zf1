<?php
class ZendExt_Forms_SearchCampaignsForm extends ZendExt_Forms_Form
{
	public function init() 
	{
		$this->setAction('/campaigns');
		$this->setMethod('post');
		     
		$siteUrl = $this->createElement('text', 'site_url', array('class' => 'half'));
		$siteUrl->setLabel('Site:');
		$siteUrl->setDescription('e.g. www.example.com');
		$this->addElement($siteUrl); 
		
		$submit = $this->createElement('submit', 'add', array('class' => 'btn btn-green big'));
		$submit->setLabel('Search');
		$this->addElement($submit);
	}
}