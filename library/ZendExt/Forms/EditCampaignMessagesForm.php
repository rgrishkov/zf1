<?php
class ZendExt_Forms_EditCampaignMessagesForm extends ZendExt_Forms_Form
{
	private $defaultMessagesArray = array();
	
	public function init() 
	{
		$this->setAction('/campaigns/messages');
		$this->setMethod('post');
		
		$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/messages.ini');
		
		$subform = new ZendExt_Forms_SubForm();
		$subform->removeDecorator('HtmlTag');
		$subform->removeDecorator('DtDdWrapper');
		
		foreach ($config->message_type as $messageType => $messageTitle) {
			$message = $this->createElement('text', $messageType, array('class' => 'full'));
			$message->setLabel($messageTitle.':');
			$subform->addElement($message);   
		} 
		
		$this->addSubForm($subform, 'messages');
		
		$campaignId = $this->createElement('hidden', 'campaign_id');
		$campaignId->removeDecorator('Label');
		$campaignId->removeDecorator('HtmlTag');
		$this->addElement($campaignId);
		 
		$submit = $this->createElement('submit', 'add', array('class' => 'btn btn-green big'));
		$submit->setLabel('Update');
		$this->addElement($submit);
	}
	
	public function setDefaultMessages($defaultMessagesList) 
	{
		$defaultMessagesArray = array();
		foreach ($defaultMessagesList as $message) {
        	$defaultMessagesArray[$message->getMessage_Type()] = $message->getMessage_Value();
        }
		
        $subForm = $this->getSubForm('messages');
		$formElements = $subForm->getElements();
		
		foreach ($formElements as $element) {
			if (array_key_exists($element->getId(), $defaultMessagesArray)) {
				$element->setDescription('Default: '.$defaultMessagesArray[$element->getId()]);
			}
		}
	}

}