<?php
class ZendExt_Forms_SearchCampaignResultsForm extends ZendExt_Forms_Form
{
	public function init() 
	{
		$this->setAction('/results');
		$this->setMethod('post');
		     
		$siteUrl = $this->createElement('text', 'site_url', array('class' => 'half'));
		$siteUrl->setLabel('Site:');
		$siteUrl->setDescription('e.g. www.example.com');
		$this->addElement($siteUrl); 
		
		$externalId = $this->createElement('text', 'external_campaign_id', array('class' => 'half'));
		$externalId->setLabel('Campaign Id:');
		$this->addElement($externalId);
		
		
		$submit = $this->createElement('submit', 'add', array('class' => 'btn btn-green big'));
		$submit->setLabel('Search');
		$this->addElement($submit);
	}

    public function fillCampaignStatus($optionsList)
	{
		$select = $this->getElement('campaign_status');

		foreach ($optionsList as $id => $title) {
			$select->addMultiOption($id, $title);
		}
	}
}