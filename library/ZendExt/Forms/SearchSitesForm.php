<?php
class ZendExt_Forms_SearchSitesForm extends ZendExt_Forms_Form
{
	public function init() 
	{
		$this->setAction('/sites');
		$this->setMethod('post');
		     
		$siteId = $this->createElement('text', 'external_site_id', array('class' => 'half'));
		$siteId->setLabel('Site Id:');
		$this->addElement($siteId);
		
		$siteUrl = $this->createElement('text', 'site_url', array('class' => 'half'));
		$siteUrl->setLabel('Site Url:');
		$siteUrl->setDescription('e.g. www.example.com');
		$this->addElement($siteUrl);
		
		
		$submit = $this->createElement('submit', 'add', array('class' => 'btn btn-green big'));
		$submit->setLabel('Search');
		$this->addElement($submit);
		
		//$testElemet = new Zend_Form_Decorator_HtmlTag("")
		//$this->removeDecorator('DtDdWrapper');
	}
}