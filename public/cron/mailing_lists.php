<?php

// Define path to application directory
defined('APPLICATION_PATH')
|| define(
    'APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application')
);

// Define application environment
defined('APPLICATION_ENV')
|| define(
    'APPLICATION_ENV',
    (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production')
);

// Define path to upload directory
defined('UPLOAD_PATH')
|| define('UPLOAD_PATH', realpath(dirname(__FILE__) . '/upload'));

defined('EMAIL_TEMPLATES_PATH')
|| define('EMAIL_TEMPLATES_PATH', realpath(dirname(__FILE__) . '/emails'));

defined('THUMBNAILS_PATH')
|| define(
    'THUMBNAILS_PATH', realpath(dirname(__FILE__) . '/upload/thumbnails')
);

// Ensure library/ is on include_path
set_include_path(
    implode(
        PATH_SEPARATOR, array(
            realpath(APPLICATION_PATH . '/../library'),
            get_include_path(),
        )
    )
);

/** Zend_Application */
require_once 'Zend/Application.php';
require_once realpath(dirname(__FILE__) . '/../../library/Ext/class-IXR.php');
require_once realpath(
    dirname(__FILE__) . '/../../application/constants/constants.php'
);
// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap();

$entriesMapper = new Application_Model_CampaignCompEntryMapper();
$siteMailListIntegrationMapper
    = new Application_Model_SiteMailListIntegrationMapper();
$competitionMapper = new Application_Model_CampaignTokenCompMapper();
$campaignMapper = new Application_Model_CampaignMapper();

$mailProvidersManager = new ZendExt_MailListsProviders_MailProvidersManager();

$entriesList = $entriesMapper->getListForMailing();

//Zend_Debug::dump($entriesList);
$competition = new Application_Model_CampaignTokenComp();
$campaign = new Application_Model_Campaign();
$siteMailListIntegrationData = null;

foreach ($entriesList as $entry) {

    if ($entry->getCampaign_Comp_Id() != $competition->getCampaign_Comp_Id()) {

        $competitionMapper->loadCampaignTokenCompById(
            $entry->getCampaign_Comp_Id(), $competition
        );
        $campaignMapper->loadCampaignById(
            $competition->getCampaign_Id(), $campaign
        );

        $mailListProvider = $mailProvidersManager->getProviderById(
            $competition->getMail_Provider_Id()
        );
        $siteMailListIntegrationData = $mailListProvider->getCredentials(
            $campaign->getSite_Id()
        );
    }

    if ($siteMailListIntegrationData != null) {

        $mailProviderData = array(
            'list_id'  => $competition->getMail_List_Id(),
            'api_key'  => $siteMailListIntegrationData->getApi_Key(),
            'username' => $siteMailListIntegrationData->getUsername(),
            'password' => $siteMailListIntegrationData->getPassword(),
        );

        //$mailListProvider->sendToList($mailProviderData);
        $mailListProvider->addToList(
            $entry->getEntrant_Email(), $entry->getEntrant_Firstname(),
            $entry->getEntrant_Lastname(), $mailProviderData
        );

        $entry->setSent_To_Mprovider(1);
        $entry->setSent_To_Mprovider_Date($competitionMapper->getCurrentDate());

        $entriesMapper->save($entry);
    }
}
?>