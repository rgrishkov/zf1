<?php

// Define path to application directory
defined('APPLICATION_PATH')
|| define(
    'APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application')
);

// Define application environment
defined('APPLICATION_ENV')
|| define(
    'APPLICATION_ENV',
    (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production')
);

// Define path to upload directory
defined('UPLOAD_PATH')
|| define('UPLOAD_PATH', realpath(dirname(__FILE__) . '/upload'));

defined('EMAIL_TEMPLATES_PATH')
|| define('EMAIL_TEMPLATES_PATH', realpath(dirname(__FILE__) . '/emails'));

defined('THUMBNAILS_PATH')
|| define(
    'THUMBNAILS_PATH', realpath(dirname(__FILE__) . '/upload/thumbnails')
);

// Ensure library/ is on include_path
set_include_path(
    implode(
        PATH_SEPARATOR, array(
            realpath(APPLICATION_PATH . '/../library'),
            get_include_path(),
        )
    )
);

/** Zend_Application */
require_once 'Zend/Application.php';
require_once realpath(dirname(__FILE__) . '/../../library/Ext/class-IXR.php');
require_once realpath(
    dirname(__FILE__) . '/../../application/constants/constants.php'
);
// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$application->bootstrap();


$competitionMapper = new Application_Model_CampaignTokenCompMapper();
$competitionsListForStart = $competitionMapper->getListForStart();

//Zend_Debug::dump($competitionsListForStart);

foreach ($competitionsListForStart as $competition) {
    $competition->setCampaign_Comp_Status(CAMPAIGN_STATUS_RUNNING);
    $competitionMapper->save($competition);
}

$competitionsListForStop = $competitionMapper->getListForStop();
//Zend_Debug::dump($competitionsListForStop);

foreach ($competitionsListForStop as $competition) {
    $competition->setCampaign_Comp_Status(CAMPAIGN_STATUS_COMPLETED);
    $competitionMapper->save($competition);
}

?>