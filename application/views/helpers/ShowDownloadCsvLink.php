<?php

class Zend_View_Helper_ShowDownloadCsvLink extends Zend_View_Helper_Abstract
{

    public function showDownloadCsvLink($campaignId, $competitionId)
    {
        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $campaignTokenCompMapper
            = new Application_Model_CampaignTokenCompMapper();
        $competition = new Application_Model_CampaignTokenComp();
        $campaignTokenCompMapper->loadCampaignTokenCompById(
            $competitionId, $competition
        );

        //$competitionName = strtolower(str_replace(' ', '_', $competition->getCampaign_Comp_Name()));
        $campaignNameFormatted = strtolower(
            str_replace(' ', '_', $campaign->getCampaign_Name())
        );

        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/application.ini', 'development'
        );

        $filename = $config->upload_path->csv . '/'
            . $campaign->getExternal_Campaign_Id() . '/wingrin _results_'
            . $campaign->getExternal_Campaign_Id() . '_'
            . $campaignNameFormatted . '.csv';

        $entriesMapper = new Application_Model_CampaignCompEntryMapper();
        $lastEntry = $entriesMapper->getLastEntryForCompetition($competitionId);

        if ($lastEntry != null && file_exists($filename) == true) {
            $lastEntryDate = new Zend_Date(
                $lastEntry->getCampaign_Comp_Entry_Date()
            );
            $lastReportDate = new Zend_Date($competition->getLastReportDate());

            if ($lastEntryDate->isLater($lastReportDate) == true) {
                $result
                    = '<input type="button" class="btn btn-red big" value="Update CSV" onclick="generateCSV('
                    . $campaignId . ',' . $competitionId
                    . '); return false;"/>';
            } else {
                $url = $this->view->url(
                    array('controller' => 'campaigns',
                          'action' => 'downloadcsv', 'id' => $campaignId,
                          'campaign_comp_id' => $competitionId)
                );
                $result = '<a href="' . $url . '">Download</a>';
            }
        } else {
            $result
                = '<input type="button" class="btn btn-blue big" value="Generate CSV" onclick="generateCSV('
                . $campaignId . ',' . $competitionId . '); return false;"/>';
        }

        return $result;
    }

}

?>