<?php

class Zend_View_Helper_ShowCampaignStatus extends Zend_View_Helper_Abstract
{

    public function showCampaignStatus($campaignStatus)
    {
        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/campaign_statuses.ini', $campaignStatus
        );
        return $config->title;
    }

}

?>