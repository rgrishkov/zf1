<?php

class Zend_View_Helper_PrepareUrl extends Zend_View_Helper_Abstract
{

    public function prepareUrl($url)
    {
        if (substr($url, 0, 4) == 'http') {
            $resultUrl = $url;
        } else {
            $resultUrl = 'http://' . $url;
        }
        return $resultUrl;
    }

}

?>