<?php
define('USER_ROLE_USER', 1);
define('USER_ROLE_ADMIN', 2);

define('ORDER_STATUS_PENDING', 'pending');
define('ORDER_STATUS_PAYED', 'payed');


define('CAMPAIGN_STATUS_NEW', 'new');
define('CAMPAIGN_STATUS_INPROGRESS', 'inprogress');
define('CAMPAIGN_STATUS_VERIFICATION', 'verification');
define('CAMPAIGN_STATUS_CANCELLED', 'cancelled');
define('CAMPAIGN_STATUS_SCHEDULED', 'scheduled');
define('CAMPAIGN_STATUS_PUBLISHED', 'published');
define('CAMPAIGN_STATUS_RUNNING', 'running');
define('CAMPAIGN_STATUS_WINNER_PENDING', 'winner_pending');
define('CAMPAIGN_STATUS_COMPLETED', 'completed');
define('CAMPAIGN_STATUS_PAUSED', 'paused');

define('COMPETITION_TYPE_BASIC_COMPETITION', 'basic_competition');
define('COMPETITION_TYPE_BASIC_DEAL', 'basic_deal');
define('COMPETITION_TYPE_BASIC_SIGNUP_FORM', 'basic_signup_form');
define('COMPETITION_TYPE_BASIC_ANNOUNCEMENT', 'basic_announcement');

define('MEDIA_OWNER_TYPE_SITE', 'site');
define('MEDIA_OWNER_TYPE_CAMPAIGN', 'campaign');

define('MEDIA_TYPE_LOGO', 'logo');
define('MEDIA_TYPE_OTHER', 'other');
define('MEDIA_TYPE_DEFAULT_SIDEBAR_IMAGE', 'default_sidebar_image');
define('MEDIA_TYPE_PRIMARY_SIDEBAR_IMAGE', 'sidebar_primary_image');
define('MEDIA_TYPE_DEFAULT_TAB_IMAGE', 'default_tab_image');
define('MEDIA_TYPE_PRIMARY_TAB_IMAGE', 'tab_primary_image');

define('LOCATION_TYPE_DEFAULT', 'default');

define('LOCATION_TYPE_REGEXP', 'regular_expression');
define('LOCATION_TYPE_URL', 'page_url');

define('FOLLOW_UP_OFFER_HELP_TEXT', 'Some help text');

define('PAYMENT_TYPE_REQUIRED', 'payment_required');
define('PAYMENT_TYPE_BYPASS', 'payment_bypass');

define('CONDITION_TYPE_SAMPLE', 'sample');
define('CONDITION_TYPE_USER', 'user');

define(
    'EDIT_COMPETITION_CONFIRM',
    'This competition  is running. Any changes made will affect the live competition. Do you want to save ?'
);

