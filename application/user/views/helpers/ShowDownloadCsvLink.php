<?php

class Zend_View_Helper_ShowDownloadCsvLink extends Zend_View_Helper_Abstract
{

    public function showDownloadCsvLink($campaignId, $competitionId)
    {
        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $campaignTokenCompMapper
            = new Application_Model_CampaignTokenCompMapper();
        $competition = new Application_Model_CampaignTokenComp();
        $campaignTokenCompMapper->loadCampaignTokenCompById(
            $competitionId, $competition
        );

        //$competitionName = strtolower(str_replace(' ', '_', $competition->getCampaign_Comp_Name()));
        $campaignNameFormatted = strtolower(
            str_replace(' ', '_', $campaign->getCampaign_Name())
        );

        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/application.ini', 'development'
        );

        $filename = $config->upload_path->csv . '/'
            . $campaign->getExternal_Campaign_Id() . '/wingrin_results_'
            . $campaign->getExternal_Campaign_Id() . '_'
            . $campaignNameFormatted . '.csv';

        $entriesMapper = new Application_Model_CampaignCompEntryMapper();
        $lastEntry = $entriesMapper->getLastEntryForCompetition($competitionId);

        if ($lastEntry != null && file_exists($filename) == true) {
            $lastEntryDate = new Zend_Date(
                $lastEntry->getCampaign_Comp_Entry_Date()
            );
            $lastReportDate = new Zend_Date($competition->getLastReportDate());

            $url = $this->view->url(
                array('module' => null, 'controller' => 'campaigns',
                      'action' => 'downloadcsv', 'id' => $campaignId,
                      'campaign_comp_id' => $competitionId)
            );

            if ($lastEntryDate->isLater($lastReportDate) == true) {
                $result = '<a href="#" onclick="generateCSV(' . $campaignId
                    . ',' . $competitionId
                    . '); return false;">Generate CSV</a>&nbsp;/&nbsp;';
                $result .= '<a href="' . $url . '">Download</a>';
            } else {
                $result = '<a href="' . $url . '">Download</a>';
            }
        } else {
            $result = '<a href="#" onclick="generateCSV(' . $campaignId . ','
                . $competitionId . '); return false;">Generate CSV</a>';
        }

        return $result;
    }

}

?>