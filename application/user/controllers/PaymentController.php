<?php

class User_PaymentController extends ZendExt_Controllers_WinGrinUserController
{

    protected function getPaymentConfig()
    {
        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/payment.ini', 'paypal'
        );

        if ($config->use_sandbox == 1) {
            $paymentConfig = new Zend_Config_Ini(
                APPLICATION_PATH . '/configs/payment.ini', 'sandbox'
            );
        } else {
            $paymentConfig = new Zend_Config_Ini(
                APPLICATION_PATH . '/configs/payment.ini', 'real'
            );
        }
        return $paymentConfig;
    }

    public function requestAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competitionId = $this->_getParam('competition_id', '');

        $competition = new Application_Model_CampaignTokenComp();
        $competitionMapper = new Application_Model_CampaignTokenCompMapper();
        $competitionMapper->loadCampaignTokenCompById(
            $competitionId, $competition
        );

        $campaignId = $competition->getCampaign_Id();
        $campaignOrder = new Application_Model_CampaignOrder();
        $campaignOrderMapper = new Application_Model_CampaignOrderMapper();

        if ($campaignOrderMapper->loadOrderByCampaignId(
                $campaignId, $campaignOrder
            ) == true
        ) {
            $totalOrderPrice = $campaignOrderMapper->getTotalOrderPrice(
                $campaignOrder->getOrder_Id()
            );
        } else {
            throw new Exception('Invalid competition_id');
        }

        $config = $this->getPaymentConfig();

        $baseUrl = ZendExt_Utils_ConfigUtils::getBaseUrl();
        $confirmUrl = $baseUrl . $this->view->url(
                array('module'         => 'user', 'controller' => 'payment',
                      'action'         => 'confirm',
                      'competition_id' => $competitionId), null, true
            );
        $returnUrl = $baseUrl . $this->view->url(
                array('module'         => 'user',
                      'controller'     => 'competitions', 'action' => 'summary',
                      'competition_id' => $competitionId), null, true
            );

        $requestData = array(
            'USER'                           => $config->user,
            'PWD'                            => $config->password,
            'SIGNATURE'                      => $config->signature,
            'VERSION'                        => $config->version,
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'Authorization',
            'PAYMENTREQUEST_0_AMT'           => $totalOrderPrice,
            'PAYMENTREQUEST_0_CURRENCYCODE'  => 'USD',
            'RETURNURL'                      => $confirmUrl,
            'CANCELURL'                      => $returnUrl,
            'METHOD'                         => 'SetExpressCheckout'

        );

        $client = new Zend_Http_Client($config->webservice_url);
        $client->setMethod(Zend_Http_Client::POST);
        $client->setParameterPost($requestData);
        $response = $client->request();

        $responseArray = ZendExt_Utils_StringUtils::parseResponseString(
            $response->getBody()
        );

        if ($responseArray['ACK'] != 'Success') {
            throw new Exception($responseArray['L_LONGMESSAGE0']);
        }

        $redirectUrl = $config->redirect_url . '?cmd=_express-checkout&token='
            . $responseArray['TOKEN'];
        $this->_helper->redirector->gotoUrl($redirectUrl);
    }

    public function confirmAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }
        $competitionId = $this->_getParam('competition_id', '');

        if ($this->_getParam('token', null) == null) {
            throw new Exception('Invalid token');
        }
        $tokenId = $this->_getParam('token', '');
        //$payerId = $this->_getParam('PayerID', null);


        $competition = new Application_Model_CampaignTokenComp();
        $competitionMapper = new Application_Model_CampaignTokenCompMapper();
        $competitionMapper->loadCampaignTokenCompById(
            $competitionId, $competition
        );
        $campaignId = $competition->getCampaign_Id();

        $campaignOrder = new Application_Model_CampaignOrder();
        $campaignOrderMapper = new Application_Model_CampaignOrderMapper();

        if ($campaignOrderMapper->loadOrderByCampaignId(
                $campaignId, $campaignOrder
            ) == true
        ) {
            $totalOrderPrice = $campaignOrderMapper->getTotalOrderPrice(
                $campaignOrder->getOrder_Id()
            );
        } else {
            throw new Exception('Invalid competition_id');
        }

        $config = $this->getPaymentConfig();

        $requestData = array(
            'USER'      => $config->user,
            'PWD'       => $config->password,
            'SIGNATURE' => $config->signature,
            'VERSION'   => $config->version,
            'TOKEN'     => $tokenId,
            'METHOD'    => 'GetExpressCheckoutDetails'

        );

        $client = new Zend_Http_Client($config->webservice_url);
        $client->setMethod(Zend_Http_Client::POST);
        $client->setParameterPost($requestData);
        $response = $client->request();

        $responseArray = ZendExt_Utils_StringUtils::parseResponseString(
            $response->getBody()
        );

        if ($responseArray['ACK'] != 'Success') {
            throw new Exception($responseArray['L_LONGMESSAGE0']);
        }

        $pairId = $responseArray['PAYERID'];
        $amount = $responseArray['AMT'];

        if ($totalOrderPrice != $amount) {
            throw new Exception('Invalid amount');
        }

        $requestData = array(
            'USER'                           => $config->user,
            'PWD'                            => $config->password,
            'SIGNATURE'                      => $config->signature,
            'VERSION'                        => $config->version,
            'PAYMENTREQUEST_0_PAYMENTACTION' => 'Authorization',
            'PAYMENTREQUEST_0_AMT'           => $amount,
            'PAYMENTREQUEST_0_CURRENCYCODE'  => 'USD',
            'TOKEN'                          => $tokenId,
            'PAYERID'                        => $pairId,
            'METHOD'                         => 'DoExpressCheckoutPayment'

        );

        $client = new Zend_Http_Client($config->webservice_url);
        $client->setMethod(Zend_Http_Client::POST);
        $client->setParameterPost($requestData);
        $response = $client->request();

        $responseArray = ZendExt_Utils_StringUtils::parseResponseString(
            $response->getBody()
        );

        if ($responseArray['ACK'] != 'Success') {
            throw new Exception($responseArray['L_LONGMESSAGE0']);
        }

        $campaignOrder->setStatus(ORDER_STATUS_PAYED);

        $payedDate = new Zend_Date();
        $campaignOrder->setPayed_Date(
            $payedDate->toString(
                ZendExt_UserForms_EditCompetitionForm::DATABASE_DATE_FORMAT
            )
        );
        $campaignOrderMapper->save($campaignOrder);

        //$this->sendSuccessEmail();
        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);
        ZendExt_Facades_CompetitionsFacade::changeCampaignStatus(
            $competition, CAMPAIGN_STATUS_SCHEDULED
        );

        $this->_helper->redirector(
            'paymentsuccess', 'competitions', 'user',
            array('competition_id' => $competitionId)
        );
    }

    public function bypassAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $authentication = Zend_Auth::getInstance();
        $userEmail = $authentication->getIdentity();

        $usersMapper = new Application_Model_UsersMapper();
        $user = new Application_Model_Users();
        $usersMapper->loadUserByEmail($userEmail, $user);

        /* if($user->getPayment_Type() != PAYMENT_TYPE_BYPASS) {
             throw new Exception('Invalid payment type');
         }*/

        $competitionId = $this->_getParam('competition_id', '');

        $competition = new Application_Model_CampaignTokenComp();
        $competitionMapper = new Application_Model_CampaignTokenCompMapper();
        $competitionMapper->loadCampaignTokenCompById(
            $competitionId, $competition
        );

        $campaignId = $competition->getCampaign_Id();
        $campaignOrder = new Application_Model_CampaignOrder();
        $campaignOrderMapper = new Application_Model_CampaignOrderMapper();

        $isLoaded = $campaignOrderMapper->loadOrderByCampaignId(
            $campaignId, $campaignOrder
        );

        if ($isLoaded == false) {
            throw new Exception('Invalid competition_id');
        }

        $campaignOrder->setStatus(ORDER_STATUS_PAYED);

        $payedDate = new Zend_Date();
        $campaignOrder->setPayed_Date(
            $payedDate->toString(
                ZendExt_UserForms_EditCompetitionForm::DATABASE_DATE_FORMAT
            )
        );
        $campaignOrderMapper->save($campaignOrder);

        //$this->sendSuccessEmail();
        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);
        ZendExt_Facades_CompetitionsFacade::changeCampaignStatus(
            $competition, CAMPAIGN_STATUS_SCHEDULED
        );

        $this->_helper->redirector(
            'paymentsuccess', 'competitions', 'user',
            array('competition_id' => $competitionId)
        );
        //$this->_helper->redirector('competition', 'dashboard', 'user', array('competition_id' => $competitionId));
    }

    /*protected function sendSuccessEmail()
    {
        $authentication = Zend_Auth::getInstance();
        $userEmail = $authentication->getIdentity();

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'emails');

        $mail = new Zend_Mail();
        $mail->setBodyText('Some text');
        $mail->setFrom($config->department->billing);
        $mail->addTo($userEmail);
        $mail->setSubject('Payment');

        ZendExt_Facades_MessagesFacade::sendEmail($mail);
    }*/
}







