<?php

class User_HelpController extends ZendExt_Controllers_WinGrinUserController
{

    public function contactAction()
    {
        $form = new ZendExt_Forms_ContactForm();
        $form->setAction('/user/help/contact');
        $this->view->form = $form;

        $authentication = Zend_Auth::getInstance();
        $userEmail = $authentication->getIdentity();

        $usersMapper = new Application_Model_UsersMapper();
        $user = new Application_Model_Users();

        $usersMapper->loadUserByEmail($userEmail, $user);

        $this->view->form->setEmail($userEmail);
        $this->view->form->setName(
            $user->getFirstname() . ' ' . $user->getLastname()
        );

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $mail = new Zend_Mail();
                $mail->setBodyText($formData['message']);
                $mail->setFrom($formData['email']);
                $mail->addTo($formData['department']);
                $mail->setSubject($formData['subject']);

                ZendExt_Facades_MessagesFacade::sendEmail($mail);

                $this->_helper->redirector('contact', 'help', 'user');
            }

            $form->populate($formData);
        }
    }

    public function documentationAction()
    {
        if ($this->_getParam('asset_id', null) != null) {
            $assetId = $this->_getParam('asset_id', '');
        } else {
            $config = new Zend_Config_Ini(
                APPLICATION_PATH . '/configs/application.ini', 'help'
            );
            $assetId = $config->documentation->default_user_asset_id;
        }

        $globalContentMapper = new Application_Model_GlobalContentMapper();

        $documentation = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById($assetId, $documentation);
        $this->view->assign('documentation', $documentation);

        $helpMenu = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById(
            'HELP_SECTIONS_MENU_USER', $helpMenu
        );

        $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
        $helpMenuHtml = str_replace(
            '{BASE_URL}', $baseUrl, $helpMenu->getBody()
        );
        $this->view->assign('helpMenu', $helpMenuHtml);
    }

    public function conditionsAction()
    {

    }
}







