<?php

class User_CustomiseController extends ZendExt_Controllers_WinGrinUserController
{

    public function indexAction()
    {
        $form = new ZendExt_Forms_SearchSitesForm();
        $this->view->form = $form;

        $searchParams = null;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $searchParams = $formData;
            }
            $form->populate($formData);
        }


        $paginateParamsForm = new ZendExt_Forms_PaginateParamsForm();
        $paginateParamsForm->setAction(
            $this->view->url(array('controller' => 'sites'))
        );

        $itemsPerPage = (int)$this->_getParam('per_page', 10);
        $this->view->paginateParamsForm = $paginateParamsForm;
        $this->view->paginateParamsForm->populate(
            array('per_page' => $itemsPerPage)
        );

        $pageNumber = (int)$this->_getParam('page', 1);

        $authentication = Zend_Auth::getInstance();
        $userEmail = $authentication->getIdentity();

        $siteMapper = new Application_Model_SiteMapper();
        $paginatedSites = $siteMapper->getUserSitesForPaginate(
            $userEmail, $pageNumber, $itemsPerPage, $searchParams
        );

        $itemsStart = ($pageNumber - 1) * $paginatedSites->getCurrentItemCount()
            + 1;
        $itemsEnd = $pageNumber * $paginatedSites->getCurrentItemCount();
        $totalItems = $paginatedSites->getTotalItemCount();

        $this->view->itemsStart = $itemsStart;
        $this->view->itemsEnd = $itemsEnd;
        $this->view->totalItems = $totalItems;
        $this->view->itemsPerPage = $itemsPerPage;

        $this->view->paginatedSites = $paginatedSites;
    }

    public function styleAction()
    {
        if ($this->_getParam('site_id', null) != null) {
            $siteId = $this->_getParam('site_id', '');
        } elseif ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $siteId = $formData['site_id'];
        } else {
            throw new Exception('Invalid site_id');
        }

        $site = new Application_Model_Site();
        $siteMapper = new Application_Model_SiteMapper();
        $siteMapper->loadSiteById($siteId, $site);
        $this->view->assign('site', $site);

        $sidebarThemeMapper = new Application_Model_SidebarThemeMapper();
        $siteSidebarThemeMapper
            = new Application_Model_CampaignCompSidebarThemeMapper();
        $siteSidebarTheme = new  Application_Model_CampaignCompSidebarTheme();

        $isLoaded = $siteSidebarThemeMapper->loadSelectedTheme(
            $siteId, $siteSidebarTheme
        );

        if ($isLoaded == true) {

            $sidebarTheme = new Application_Model_SidebarTheme();
            $sidebarThemeMapper->loadThemeById(
                $siteSidebarTheme->getTheme_Id(), $sidebarTheme
            );

            $this->view->assign(
                'externalThemeId', $sidebarTheme->getExternal_Theme_Id()
            );
        } else {
            $this->view->assign('externalThemeId', null);
        }

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $themeId = $formData['theme_id'];

            $sidebarTheme = new Application_Model_SidebarTheme();
            $sidebarThemeMapper->loadThemeById($themeId, $sidebarTheme);

            if ($sidebarTheme->getCss_Name() != null) {
                $config = new Zend_Config_Ini(
                    APPLICATION_PATH . '/configs/application.ini',
                    'sidebar_themes_css'
                );

                $mainThemePath = $config->main_themes_path . '/'
                    . $sidebarTheme->getCss_Name();
                $userThemePath = $config->user_themes_path . '/wingrin-'
                    . $site->getExternal_Site_Id() . '.css';

                $isCopied = copy($mainThemePath, $userThemePath);
                if ($isCopied == false) {
                    throw new Exception("CSS file copying failed !");
                }
            }

            $siteSidebarTheme->setCampaign_Comp_Id($siteId);
            $siteSidebarTheme->setTheme_Id($themeId);
            $siteSidebarTheme->setSelected(1);

            $siteSidebarThemeMapper->save($siteSidebarTheme);

            $this->_helper->redirector(
                'style', 'customise', 'user',
                array('site_id' => $formData['site_id'])
            );
        } else {

            $themesList = $sidebarThemeMapper->getBasicThemesForGallery();
            $this->view->assign('themesList', $themesList);

            $sidebarThemesGallery = new ZendExt_Elements_SidebarThemesGallery(
                'themes_gallery'
            );
            $sidebarThemesGallery->setSiteId($siteId);
            $sidebarThemesGallery->fillThemesList($themesList);
            $sidebarThemesGallery->setModule('user');

            $this->view->assign('sidebarThemesGallery', $sidebarThemesGallery);
        }
    }


}



