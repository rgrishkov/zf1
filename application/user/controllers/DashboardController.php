<?php

class User_DashboardController extends ZendExt_Controllers_WinGrinUserController
{

    public function indexAction()
    {
        //$campaignMapper = new Application_Model_CampaignMapper();
        $authentication = Zend_Auth::getInstance();
        $userEmail = $authentication->getIdentity();

        $competitionsMapper = new Application_Model_CampaignTokenCompMapper();
        $competitionsList = $competitionsMapper->getListByUserEmail($userEmail);
        $this->view->assign('competitionsList', $competitionsList);

        $sitesMapper = new Application_Model_SiteMapper();
        $sitesForSelectList = $sitesMapper->getUserSitesForSelect($userEmail);
        $this->view->assign('sitesList', $sitesForSelectList);

        $sitesWithoutSidebar = $sitesMapper->getSitesForDashboardExt(
            $userEmail, false
        );
        $this->view->assign('sitesWithoutSidebar', $sitesWithoutSidebar);

        $weekWithoutSidebar = $sitesMapper->getWeekWithoutSidebar($userEmail);
        $this->view->assign('weekWithoutSidebar', $weekWithoutSidebar);

        $globalContentMapper = new Application_Model_GlobalContentMapper();
        $installSidebarMessage = new Application_Model_GlobalContent();

        if (count($weekWithoutSidebar) > 0) {
            $globalContentMapper->loadContentById(
                'INSTALL_SIDEBAR_MESSAGE_WEEK', $installSidebarMessage
            );
        } else {
            $globalContentMapper->loadContentById(
                'INSTALL_SIDEBAR_MESSAGE', $installSidebarMessage
            );
        }

        $this->view->assign('installSidebarMessage', $installSidebarMessage);

        $sitesWithSidebar = $sitesMapper->getSitesForDashboardExt(
            $userEmail, true
        );
        $this->view->assign('sitesWithSidebar', $sitesWithSidebar);

        $sitesList = $sitesMapper->getUserSites($userEmail);
        if (count($sitesList) > 0) {
            $firstSiteId = $sitesList[0]->getSite_Id();
        } else {
            $firstSiteId = null;
        }
        $this->view->assign('defaultSiteId', $firstSiteId);

        $globalNewsMapper = new Application_Model_GlobalNewsMapper();
        $latestNewsList = $globalNewsMapper->getLatestNews();
        $this->view->assign('newsList', $latestNewsList);

        /*$authentication = Zend_Auth::getInstance();
        $userEmail = $authentication->getIdentity();

        $messageMapper = new Application_Model_UserMessageMapper();
        $latestMessagesList = $messageMapper->getLatestUserMessages($userEmail);
        $this->view->assign('messagesList', $latestMessagesList);*/

        $quickInfo = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById(
            'USER_DASHBOARD_QUICK_INFO', $quickInfo
        );
        $this->view->assign('quickInfo', $quickInfo);

        $testModePopupContent = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById(
            'USER_TEST_MODE_POPUP_DASHBOARD', $testModePopupContent
        );
        $this->view->assign('testModePopupContent', $testModePopupContent);

        $noCompetitionsMessage = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById(
            'USER_DASHBOARD_NO_COMPETITIONS_MESSAGE', $noCompetitionsMessage
        );
        $this->view->assign('noCompetitionsMessage', $noCompetitionsMessage);

        $noCompetitionsPopupContent = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById(
            'USER_DASHBOARD_NO_COMPETITIONS_POPUP', $noCompetitionsPopupContent
        );
        $this->view->assign(
            'noCompetitionsPopupContent', $noCompetitionsPopupContent
        );
    }

    public function competitionAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competitionId = $this->competition->getCampaign_Comp_Id();

        $competitionMapper = new Application_Model_CampaignTokenCompMapper();
        $competitionMapper->loadCampaignTokenCompByIdExt(
            $competitionId, $this->competition
        );

        $campaign = $this->campaign;
        $competition = $this->competition;

        $this->view->assign('competitionId', $competitionId);

        $entriesMapper = new Application_Model_CampaignCompEntryMapper();
        $lastEntriesList = $entriesMapper->getLastEntriesForCompetition(
            $competitionId, 10
        );

        $this->view->assign('lastEntriesList', $lastEntriesList);

        $statsMapper = new Application_Model_StatsEntriesPerDayMapper();
        $statsList = $statsMapper->getListByCampaignId(
            $campaign->getCampaign_Id()
        );

        $this->view->assign('statsList', $statsList);
        $this->view->assign('campaign', $campaign);
        $this->view->assign('competition', $competition);

        $graphDatesRange = array();

        $locale = Zend_Registry::get('Zend_Locale');
        $format = Zend_Locale_Data::getContent($locale, 'date', 'medium');

        if (count($statsList) > 0) {
            $lastStatDate = new Zend_Date(
                $statsList[count($statsList) - 1]->getStat_Date(),
                Zend_Date::ISO_8601
            );
            $currentDate = new Zend_Date(
                $statsList[0]->getStat_Date(), Zend_Date::ISO_8601
            );

            while ($currentDate <= $lastStatDate) {
                $graphDatesRange[(string)$currentDate->toString($format)]
                    = $currentDate->getTimestamp();
                $currentDate->addDay(1);
            }
        }

        $this->view->assign('graphDatesRange', $graphDatesRange);

        /*$campaignTokenCompMapper = new Application_Model_CampaignTokenCompMapper();
        $competitionsList = $campaignTokenCompMapper->getListByCampaignId($campaign->getCampaign_Id());
	    $this->view->assign('competitionsList', $competitionsList);*/
    }
}

