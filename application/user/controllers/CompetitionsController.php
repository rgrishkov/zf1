<?php

class User_CompetitionsController
    extends ZendExt_Controllers_WinGrinUserController
{
    public function createDefaultAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        if ($this->_getParam('competition_type', null) == null) {
            throw new Exception('competition_type');
        }

        $competitionType = $this->_getParam('competition_type', '');

        $userEmail = $this->user->getEmail();
        $siteMapper = new Application_Model_SiteMapper();
        $site = $siteMapper->getUserSite($userEmail);

        $siteId = $site->getSite_Id();
        $competition = $this->createDefaultCompetition(
            $siteId, $competitionType
        );

        $messagesFacade = new ZendExt_Facades_MessagesFacade();
        $messagesFacade->sendSystemMessage($this->user, 'COMPETITION_CREATED');

        $this->_helper->redirector(
            'look', 'competitions', 'user',
            array('competition_id' => $competition->getCampaign_Comp_Id())
        );
    }

    private function createDefaultCompetition($siteId, $competitionType)
    {
        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/user_campaign_defaults.ini',
            'competition'
        );

        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();

        $campaign->setCampaign_Name($config->competition_name);
        $campaign->setSite_Id($siteId);

        $externalCampaignId = ZendExt_Utils_StringUtils::randomStr(20);
        $campaign->setExternal_Campaign_Id($externalCampaignId);

        $campaignMapper->save($campaign);

        $competition = new Application_Model_CampaignTokenComp();
        $campaignTokenCompMapper
            = new Application_Model_CampaignTokenCompMapper();

        $competition->setCampaign_Comp_Name($config->competition_name);
        $competition->setCampaign_Id($campaign->getCampaign_Id());
        $competition->setCampaign_Comp_Status(CAMPAIGN_STATUS_NEW);
        $competition->setCampaign_Comp_Type($competitionType);

        $competition->setShow_Sidebar_Anon($config->show_sidebar_anon);
        $competition->setNum_Secondary_Sidebar_Images(
            $config->num_secondary_sidebar_images
        );

        $competition->setCampaign_Comp_Requires_Skill_Question(
            $config->requires_skill_question
        );
        $competition->setCampaign_Comp_Skill_Question_Max_Woords(
            $config->question_max_words
        );

        $campaignTokenCompMapper->save($competition);

        return $competition;
    }

    private function createCampaignOrder(Application_Model_CampaignTokenComp $competition
    ) {

        $competitionMapper = new Application_Model_CampaignTokenCompMapper();
        $currentDateString = $competitionMapper->getCurrentDate();

        $currentDate = new Zend_Date($currentDateString, 'YYYY-MM-DD');
        $currentDate->setTimeZone('GMT');

        $startDateLive = $competition->getStartZendDate();
        $startDateLive->setTimeZone('GMT');

        if ($startDateLive->isEarlier($currentDate)) {
            throw new Exception(
                'Start date is earlier then current date [' . $currentDate
                . '] [' . $startDateLive . ']'
            );
        }

        $competitionLength = $competition->getCompetitionLength();

        $product = new Application_Model_Product();
        $productMapper = new Application_Model_ProductMapper();

        $campaignOrder = new Application_Model_CampaignOrder();
        $campaignOrderMapper = new Application_Model_CampaignOrderMapper();
        $haveOrder = $campaignOrderMapper->loadOrderByCampaignId(
            $competition->getCampaign_Id(), $campaignOrder
        );

        if ($haveOrder == false) {
            $isLoaded = $productMapper->loadProductByCompetitionLength(
                $competitionLength, $product
            );

            if ($isLoaded == false) {
                throw new Exception(
                    "You don't have a product for " . $competitionLength
                    . " days"
                );
            }

            $this->saveCampaignOrder(
                $competition->getCampaign_Id(), $campaignOrder, $product
            );
        } else {
            $productMapper->loadProductById(
                $campaignOrder->getProduct_Id(), $product
            );

            if ($competitionLength < $product->getMin_Days()
                || $competitionLength > $product->getMax_Days()
            ) {

                $newProduct = new Application_Model_Product();
                $productMapper->loadProductByCompetitionLength(
                    $competitionLength, $newProduct
                );

                $this->saveCampaignOrder(
                    $competition->getCampaign_Id(), $campaignOrder, $newProduct
                );
            }
        }
    }

    private function saveCampaignOrder($campaignId,
        Application_Model_CampaignOrder $campaignOrder,
        Application_Model_Product $product
    ) {
        $campaignOrderMapper = new Application_Model_CampaignOrderMapper();

        $campaignOrder->setCampaign_Id($campaignId);
        $campaignOrder->setProduct_Id($product->getProduct_Id());
        $campaignOrder->setName($product->getName());
        $campaignOrder->setList_Price($product->getList_Price());
        $campaignOrder->setStatus(ORDER_STATUS_PENDING);

        $campaignOrderMapper->save($campaignOrder);
    }

    private function savePrimarySidebarImage($defaultPrimarySidebarImageId,
        Application_Model_Campaign $campaign
    ) {
        ZendExt_Utils_FilesystemUtils::configureCampaignUploadFolders(
            $campaign->getExternal_Campaign_Id()
        );

        $defaultSidebarImage = new Application_Model_MediaFile();
        $mediaFileMapper = new Application_Model_MediaFileMapper();
        $mediaFileMapper->loadMediaById(
            $defaultPrimarySidebarImageId, $defaultSidebarImage
        );

        $primarySidebarImage = new Application_Model_MediaFile();
        $isLoaded = $mediaFileMapper->loadCampaignPrimarySidebarImage(
            $campaign->getCampaign_Id(), $primarySidebarImage
        );

        if ($isLoaded == true) {
            ZendExt_Utils_FilesystemUtils::deleteCampaignImage(
                $campaign->getExternal_Campaign_Id(),
                $primarySidebarImage->getMedia_Name()
            );
            $mediaFileMapper->deleteFileById(
                $primarySidebarImage->getMedia_Id()
            );
        }

        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/application.ini', 'development'
        );

        $defaultSidebarImagePath = UPLOAD_PATH
            . $defaultSidebarImage->getMedia_Location();
        $primarySidebarImagePath = $config->upload_path->campaigns . '/'
            . $campaign->getExternal_Campaign_Id() . '/images/'
            . $defaultSidebarImage->getMedia_Name();

        if (copy($defaultSidebarImagePath, $primarySidebarImagePath) == false) {
            throw new Exception(
                'Cant copy file src [' . $defaultSidebarImagePath . '] dst['
                . $primarySidebarImagePath . ']'
            );
        }

        $defaultSidebarThumbnailPath = str_replace(
            '/images/', '/thumbnails/', $defaultSidebarImagePath
        );
        $primarySidebarThumbnailPath = str_replace(
            '/images/', '/thumbnails/', $primarySidebarImagePath
        );

        if (copy($defaultSidebarThumbnailPath, $primarySidebarThumbnailPath)
            == false
        ) {
            throw new Exception(
                'Cant copy file src [' . $defaultSidebarThumbnailPath . '] dst['
                . $primarySidebarThumbnailPath . ']'
            );
        }

        $imageUrl = '/campaigns/' . $campaign->getExternal_Campaign_Id()
            . '/images/' . $defaultSidebarImage->getMedia_Name();

        $mediaFile = new Application_Model_MediaFile();
        $mediaFile->setMedia_Location($imageUrl);
        $mediaFile->setMedia_Name($defaultSidebarImage->getMedia_Name());
        $mediaFile->setMedia_Owner_Id($campaign->getCampaign_Id());
        $mediaFile->setMedia_Owner_Type(MEDIA_OWNER_TYPE_CAMPAIGN);
        $mediaFile->setMedia_Type(MEDIA_TYPE_PRIMARY_SIDEBAR_IMAGE);
        $mediaFileMapper->save($mediaFile);
    }

    /*    private function saveSidebarTabImage($defaultTabImageId, Application_Model_Campaign $campaign, Application_Model_CampaignTokenComp $competition)
        {
            ZendExt_Utils_FilesystemUtils::configureTabsUploadFolders($campaign->getExternal_Campaign_Id());

            $defaultTabImage = new Application_Model_MediaFile();
            $mediaFileMapper = new Application_Model_MediaFileMapper();
            $mediaFileMapper->loadMediaById($defaultTabImageId, $defaultTabImage);

            $currentTabImage = new Application_Model_MediaFile();
            $isLoaded = $mediaFileMapper->loadCampaignSidebarTabImage($campaign->getCampaign_Id(), $currentTabImage);

            if($isLoaded == true) {
                ZendExt_Utils_FilesystemUtils::deleteTabImage($campaign->getExternal_Campaign_Id(), $currentTabImage->getMedia_Name());
                $mediaFileMapper->deleteFileById($currentTabImage->getMedia_Id());
            }

            $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');

            $defaultTabImagePath = UPLOAD_PATH.$defaultTabImage->getMedia_Location();
            $currentTabImagePath = $config->upload_path->tabs.'/'.$campaign->getExternal_Campaign_Id().'/'.$defaultTabImage->getMedia_Name();

            if (copy($defaultTabImagePath, $currentTabImagePath) == false) {
                throw new Exception('Cant copy file src [' .$defaultTabImagePath . '] dst[' . $currentTabImagePath . ']');
            }

            $imageUrl = '/tabs/'.$campaign->getExternal_Campaign_Id().'/'.$defaultTabImage->getMedia_Name();

            $mediaFile = new Application_Model_MediaFile();
            $mediaFile->setMedia_Location($imageUrl);
            $mediaFile->setMedia_Name($defaultTabImage->getMedia_Name());
            $mediaFile->setMedia_Owner_Id($campaign->getCampaign_Id());
            $mediaFile->setMedia_Owner_Type(MEDIA_OWNER_TYPE_CAMPAIGN);
            $mediaFile->setMedia_Type(MEDIA_TYPE_PRIMARY_TAB_IMAGE);
            $mediaFileId = $mediaFileMapper->save($mediaFile);

            $competitionSidebarTheme = new Application_Model_CampaignCompSidebarTheme();
            $competitionSidebarThemeMapper = new Application_Model_CampaignCompSidebarThemeMapper();
            $competitionSidebarThemeMapper->loadSelectedTheme($competition->getCampaign_Comp_Id(), $competitionSidebarTheme);

            $competitionSidebarTheme->setTab_Media_Id($mediaFileId);
            $competitionSidebarThemeMapper->save($competitionSidebarTheme);
        }*/

    /*    public function editAction()
        {
            if ($this->_getParam('competition_id', null) == null) {
                throw new Exception('Invalid competition_id');
            }

            $competition = $this->competition;
            $campaign = $this->campaign;
            $site = $this->site;

            $competitionId = $competition->getCampaign_Comp_Id();

            $this->view->assign('competitionId', $competitionId);
            $this->view->assign('campaign', $campaign);
            $this->view->assign('competition', $competition);
            $this->view->assign('site', $site);
            $this->view->assign('siteId', $site->getSite_Id());
            $this->view->assign('siteUrl', $site->getSite_Url());
            $this->assignGlobalContent('USER_CONFIG_COMPETITION', true);
            //$this->assignGlobalContent('PLAN_DAYS_HELP', false);

            $form = new ZendExt_UserForms_EditCompetitionForm();
            $form->setCompetition($competition);

            $format = $this->getLocaleDateFormat();
            $calendarFormat = ZendExt_Utils_StringUtils::reformatDateFormatForCalendar($format);
            $this->view->assign('dateformat', $calendarFormat);

            $campaignOrder = new Application_Model_CampaignOrder();
            $campaignOrderMapper = new Application_Model_CampaignOrderMapper();

            if ($campaignOrderMapper->loadOrderByCampaignId($campaign->getCampaign_Id(), $campaignOrder) == true) {
                $product = new Application_Model_Product();
                $productMapper = new Application_Model_ProductMapper();
                $productMapper->loadProductById($campaignOrder->getProduct_Id(), $product);

                $form->setProduct($product);
            }

            if ($competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_COMPLETED) {
                $this->view->assign('isLocked', true);
            } elseif($competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_RUNNING) {
                $this->view->assign('isRunning', true);
            }

            $form->lock();
            $this->view->form = $form;

            $mediaFileMapper = new Application_Model_MediaFileMapper();

            if ($this->getRequest()->isPost()) {

                $formData = $this->getRequest()->getPost();

                if ($competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_COMPLETED) {
                    throw new Exception('Form is Locked !');
                }

                if ($form->isValid($formData)) {
                    ZendExt_Facades_CompetitionsFacade::setInprogressStatus($competition);

                    $competition->setCampaign_Comp_Intro_Text($formData['intro_text']);
                    $competition->setCampaign_Comp_Name($formData['campaign_name']);

                    if ($competition->getCampaign_Comp_Status() != CAMPAIGN_STATUS_RUNNING) {
                        if (isset($formData['start_date'])) {
                            $startDateString = $formData['start_date'].' '.$formData['start_time_hours'].':'.$formData['start_time_minutes'].':00';
                            $startDate = new Zend_Date($startDateString);

                            $endDateString = $formData['start_date'].' '.$formData['end_time_hours'].':'.$formData['end_time_minutes'].':00';
                            $endDate = new Zend_Date($endDateString);
                            $endDate->addDay($formData['competition_length']);

                            $competition->setLocalisedStartDate($startDate);
                            $competition->setLocalisedEndDate($endDate);

                        } elseif(!isset($formData['start_date']) && isset($formData['competition_length']) && $campaign->getCompetitionLength() != $formData['competition_length']) {
                            $daysDiff = $formData['competition_length'] - $competition->getCompetitionLength();

                            $endDate = $competition->getEndZendDate();
                            $endDate->addDay($daysDiff);
                            $competition->setLocalisedEndDate($endDate);
                        }
                    }

                    $competitionMapper = new Application_Model_CampaignTokenCompMapper();
                    $competitionMapper->save($competition);

                    if (isset($formData['default_sidebar_image_id']) && $formData['default_sidebar_image_id'] != '') {
                        $this->savePrimarySidebarImage($formData['default_sidebar_image_id'], $campaign);
                    }

                    if ($competition->getCampaign_Comp_Status() == CAMPAIGN_STATUS_INPROGRESS) {
                        $this->createCampaignOrder($competition);
                    }

                    $this->_helper->redirector('questions', 'competitions', 'user', array('competition_id' => $competitionId));
                } else {
                    $formData['media_files_list'] = $mediaFileMapper->getDefaultSidebarImagesList();

                    $primarySidebarImage = new Application_Model_MediaFile();
                    $isLoaded = $mediaFileMapper->loadCampaignPrimarySidebarImage($campaign->getCampaign_Id(), $primarySidebarImage);

                    if($isLoaded == true) {
                        $formData['primary_sidebar_image'] = $primarySidebarImage;
                    }

                    $this->showTopErrorBox();
                    $form->populate($formData);
                }
            } else {
                $formData = array();

                $formData['competition_id'] = $competition->getCampaign_Comp_Id();
                $formData['campaign_id'] = $campaign->getCampaign_Id();
                $formData['campaign_name'] = $competition->getCampaign_Comp_Name();

                $formData['competition_length'] = '';

                if ($competition->getCampaign_Comp_Start_Date() != null) {
                    $startDate = $competition->getStartZendDate();

                    $formData['start_date'] = $startDate->toString($format);
                    $formData['start_time_hours'] = $startDate->toString('HH');
                    $formData['start_time_minutes'] = $startDate->toString('mm');

                    $endDate = $competition->getEndZendDate();

                    $formData['end_date'] = $endDate->toString($format);
                    $formData['end_time_hours'] = $endDate->toString('HH');
                    $formData['end_time_minutes'] = $endDate->toString('mm');

                    $formData['competition_length'] = $competition->getCompetitionLength();
                } else {
                    $formData['start_date'] = '';
                    $formData['end_date'] = '';
                }

                $formData['intro_text'] = $competition->getCampaign_Comp_Intro_Text();

                $postDetailsMapper = new Application_Model_CampaignPostDetailsMapper();
                $postDetails = new Application_Model_CampaignPostDetails();
                $postDetailsMapper->loadPostDetailsByCampaignId($campaign->getCampaign_Id(), $postDetails);

                $formData['media_files_list'] = $mediaFileMapper->getDefaultSidebarImagesList();

                $primarySidebarImage = new Application_Model_MediaFile();
                $isLoaded = $mediaFileMapper->loadCampaignPrimarySidebarImage($campaign->getCampaign_Id(), $primarySidebarImage);

                if($isLoaded == true) {
                    $formData['primary_sidebar_image'] = $primarySidebarImage;
                }

                $this->view->form->populate($formData);
            }
        }*/

    public function ajaxImageUploadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $form = new ZendExt_UserForms_LookForm();

        $formData = $this->getRequest()->getPost();
        $uploadForm = $form->getElement('upload_logo');

        $mediaFileMapper = new Application_Model_MediaFileMapper();

        $competition = $this->competition;
        $campaign = $this->campaign;

        if ($uploadForm->isValidAjax($formData)) {

            ZendExt_Utils_FilesystemUtils::configureCampaignUploadFolders(
                $campaign->getExternal_Campaign_Id()
            );

            $config = new Zend_Config_Ini(
                APPLICATION_PATH . '/configs/application.ini', 'development'
            );

            $uploadPath = $config->upload_path->campaigns . '/'
                . $campaign->getExternal_Campaign_Id() . '/images';
            $thumbnailsPath = $config->upload_path->campaigns . '/'
                . $campaign->getExternal_Campaign_Id() . '/thumbnails';

            $upload = new Zend_File_Transfer_Adapter_Http();

            $fileInfo = $upload->getFileInfo();
            $tempImageName = trim($fileInfo['image_file']['name']);
            $tempImageName = str_replace(' ', '_', $tempImageName);
            $uploadedImageName = time() . '_' . $tempImageName;

            $upload->addFilter('Rename', $uploadedImageName);
            $upload->setDestination($uploadPath);
            $upload->receive();

            $primarySidebarImage = new Application_Model_MediaFile();
            $isLoaded = $mediaFileMapper->loadCampaignPrimarySidebarImage(
                $campaign->getCampaign_Id(), $primarySidebarImage
            );

            if ($isLoaded == true) {
                ZendExt_Utils_FilesystemUtils::deleteCampaignImage(
                    $campaign->getExternal_Campaign_Id(),
                    $primarySidebarImage->getMedia_Name()
                );
                $mediaFileMapper->deleteFileById(
                    $primarySidebarImage->getMedia_Id()
                );
            }

            $fullSizeImage = new ZendExt_Image_FullSizeImage(
                $uploadPath . "/" . $uploadedImageName
            );

            $filenameArray = explode('.', $tempImageName);
            $resizedImageName = time() . '_' . $filenameArray[0]
                . '_resized.png';
            $imageUrl = '/campaigns/' . $campaign->getExternal_Campaign_Id()
                . '/images/' . $resizedImageName;


            $resizedImage = new ZendExt_Image_Thumbnail(
                $fullSizeImage, "100", "100"
            );
            $resizedImage->setPath($uploadPath);
            $resizedImage->setName($resizedImageName);
            $resizedImage->save();

            unlink($uploadPath . "/" . $uploadedImageName);

            $mediaFile = new Application_Model_MediaFile();
            $mediaFile->setMedia_Location($imageUrl);
            $mediaFile->setMedia_Name($resizedImageName);
            $mediaFile->setMedia_Owner_Id($campaign->getCampaign_Id());
            $mediaFile->setMedia_Owner_Type(MEDIA_OWNER_TYPE_CAMPAIGN);
            $mediaFile->setMedia_Type(MEDIA_TYPE_PRIMARY_SIDEBAR_IMAGE);
            $mediaFileMapper->save($mediaFile);

            ZendExt_Facades_CompetitionsFacade::setInprogressStatus(
                $competition
            );


            $uploadedImageUrl
                = $imagePath = $config->image->uploaded_img_prefix . $imageUrl;
            $thumbnailUrl = str_replace(
                '/images/', '/thumbnails/', $uploadedImageUrl
            );

            $result = array(
                'error'         => false,
                'image_name'    => $resizedImageName,
                'image_url'     => $uploadedImageUrl,
                'thumbnail_url' => $thumbnailUrl,
                'image_size'    => round(
                    $fileInfo['image_file']['size'] / 1024
                ),
                'owner_id'      => $competition->getCampaign_Comp_Id(),
                'image_id'      => time()
            );
        } else {

            $result = array(
                'error'         => true,
                'error_message' => $uploadForm->getErrorMessageText()
            );
        }

        $json = Zend_Json::encode($result);
        echo $json;
    }

    public function ajaxDeleteImageAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $campaign = $this->campaign;
        $imageName = $this->_getParam('image_name');

        $mediaFileMapper = new Application_Model_MediaFileMapper();
        $primarySidebarImage = new Application_Model_MediaFile();

        $isLoaded = $mediaFileMapper->loadCampaignPrimarySidebarImage(
            $campaign->getCampaign_Id(), $primarySidebarImage
        );
        if ($isLoaded == true) {
            $mediaFileMapper->deleteFileById(
                $primarySidebarImage->getMedia_Id()
            );

            //ZendExt_Utils_FilesystemUtils::deleteSiteImage($site->getExternal_Site_Id(), $imageName);
            ZendExt_Utils_FilesystemUtils::deleteCampaignImage(
                $campaign->getExternal_Campaign_Id(), $imageName
            );
        }
    }

    public function copyGalleryImageAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $campaign = $this->campaign;
        $competition = $this->competition;

        $imageName = $this->_getParam('image_name', null);

        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/application.ini', 'development'
        );
        $galleryPath = $config->image->competition_gallery_folder;

        if (is_file($galleryPath . '/' . $imageName)) {
            ZendExt_Utils_FilesystemUtils::configureCampaignUploadFolders(
                $campaign->getExternal_Campaign_Id()
            );

            $mediaFileMapper = new Application_Model_MediaFileMapper();
            $primarySidebarImage = new Application_Model_MediaFile();

            $uploadPath = $config->upload_path->campaigns . '/'
                . $campaign->getExternal_Campaign_Id() . '/images';
            $newImageName = time() . '_' . $imageName;

            $isCopied = copy(
                $galleryPath . '/' . $imageName,
                $uploadPath . '/' . $newImageName
            );

            if ($isCopied) {

                $isLoaded = $mediaFileMapper->loadCampaignPrimarySidebarImage(
                    $campaign->getCampaign_Id(), $primarySidebarImage
                );

                if ($isLoaded == true) {
                    ZendExt_Utils_FilesystemUtils::deleteCampaignImage(
                        $campaign->getExternal_Campaign_Id(),
                        $primarySidebarImage->getMedia_Name()
                    );
                    $mediaFileMapper->deleteFileById(
                        $primarySidebarImage->getMedia_Id()
                    );
                }

                $imageUrl = '/campaigns/' . $campaign->getExternal_Campaign_Id()
                    . '/images/' . $newImageName;

                $mediaFile = new Application_Model_MediaFile();
                $mediaFile->setMedia_Location($imageUrl);
                $mediaFile->setMedia_Name($newImageName);
                $mediaFile->setMedia_Owner_Id($campaign->getCampaign_Id());
                $mediaFile->setMedia_Owner_Type(MEDIA_OWNER_TYPE_CAMPAIGN);
                $mediaFile->setMedia_Type(MEDIA_TYPE_PRIMARY_SIDEBAR_IMAGE);
                $mediaFileMapper->save($mediaFile);

                ZendExt_Facades_CompetitionsFacade::setInprogressStatus(
                    $competition
                );

                $uploadedImageUrl
                    =
                $imagePath = $config->image->uploaded_img_prefix . $imageUrl;

                $result = array(
                    'error'     => false,
                    'image_url' => $uploadedImageUrl
                );

            } else {
                $result = array(
                    'error' => true,
                );
            }

        } else {
            $result = array(
                'error' => true,
            );
        }

        $json = Zend_Json::encode($result);
        echo $json;
    }

    /*   public function ajaxImageUploadAction()
       {
           $this->_helper->viewRenderer->setNoRender(true);
           $this->view->layout()->disableLayout();

           $form = new ZendExt_UserForms_EditCompetitionForm();

           $formData = $this->getRequest()->getPost();
           $uploadForm = $form->getElement('competition_images_gallery');

           $campaign = $this->campaign;
           $competition = $this->competition;

           $mediaFileMapper = new Application_Model_MediaFileMapper();

           if ($uploadForm->isValidAjax($formData)){
               ZendExt_Utils_FilesystemUtils::configureCampaignUploadFolders($campaign->getExternal_Campaign_Id());

               $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');

               $uploadPath = $config->upload_path->campaigns.'/'.$campaign->getExternal_Campaign_Id().'/images';
               $thumbnailsPath = $config->upload_path->campaigns.'/'.$campaign->getExternal_Campaign_Id().'/thumbnails';

               $upload = new Zend_File_Transfer_Adapter_Http();

               $fileInfo = $upload->getFileInfo();
               $newName = trim($fileInfo['image_file']['name']);
               $newName = str_replace(' ', '_', $newName);
               $newName = time().'_'.$newName;

               $imageUrl = '/campaigns/'.$campaign->getExternal_Campaign_Id().'/images/'.$newName;

               $upload->addFilter('Rename', $newName);
               $upload->setDestination($uploadPath);
               $upload->receive();

               $primarySidebarImage = new Application_Model_MediaFile();
               $isLoaded = $mediaFileMapper->loadCampaignPrimarySidebarImage($campaign->getCampaign_Id(), $primarySidebarImage);

               if($isLoaded == true) {
                   ZendExt_Utils_FilesystemUtils::deleteCampaignImage($campaign->getExternal_Campaign_Id(), $primarySidebarImage->getMedia_Name());
                   $mediaFileMapper->deleteFileById($primarySidebarImage->getMedia_Id());
               }

               $fullSizeImage = new ZendExt_Image_FullSizeImage($uploadPath."/". $newName);
               $thumbnail = new ZendExt_Image_Thumbnail($fullSizeImage, "64", "64");
               $thumbnail->setPath($thumbnailsPath);
               $thumbnail->save();

               $mediaFile = new Application_Model_MediaFile();
               $mediaFile->setMedia_Location($imageUrl);
               $mediaFile->setMedia_Name($newName);
               $mediaFile->setMedia_Owner_Id($campaign->getCampaign_Id());
               $mediaFile->setMedia_Owner_Type(MEDIA_OWNER_TYPE_CAMPAIGN);
               $mediaFile->setMedia_Type(MEDIA_TYPE_PRIMARY_SIDEBAR_IMAGE);
               $mediaFileMapper->save($mediaFile);

               ZendExt_Facades_CompetitionsFacade::setInprogressStatus($competition);

               $uploadedImageUrl = $imagePath = $config->image->uploaded_img_prefix.$imageUrl;

               $result = array(
                  'error' => false,
                  'image_url' => $uploadedImageUrl
               );

           } else {
               $result = array(
                   'error' => true,
                   'error_message' => $uploadForm->getErrorMessageText()
               );
           }

           $json = Zend_Json::encode($result);
           echo $json;
       }

       public function ajaxTabUploadAction()
       {
           $this->_helper->viewRenderer->setNoRender(true);
           $this->view->layout()->disableLayout();

           $form = new ZendExt_UserForms_ThemesForm();

           $formData = $this->getRequest()->getPost();
           $uploadForm = $form->getElement('tabs_gallery');

           $competition = $this->competition;
           $campaign = $this->campaign;

           $competitionId = $competition->getCampaign_Comp_Id();
           $mediaFileMapper = new Application_Model_MediaFileMapper();

           if ($uploadForm->isValidAjax($formData)){
               ZendExt_Utils_FilesystemUtils::configureTabsUploadFolders($campaign->getExternal_Campaign_Id());

               $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');

               $uploadPath = $config->upload_path->tabs.'/'.$campaign->getExternal_Campaign_Id();

               $upload = new Zend_File_Transfer_Adapter_Http();

                $fileInfo = $upload->getFileInfo();
                $newName = time().'_'.$fileInfo['image_file']['name'];

                $imageUrl = '/tabs/'.$campaign->getExternal_Campaign_Id().'/'.$newName;

                $upload->addFilter('Rename', $newName);
                $upload->setDestination($uploadPath);
                $upload->receive();

               $sidebarTabImage = new Application_Model_MediaFile();
               $isLoaded = $mediaFileMapper->loadCampaignSidebarTabImage($campaign->getCampaign_Id(), $sidebarTabImage);

               if($isLoaded == true) {
                   ZendExt_Utils_FilesystemUtils::deleteTabImage($campaign->getExternal_Campaign_Id(), $sidebarTabImage->getMedia_Name());
                   $mediaFileMapper->deleteFileById($sidebarTabImage->getMedia_Id());
               }

               $mediaFile = new Application_Model_MediaFile();
               $mediaFile->setMedia_Location($imageUrl);
               $mediaFile->setMedia_Name($newName);
               $mediaFile->setMedia_Owner_Id($campaign->getCampaign_Id());
               $mediaFile->setMedia_Owner_Type(MEDIA_OWNER_TYPE_CAMPAIGN);
               $mediaFile->setMedia_Type(MEDIA_TYPE_PRIMARY_TAB_IMAGE);
               $mediaFileId = $mediaFileMapper->save($mediaFile);

               $competitionSidebarTheme = new Application_Model_CampaignCompSidebarTheme();
               $competitionSidebarThemeMapper = new Application_Model_CampaignCompSidebarThemeMapper();
               $competitionSidebarThemeMapper->loadSelectedTheme($competitionId, $competitionSidebarTheme);

               $competitionSidebarTheme->setTab_Media_Id($mediaFileId);
               $competitionSidebarThemeMapper->save($competitionSidebarTheme);

               ZendExt_Facades_CompetitionsFacade::setInprogressStatus($competition);

               $uploadedImageUrl = $imagePath = $config->image->uploaded_img_prefix.$imageUrl;

               $result = array(
                  'error' => false,
                  'image_url' => $uploadedImageUrl
               );

           } else {

               $result = array(
                   'error' => true,
                   'error_message' => $uploadForm->getErrorMessageText()
               );
           }

           $json = Zend_Json::encode($result);
           echo $json;
       }*/

    public function questionsAction()
    {

        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competition = $this->competition;
        $campaign = $this->campaign;
        $site = $this->site;

        $competitionId = $competition->getCampaign_Comp_Id();

        $this->view->assign('competitionId', $competitionId);
        $this->view->assign('competition', $competition);
        $this->view->assign('campaign', $campaign);
        $this->view->assign('site', $site);
        $this->view->assign('siteUrl', $campaign->getSite_Url());
        $this->assignGlobalContent('USER_CONFIG_QUESTIONS', true);

        $questionMapper = new Application_Model_CampaignQuestionMapper();

        $form = new ZendExt_UserForms_CampaignQuestionsForm($competition);
        $form->setCompetitionId($competitionId);
        $form->setSiteUrl($campaign->getSite_Url());
        $form->setCompetitionType($competition->getCampaign_Comp_Type());

        /*if ($competition->get) {
            $this->view->assign('isSaved', true);
        } else {
            $this->view->assign('isSaved', false);
        }*/

        if ($competition->getCampaign_Comp_Status()
            == CAMPAIGN_STATUS_COMPLETED
        ) {
            $this->view->assign('isLocked', true);
            $form->lock();
        } elseif ($competition->getCampaign_Comp_Status()
            == CAMPAIGN_STATUS_RUNNING
        ) {
            $this->view->assign('isRunning', true);
        }

        $this->view->form = $form;

        $formData = array();
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                ZendExt_Facades_CompetitionsFacade::setInprogressStatus(
                    $competition
                );
                //$campaignMapper = new Application_Model_CampaignMapper();
                $competitionMapper
                    = new Application_Model_CampaignTokenCompMapper();

                $campaignQuestion = new Application_Model_CampaignQuestion();

                if ((bool)$formData['allow_multiple_choice_question'] == true) {
                    $campaignQuestion->setCampaign_Comp_Id($competitionId);
                    $campaignQuestion->setQuestion_Id($formData['question_id']);
                    $campaignQuestion->setQuestion_Type('m_choise');
                    $campaignQuestion->setQuestion($formData['question']);
                    $campaignQuestion->setAnswer_1($formData['answer1']);
                    $campaignQuestion->setAnswer_2($formData['answer2']);
                    $campaignQuestion->setAnswer_3($formData['answer3']);
                    $campaignQuestion->setAnswer_4($formData['answer4']);
                    $campaignQuestion->setAnswer_1_Correct(
                        $formData['answer1_correct']
                    );
                    $campaignQuestion->setAnswer_2_Correct(
                        $formData['answer2_correct']
                    );
                    $campaignQuestion->setAnswer_3_Correct(
                        $formData['answer3_correct']
                    );
                    $campaignQuestion->setAnswer_4_Correct(
                        $formData['answer4_correct']
                    );

                    if ($competition->getCampaign_Comp_Type()
                        == CAMPAIGN_TYPE_COIN_TRIG
                    ) {
                        $campaignQuestion->setQuestion_Hint(
                            $formData['question_hint']
                        );
                    } else {
                        $campaignQuestion->setQuestion_Hint(
                            'Find answer <a href="' . $formData['trigger_url']
                            . '">here</a>'
                        );
                    }

                    $questionMapper->save($campaignQuestion);
                } else {
                    if (isset($formData['question_id'])
                        && $formData['question_id'] != null
                    ) {
                        $questionMapper->deleteQuestionById(
                            $formData['question_id']
                        );
                    }
                }

                /*if ($campaign->getTest_Mode_Id() == null && $campaign->isConfigured() == true) {
                    $testModeId = ZendExt_Utils_StringUtils::randomStr(5);
                    $campaign->setTest_Mode_Id($testModeId);
                    $campaignMapper->save($campaign);
                }*/

                if ((bool)$formData['allow_skill_question'] == true) {
                    $competition->setCampaign_Comp_Requires_Skill_Question(1);
                    $competition->setCampaign_Comp_Skill_Question(
                        $formData['skill_question']
                    );
                    $competition->setCampaign_Comp_Skill_Question_Max_Woords(
                        $formData['max_words']
                    );
                } else {
                    $competition->setCampaign_Comp_Requires_Skill_Question(0);
                    $competition->setCampaign_Comp_Skill_Question(null);
                }

                /*if ($formData['trigger_url_type'] == 'specified_url') {
                    $competition->setPrimary_Trigger_Url(ZendExt_Utils_StringUtils::clearUrl($formData['trigger_url']));
                } else {
                    $competition->setPrimary_Trigger_Url(null);
                }
                $competition->setTrigger_Url_Type($formData['trigger_url_type']);

                if ($formData['post_entry_link_type'] == 'specified_url') {
                    $competition->setPost_Entry_Link(ZendExt_Utils_StringUtils::clearUrl($formData['post_entry_link']));
                } else {
                    $competition->setPost_Entry_Link(null);
                }*/

                if ($formData['user_details'] == 'send_emails'
                    && isset($formData['mail_list_id'])
                ) {
                    $competition->setMail_Provider_Id(
                        $formData['mail_provider_id']
                    );
                    $competition->setMail_List_Id($formData['mail_list_id']);

                    /*$mailProvidersManager = new ZendExt_MailListsProviders_MailProvidersManager();
                    $provider = $mailProvidersManager->getProviderById($formData['mail_provider_id']);

                    $provider->storeCredentials($site->getSite_Id(), $formData);*/
                }

                //$competition->setPost_Entry_Link_Type($formData['post_entry_link_type']);

                $competitionMapper->save($competition);

                if ($competition->getCampaign_Comp_Status()
                    == CAMPAIGN_STATUS_NEW
                ) {
                    ZendExt_Facades_CompetitionsFacade::changeCampaignStatus(
                        $competition, CAMPAIGN_STATUS_INPROGRESS
                    );
                }

                $this->_helper->redirector(
                    'conditions', 'competitions', 'user',
                    array('competition_id' => $formData['competition_id'])
                );
            } else {
                $this->showTopErrorBox();
                $form->populate($formData);
            }
        } else {
            $campaignQuestion = new Application_Model_CampaignQuestion();
            $questionMapper->loadQuestionByCompetitionId(
                $competitionId, $campaignQuestion
            );

            $competitionSidebarThemeMapper
                = new Application_Model_CampaignCompSidebarThemeMapper();
            $competitionSidebarTheme
                = new  Application_Model_CampaignCompSidebarTheme();
            $competitionSidebarThemeMapper->loadSelectedTheme(
                $competitionId, $competitionSidebarTheme
            );

            $formData['campaign_comp_id']
                = $campaignQuestion->getCampaign_Comp_Id();
            $formData['question_id'] = $campaignQuestion->getQuestion_Id();
            $formData['question'] = $campaignQuestion->getQuestion();
            $formData['answer1'] = $campaignQuestion->getAnswer_1();
            $formData['answer2'] = $campaignQuestion->getAnswer_2();
            $formData['answer3'] = $campaignQuestion->getAnswer_3();
            $formData['answer4'] = $campaignQuestion->getAnswer_4();
            $formData['answer1_correct']
                = $campaignQuestion->getAnswer_1_Correct();
            $formData['answer2_correct']
                = $campaignQuestion->getAnswer_2_Correct();
            $formData['answer3_correct']
                = $campaignQuestion->getAnswer_3_Correct();
            $formData['answer4_correct']
                = $campaignQuestion->getAnswer_4_Correct();
            $formData['question_hint'] = $campaignQuestion->getQuestion_Hint();
            $formData['trigger_url'] = $competition->getPrimary_Trigger_Url();
            $formData['trigger_url_type'] = $competition->getTrigger_Url_Type();
            $formData['post_entry_link'] = $competition->getPost_Entry_Link();
            $formData['post_entry_link_type']
                = $competition->getPost_Entry_Link_Type();
            $formData['skill_question']
                = $competition->getCampaign_Comp_Skill_Question();
            $formData['max_words']
                = $competition->getCampaign_Comp_Skill_Question_Max_Woords();
            $formData['mail_provider_id'] = $competition->getMail_Provider_Id();
            $formData['mail_list_id'] = $competition->getMail_List_Id();

            $formData['primary_highlight_color']
                = $competitionSidebarTheme->getPrimary_Highlight_Color();
            $formData['primary_background_color']
                = $competitionSidebarTheme->getPrimary_Background_Color();
            $formData['primary_font_color']
                = $competitionSidebarTheme->getPrimary_Font_Color();
            $formData['primary_border_color']
                = $competitionSidebarTheme->getPrimary_Border_Color();
            $formData['primary_button_color']
                = $competitionSidebarTheme->getPrimary_Button_Color();
            $formData['primary_button_font_color']
                = $competitionSidebarTheme->getPrimary_Button_Font_Color();
            $formData['get_started_button_text']
                = $competition->getGet_Started_Button_Text();
            $formData['show_countdown_timer']
                = $competitionSidebarTheme->getShow_Countdown_Timer();
            $formData['competition_name'] = $competition->getCampaign_Comp_Name(
            );
            $formData['intro_text'] = $competition->getCampaign_Comp_Intro_Text(
            );
            $formData['primary_header_color']
                = $competitionSidebarTheme->getPrimary_Header_Color();
            $formData['primary_button_gradient_1']
                = $competitionSidebarTheme->getPrimary_Button_Gradient_1();
            $formData['primary_button_gradient_2']
                = $competitionSidebarTheme->getPrimary_Button_Gradient_2();

            $primarySidebarImage = new Application_Model_MediaFile();
            $mediaFileMapper = new Application_Model_MediaFileMapper();
            $isLoaded = $mediaFileMapper->loadCampaignPrimarySidebarImage(
                $campaign->getCampaign_Id(), $primarySidebarImage
            );

            if ($isLoaded == true) {
                $config = new Zend_Config_Ini(
                    APPLICATION_PATH . '/configs/application.ini', 'development'
                );

                $formData['full_image_path']
                    = $config->image->uploaded_img_prefix
                    . $primarySidebarImage->getMedia_Location();
                $formData['show_image'] = true;
            } else {
                $formData['full_image_path'] = '';
                $formData['show_image'] = false;
            }

            $this->view->assign(
                'mailProviderId', $competition->getMail_Provider_Id()
            );
            $this->view->assign('mailListId', $competition->getMail_List_Id());

            if ($competition->getMail_Provider_Id() != null) {

                $mailProvidersManager
                    = new ZendExt_MailListsProviders_MailProvidersManager();
                $mailListProvider = $mailProvidersManager->getProviderById(
                    $competition->getMail_Provider_Id()
                );
                $siteMailListIntegration = $mailListProvider->getCredentials(
                    $site->getSite_Id()
                );

                $this->view->assign(
                    'apiKey', $siteMailListIntegration->getApi_Key()
                );
                $this->view->assign(
                    'username', $siteMailListIntegration->getUsername()
                );
                $this->view->assign(
                    'password', $siteMailListIntegration->getPassword()
                );

                $formData['mail_list_provider_name']
                    = $mailListProvider->getTitle();
                $mailLists = $mailListProvider->getAvailableLists(
                    array('api_key'  => $siteMailListIntegration->getApi_Key(),
                          'username' => $siteMailListIntegration->getUsername(),
                          'password' => $siteMailListIntegration->getPassword())
                );

                $formData['mail_list_name']
                    = $mailLists[$competition->getMail_List_Id()];
            }

            $form->populate($formData);
        }
    }

    public function lookAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competition = $this->competition;
        $campaign = $this->campaign;
        $site = $this->site;

        $competitionId = $competition->getCampaign_Comp_Id();

        if ($competition->getCampaign_Comp_Type()
            == COMPETITION_TYPE_BASIC_ANNOUNCEMENT
        ) {
            //$this->_helper->viewRenderer->setViewScriptPathSpec(':controller/types/basic_announcement/:action.:suffix');
            $this->_helper->viewRenderer('types/basic_announcement/look');
            $redirectAction = 'look';
        } else {
            $redirectAction = 'questions';
        }

        $this->view->assign('competitionId', $competitionId);
        $this->view->assign('campaign', $campaign);
        $this->view->assign('competition', $competition);
        $this->view->assign('site', $site);
        $this->assignGlobalContent('USER_CONFIG_THEME', true);

        $form = new ZendExt_UserForms_LookForm($competition);
        //$form->setCompetition($competition);
        $form->setCompetitionId($competitionId);
        $form->setCompetitionType($competition->getCampaign_Comp_Type());

        $this->view->form = $form;

        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/sidebar_fonts.ini'
        );
        $sidebarFontsList = array();

        foreach ($config as $font) {
            $sidebarFontsList[$font] = $font;
        }

        $form->fillFonts($sidebarFontsList);

        $sidebarThemeMapper = new Application_Model_SidebarThemeMapper();
        $competitionSidebarThemeMapper
            = new Application_Model_CampaignCompSidebarThemeMapper();
        $competitionSidebarTheme
            = new  Application_Model_CampaignCompSidebarTheme();
        $competitionMapper = new Application_Model_CampaignTokenCompMapper();

        $competitionSidebarThemeMapper->loadSelectedTheme(
            $competitionId, $competitionSidebarTheme
        );

        $format = $this->getLocaleDateFormat();
        $calendarFormat
            = ZendExt_Utils_StringUtils::reformatDateFormatForCalendar($format);
        $this->view->assign('dateformat', $calendarFormat);

        //$campaignOrder = new Application_Model_CampaignOrder();
        //$campaignOrderMapper = new Application_Model_CampaignOrderMapper();

        if ($competition->getCampaign_Comp_Status()
            == CAMPAIGN_STATUS_COMPLETED
        ) {
            $this->view->assign('isLocked', true);
        } elseif ($competition->getCampaign_Comp_Status()
            == CAMPAIGN_STATUS_RUNNING
        ) {
            $this->view->assign('isRunning', true);
        }

        $form->lock();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($competition->getCampaign_Comp_Status()
                == CAMPAIGN_STATUS_COMPLETED
            ) {
                throw new Exception('Form is Locked !');
            }

            if ($form->isValid($formData)) {
                ZendExt_Facades_CompetitionsFacade::setInprogressStatus(
                    $competition
                );

                $themeId = 1;

                $sidebarTheme = new Application_Model_SidebarTheme();
                $sidebarThemeMapper->loadThemeById($themeId, $sidebarTheme);

                $competitionSidebarTheme->setCampaign_Comp_Id($competitionId);
                $competitionSidebarTheme->setTheme_Id($themeId);
                $competitionSidebarTheme->setShow_Countdown_Timer(
                    $formData['show_countdown_timer']
                );
                $competitionSidebarTheme->setExternal_Site_Id(
                    $site->getExternal_Site_Id()
                );
                $competitionSidebarTheme->setExternal_Campaign_Id(
                    $campaign->getExternal_Campaign_Id()
                );
                $competitionSidebarTheme->setPrimary_Highlight_Color(
                    $formData['primary_highlight_color']
                );
                $competitionSidebarTheme->setPrimary_Background_Color(
                    $formData['primary_background_color']
                );
                $competitionSidebarTheme->setPrimary_Font_Color(
                    $formData['primary_font_color']
                );
                $competitionSidebarTheme->setPrimary_Border_Color(
                    $formData['primary_border_color']
                );
                $competitionSidebarTheme->setPrimary_Button_Color(
                    $formData['primary_button_color']
                );
                $competitionSidebarTheme->setPrimary_Button_Font_Color(
                    $formData['primary_button_font_color']
                );
                $competitionSidebarTheme->setPrimary_Font_Style(
                    $formData['primary_font_style']
                );
                $competitionSidebarTheme->setPrimary_Header_Color(
                    $formData['primary_header_color']
                );
                $competitionSidebarTheme->setPrimary_Button_Gradient_1(
                    $formData['primary_button_gradient_1']
                );
                $competitionSidebarTheme->setPrimary_Button_Gradient_2(
                    $formData['primary_button_gradient_2']
                );

                $competitionSidebarThemeMapper->save($competitionSidebarTheme);

                if ($competition->getCampaign_Comp_Status()
                    != CAMPAIGN_STATUS_RUNNING
                ) {

                    if (isset($formData['start_date'])
                        && isset($formData['end_date'])
                    ) {
                        $startDateString = $formData['start_date'] . ' '
                            . $formData['start_time_hours'] . ':'
                            . $formData['start_time_minutes'] . ':00';
                        $startDate = new Zend_Date($startDateString);

                        $endDateString = $formData['end_date'] . ' '
                            . $formData['end_time_hours'] . ':'
                            . $formData['end_time_minutes'] . ':00';
                        $endDate = new Zend_Date($endDateString);
                        //$endDate->addDay($formData['competition_length']);

                        $competition->setLocalisedStartDate($startDate);
                        $competition->setLocalisedEndDate($endDate);

                    } /*elseif(!isset($formData['start_date']) && isset($formData['competition_length']) && $campaign->getCompetitionLength() != $formData['competition_length']) {
                        $daysDiff = $formData['competition_length'] - $competition->getCompetitionLength();

                        $endDate = $competition->getEndZendDate();
                        $endDate->addDay($daysDiff);
                        $competition->setLocalisedEndDate($endDate);
                    }*/
                } else {

                    if (isset($formData['end_date'])) {


                        $currentDateString = $competitionMapper->getCurrentDate(
                        );

                        $currentDate = new Zend_Date(
                            $currentDateString, 'YYYY-MM-DD'
                        );
                        $currentDate->setTimeZone('GMT');

                        $endDateString = $formData['end_date'] . ' '
                            . $formData['end_time_hours'] . ':'
                            . $formData['end_time_minutes'] . ':00';
                        $endDate = new Zend_Date($endDateString);

                        if ($endDate->isEarlier($currentDate)) {
                            $competition->setCampaign_Comp_Status(
                                CAMPAIGN_STATUS_COMPLETED
                            );
                        }
                    }
                }

                if ($formData['trigger_url_type'] == 'specified_url') {
                    $competition->setPrimary_Trigger_Url(
                        ZendExt_Utils_StringUtils::clearUrl(
                            $formData['trigger_url']
                        )
                    );
                } else {
                    $competition->setPrimary_Trigger_Url(null);
                }
                $competition->setTrigger_Url_Type(
                    $formData['trigger_url_type']
                );
                $competition->setCampaign_Comp_Name(
                    $formData['competition_name']
                );
                $competition->setCampaign_Comp_Intro_Text(
                    $formData['intro_text']
                );
                $competition->setGet_Started_Button_Text(
                    $formData['get_started_button_text']
                );


                $competitionMapper->save($competition);

                if ($competition->getCampaign_Comp_Status()
                    == CAMPAIGN_STATUS_INPROGRESS
                ) {
                    $this->createCampaignOrder($competition);
                }

                $this->_helper->redirector(
                    $redirectAction, 'competitions', 'user',
                    array('competition_id' => $competitionId)
                );
            } else {

                $primarySidebarImage = new Application_Model_MediaFile();
                $mediaFileMapper = new Application_Model_MediaFileMapper();
                $isLoaded = $mediaFileMapper->loadCampaignPrimarySidebarImage(
                    $campaign->getCampaign_Id(), $primarySidebarImage
                );

                if ($isLoaded == true) {
                    $formData['media_file'] = $primarySidebarImage;
                    $formData['show_image'] = true;
                }

                $formData['campaign_id'] = $campaign->getCampaign_Id();
                $formData['external_campaign_id']
                    = $campaign->getExternal_Campaign_Id();

                $this->showTopErrorBox();
                $form->populate($formData);
            }

        } else {
            $formData = array();

            if ($competition->getCampaign_Comp_Status()
                == CAMPAIGN_STATUS_COMPLETED
            ) {
                $form->lock();
                $this->view->assign('isLocked', true);
            } elseif ($competition->getCampaign_Comp_Status()
                == CAMPAIGN_STATUS_RUNNING
            ) {
                $this->view->assign('isRunning', true);
            }

            $formData['competition_name'] = $competition->getCampaign_Comp_Name(
            );
            $formData['intro_text'] = $competition->getCampaign_Comp_Intro_Text(
            );
            $formData['get_started_button_text']
                = $competition->getGet_Started_Button_Text();
            $formData['show_countdown_timer']
                = $competitionSidebarTheme->getShow_Countdown_Timer();
            $formData['primary_highlight_color']
                = $competitionSidebarTheme->getPrimary_Highlight_Color();
            $formData['primary_background_color']
                = $competitionSidebarTheme->getPrimary_Background_Color();
            $formData['primary_font_color']
                = $competitionSidebarTheme->getPrimary_Font_Color();
            $formData['primary_border_color']
                = $competitionSidebarTheme->getPrimary_Border_Color();
            $formData['primary_button_color']
                = $competitionSidebarTheme->getPrimary_Button_Color();
            $formData['primary_button_font_color']
                = $competitionSidebarTheme->getPrimary_Button_Font_Color();
            $formData['primary_font_style']
                = $competitionSidebarTheme->getPrimary_Font_Style();
            $formData['primary_header_color']
                = $competitionSidebarTheme->getPrimary_Header_Color();
            $formData['primary_button_gradient_1']
                = $competitionSidebarTheme->getPrimary_Button_Gradient_1();
            $formData['primary_button_gradient_2']
                = $competitionSidebarTheme->getPrimary_Button_Gradient_2();
            $formData['site_url'] = $site->getSite_Url();
            $formData['trigger_url'] = $competition->getPrimary_Trigger_Url();
            $formData['trigger_url_type'] = $competition->getTrigger_Url_Type();
            $formData['campaign_id'] = $campaign->getCampaign_Id();
            $formData['external_campaign_id']
                = $campaign->getExternal_Campaign_Id();

            if ($competition->getCampaign_Comp_Start_Date() != null) {
                $startDate = $competition->getStartZendDate();

                $formData['start_date'] = $startDate->toString($format);
                $formData['start_time_hours'] = $startDate->toString('HH');
                $formData['start_time_minutes'] = $startDate->toString('mm');

                $endDate = $competition->getEndZendDate();

                $formData['end_date'] = $endDate->toString($format);
                $formData['end_time_hours'] = $endDate->toString('HH');
                $formData['end_time_minutes'] = $endDate->toString('mm');

                $formData['competition_length']
                    = $competition->getCompetitionLength();
            } else {
                $formData['start_date'] = '';
                $formData['end_date'] = '';
            }

            $primarySidebarImage = new Application_Model_MediaFile();
            $mediaFileMapper = new Application_Model_MediaFileMapper();
            $isLoaded = $mediaFileMapper->loadCampaignPrimarySidebarImage(
                $campaign->getCampaign_Id(), $primarySidebarImage
            );

            if ($isLoaded == true) {
                $formData['media_file'] = $primarySidebarImage;
                $formData['show_image'] = true;
            }

            $this->view->form->populate($formData);
        }
    }

    public function summaryAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competition = $this->competition;
        $campaign = $this->campaign;
        $site = $this->site;

        $competitionId = $competition->getCampaign_Comp_Id();

        $this->view->assign('competition', $competition);
        $this->view->assign('competitionId', $competitionId);
        $this->view->assign('siteId', $site->getSite_Id());
        $this->view->assign('site', $site);
        $this->view->assign('campaign', $campaign);

        $campaignOrder = new Application_Model_CampaignOrder();
        $campaignOrderMapper = new Application_Model_CampaignOrderMapper();

        $siteSidebarThemeMapper
            = new Application_Model_CampaignCompSidebarThemeMapper();
        $siteSidebarTheme = new  Application_Model_CampaignCompSidebarTheme();
        $isThemeSelected = $siteSidebarThemeMapper->loadSelectedTheme(
            $competitionId, $siteSidebarTheme
        );
        $this->view->assign('isThemeSelected', $isThemeSelected);

        if ($competition->getCampaign_Comp_Start_Date() != null) {
            $competitionMapper = new Application_Model_CampaignTokenCompMapper(
            );
            $currentDateString = $competitionMapper->getCurrentDate();

            $currentDate = new Zend_Date($currentDateString);
            $currentDate->setTimeZone('GMT');

            $startDate = $competition->getStartZendDate();
            $startDate->setTimeZone('GMT');

            if ($startDate->isEarlier($currentDate)) {
                $this->view->assign('isEarlierError', true);
            }
        } else {
            $this->view->assign('isEarlierError', false);
        }

        if ($campaignOrderMapper->loadOrderByCampaignId(
                $campaign->getCampaign_Id(), $campaignOrder
            ) == true
        ) {

            $orderOptionMapper
                = new Application_Model_CampaignOrderOptionMapper();
            $orderOptionsList = $orderOptionMapper->getListByOrderId(
                $campaignOrder->getOrder_Id()
            );

            $totalOrderPrice = $campaignOrderMapper->getTotalOrderPrice(
                $campaignOrder->getOrder_Id()
            );

            $this->view->assign('campaignOrder', $campaignOrder);
            $this->view->assign('orderOptions', $orderOptionsList);
            $this->view->assign('totalOrderPrice', $totalOrderPrice);
        }
    }

    public function paymentsuccessAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competition = $this->competition;
        $site = $this->site;

        $competitionId = $competition->getCampaign_Comp_Id();

        $this->view->assign('competitionId', $competitionId);
        $this->view->assign('site', $site);

        $globalContentMapper = new Application_Model_GlobalContentMapper();
        $pageContent = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById('PAYMENT_SUCCESS', $pageContent);
        $this->view->assign('pageContent', $pageContent);
    }

    public function conditionsAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competition = $this->competition;
        $campaign = $this->campaign;
        $site = $this->site;

        $competitionId = $competition->getCampaign_Comp_Id();

        $this->view->assign('competitionId', $competitionId);
        $this->view->assign('campaign', $campaign);
        $this->view->assign('competition', $competition);
        $this->view->assign('site', $site);
        $this->view->assign('siteUrl', $site->getSite_Url());
        $this->assignGlobalContent('USER_CONFIG_CONDITIONS', true);

        $conditionsMapper = new Application_Model_ConditionsMapper();
        $conditionsList = $conditionsMapper->getListForSelect();

        $form = new ZendExt_UserForms_EditConditionsForm($competition);
        $form->fillConditionsList($conditionsList);
        $form->setCompetitionId($competitionId);
        $form->setCompetitionType($competition->getCampaign_Comp_Type());

        if ($competition->getCampaign_Comp_Status()
            == CAMPAIGN_STATUS_COMPLETED
        ) {
            $this->view->assign('isLocked', true);
            $form->lock();
        } elseif ($competition->getCampaign_Comp_Status()
            == CAMPAIGN_STATUS_RUNNING
        ) {
            $this->view->assign('isRunning', true);
        }

        $this->view->form = $form;
        $formData = array();

        $campaignOrder = new Application_Model_CampaignOrder();
        $campaignOrderMapper = new Application_Model_CampaignOrderMapper();
        $campaignOrderMapper->loadOrderByCampaignId(
            $campaign->getCampaign_Id(), $campaignOrder
        );

        $formData['order_status'] = $campaignOrder->getStatus();

        $condition = new Application_Model_Conditions();
        $conditionsMapper = new Application_Model_ConditionsMapper();

        $conditionsMapper->loadConditionById(
            $competition->getCondition_Id(), $condition
        );

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {

                ZendExt_Facades_CompetitionsFacade::setInprogressStatus(
                    $competition
                );

                $condition->setCondition_Id($formData['condition_id']);
                $condition->setName('user condition');
                $condition->setContent($formData['condition_content']);
                $condition->setType(CONDITION_TYPE_USER);

                $conditionId = $conditionsMapper->save($condition);

                $competitionMapper
                    = new Application_Model_CampaignTokenCompMapper();
                $competition->setCondition_Id($conditionId);
                $competitionMapper->save($competition);

                $this->_helper->redirector(
                    'conditions', 'competitions', 'user',
                    array('competition_id' => $competitionId)
                );
            } else {
                $this->showTopErrorBox();
                $form->populate($formData);
            }
        } else {
            $formData['condition_id'] = $condition->getCondition_Id();
            $formData['condition_content'] = $condition->getContent();

            $form->populate($formData);
        }
    }

    public function previewConditionAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $conditionSampleId = $this->_getParam('condition_sample_id', null);

        $condition = new Application_Model_Conditions();
        $conditionsMapper = new Application_Model_ConditionsMapper();
        $conditionsMapper->loadConditionById($conditionSampleId, $condition);

        $result = array(
            'condition_id'      => $condition->getCondition_Id(),
            'condition_name'    => $condition->getName(),
            'condition_content' => $condition->getContent()
        );

        $json = Zend_Json::encode($result);
        echo $json;
    }

    public function getProvidersListAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $mailProvidersManager
            = new ZendExt_MailListsProviders_MailProvidersManager();
        $result = $mailProvidersManager->getProvidersForSelect();

        $json = Zend_Json::encode($result);
        echo $json;
    }

    public function getConnectionPanelAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $site = $this->site;
        $providerId = $this->_getParam('provider_id', null);

        $mailProvidersManager
            = new ZendExt_MailListsProviders_MailProvidersManager();
        $provider = $mailProvidersManager->getProviderById($providerId);

        $siteMailListIntegration = $provider->getCredentials(
            $site->getSite_Id()
        );
        $formData = array();

        if ($siteMailListIntegration != null
            && $siteMailListIntegration->getProvider_Id() == $providerId
        ) {

            $formData['api_key'] = $siteMailListIntegration->getApi_Key();
            $formData['username'] = $siteMailListIntegration->getUsername();
            $formData['password'] = $siteMailListIntegration->getPassword();
        }

        echo $provider->getAPIConnectionPanel($formData);
    }

    public function getConfigurationPanelAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();


        $providerId = $this->_getParam('provider_id', null);
        $formData = $this->getRequest()->getParams();

        $mailProvidersManager
            = new ZendExt_MailListsProviders_MailProvidersManager();
        $provider = $mailProvidersManager->getProviderById($providerId);

        echo $provider->getConfigurationPanel($formData);
    }

    public function testProviderConnectionAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $providerId = $this->_getParam('provider_id', null);
        $formData = $this->getRequest()->getParams();

        $mailProvidersManager
            = new ZendExt_MailListsProviders_MailProvidersManager();
        $provider = $mailProvidersManager->getProviderById($providerId);

        $result = $provider->validateConnection($formData);

        if ($result != false) {
            $provider->storeCredentials($this->site->getSite_Id(), $formData);
        }

        $json = Zend_Json::encode($result);
        echo $json;
    }

    public function assignGlobalContent($contentId, $assignTestModeData = false)
    {
        $globalContentMapper = new Application_Model_GlobalContentMapper();
        $quickInfo = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById($contentId, $quickInfo);
        $this->view->assign('quickInfo', $quickInfo);

        if ($assignTestModeData == true) {
            $testModePopupContent = new Application_Model_GlobalContent();
            $globalContentMapper->loadContentById(
                'USER_TEST_MODE_POPUP_SITE', $testModePopupContent
            );
            $this->view->assign('testModePopupContent', $testModePopupContent);

            $testModePageContent = new Application_Model_GlobalContent();
            $globalContentMapper->loadContentById(
                'USER_TEST_MODE_PAGE', $testModePageContent
            );
            $this->view->assign('testModePageContent', $testModePageContent);

            $testModeDisabledContent = new Application_Model_GlobalContent();
            $globalContentMapper->loadContentById(
                'USER_TEST_MODE_DISABLED', $testModeDisabledContent
            );
            $this->view->assign(
                'testModeDisabledContent', $testModeDisabledContent
            );

            $planDaysHelpContent = new Application_Model_GlobalContent();
            $globalContentMapper->loadContentById(
                'PLAN_DAYS_HELP', $planDaysHelpContent
            );
            $this->view->assign('planDaysHelpContent', $planDaysHelpContent);
            //PLAN_DAYS_HELP

            $config = new Zend_Config_Ini(
                APPLICATION_PATH . '/configs/application.ini', 'test_mode'
            );
            $this->view->assign('testUrlPrefix', $config->test_url_prefix);
        }
    }

    public function recalculateCompetitionLengthAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $competition = $this->competition;
        $formData = $this->getRequest()->getQuery();

        $startDateString = $formData['start_date'] . ' '
            . $formData['start_time_hours'] . ':'
            . $formData['start_time_minutes'] . ':00';
        $startDate = new Zend_Date($startDateString);

        $endDateString = $formData['end_date'] . ' '
            . $formData['end_time_hours'] . ':' . $formData['end_time_minutes']
            . ':00';
        $endDate = new Zend_Date($endDateString);

        $competition->setLocalisedStartDate($startDate);
        $competition->setLocalisedEndDate($endDate);

        $result = array(
            'competition_length' => $competition->getCompetitionLength()
        );

        $json = Zend_Json::encode($result);
        echo $json;
    }

    public function pauseCompetitionAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competition = $this->competition;
        ZendExt_Facades_CompetitionsFacade::pauseCompetition($competition);

        $this->_helper->redirector(
            'competition', 'dashboard', 'user',
            array('competition_id' => $competition->getCampaign_Comp_Id())
        );
    }

    public function startCompetitionAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competition = $this->competition;
        ZendExt_Facades_CompetitionsFacade::startCompetition($competition);

        $this->_helper->redirector(
            'competition', 'dashboard', 'user',
            array('competition_id' => $competition->getCampaign_Comp_Id())
        );
    }


    /* public function locationsAction() {

    	$form = new ZendExt_UserForms_CampaignLocationsForm();

        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competitionId = $this->_getParam('competition_id', '');
        $form->setCompetitionId($competitionId);
        $this->view->assign('competitionId', $competitionId);

        $competition = new Application_Model_CampaignTokenComp();
        $competitionMapper = new Application_Model_CampaignTokenCompMapper();
        $competitionMapper->loadCampaignTokenCompById($competitionId, $competition);

        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($competition->getCampaign_Id(), $campaign);

    	$locationsMapper = new Application_Model_CampaignTokenLocationMapper();
    	$siteMapper = new Application_Model_SiteMapper();

        $siteId = Application_Model_CampaignTokenCompMapper::getSiteIdByCompId($competitionId);
        $isLocked = $siteMapper->isLocked($siteId);

        if ($isLocked == true) {
            $form->lock();
            $this->view->assign('isLocked', true);
        }

    	$form->setCampaignId($campaign->getCampaign_Id());
    	$this->view->form = $form;

    	$this->view->assign('siteUrl', $campaign->getSite_Url());

        $globalContentMapper = new Application_Model_GlobalContentMapper();
        $quickInfo = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById('USER_CONFIG_LOCATIONS', $quickInfo);
        $this->view->assign('quickInfo', $quickInfo);

    	$formData = array();
    	if ($this->getRequest()->isPost()) {
        	$formData = $this->getRequest()->getPost();

            if ($isLocked == true) {
                throw new Exception('Form is Locked !');
            }

        	if ($form->isValid($formData)) {
        		$locationsTexts = explode(',', $formData['match_text']);

        		foreach ($locationsTexts as $matchText) {

        			$location = new Application_Model_CampaignTokenLocation();

        			$matchText = trim($matchText, ' "\'');

        			$locationUrl =  $this->_getParam('page_url', null);
        			$locationUrl = ZendExt_Utils_StringUtils::clearUrl($locationUrl);

                    $location->setLocation_Id($formData['location_id']);
                    $location->setCampaign_Id($formData['campaign_id']);
                    $location->setPage_Url($locationUrl);
                    $location->setMatch_Text($matchText);

                    $location->setLocation_Type($formData['type']);
                    if ($formData['type'] == LOCATION_TYPE_DEFAULT) {
                        $location->setDefault(1);
                    }

                    $locationsMapper->save($location);
        		}
                ZendExt_Facades_CompetitionsFacade::changeCampaignStatus($campaign, CAMPAIGN_STATUS_INPROGRESS);
                $this->_helper->redirector('locations', 'competitions', 'user', array('competition_id' => $competitionId));

        	} else {
        		$form->populate($formData);
        	}
    	} elseif($this->_getParam('location', null) != null) {

            $locationId = $this->_getParam('location', null);
    		$location = new Application_Model_CampaignTokenLocation();
    		$locationsMapper->loadLocationById($locationId, $location);

    		$formData['location_id'] = $location->getLocation_Id();
            $formData['campaign_id'] = $location->getCampaign_Id();
    		$formData['page_url'] = $location->getPage_Url();
    		$formData['match_text'] = $location->getMatch_Text();
    		$formData['type'] = $location->getLocation_Type();

    		$form->populate($formData);
    	}

    	$paginateParamsForm = new ZendExt_Forms_PaginateParamsForm();
    	$paginateParamsForm->setAction('/user/competitions/locations/id/'.$campaign->getCampaign_Id());

   		$itemsPerPage = (int) $this->_getParam('per_page', 10);
   		$this->view->paginateParamsForm = $paginateParamsForm;
		$this->view->paginateParamsForm->populate(array('per_page' => $itemsPerPage));

   		$pageNumber = (int) $this->_getParam('page', 1);

   		$paginatedLocations = $locationsMapper->getLocationsForPaginate($campaign->getCampaign_Id(), $pageNumber, $itemsPerPage, null);

   		$itemsStart = ($pageNumber-1)*$paginatedLocations->getCurrentItemCount() + 1;
   		$itemsEnd = $pageNumber*$paginatedLocations->getCurrentItemCount();
   		$totalItems = $paginatedLocations->getTotalItemCount();

   		$this->view->itemsStart = $itemsStart;
   		$this->view->itemsEnd = $itemsEnd;
   		$this->view->totalItems = $totalItems;
   		$this->view->itemsPerPage = $itemsPerPage;

   		$this->view->paginatedLocations = $paginatedLocations;
    }

 	public function deletelocationsAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competitionId = $this->_getParam('competition_id', '');

    	$this->_helper->viewRenderer->setNoRender(true);
    	$this->view->layout()->disableLayout();

    	if ($this->getRequest()->isPost()) {
        	$formData = $this->getRequest()->getPost();
        	$locationsIdList = $formData['locations_ids'];

        	$locationMapper = new Application_Model_CampaignTokenLocationMapper();
        	foreach ($locationsIdList as $locationId) {
        		$locationMapper->deleteLocationById($locationId);
        	}

            $competition = new Application_Model_CampaignTokenComp();
            $competitionMapper = new Application_Model_CampaignTokenCompMapper();
            $competitionMapper->loadCampaignTokenCompById($competitionId, $competition);

            $campaign = new Application_Model_Campaign();
            $campaignMapper = new Application_Model_CampaignMapper();
            $campaignMapper->loadCampaignById($competition->getCampaign_Id(), $campaign);

            ZendExt_Facades_CompetitionsFacade::changeCampaignStatus($campaign, CAMPAIGN_STATUS_INPROGRESS);
    	}

        $this->_helper->redirector('locations', 'competitions', 'user', array('competition_id' => $competitionId));
    }

       private function savePostData($campaignId, $siteId, $formData)
    {
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaign = new Application_Model_Campaign();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $siteMapper = new Application_Model_SiteMapper();
        $site = new Application_Model_Site();
        $siteMapper->loadSiteById($siteId, $site);

        $postDetailsMapper = new Application_Model_CampaignPostDetailsMapper();
		$postDetails = new Application_Model_CampaignPostDetails();

        $postDetailsMapper->loadPostDetailsByCampaignId($campaignId, $postDetails);

        $postDetails->setCampaign_Id($campaignId);

        $postTitle = $campaign->getCampaign_Name().' from '.$site->getSite_Name();
        $postDescription = $campaign->getCampaign_Name().' '.$site->getSite_Name();

        $postDetails->setPost_Title($postTitle);
        $postDetails->setPost_Name($postTitle);
        $postDetails->setPost_Keywords($postDescription);
        $postDetails->setPost_Description($postDescription);
        $postDetails->setPost_Content($formData['post_content']);
        $postDetails->setPost_Exerpt(' ');
        $postDetails->setPost_Conditions(' ');
        $postDetails->setPromotion_Type('user promo');
        $postDetails->setShow_Post_Features(1);
        $postDetails->setFeature_Buddy_Deal(0);
        $postDetails->setFeature_Skill_Question(1);

        $postDetailsMapper->save($postDetails);
    }
       */
}







