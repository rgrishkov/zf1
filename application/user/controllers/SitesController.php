<?php

class User_SitesController extends ZendExt_Controllers_WinGrinUserController
{

    public function indexAction()
    {
        $authentication = Zend_Auth::getInstance();
        $userEmail = $authentication->getIdentity();

        $paginateParamsForm = new ZendExt_Forms_PaginateParamsForm();
        $paginateParamsForm->setAction(
            $this->view->url(array('controller' => 'sites'))
        );

        $itemsPerPage = (int)$this->_getParam('per_page', 10);
        $this->view->paginateParamsForm = $paginateParamsForm;
        $this->view->paginateParamsForm->populate(
            array('per_page' => $itemsPerPage)
        );

        $pageNumber = (int)$this->_getParam('page', 1);

        $siteMapper = new Application_Model_SiteMapper();
        $paginatedSites = $siteMapper->getUserSitesForPaginate(
            $userEmail, $pageNumber, $itemsPerPage
        );

        $itemsStart = ($pageNumber - 1) * $paginatedSites->getCurrentItemCount()
            + 1;
        $itemsEnd = $pageNumber * $paginatedSites->getCurrentItemCount();
        $totalItems = $paginatedSites->getTotalItemCount();

        $this->view->itemsStart = $itemsStart;
        $this->view->itemsEnd = $itemsEnd;
        $this->view->totalItems = $totalItems;
        $this->view->itemsPerPage = $itemsPerPage;

        $this->view->paginatedSites = $paginatedSites;
    }

    public function addAction()
    {
        $form = new ZendExt_UserForms_AddSiteForm();
        $this->view->form = $form;

        $externalSiteId = ZendExt_Utils_StringUtils::randomStr(20);
        $form->setExternalId($externalSiteId);

        $globalContentMapper = new Application_Model_GlobalContentMapper();
        $quickInfo = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById('USER_CONFIG_SITE', $quickInfo);
        $this->view->assign('quickInfo', $quickInfo);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $site = new Application_Model_Site();

                $site->setExternal_Site_Id($formData['external_site_id']);
                $site->setSite_Name($formData['site_name']);
                $site->setSite_Url(
                    ZendExt_Utils_StringUtils::clearUrl($formData['site_url'])
                );
                $site->setSite_Bio($formData['site_bio']);

                $siteMapper = new Application_Model_SiteMapper();
                $siteId = $siteMapper->save($site);

                $authentication = Zend_Auth::getInstance();
                $userEmail = $authentication->getIdentity();

                $userSitesMapper = new Application_Model_UserSitesMapper();
                $userSitesMapper->addUserSite($userEmail, $siteId);

                $this->_helper->redirector(
                    'edit', 'sites', 'user', array('site_id' => $siteId)
                );
            } else {
                $this->showTopErrorBox();
                $form->populate($formData);
            }
        }
    }

    public function editAction()
    {
        $siteMapper = new Application_Model_SiteMapper();
        $site = $siteMapper->getUserSite($this->user->getEmail());
        $siteId = $site->getSite_Id();

        $form = new ZendExt_UserForms_EditSiteForm();
        $this->view->form = $form;

        $site = new Application_Model_Site();
        $siteMapper = new Application_Model_SiteMapper();
        $siteMapper->loadSiteById($siteId, $site);

        $this->view->assign('siteId', $siteId);
        $this->view->assign('site', $site);
        $this->view->assign('siteUrl', $site->getSite_Url());

        $globalContentMapper = new Application_Model_GlobalContentMapper();
        $quickInfo = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById('USER_CONFIG_SITE', $quickInfo);
        $this->view->assign('quickInfo', $quickInfo);

        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/application.ini', 'test_mode'
        );
        $this->view->assign('testUrlPrefix', $config->test_url_prefix);

        if ($this->_getParam('saved_flag', null) == 1) {
            $this->view->assign('isSaved', true);
        } else {
            $this->view->assign('isSaved', false);
        }

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $site->setSite_Url(
                    ZendExt_Utils_StringUtils::clearUrl($formData['site_url'])
                );

                $siteMapper->save($site);
                $this->_helper->redirector(
                    'edit', 'sites', 'user', array('saved_flag' => '1')
                );

            } else {
                $this->showTopErrorBox();
                $form->populate($formData);
            }
        } else {
            $formData = array();
            $formData['external_site_id'] = $site->getExternal_Site_Id();
            $formData['site_url'] = $site->getSite_Url();

            $form->populate($formData);
        }
    }

    public function javascriptAction()
    {
        if ($this->_getParam('site_id', null) != null) {
            $siteId = $this->_getParam('site_id', '');
        } else {
            throw new Exception('Invalid site_id');
        }

        $site = new Application_Model_Site();
        $siteMapper = new Application_Model_SiteMapper();
        $siteMapper->loadSiteById($siteId, $site);

        $this->view->assign('siteId', $siteId);
        $this->view->assign('site', $site);
        $this->view->assign('siteUrl', $site->getSite_Url());

        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/application.ini', 'test_mode'
        );
        $this->view->assign('testUrlPrefix', $config->test_url_prefix);

        $globalContentMapper = new Application_Model_GlobalContentMapper();
        $quickInfo = new Application_Model_GlobalContent();

        $globalContentMapper->loadContentById(
            'SETUP_JAVASCRIPT_PAGE', $quickInfo
        );
        $this->view->assign('quickInfo', $quickInfo);
    }

    public function ajaxImageUploadAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $form = new ZendExt_UserForms_EditSiteForm();

        $formData = $this->getRequest()->getPost();
        $uploadForm = $form->getElement('upload_logo');

        $mediaFileMapper = new Application_Model_MediaFileMapper();
        $mediaFile = new Application_Model_MediaFile();

        $siteId = $formData['site_id'];
        $site = new Application_Model_Site();
        $siteMapper = new Application_Model_SiteMapper();
        $siteMapper->loadSiteById($siteId, $site);

        if ($uploadForm->isValidAjax($formData)) {
            ZendExt_Utils_FilesystemUtils::configureSiteUploadFolders(
                $site->getExternal_Site_Id()
            );

            $config = new Zend_Config_Ini(
                APPLICATION_PATH . '/configs/application.ini', 'development'
            );

            $uploadPath = $config->upload_path->sites . '/'
                . $site->getExternal_Site_Id() . '/images';
            $thumbnailsPath = $config->upload_path->sites . '/'
                . $site->getExternal_Site_Id() . '/thumbnails';

            $upload = new Zend_File_Transfer_Adapter_Http();

            $fileInfo = $upload->getFileInfo();
            $newName = trim($fileInfo['image_file']['name']);
            $newName = str_replace(' ', '_', $newName);
            $newName = time() . '_' . $newName;

            $imageUrl = '/sites/' . $site->getExternal_Site_Id() . '/images/'
                . $newName;

            $upload->addFilter('Rename', $newName);
            $upload->setDestination($uploadPath);
            $upload->receive();

            $fullSizeImage = new ZendExt_Image_FullSizeImage(
                $uploadPath . "/" . $newName
            );
            $thumbnail = new ZendExt_Image_Thumbnail(
                $fullSizeImage, "64", "64"
            );
            $thumbnail->setPath($thumbnailsPath);
            $thumbnail->save();

            $loadResult = $mediaFileMapper->loadSiteLogo($siteId, $mediaFile);
            if ($loadResult == true) {
                ZendExt_Utils_FilesystemUtils::deleteSiteImage(
                    $site->getExternal_Site_Id(), $mediaFile->getMedia_Name()
                );
            }

            //$mediaFile->setMedia_Id();
            $mediaFile->setMedia_Location($imageUrl);
            $mediaFile->setMedia_Name($newName);
            $mediaFile->setMedia_Owner_Id($siteId);
            $mediaFile->setMedia_Owner_Type(MEDIA_OWNER_TYPE_SITE);
            $mediaFile->setMedia_Type(MEDIA_TYPE_LOGO);
            $mediaFileMapper->save($mediaFile);

            $uploadedImageUrl
                = $imagePath = $config->image->uploaded_img_prefix . $imageUrl;
            $thumbnailUrl = str_replace(
                '/images/', '/thumbnails/', $uploadedImageUrl
            );

            $result = array(
                'error'         => false,
                'image_name'    => $newName,
                'image_url'     => $uploadedImageUrl,
                'thumbnail_url' => $thumbnailUrl,
                'image_size'    => round(
                    $fileInfo['image_file']['size'] / 1024
                ),
                'owner_id'      => $siteId,
                'image_id'      => time()
            );
        } else {

            $result = array(
                'error'         => true,
                'error_message' => $uploadForm->getErrorMessageText()
            );
        }

        $json = Zend_Json::encode($result);
        echo $json;
    }

    public function ajaxDeleteImageAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $siteId = $this->_getParam('owner_id');
        $imageName = $this->_getParam('image_name');

        $mediaFileMapper = new Application_Model_MediaFileMapper();
        $mediaFile = new Application_Model_MediaFile();

        $isLoaded = $mediaFileMapper->loadMediaByNameAndOwnerId(
            $imageName, $siteId, $mediaFile
        );
        if ($isLoaded == true) {
            $mediaFileMapper->deleteFileById($mediaFile->getMedia_Id());

            $siteMapper = new Application_Model_SiteMapper();
            $site = new Application_Model_Site();
            $siteMapper->loadSiteById($siteId, $site);

            ZendExt_Utils_FilesystemUtils::deleteSiteImage(
                $site->getExternal_Site_Id(), $imageName
            );
        }
    }

    /* public function getSiteImagesList($siteId)
     {
            $mediaFileMapper = new Application_Model_MediaFileMapper();
         $mediaFilesList = $mediaFileMapper->getListByOwnerIdAndOwnerType($siteId, MEDIA_OWNER_TYPE_SITE);
 
         return $mediaFilesList;
     }
 
     public function deleteimageAction()
     {
         $this->_helper->viewRenderer->setNoRender(true);
         $this->view->layout()->disableLayout();
 
         $competitionId = $this->_getParam('owner_id');
         $siteId = Application_Model_CampaignTokenCompMapper::getSiteIdByCompId($competitionId);
         $imageName = $this->_getParam('image_name');
 
         $mediaFileMapper = new Application_Model_MediaFileMapper();
         $mediaFile = new Application_Model_MediaFile();
 
         $isLoaded = $mediaFileMapper->loadMediaByNameAndOwnerId($imageName, $siteId, $mediaFile);
         if ($isLoaded == true) {
             $mediaFileMapper->deleteFileById($mediaFile->getMedia_Id());
 
             $siteMapper = new Application_Model_SiteMapper();
             $site = new Application_Model_Site();
             $siteMapper->loadSiteById($siteId, $site);
 
             ZendExt_Utils_FilesystemUtils::deleteSiteImage($site->getExternal_Site_Id(), $imageName);
         }
 
         $this->_helper->redirector('edit', 'sites', 'user', array('competition_id' => $competitionId));
     }*/

    public function gettestinfoAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $result = new stdClass();

        $competitionId = $this->_getParam('competition_id');
        $siteId = Application_Model_CampaignTokenCompMapper::getSiteIdByCompId(
            $competitionId
        );

        $site = new Application_Model_Site();
        $siteMapper = new Application_Model_SiteMapper();
        $siteMapper->loadSiteById($siteId, $site);

        $competition = new Application_Model_CampaignTokenComp();
        $competitionMapper = new Application_Model_CampaignTokenCompMapper();
        $competitionMapper->loadCampaignTokenCompById(
            $competitionId, $competition
        );

        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById(
            $competition->getCampaign_Id(), $campaign
        );

        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/application.ini', 'test_mode'
        );

        $result->test_url = $config->test_url_prefix . '/Session/test/'
            . $campaign->getExternal_Campaign_Id() . '/'
            . $campaign->getTest_Mode_Id();
        $result->sidebar_script_active = $site->getSidebar_Script_Active();

        $json = Zend_Json::encode($result);
        echo $json;
    }

    public function deleteAction()
    {
        $siteId = $this->_getParam('site_id', null);

        $siteMapper = new Application_Model_SiteMapper();
        $site = new Application_Model_Site();

        $loadResult = $siteMapper->loadSiteById($siteId, $site);

        if ($loadResult != false) {
            $siteMapper->deleteSiteById($siteId);
        }
        $this->_helper->redirector('index', 'sites', 'user');
    }

}



