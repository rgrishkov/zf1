<?php

class User_SettingsController extends ZendExt_Controllers_WinGrinUserController
{

    public function editAction()
    {
        $authentication = Zend_Auth::getInstance();
        $userEmail = $authentication->getIdentity();

        $userMapper = new Application_Model_UsersMapper();
        $user = new Application_Model_Users();
        $userMapper->loadUserByEmail($userEmail, $user);

        $form = new ZendExt_UserForms_EditUserForm();
        $countriesMapper = new Application_Model_CountriesMapper();

        $topCountriesList = $countriesMapper->getListForSelectTop();
        $form->fillTopCountries($topCountriesList);

        $bottomCountriesList = $countriesMapper->getListForSelectBottom();
        $form->fillBottomCountries($bottomCountriesList);

        $timezonesMapper = new Application_Model_TimezoneMapper();
        $timezonesList = $timezonesMapper->getListForSelect();
        $form->fillTimezones($timezonesList);

        $form->setCurrentPassword($user->getPassword());
        $this->view->form = $form;

        if ($this->_getParam('saved_flag', null) == 1) {
            $this->view->assign('isSaved', true);
        } else {
            $this->view->assign('isSaved', false);
        }

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($formData['new_password'] != '') {
                $form->allowPasswordValidation();
            }

            if ($form->isValid($formData)) {

                $user->setFirstname($formData['firstname']);
                $user->setLastname($formData['lastname']);
                $user->setPhone($formData['phone']);
                if (isset($formData['new_password'])
                    && $formData['new_password'] != ''
                ) {
                    $user->setPassword(md5($formData['new_password']));
                }

                $user->setCountry($formData['country']);
                $user->setTime_Zone_Name($formData['timezone']);

                $usersFacade = new ZendExt_Facades_UsersFacade();
                $usersFacade->saveUser($user);

                $this->_helper->redirector(
                    'edit', 'settings', 'user', array('saved_flag' => '1')
                );
            } else {
                $form->populate($formData);
            }
        } else {
            $formData = array();
            $formData['firstname'] = $user->getFirstname();
            $formData['lastname'] = $user->getLastname();
            $formData['phone'] = $user->getPhone();
            $formData['country'] = $user->getCountry();
            $formData['timezone'] = $user->getTime_Zone_Name();
            $formData['email'] = $user->getEmail();

            $form->populate($formData);
        }

    }

}



