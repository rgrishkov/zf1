<?php

class User_PromotionController extends ZendExt_Controllers_WinGrinUserController
{

    public function listingAction()
    {

        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competition = $this->competition;
        $campaign = $this->campaign;
        $site = $this->site;

        $competitionId = $competition->getCampaign_Comp_Id();

        $this->view->assign('competitionId', $competitionId);
        $this->view->assign('campaign', $campaign);
        $this->view->assign('competition', $competition);
        $this->view->assign('site', $site);
        $this->view->assign('siteUrl', $site->getSite_Url());
        $this->view->assign('siteId', $site->getSite_Id());
        $this->assignGlobalContent('USER_CONFIG_POST_DETAILS', true);

        $form = new ZendExt_UserForms_PostDetailsForm();
        $form->setCompetitionId($competitionId);
        $form->setSiteId($site->getSite_Id());

        if ($competition->getCampaign_Comp_Status()
            == CAMPAIGN_STATUS_COMPLETED
        ) {
            $this->view->assign('isLocked', true);
            $form->lock();
        } elseif ($competition->getCampaign_Comp_Status()
            == CAMPAIGN_STATUS_RUNNING
        ) {
            $this->view->assign('isRunning', true);
        }

        $this->view->form = $form;

        $siteMapper = new Application_Model_SiteMapper();
        $postDetailsMapper = new Application_Model_CampaignPostDetailsMapper();
        $postDetails = new Application_Model_CampaignPostDetails();

        $formData = array();
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->setCurrentUrl($site->getSite_Url());

            if (isset($formData['upload_image'])) {
                $this->processImageUpload($formData);
            } elseif ($form->isValid($formData)) {

                if ((bool)$formData['allow_post_details'] == true) {

                    $postDescription = $campaign->getCampaign_Name() . ' '
                        . $site->getSite_Name();

                    $postDetails->setId($formData['post_details_id']);
                    $postDetails->setCampaign_Id($campaign->getCampaign_Id());
                    $postDetails->setPost_Title($formData['post_name']);
                    $postDetails->setPost_Name($formData['post_name']);
                    $postDetails->setPost_Keywords($postDescription);
                    $postDetails->setPost_Description($postDescription);
                    $postDetails->setPost_Content($formData['post_content']);
                    $postDetails->setPost_Exerpt(' ');
                    $postDetails->setPost_Conditions(' ');
                    $postDetails->setPromotion_Type('user promo');
                    $postDetails->setShow_Post_Features(1);
                    $postDetails->setFeature_Buddy_Deal(0);
                    $postDetails->setFeature_Skill_Question(1);

                    $postDetailsMapper->save($postDetails);

                    $site->setSite_Name($formData['site_name']);
                    $site->setSite_Url(
                        ZendExt_Utils_StringUtils::clearUrl(
                            $formData['site_url']
                        )
                    );
                    $site->setSite_Bio($formData['site_bio']);
                    $siteMapper->save($site);
                } else {
                    $postDetailsMapper->deletePostDetailsById(
                        $formData['post_details_id']
                    );
                }

                $this->_helper->redirector(
                    'broadcast', 'promotion', 'user',
                    array('competition_id' => $competitionId)
                );
            } else {
                $this->view->assign('isPostDetailsLoaded', true);
                $this->showTopErrorBox();
                $form->populate($formData);
            }
        } else {
            $postDetailsMapper
                = new Application_Model_CampaignPostDetailsMapper();
            $postDetails = new Application_Model_CampaignPostDetails();

            $isPostDetailsLoaded
                = $postDetailsMapper->loadPostDetailsByCampaignId(
                $campaign->getCampaign_Id(), $postDetails
            );
            $this->view->assign('isPostDetailsLoaded', $isPostDetailsLoaded);

            $formData['post_details_id'] = $postDetails->getId();
            $formData['post_name'] = $postDetails->getPost_Name();
            $formData['post_content'] = $postDetails->getPost_Content();
            $formData['site_name'] = $site->getSite_Name();
            $formData['site_url'] = $site->getSite_Url();
            $formData['site_bio'] = $site->getSite_Bio();
            $formData['site_id'] = $site->getSite_Id();
            $formData['external_site_id'] = $site->getExternal_Site_Id();

            $mediaFile = new Application_Model_MediaFile();
            $mediaFileMapper = new Application_Model_MediaFileMapper();
            $isLoaded = $mediaFileMapper->loadSiteLogo(
                $site->getSite_Id(), $mediaFile
            );

            if ($isLoaded == true) {
                $formData['media_file'] = $mediaFile;
            }

            $form->populate($formData);
        }
    }

    public function broadcastAction()
    {

        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competition = $this->competition;
        $campaign = $this->campaign;
        $site = $this->site;

        $competitionId = $competition->getCampaign_Comp_Id();

        $this->view->assign('competitionId', $competitionId);
        $this->view->assign('campaign', $campaign);
        $this->view->assign('competition', $competition);
        $this->view->assign('site', $site);
        $this->view->assign('siteUrl', $site->getSite_Url());
        $this->assignGlobalContent('USER_CONFIG_POST_DETAILS', true);

        $form = new ZendExt_UserForms_PromotionForm();
        $form->setCompetitionId($competitionId);

        if ($competition->getCampaign_Comp_Status()
            == CAMPAIGN_STATUS_COMPLETED
        ) {
            $this->view->assign('isLocked', true);
            $form->lock();
        } elseif ($competition->getCampaign_Comp_Status()
            == CAMPAIGN_STATUS_RUNNING
        ) {
            $this->view->assign('isRunning', true);
        }

        $this->view->form = $form;

        $formData = array();
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {

                $competition->setMail_Promotion($formData['mail_promotion']);
                $competition->setTwitter_Promotion(
                    $formData['twitter_promotion']
                );
                $competition->setFacebook_Promotion(
                    $formData['facebook_promotion']
                );

                $competitionMapper
                    = new Application_Model_CampaignTokenCompMapper();
                $competitionMapper->save($competition);

                $this->_helper->redirector(
                    'offer', 'promotion', 'user',
                    array('competition_id' => $competitionId)
                );
            } else {
                $this->showTopErrorBox();
                $form->populate($formData);
            }
        } else {
            $formData['mail_promotion'] = $competition->getMail_Promotion();
            $formData['twitter_promotion'] = $competition->getTwitter_Promotion(
            );
            $formData['facebook_promotion']
                = $competition->getFacebook_Promotion();

            $form->populate($formData);
        }
    }

    public function offerAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competition = $this->competition;
        $campaign = $this->campaign;
        $site = $this->site;

        $competitionId = $competition->getCampaign_Comp_Id();

        $this->view->assign('competitionId', $competitionId);
        $this->view->assign('campaign', $campaign);
        $this->view->assign('competition', $competition);
        $this->view->assign('site', $site);
        $this->view->assign('siteUrl', $site->getSite_Url());
        $this->assignGlobalContent('USER_CONFIG_POST_DETAILS', true);

        $form = new ZendExt_UserForms_SecondChanceOfferForm();
        $form->setCompetitionId($competitionId);

        if ($competition->getCampaign_Comp_Status()
            == CAMPAIGN_STATUS_COMPLETED
        ) {
            $this->view->assign('isLocked', true);
            $form->lock();
        } elseif ($competition->getCampaign_Comp_Status()
            == CAMPAIGN_STATUS_RUNNING
        ) {
            $this->view->assign('isRunning', true);
        }

        $this->view->form = $form;

        $competitionMapper = new Application_Model_CampaignTokenCompMapper();

        $formData = array();
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            if ($form->isValid($formData)) {

                if ((bool)$formData['allow_second_chance_offer'] == true) {
                    $competition->setSecond_Chance_Offer_Summary(
                        $formData['offer_summary']
                    );
                    $competition->setSecond_Chance_Offer_Details(
                        $formData['offer_details']
                    );
                } else {
                    $competition->setSecond_Chance_Offer_Summary('');
                    $competition->setSecond_Chance_Offer_Details('');
                }

                $competitionMapper->save($competition);

                $this->_helper->redirector(
                    'summary', 'competitions', 'user',
                    array('competition_id' => $competitionId)
                );
            } else {
                $this->view->assign('isSecondChanceOfferLoaded', true);
                $this->showTopErrorBox();
                $form->populate($formData);
            }
        } else {
            if ($competition->getSecond_Chance_Offer_Summary() != null
                || $competition->getSecond_Chance_Offer_Details() != null
            ) {
                $this->view->assign('isSecondChanceOfferLoaded', true);
            } else {
                $this->view->assign('isSecondChanceOfferLoaded', false);
            }
            $formData['offer_summary']
                = $competition->getSecond_Chance_Offer_Summary();
            $formData['offer_details']
                = $competition->getSecond_Chance_Offer_Details();

            $form->populate($formData);
        }
    }

    /* public function ajaxImageUploadAction()
     {
         $this->_helper->viewRenderer->setNoRender(true);
         $this->view->layout()->disableLayout();
 
         $form = new ZendExt_UserForms_PostDetailsForm();
         $uploadForm = $form->getElement('upload_logo');
 
         $mediaFileMapper = new Application_Model_MediaFileMapper();
         $mediaFile = new Application_Model_MediaFile();
 
         $formData = $this->getRequest()->getPost();
         $siteId = $formData['site_id'];
         $site = new Application_Model_Site();
         $siteMapper = new Application_Model_SiteMapper();
         $siteMapper->loadSiteById($siteId, $site);
 
         if ($uploadForm->isValid($formData)){
             ZendExt_Utils_FilesystemUtils::configureSiteUploadFolders($site->getExternal_Site_Id());
 
             $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'development');
 
             $uploadPath = $config->upload_path->sites.'/'.$site->getExternal_Site_Id().'/images';
             $thumbnailsPath = $config->upload_path->sites.'/'.$site->getExternal_Site_Id().'/thumbnails';
 
             $upload = new Zend_File_Transfer_Adapter_Http();
 
              $fileInfo = $upload->getFileInfo();
             $newName = trim($fileInfo['image_file']['name']);
             $newName = str_replace(' ', '_', $newName);
             $newName = time().'_'.$newName;
 
              $imageUrl = '/sites/'.$site->getExternal_Site_Id().'/images/'.$newName;
 
              $upload->addFilter('Rename', $newName);
              $upload->setDestination($uploadPath);
 
 
              $upload->receive();
 
             $fullSizeImage = new ZendExt_Image_FullSizeImage($uploadPath."/". $newName);
             $thumbnail = new ZendExt_Image_Thumbnail($fullSizeImage, "64", "64");
             $thumbnail->setPath($thumbnailsPath);
             $thumbnail->save();
 
             $loadResult = $mediaFileMapper->loadSiteLogo($siteId, $mediaFile);
             if ($loadResult == true) {
                 ZendExt_Utils_FilesystemUtils::deleteSiteImage($site->getExternal_Site_Id(), $mediaFile->getMedia_Name());
             }
 
             //$mediaFile->setMedia_Id();
             $mediaFile->setMedia_Location($imageUrl);
             $mediaFile->setMedia_Name($newName);
             $mediaFile->setMedia_Owner_Id($siteId);
             $mediaFile->setMedia_Owner_Type(MEDIA_OWNER_TYPE_SITE);
             $mediaFile->setMedia_Type(MEDIA_TYPE_LOGO);
             $mediaFileMapper->save($mediaFile);
 
             $uploadedImageUrl = $imagePath = $config->image->uploaded_img_prefix.$imageUrl;
             $thumbnailUrl = str_replace('/images/', '/thumbnails/', $uploadedImageUrl);
 
             $deleteImageUrl = $this->view->url(array('module' => 'user', 'controller' => 'sites', 'action' => 'deleteimage', 'owner_id' => $siteId, 'image_name' => $newName), null, true);
 
             $result = array(
                 'error' => false,
                 'image_name' => $newName,
                 'image_url' => $uploadedImageUrl,
                 'thumbnail_url' => $thumbnailUrl,
                 'image_size' => round($fileInfo['image_file']['size']/1024),
                 'delete_image_url' => $deleteImageUrl,
                 'image_id' => time()
             );
         } else {
 
             $result = array(
                 'error' => true,
                 'error_message' => $uploadForm->getErrorMessageText()
             );
         }
 
         $json = Zend_Json::encode($result);
         echo $json;
     }*/

    public function assignGlobalContent($contentId, $assignTestModeData = false)
    {
        $globalContentMapper = new Application_Model_GlobalContentMapper();
        $quickInfo = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById($contentId, $quickInfo);
        $this->view->assign('quickInfo', $quickInfo);

        if ($assignTestModeData == true) {
            /*$testModePopupContent = new Application_Model_GlobalContent();
            $globalContentMapper->loadContentById('USER_TEST_MODE_POPUP_SITE', $testModePopupContent);
            $this->view->assign('testModePopupContent', $testModePopupContent);

            $testModePageContent = new Application_Model_GlobalContent();
            $globalContentMapper->loadContentById('USER_TEST_MODE_PAGE', $testModePageContent);
            $this->view->assign('testModePageContent',$testModePageContent);

            $testModeDisabledContent = new Application_Model_GlobalContent();
            $globalContentMapper->loadContentById('USER_TEST_MODE_DISABLED', $testModeDisabledContent);
            $this->view->assign('testModeDisabledContent', $testModeDisabledContent);*/

            $postDetailsPageContent = new Application_Model_GlobalContent();
            $globalContentMapper->loadContentById(
                'USER_CONFIG_POST_DETAILS_PAGE', $postDetailsPageContent
            );
            $this->view->assign(
                'postDetailsPageContent', $postDetailsPageContent
            );

            $broadcastPageContent = new Application_Model_GlobalContent();
            $globalContentMapper->loadContentById(
                'USER_CONFIG_BROADCAST_PAGE', $broadcastPageContent
            );
            $this->view->assign('broadcastPageContent', $broadcastPageContent);

            $offerPageContent = new Application_Model_GlobalContent();
            $globalContentMapper->loadContentById(
                'USER_CONFIG_BROADCAST_PAGE', $offerPageContent
            );
            $this->view->assign('offerPageContent', $offerPageContent);

            $config = new Zend_Config_Ini(
                APPLICATION_PATH . '/configs/application.ini', 'test_mode'
            );
            $this->view->assign('testUrlPrefix', $config->test_url_prefix);
        }
    }


    /*public function editAction()
    {
        if ($this->_getParam('competition_id', null) == null) {
            throw new Exception('Invalid competition_id');
        }

        $competitionId = $this->_getParam('competition_id', null);
        $this->view->assign('competitionId', $competitionId);

        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignType = $campaignMapper->getCampaignTypeByCompetitionId($competitionId);

        if ($campaignType == CAMPAIGN_TYPE_COIN_TRIG) {
            $this->view->assign('showLocations', true);
        } else {
            $this->view->assign('showLocations', false);
        }

        $competition = new Application_Model_CampaignTokenComp();
        $competitionMapper = new Application_Model_CampaignTokenCompMapper();
        $competitionMapper->loadCampaignTokenCompById($competitionId, $competition);

        $campaignId = $competition->getCampaign_Id();
        $this->view->assign('campaignId', $campaignId);
        $siteId = Application_Model_CampaignTokenCompMapper::getSiteIdByCompId($competitionId);

        $campaignOrder = new Application_Model_CampaignOrder();
        $campaignOrderMapper = new Application_Model_CampaignOrderMapper();

        $site = new Application_Model_Site();
        $siteMapper = new Application_Model_SiteMapper();
    	$siteMapper->loadSiteById($siteId, $site);
        $this->view->assign('siteUrl', $site->getSite_Url());

        $globalContentMapper = new Application_Model_GlobalContentMapper();
        $quickInfo = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById('USER_CONFIG_PROMOTION', $quickInfo);
        $this->view->assign('quickInfo', $quickInfo);

        $productMapper = new Application_Model_ProductMapper();

        if ($campaignOrderMapper->loadOrderByCampaignId($campaignId, $campaignOrder) == true) {
            $product = new Application_Model_Product();
            $productMapper->loadProductById($campaignOrder->getProduct_Id(), $product);

            $orderOptionMapper = new Application_Model_CampaignOrderOptionMapper();
            $orderOptionsList = $orderOptionMapper->getListByOrderId($campaignOrder->getOrder_Id());

            $productOptionsList = array();
            $productOptionsMapper = new Application_Model_ProductOptionMapper();

            foreach($orderOptionsList as $orderOption) {
                $productOption = new Application_Model_ProductOption();
                $productOptionsMapper->loadProductOptionById($orderOption->getProduct_Option_Id(), $productOption);
                $productOptionsList[] = $productOption;

            }
            $product->setOptions_List($productOptionsList);
            //$product->setOptions_List($orderOptionsList);
            $this->view->assign('campaignOrder', $campaignOrder);
            $this->view->assign('loadedProduct', $product);
        }

        $productsList = $productMapper->getListWithOptions();
        $this->view->assign('productsList', $productsList);
    }

    public function saveAction()
    {
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $competitionId = $formData['competition_id'];
            $campaignId = $formData['campaign_id'];
            $orderId = $formData['order_id'];

            $campaignOrder = new Application_Model_CampaignOrder();
            $orderMapper = new Application_Model_CampaignOrderMapper();
            $orderOptionMapper = new Application_Model_CampaignOrderOptionMapper();

            if ($orderId != null) {

                $orderMapper->loadOrderById($orderId, $campaignOrder);
                $orderOptionMapper->deleteOrderOptions($orderId);
                //die('ok');
            }

            $campaignOrder->setCampaign_Id($campaignId);

            $product = new Application_Model_Product();
            $productMapper = new Application_Model_ProductMapper();
            $productMapper->loadProductById($formData['product_id'], $product);

            $campaignOrder->setProduct_Id($formData['product_id']);
            $campaignOrder->setName($product->getName());
            $campaignOrder->setList_Price($product->getList_Price());
            $campaignOrder->setStatus(ORDER_STATUS_PENDING);

            $orderId = $orderMapper->save($campaignOrder);

            if (isset($formData['options_ids']) && count($formData['options_ids']) > 0) {

                foreach($formData['options_ids'] as $optionId) {
                    $campaignOrderOption = new Application_Model_CampaignOrderOption();

                    $campaignOrderOption->setOrder_Id($orderId);
                    $campaignOrderOption->setProduct_Option_Id($optionId);

                    $productOption = new Application_Model_ProductOption();
                    $productOptionMapper = new Application_Model_ProductOptionMapper();
                    $productOptionMapper->loadProductOptionById($optionId, $productOption);

                    $campaignOrderOption->setName($productOption->getName());
                    $campaignOrderOption->setList_Price($productOption->getList_Price());

                    $orderOptionMapper->save($campaignOrderOption);
                }
            }

            $campaign = new Application_Model_Campaign();
            $campaignMapper = new Application_Model_CampaignMapper();
            $campaignMapper->loadCampaignById($campaignId, $campaign);
            ZendExt_Facades_CompetitionsFacade::changeCampaignStatus($campaign, CAMPAIGN_STATUS_INPROGRESS);

            $this->_helper->redirector('edit', 'competitions', 'user', array('competition_id' => $competitionId));
        }
    }*/


}







