<?php

class Application_Model_UsersMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Users');
        }
        return $this->_dbTable;
    }

    public function save(Application_Model_Users $user)
    {
        $data = array();

        if ($user->getEmail() != null) {
            $data['email'] = $user->getEmail();
        }
        if ($user->getFirstname() != null) {
            $data['firstname'] = $user->getFirstname();
        }
        if ($user->getLastname() != null) {
            $data['lastname'] = $user->getLastname();
        }

        if ($user->getCountry() != null) {
            $data['country'] = $user->getCountry();
        }
        if ($user->getTime_Zone_Name() != null) {
            $data['time_zone_name'] = $user->getTime_Zone_Name();
        }
        if ($user->getRole() != null) {
            $data['role'] = $user->getRole();
        }
        if ($user->getPassword() != null) {
            //$data['password'] = md5($user->getPassword());
            $data['password'] = $user->getPassword();
        }
        if ($user->getEnable() != null) {
            $data['enable'] = $user->getEnable();
        }
        if ($user->getPayment_Type() != null) {
            $data['payment_type'] = $user->getPayment_Type();
        }

        $data['phone'] = $user->getPhone();
        $data['password_recovery_key'] = $user->getPassword_Recovery_Key();
        $data['password_recovery_date'] = $user->getPassword_Recovery_Date();

        $findResult = $this->getDbTable()->find($user->getEmail());

        if ($user->getOldEmail() != null) {
            $findResultOldEmail = $this->getDbTable()->find(
                $user->getOldEmail()
            );
            $email = $user->getOldEmail();
        } else {
            $email = $user->getEmail();
            $findResultOldEmail = array();
        }

        if (count($findResult) > 0 || count($findResultOldEmail) > 0) {
            $this->getDbTable()->update($data, array('email = ?' => $email));
        } else {
            $this->getDbTable()->insert($data);
        }
    }

    public function loadUserByEmail($email, Application_Model_Users $user)
    {
        $result = $this->getDbTable()->find($email);
        if (count($result) > 0) {
            $row = $result->current();
            $this->setMainFields($user, $row);

            return true;
        } else {
            return false;
        }

    }

    public function loadUserByRecoveryKey($recoveryKey,
        Application_Model_Users $user
    ) {
        $select = $this->getDbTable()->select();
        $select->where('password_recovery_key = ?', $recoveryKey);
        $select->where('DATEDIFF(NOW(), password_recovery_date) = ?', 0);
        $select->where(
            'TIMEDIFF(NOW(), password_recovery_date) < ?', '01:00:00'
        );

        $result = $this->getDbTable()->fetchAll($select);
        if (count($result) > 0) {
            $row = $result->current();
            $this->setMainFields($user, $row);

            return true;
        } else {
            return false;
        }

    }

    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries = array();
        foreach ($resultSet as $row) {
            $entry = new Application_Model_Users();
            $this->setMainFields($entry, $row);
            $entries[] = $entry;
        }
        return $entries;
    }

    public function getUserForPaginate($pageNumber = 1, $itemsPerPage = 10,
        $searchParams = null
    ) {
        $select = $this->getDbTable()->select();

        if ($searchParams != null) {
            if ($searchParams['name'] != '') {
                $select->where(
                    'firstname LIKE ?', '%' . $searchParams['name'] . '%'
                );
                $select->orWhere(
                    'lastname LIKE ?', '%' . $searchParams['name'] . '%'
                );
            }
            if ($searchParams['email'] != '') {
                $select->where('email = ?', $searchParams['email']);
            }
        }

        $paginator = Zend_Paginator::factory($select);
        $paginator->setCurrentPageNumber($pageNumber);
        $paginator->setItemCountPerPage($itemsPerPage);
        $paginator->setPageRange(1);

        return $paginator;
    }

    public function setMainFields(Application_Model_Users $entry, $row)
    {
        $entry->setEmail($row['email']);
        $entry->setFirstname($row['firstname']);
        $entry->setLastname($row['lastname']);
        $entry->setPhone($row['phone']);
        $entry->setRole($row['role']);
        $entry->setPassword($row['password']);
        $entry->setEnable($row['enable']);
        $entry->setCountry($row['country']);
        $entry->setTime_Zone_Name($row['time_zone_name']);
        $entry->setPassword_Recovery_Key($row['password_recovery_key']);
        $entry->setPassword_Recovery_Date($row['password_recovery_date']);
        $entry->setPayment_Type($row['payment_type']);
    }

    public function deleteUserByEmail($email)
    {
        $this->getDbTable()->delete("email = '" . $email . "'");
    }

}

