<?php

class Application_Model_UserSitesMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_UserSites');
        }
        return $this->_dbTable;
    }

    public function saveUserSites($email, $sitesIdsList)
    {
        $sitesIdsList = array_unique($sitesIdsList);
        $this->getDbTable()->delete("user_email = '" . $email . "'");

        if (count($sitesIdsList) > 0) {
            foreach ($sitesIdsList as $siteId) {
                $this->getDbTable()->insert(
                    array('user_email' => $email, 'site_id' => $siteId)
                );
            }
        }
    }

    public function addUserSite($email, $siteId)
    {
        $this->getDbTable()->insert(
            array('user_email' => $email, 'site_id' => $siteId)
        );
    }

    public function isUserSite($email, $siteId)
    {
        $select = $this->getDbTable()->select();
        $select->where('user_email = ?', $email);
        $select->where('site_id = ?', $siteId);

        $result = $this->getDbTable()->fetchAll($select);
        if (count($result) > 0) {
            return true;
        } else {
            return false;
        }
    }

    /*public function save(Application_Model_Users $user)
    {
        $data = array();

    	if ($user->getEmail() != null) {
       		$data['email'] = $user->getEmail();
        }
        if ($user->getFirstname() != null) {
       		$data['firstname'] = $user->getFirstname();
        }
    	if ($user->getLastname() != null) {
       		$data['lastname'] = $user->getLastname();
        }
    	if ($user->getPhone() != null) {
       		$data['phone'] = $user->getPhone();
        }
        if ($user->getRole() != null) {
       		$data['role'] = $user->getRole();
        }
    	if ($user->getPassword() != null) {
       		$data['password'] = $user->getPassword();
        }
    	if ($user->getEnable() != null) {
       		$data['enable'] = $user->getEnable();
        }

        $findResult = $this->getDbTable()->find($user->getEmail());

        if ($user->getOldEmail() != null) {
        	$findResultOldEmail = $this->getDbTable()->find($user->getOldEmail());
        	$email = $user->getOldEmail();
        } else {
        	$email = $user->getEmail();
        	$findResultOldEmail = array();
        }

        if (count($findResult) > 0 || count($findResultOldEmail) > 0) {
            $this->getDbTable()->update($data, array('email = ?' => $email));
        } else {
        	$this->getDbTable()->insert($data);
        }
    }*/

    /*	public function loadUserByEmail($email, Application_Model_Users  $user)
        {
            $result = $this->getDbTable()->find($email);
            if (count($result) > 0) {
                $row = $result->current();

                $user->setEmail($row->email);
                   $user->setFirstname($row->firstname);
                   $user->setLastname($row->lastname);
                   $user->setPhone($row->phone);
                   $user->setRole($row->role);
                   $user->setPassword($row->password);
                   $user->setEnable($row->enable);

                   return true;
            } else {
                return false;
            }

        }*/

    /*	public function fetchAll()
        {
            $resultSet = $this->getDbTable()->fetchAll();
            $entries = array();
            foreach ($resultSet as $row) {
                $entry = new Application_Model_Users();

                $entry->setEmail($row->email);
                   $entry->setFirstname($row->firstname);
                   $entry->setLastname($row->lastname);
                   $entry->setPhone($row->phone);
                   $entry->setRole($row->role);
                   $entry->setPassword($row->password);
                   $entry->setEnable($row->enable);
                $entries[] = $entry;
            }
            return $entries;
        }*/

    /*public function getSitesForPaginate($pageNumber = 1, $itemsPerPage = 10, $searchParams = null)
    {
        $select = $this->getDbTable()->select();

        if ($searchParams != null) {
            if ($searchParams['external_site_id'] != '') {
                $select->where('external_site_id = ?', $searchParams['external_site_id']);
            }
            if ($searchParams['site_url'] != '') {
                $select->where('site_url = ?', $searchParams['site_url']);
            }
        }

          $paginator = Zend_Paginator::factory($select);
          $paginator->setCurrentPageNumber($pageNumber);
          $paginator->setItemCountPerPage($itemsPerPage);
          $paginator->setPageRange(1);

          return $paginator;
    }*/


    /*public function deleteUserByEmail($email) {
    	$this->getDbTable()->delete("email = '".$email."'");
    }*/

}

