<?php

class Application_Model_UserMessageMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_UserMessage');
        }
        return $this->_dbTable;
    }

    public function save(Application_Model_UserMessage $userMessage)
    {
        $data = array();

        if ($userMessage->getUserEmail() != null) {
            $data['user_email'] = $userMessage->getUserEmail();
        }

        if ($userMessage->getSubject() != null) {
            $data['subject'] = $userMessage->getSubject();
        }

        if ($userMessage->getBody() != null) {
            $data['body'] = $userMessage->getBody();
        }

        if ($userMessage->getCreated_Date() != null) {
            $data['created_date'] = $userMessage->getCreated_Date();
        }

        if ($userMessage->getFrom() != null) {
            $data['from'] = $userMessage->getFrom();
        }

        if ($userMessage->getMessage_Id() != null) {
            $this->getDbTable()->update(
                $data, array('message_id = ?' => $userMessage->getMessage_Id())
            );
            $messageId = $userMessage->getMessage_Id();
        } else {
            $messageId = $this->getDbTable()->insert($data);
        }

        return $messageId;
    }

    public function getLatestUserMessages($userEmail)
    {
        $select = $this->getDbTable()->select();
        $select->where('user_email = ?', $userEmail);
        $select->order('created_date DESC');
        $select->limit(5, 0);

        $result = $this->getDbTable()->fetchAll($select);
        $entries = array();

        foreach ($result as $row) {
            $entry = new Application_Model_UserMessage();

            $entry->setMessage_Id($row['message_id']);
            $entry->setUserEmail($row['user_email']);
            $entry->setSubject($row['subject']);
            $entry->setBody($row['body']);
            $entry->setCreated_Date($row['created_date']);
            $entry->setFrom($row['from']);

            $entries[] = $entry;
        }
        return $entries;
    }


}

