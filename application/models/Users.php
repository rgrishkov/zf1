<?php

class Application_Model_Users
{
    const ADMIN_ROLE_ID = 2;

    protected $_email;
    protected $_firstname;
    protected $_lastname;
    protected $_phone;
    protected $_country;
    protected $_time_zone_name;
    protected $_role;
    protected $_password;
    protected $_enable;
    protected $_oldEmail;
    protected $_userSitesIds;
    protected $_password_recovery_key;
    protected $_password_recovery_date;
    protected $_payment_type;


    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setEmail($email)
    {
        $this->_email = (string)$email;
        return $this;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setFirstname($firstname)
    {
        $this->_firstname = (string)$firstname;
        return $this;
    }

    public function getFirstname()
    {
        return $this->_firstname;
    }

    public function setLastname($lastname)
    {
        $this->_lastname = (string)$lastname;
        return $this;
    }

    public function getLastname()
    {
        return $this->_lastname;
    }

    public function setPhone($phone)
    {
        $this->_phone = (string)$phone;
        return $this;
    }

    public function getPhone()
    {
        return $this->_phone;
    }

    public function setCountry($country)
    {
        $this->_country = $country;
        return $this;
    }

    public function getCountry()
    {
        return $this->_country;
    }

    public function setTime_Zone_Name($timeZoneName)
    {
        $this->_time_zone_name = $timeZoneName;
        return $this;
    }

    public function getTime_Zone_Name()
    {
        return $this->_time_zone_name;
    }

    public function setRole($role)
    {
        $this->_role = (string)$role;
        return $this;
    }

    public function getRole()
    {
        return $this->_role;
    }

    public function setPassword($password)
    {
        $this->_password = (string)$password;
        return $this;
    }

    public function getPassword()
    {
        return $this->_password;
    }

    public function setEnable($enable)
    {
        $this->_enable = (string)$enable;
        return $this;
    }

    public function getEnable()
    {
        return $this->_enable;
    }

    public function setPassword_Recovery_Key($value)
    {
        $this->_password_recovery_key = $value;
        return $this;
    }

    public function getPassword_Recovery_Key()
    {
        return $this->_password_recovery_key;
    }

    public function setPassword_Recovery_Date($value)
    {
        $this->_password_recovery_date = $value;
        return $this;
    }

    public function getPassword_Recovery_Date()
    {
        return $this->_password_recovery_date;
    }

    public function setPayment_Type($value)
    {
        $this->_payment_type = $value;
        return $this;
    }

    public function getPayment_Type()
    {
        return $this->_payment_type;
    }

    public function setOldEmail($oldEmail)
    {
        $this->_oldEmail = (string)$oldEmail;
        return $this;
    }

    public function getOldEmail()
    {
        return $this->_oldEmail;
    }

    public function setUserSitesIds($userSitesIds)
    {
        $this->_userSitesIds = $userSitesIds;
        return $this;
    }

    public function getUserSitesIds()
    {
        return $this->_userSitesIds;
    }

    public function isAdmin()
    {
        if ($this->getRole() == self::ADMIN_ROLE_ID) {
            return true;
        } else {
            return false;
        }
    }
}

