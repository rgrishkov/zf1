<?php

class Application_Model_UserMessage
{
    protected $_message_id;
    protected $_user_email;
    protected $_subject;
    protected $_body;
    protected $_created_date;
    protected $_from;


    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid site property');
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid site property');
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setMessage_Id($value)
    {
        $this->_message_id = (int)$value;
        return $this;
    }

    public function getMessage_Id()
    {
        return $this->_message_id;
    }

    public function setUserEmail($value)
    {
        $this->_user_email = $value;
        return $this;
    }

    public function getUserEmail()
    {
        return $this->_user_email;
    }

    public function setSubject($value)
    {
        $this->_subject = $value;
        return $this;
    }

    public function getSubject()
    {
        return $this->_subject;
    }

    public function setBody($value)
    {
        $this->_body = $value;
        return $this;
    }

    public function getBody()
    {
        return $this->_body;
    }

    public function setCreated_Date($value)
    {
        $this->_created_date = $value;
        return $this;
    }

    public function getCreated_Date()
    {
        return $this->_created_date;
    }

    public function setFrom($value)
    {
        $this->_from = $value;
        return $this;
    }

    public function getFrom()
    {
        return $this->_from;
    }

    public function getFormatedCreatedDate()
    {
        $createdDate = new Zend_Date($this->getCreated_Date());
        $formatedDate = $createdDate->toString('yyyy/MM/dd HH:mm');

        return $formatedDate;
    }

}

