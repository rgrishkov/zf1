<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    public function run()
    {
        Zend_Controller_Front::getInstance()->setBaseUrl();
        Zend_Controller_Front::getInstance()->addControllerDirectory(
            APPLICATION_PATH . '/user/controllers', 'user'
        );
        Zend_Controller_Front::getInstance()->addControllerDirectory(
            APPLICATION_PATH . '/controllers', 'default'
        );
        parent::run();

    }

}

