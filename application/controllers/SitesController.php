<?php

class SitesController extends ZendExt_Controllers_WinGrinAdminController
{

    public function indexAction()
    {
        $form = new ZendExt_Forms_SearchSitesForm();
        $this->view->form = $form;

        $searchParams = null;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $searchParams = $formData;
            }
            $form->populate($formData);
        }


        $paginateParamsForm = new ZendExt_Forms_PaginateParamsForm();
        $paginateParamsForm->setAction(
            $this->view->url(array('controller' => 'sites'))
        );

        $itemsPerPage = (int)$this->_getParam('per_page', 10);
        $this->view->paginateParamsForm = $paginateParamsForm;
        $this->view->paginateParamsForm->populate(
            array('per_page' => $itemsPerPage)
        );

        $pageNumber = (int)$this->_getParam('page', 1);

        $siteMapper = new Application_Model_SiteMapper();
        $paginatedSites = $siteMapper->getSitesForPaginate(
            $pageNumber, $itemsPerPage, $searchParams
        );

        $itemsStart = ($pageNumber - 1) * $paginatedSites->getCurrentItemCount()
            + 1;
        $itemsEnd = $pageNumber * $paginatedSites->getCurrentItemCount();
        $totalItems = $paginatedSites->getTotalItemCount();

        $this->view->itemsStart = $itemsStart;
        $this->view->itemsEnd = $itemsEnd;
        $this->view->totalItems = $totalItems;
        $this->view->itemsPerPage = $itemsPerPage;

        $this->view->paginatedSites = $paginatedSites;
    }

    public function addAction()
    {
        $form = new ZendExt_Forms_AddSiteForm();
        $this->view->form = $form;

        $externalSiteId = ZendExt_Utils_StringUtils::randomStr(20);
        $form->setExternalId($externalSiteId);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $site = new Application_Model_Site();

                $site->setExternal_Site_Id($formData['external_site_id']);
                $site->setSite_Name($formData['site_name']);
                $site->setSite_Url(
                    ZendExt_Utils_StringUtils::clearUrl($formData['site_url'])
                );
                $site->setSite_Bio($formData['site_bio']);

                $siteMapper = new Application_Model_SiteMapper();
                $siteId = $siteMapper->save($site);

                $this->_helper->redirector(
                    'edit', 'sites', null, array('id' => $siteId)
                );
            } else {
                $form->populate($formData);
            }
        }
    }

    public function editAction()
    {
        $form = new ZendExt_Forms_EditSiteForm();
        $this->view->form = $form;

        $referenceDataMapper = new Application_Model_ReferenceDataMapper();

        $siteImageTypes = $referenceDataMapper->getSiteImageTypes();
        $form->fillImageTypes($siteImageTypes);

        $siteMapper = new Application_Model_SiteMapper();
        $site = new Application_Model_Site();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $siteId = $formData['site_id'];
            $this->view->assign('siteId', $siteId);

            $siteMapper->loadSiteById($siteId, $site);

            if (isset($formData['upload_form']['upload_image'])) {
                $this->processImageUpload($formData);
            } elseif ($form->isValid($formData)) {

                $site->setSite_Id($siteId);
                $site->setExternal_Site_Id($formData['external_site_id']);
                $site->setSite_Name($formData['site_name']);
                $site->setSite_Url(
                    ZendExt_Utils_StringUtils::clearUrl($formData['site_url'])
                );
                $site->setSite_Bio($formData['site_bio']);

                $siteMapper->save($site);

                if (isset($formData['images_types_list'])) {
                    $this->saveImagesTypes($formData);
                }

                $this->_helper->redirector(
                    'edit', 'sites', null, array('id' => $siteId)
                );
            } else {
                $formData['media_files_list'] = $this->getSiteImagesList(
                    $siteId
                );
                $form->populate($formData);
            }
        } else {
            $siteId = $this->_getParam('id', null);
            $loadResult = $siteMapper->loadSiteById($siteId, $site);
            $this->view->assign('siteId', $siteId);

            if ($loadResult != false) {
                $this->view->assign('siteUrl', $site->getSite_Url());

                $formData = array();
                $formData['site_id'] = $site->getSite_Id();
                $formData['external_site_id'] = $site->getExternal_Site_Id();
                $formData['site_name'] = $site->getSite_Name();
                $formData['site_url'] = $site->getSite_Url();
                $formData['site_bio'] = $site->getSite_Bio();

                $formData['media_files_list'] = $this->getSiteImagesList(
                    $siteId
                );
                $form->populate($formData);
            }
        }

        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignsList = $campaignMapper->getListBySiteId($siteId);

        $this->view->assign("campaignsList", $campaignsList);

    }

    public function processImageUpload($formData)
    {
        $uploadForm = $this->view->form->getSubForm('upload_form');

        if ($uploadForm->isValid($formData['upload_form'])) {

            $siteId = $formData['site_id'];
            $site = new Application_Model_Site();
            $siteMapper = new Application_Model_SiteMapper();
            $siteMapper->loadSiteById($siteId, $site);

            ZendExt_Utils_FilesystemUtils::configureSiteUploadFolders(
                $site->getExternal_Site_Id()
            );

            $config = new Zend_Config_Ini(
                APPLICATION_PATH . '/configs/application.ini', 'development'
            );

            $uploadPath = $config->upload_path->sites . '/'
                . $site->getExternal_Site_Id() . '/images';
            $thumbnailsPath = $config->upload_path->sites . '/'
                . $site->getExternal_Site_Id() . '/thumbnails';

            $upload = new Zend_File_Transfer_Adapter_Http();

            $fileInfo = $upload->getFileInfo();
            $newName = time() . '_' . $fileInfo['site_logo']['name'];

            $imageUrl = '/sites/' . $site->getExternal_Site_Id() . '/images/'
                . $newName;

            $upload->addFilter('Rename', $newName);
            $upload->setDestination($uploadPath);

            /* try {
                 $upload->receive();
             } catch (Zend_File_Transfer_Exception $e) {
                 $e->getMessage();
             }*/
            $upload->receive();

            $fullSizeImage = new ZendExt_Image_FullSizeImage(
                $uploadPath . "/" . $newName
            );
            $thumbnail = new ZendExt_Image_Thumbnail(
                $fullSizeImage, "64", "64"
            );
            $thumbnail->setPath($thumbnailsPath);
            $thumbnail->save();

            $mediaFileMapper = new Application_Model_MediaFileMapper();
            $mediaFile = new Application_Model_MediaFile();
            //$mediaFile->setMedia_Id();
            $mediaFile->setMedia_Location($imageUrl);
            $mediaFile->setMedia_Name($newName);
            $mediaFile->setMedia_Owner_Id($siteId);
            $mediaFile->setMedia_Owner_Type(MEDIA_OWNER_TYPE_SITE);
            $mediaFile->setMedia_Type(MEDIA_TYPE_OTHER);
            $mediaFileMapper->save($mediaFile);

            $this->_helper->redirector(
                'edit', 'sites', null, array('id' => $siteId)
            );
        } else {
            $formData['media_files_list'] = $this->getSiteImagesList(
                $formData['site_id']
            );
            $this->view->form->populate($formData);
        }
    }

    public function getSiteImagesList($siteId)
    {
        $mediaFileMapper = new Application_Model_MediaFileMapper();
        $mediaFilesList = $mediaFileMapper->getListByOwnerIdAndOwnerType(
            $siteId, MEDIA_OWNER_TYPE_SITE
        );

        return $mediaFilesList;
    }

    public function saveImagesTypes($formData)
    {
        $siteId = $formData['site_id'];

        $mediaFileMapper = new Application_Model_MediaFileMapper();


        foreach ($formData['images_types_list'] as $imageName => $imageType) {
            $mediaFile = new Application_Model_MediaFile();
            $mediaFileMapper->loadMediaByNameAndOwnerId(
                $imageName, $siteId, $mediaFile
            );

            $mediaFile->setMedia_Type($imageType);
            $mediaFileMapper->save($mediaFile);
        }
    }

    public function deleteimageAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $siteId = $this->_getParam('owner_id');
        $imageName = $this->_getParam('image_name');

        $mediaFileMapper = new Application_Model_MediaFileMapper();
        $mediaFile = new Application_Model_MediaFile();

        $isLoaded = $mediaFileMapper->loadMediaByNameAndOwnerId(
            $imageName, $siteId, $mediaFile
        );
        if ($isLoaded == true) {
            $mediaFileMapper->deleteFileById($mediaFile->getMedia_Id());

            $siteMapper = new Application_Model_SiteMapper();
            $site = new Application_Model_Site();
            $siteMapper->loadSiteById($siteId, $site);

            ZendExt_Utils_FilesystemUtils::deleteSiteImage(
                $site->getExternal_Site_Id(), $imageName
            );
        }
        $this->_helper->redirector(
            'edit', 'sites', null, array('id' => $siteId)
        );
    }

    public function jsonlistAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $siteUrl = $this->_getParam('site_url', null);

        $siteMapper = new Application_Model_SiteMapper();
        $sitesList = $siteMapper->getSitesForJsonList($siteUrl);

        //$this->_helper->viewRenderer->setNoRender(true);
        //$this->view->layout()->disableLayout();

        $json = Zend_Json::encode($sitesList);
        echo $json;
    }

    public function deleteAction()
    {
        $siteId = $this->_getParam('id', null);

        $siteMapper = new Application_Model_SiteMapper();
        $site = new Application_Model_Site();

        $loadResult = $siteMapper->loadSiteById($siteId, $site);

        if ($loadResult != false) {
            $siteMapper->deleteSiteById($siteId);
        }
        $this->_helper->redirector('index');
    }
}



