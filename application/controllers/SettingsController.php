<?php

class SettingsController extends ZendExt_Controllers_WinGrinAdminController
{

    public function messagesAction()
    {
        $form = new ZendExt_Forms_EditMessagesForm();
        $this->view->form = $form;

        $messagesMapper = new Application_Model_CampaignMessageMapper();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                foreach ($formData['messages'] as $messageType => $messageValue)
                {
                    $message = new Application_Model_CampaignMessage();
                    $message->setCampaign_Id(null);
                    $message->setMessage_Type($messageType);
                    $message->setMessage_Value($messageValue);
                    $message->setMessage_Is_Default('1');

                    $messagesMapper->save($message);
                }
            } else {
                $form->populate($formData);
            }
        } else {
            $formData = array();
            $defaultMessagesList = $messagesMapper->getDefaultMessages();

            foreach ($defaultMessagesList as $message) {

                $formData[$message->getMessage_Type()]
                    = $message->getMessage_Value();
            }
            $form->populate($formData);
        }
    }
}







