<?php

class CampaignsController extends ZendExt_Controllers_WinGrinAdminController
{


    public function indexAction()
    {
        $form = new ZendExt_Forms_SearchCampaignsForm();
        $this->view->form = $form;

        $searchParams = null;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $searchParams = $formData;
            }
            $form->populate($formData);
        }

        $paginateParamsForm = new ZendExt_Forms_PaginateParamsForm();

        $itemsPerPage = (int)$this->_getParam('per_page', 10);
        $this->view->paginateParamsForm = $paginateParamsForm;
        $this->view->paginateParamsForm->populate(
            array('per_page' => $itemsPerPage)
        );

        $pageNumber = (int)$this->_getParam('page', 1);


        $campaignMapper = new Application_Model_CampaignMapper();
        $paginatedCampaigns = $campaignMapper->getCampaignsForPaginate(
            $pageNumber, $itemsPerPage, $searchParams
        );

        $itemsStart = ($pageNumber - 1)
            * $paginatedCampaigns->getCurrentItemCount() + 1;
        $itemsEnd = $pageNumber * $paginatedCampaigns->getCurrentItemCount();
        $totalItems = $paginatedCampaigns->getTotalItemCount();

        $this->view->itemsStart = $itemsStart;
        $this->view->itemsEnd = $itemsEnd;
        $this->view->totalItems = $totalItems;
        $this->view->itemsPerPage = $itemsPerPage;

        $this->view->paginatedCampaigns = $paginatedCampaigns;
    }

    public function addAction()
    {
        $form = new ZendExt_Forms_AddCampaignForm();

        $externalCampaignId = ZendExt_Utils_StringUtils::randomStr(20);
        $form->setExternalId($externalCampaignId);

        $siteMapper = new Application_Model_SiteMapper();
        $sitesList = $siteMapper->fetchAll();
        $form->fillSitesList($sitesList);

        $locationMapper = new Application_Model_LocationMapper();
        $locationsTree = $locationMapper->getLocationsTree();
        $form->fillLocationsTree($locationsTree);

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $campaign = new Application_Model_Campaign();

                $campaign->setExternal_Campaign_Id(
                    $formData['external_campaign_id']
                );
                $campaign->setSite_Id($formData['site_id']);
                $campaign->setCampaign_Name($formData['campaign_name']);

                $campaignMapper = new Application_Model_CampaignMapper();
                $campaignId = $campaignMapper->save($campaign);

                if (isset($formData['location_cities_list'])) {
                    $campaignsLocationsMapper
                        = new Application_Model_CampaignsLocationsMapper();
                    $campaignsLocationsMapper->saveCampaignLocations(
                        $campaignId, $formData['location_cities_list']
                    );
                }

                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        }
    }

    public function editAction()
    {
        if ($this->_getParam('id', null) != null) {
            $campaignId = $this->_getParam('id', '');
        } elseif ($this->getRequest()->isPost()) {
            $campaignId = $this->_getParam('campaign_id', '');
        } else {
            throw new Exception('Invalid campaign id');
        }

        $campaignMapper = new Application_Model_CampaignMapper();
        $campaign = new Application_Model_Campaign();

        $loadResult = $campaignMapper->loadCampaignById($campaignId, $campaign);
        if ($loadResult == false) {
            throw new Exception('Invalid campaign id');
        }

        $this->view->assign('campaignId', $campaignId);
        $this->view->assign('campaignName', $campaign->getCampaign_Name());

        $form = new ZendExt_Forms_EditCampaignForm();
        $this->view->form = $form;

        $siteMapper = new Application_Model_SiteMapper();
        $sitesList = $siteMapper->fetchAll();
        $form->fillSitesList($sitesList);

        $site = new Application_Model_Site();
        $siteMapper->loadSiteById($campaign->getSite_Id(), $site);
        $this->view->assign('siteUrl', $site->getSite_Url());

        $locationMapper = new Application_Model_LocationMapper();
        $locationsTree = $locationMapper->getLocationsTree();
        $form->fillLocationsTree($locationsTree);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $campaign->setExternal_Campaign_Id(
                    $formData['external_campaign_id']
                );
                $campaign->setSite_Id($formData['site_id']);
                $campaign->setCampaign_Name($formData['campaign_name']);

                $campaignMapper = new Application_Model_CampaignMapper();
                $campaignMapper->save($campaign);

                if (isset($formData['location_cities_list'])) {
                    $campaignsLocationsMapper
                        = new Application_Model_CampaignsLocationsMapper();
                    $campaignsLocationsMapper->saveCampaignLocations(
                        $campaignId, $formData['location_cities_list']
                    );
                }

                $this->_helper->redirector(
                    'edit', 'campaigns', null,
                    array('id' => $formData['campaign_id'])
                );
            } else {
                $form->populate($formData);
            }
        } else {
            $formData = array();

            $formData['campaign_id'] = $campaignId;
            $formData['external_campaign_id']
                = $campaign->getExternal_Campaign_Id();
            $formData['site_id'] = $campaign->getSite_Id();
            $formData['campaign_name'] = $campaign->getCampaign_Name();

            $formData['campaign_locations']
                = $locationMapper->getCampaignLocations($campaignId);
            $form->populate($formData);
        }

    }

    /*    public function locationsAction() {

            if ($this->_getParam('id', null) != null) {
                $campaignId = $this->_getParam('id', '');
            } elseif($this->getRequest()->isPost()) {
                $campaignId = $this->_getParam('campaign_id', '');
            }

            $this->view->assign('campaignId', $campaignId);

            $locationsMapper = new Application_Model_CampaignTokenLocationMapper();

            $form = new ZendExt_Forms_CampaignLocationsForm();
            $form->setCampaignId($campaignId);

            $campaign = new Application_Model_Campaign();
            $campaignMapper = new Application_Model_CampaignMapper();
            $campaignMapper->loadCampaignById($campaignId, $campaign);

            $this->view->assign('siteUrl', $campaign->getSite_Url());
            $this->view->form = $form;

            $formData = array();

            if ($this->getRequest()->isPost()) {
                $formData = $this->getRequest()->getPost();

                if ($form->isValid($formData)) {
                    $locationsTexts = explode(',', $formData['match_text']);

                    foreach ($locationsTexts as $matchText) {

                        $location = new Application_Model_CampaignTokenLocation();

                        $matchText = trim($matchText, ' "\'');

                        $locationUrl =  $this->_getParam('page_url', null);
                        $locationUrl = ZendExt_Utils_StringUtils::clearUrl($locationUrl);

                        $regularExpression =  $this->_getParam('regular_expression', null);

                        $location->setCampaign_Id($formData['campaign_id']);
                        $location->setLocation_Id($formData['location_id']);
                        $location->setPage_Url($locationUrl);
                        $location->setRegex($regularExpression);
                        $location->setMatch_Text($matchText);

                        $location->setLocation_Type($formData['type']);
                        if ($formData['type'] == LOCATION_TYPE_DEFAULT) {
                            $location->setDefault(1);
                        }

                        $locationsMapper->save($location);
                    }
                    $this->_helper->redirector('locations', 'campaigns', null, array('id' => $campaignId));
                } else {
                    $form->populate($formData);
                }
            } elseif($this->_getParam('location', null) != null) {
                $locationId = $this->_getParam('location', null);

                $location = new Application_Model_CampaignTokenLocation();
                $locationsMapper->loadLocationById($locationId, $location);

                $formData['location_id'] = $location->getLocation_Id();
                $formData['campaign_id'] = $campaignId;
                $formData['page_url'] = $location->getPage_Url();
                $formData['match_text'] = $location->getMatch_Text();
                $formData['regular_expression'] = $location->getRegex();
                $formData['type'] = $location->getLocation_Type();

                $form->populate($formData);
            }

            $paginateParamsForm = new ZendExt_Forms_PaginateParamsForm();
            $paginateParamsForm->setAction('/campaigns/locations/id/'.$campaignId);

               $itemsPerPage = (int) $this->_getParam('per_page', 10);
               $this->view->paginateParamsForm = $paginateParamsForm;
            $this->view->paginateParamsForm->populate(array('per_page' => $itemsPerPage));

               $pageNumber = (int) $this->_getParam('page', 1);

               $paginatedLocations = $locationsMapper->getLocationsForPaginate($campaignId, $pageNumber, $itemsPerPage, null);

               $itemsStart = ($pageNumber-1)*$paginatedLocations->getCurrentItemCount() + 1;
               $itemsEnd = $pageNumber*$paginatedLocations->getCurrentItemCount();
               $totalItems = $paginatedLocations->getTotalItemCount();

               $this->view->itemsStart = $itemsStart;
               $this->view->itemsEnd = $itemsEnd;
               $this->view->totalItems = $totalItems;
               $this->view->itemsPerPage = $itemsPerPage;
               $this->view->paginatedLocations = $paginatedLocations;
        }*/

    /*	public function deletelocationsAction()
       {
           $this->_helper->viewRenderer->setNoRender(true);
           $this->view->layout()->disableLayout();

           $campaignId = $this->_getParam('id', '');

           if ($this->getRequest()->isPost()) {
               $formData = $this->getRequest()->getPost();
               $locationsIdList = $formData['locations_ids'];

               $locationMapper = new Application_Model_CampaignTokenLocationMapper();
               foreach ($locationsIdList as $locationId) {
                   $locationMapper->deleteLocationById($locationId);
               }
           }
           $this->_helper->redirector('locations', 'campaigns', null, array('id' => $campaignId));
       }*/

    public function questionsAction()
    {
        if ($this->_getParam('id', null) != null) {
            $campaignId = $this->_getParam('id', '');
        } elseif ($this->getRequest()->isPost()) {
            $campaignId = $this->_getParam('campaign_id', '');
        }

        $this->view->assign('campaignId', $campaignId);

        $questionMapper = new Application_Model_CampaignQuestionMapper();

        $form = new ZendExt_Forms_CampaignQuestionsForm();
        $form->setCampaignId($campaignId);


        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $competitionMapper = new Application_Model_CampaignTokenCompMapper();
        $competitionsList = $competitionMapper->getListByCampaignId(
            $campaignId
        );
        $form->populateCompetitionsList($competitionsList);

        //$form->setSiteUrl($campaign->getSite_Url());
        $this->view->form = $form;

        $this->view->assign('siteUrl', $campaign->getSite_Url());

        $formData = array();
        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $campaignQuestion = new Application_Model_CampaignQuestion();

                $campaignQuestion->setCampaign_Comp_Id(
                    $formData['campaign_comp_id']
                );
                $campaignQuestion->setQuestion_Id($formData['question_id']);
                $campaignQuestion->setQuestion_Type($formData['question_type']);
                $campaignQuestion->setQuestion($formData['question']);
                $campaignQuestion->setAnswer_1($formData['answer1']);
                $campaignQuestion->setAnswer_2($formData['answer2']);
                $campaignQuestion->setAnswer_3($formData['answer3']);
                $campaignQuestion->setAnswer_4($formData['answer4']);
                $campaignQuestion->setAnswer_1_Correct(
                    $formData['answer1_correct']
                );
                $campaignQuestion->setAnswer_2_Correct(
                    $formData['answer2_correct']
                );
                $campaignQuestion->setAnswer_3_Correct(
                    $formData['answer3_correct']
                );
                $campaignQuestion->setAnswer_4_Correct(
                    $formData['answer4_correct']
                );
                $campaignQuestion->setQuestion_Hint($formData['question_hint']);

                $questionMapper->save($campaignQuestion);

                if ($campaign->getTest_Mode_Id() == null
                    && $campaign->isConfigured() == true
                ) {
                    $testModeId = ZendExt_Utils_StringUtils::randomStr(5);
                    $campaign->setTest_Mode_Id($testModeId);
                    $campaignMapper->save($campaign);
                }

                $this->_helper->redirector(
                    'questions', 'campaigns', null, array('id' => $campaignId)
                );
            } else {
                $form->populate($formData);
            }
        } elseif ($this->_getParam('question', null) != null) {
            $questionId = $this->_getParam('question', null);

            $campaignQuestion = new Application_Model_CampaignQuestion();
            $questionMapper->loadQuestionById($questionId, $campaignQuestion);

            $formData['campaign_comp_id']
                = $campaignQuestion->getCampaign_Comp_Id();
            $formData['question_id'] = $campaignQuestion->getQuestion_Id();
            $formData['question_type'] = $campaignQuestion->getQuestion_Type();
            $formData['question'] = $campaignQuestion->getQuestion();
            $formData['answer1'] = $campaignQuestion->getAnswer_1();
            $formData['answer2'] = $campaignQuestion->getAnswer_2();
            $formData['answer3'] = $campaignQuestion->getAnswer_3();
            $formData['answer4'] = $campaignQuestion->getAnswer_4();
            $formData['answer1_correct']
                = $campaignQuestion->getAnswer_1_Correct();
            $formData['answer2_correct']
                = $campaignQuestion->getAnswer_2_Correct();
            $formData['answer3_correct']
                = $campaignQuestion->getAnswer_3_Correct();
            $formData['answer4_correct']
                = $campaignQuestion->getAnswer_4_Correct();
            $formData['question_hint'] = $campaignQuestion->getQuestion_Hint();

            $form->populate($formData);
        }

        $paginateParamsForm = new ZendExt_Forms_PaginateParamsForm();
        $paginateParamsForm->setAction(
            '/campaigns/questions/id/' . $campaignId
        );

        $itemsPerPage = (int)$this->_getParam('per_page', 10);
        $this->view->paginateParamsForm = $paginateParamsForm;
        $this->view->paginateParamsForm->populate(
            array('per_page' => $itemsPerPage)
        );

        $pageNumber = (int)$this->_getParam('page', 1);

        $paginatedQuestions = $questionMapper->getQuestionsForPaginate(
            $campaignId, $pageNumber, $itemsPerPage
        );

        $itemsStart = ($pageNumber - 1)
            * $paginatedQuestions->getCurrentItemCount() + 1;
        $itemsEnd = $pageNumber * $paginatedQuestions->getCurrentItemCount();
        $totalItems = $paginatedQuestions->getTotalItemCount();

        $this->view->itemsStart = $itemsStart;
        $this->view->itemsEnd = $itemsEnd;
        $this->view->totalItems = $totalItems;
        $this->view->itemsPerPage = $itemsPerPage;
        $this->view->paginatedQuestions = $paginatedQuestions;
    }

    public function deletequestionsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $siteId = $this->_getParam('id', '');

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $questionsIdList = $formData['questions_ids'];

            $questionMapper = new Application_Model_CampaignQuestionMapper();
            foreach ($questionsIdList as $questionId) {
                $questionMapper->deleteQuestionById($questionId);
            }
        }
        $this->_helper->redirector(
            'questions', 'campaigns', null, array('id' => $siteId)
        );
    }

    /*public function assignquestionsAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $campaignId = $this->_getParam('id', '');

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $questionsIdList = $formData['questions_ids'];

            $campaignQuestionMapper = new Application_Model_CampaignQuestionMapper();
            $campaignQuestionMapper->saveCampaignQuestions($campaignId, $questionsIdList);
        }
        $this->_helper->redirector('questions', 'campaigns', null, array('id' => $campaignId));
    }*/

    public function detailsAction()
    {
        if ($this->_getParam('id', null) != null) {
            $campaignId = $this->_getParam('id', '');
        } elseif ($this->getRequest()->isPost()) {
            $campaignId = $this->_getParam('campaign_id', '');
        }

        $form = new ZendExt_Forms_PostDetailsForm();
        $this->view->form = $form;

        $referenceDataMapper = new Application_Model_ReferenceDataMapper();

        $campaignImageTypes = $referenceDataMapper->getCampaignImageTypes();
        $form->fillImageTypes($campaignImageTypes);

        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $this->view->assign('campaignId', $campaignId);
        $this->view->assign('campaignName', $campaign->getCampaign_Name());

        $siteMapper = new Application_Model_SiteMapper();
        $site = new Application_Model_Site();

        $siteMapper->loadSiteById($campaign->getSite_Id(), $site);
        $this->view->assign('siteUrl', $site->getSite_Url());

        $postDetailsMapper = new Application_Model_CampaignPostDetailsMapper();
        $postDetails = new Application_Model_CampaignPostDetails();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $formData['campaign_id'] = $campaignId;

            if (isset($formData['upload_form']['upload_image'])) {
                $this->processImageUpload($formData);
            } elseif ($form->isValid($formData)) {
                $postDetails->setId($formData['post_details_id']);
                $postDetails->setCampaign_Id($campaignId);
                $postDetails->setPost_Title($formData['post_title']);
                $postDetails->setPost_Name($formData['post_name']);
                $postDetails->setPost_Keywords($formData['post_keywords']);
                $postDetails->setPost_Description(
                    $formData['post_description']
                );
                $postDetails->setPost_Content($formData['post_content']);
                $postDetails->setPost_Exerpt($formData['post_exerpt']);
                $postDetails->setPost_Conditions($formData['post_conditions']);
                $postDetails->setPromotion_Type($formData['promotion_type']);
                $postDetails->setShow_Post_Features(
                    $formData['show_post_features']
                );
                $postDetails->setFeature_Buddy_Deal(
                    $formData['feature_buddy_deal']
                );
                $postDetails->setFeature_Skill_Question(
                    $formData['feature_skill_question']
                );

                $postDetailsMapper->save($postDetails);

                if (isset($formData['images_types_list'])) {
                    $this->saveImagesTypes($formData);
                }

                $this->_helper->redirector(
                    'details', 'campaigns', null, array('id' => $campaignId)
                );
            } else {
                $formData['media_files_list'] = $this->getCampaignImagesList(
                    $campaignId
                );
                $form->populate($formData);
            }
        } else {
            $loadResult = $postDetailsMapper->loadPostDetailsByCampaignId(
                $campaignId, $postDetails
            );

            if ($loadResult != false) {

                $formData = array();
                $formData['post_details_id'] = $postDetails->getId();
                $formData['post_title'] = $postDetails->getPost_Title();
                $formData['post_name'] = $postDetails->getPost_Name();
                $formData['post_keywords'] = $postDetails->getPost_Keywords();
                $formData['post_description']
                    = $postDetails->getPost_Description();
                $formData['post_content'] = $postDetails->getPost_Content();
                $formData['post_exerpt'] = $postDetails->getPost_Exerpt();
                $formData['post_conditions'] = $postDetails->getPost_Conditions(
                );
                $formData['promotion_type'] = $postDetails->getPromotion_Type();
                $formData['show_post_features']
                    = $postDetails->getShow_Post_Features();
                $formData['feature_buddy_deal']
                    = $postDetails->getFeature_Buddy_Deal();
                $formData['feature_skill_question']
                    = $postDetails->getFeature_Skill_Question();

                $formData['publish_date'] = $postDetails->getPublish_Date();
                $formData['is_live'] = $postDetails->getIs_Live();
                $formData['post_id'] = $postDetails->getPost_Id();
            } else {
                $formData['publish_date'] = '';
                $formData['is_live'] = '';
                $formData['post_id'] = '';
            }

            $formData['campaign_id'] = $campaignId;
            $formData['external_campaign_id']
                = $campaign->getExternal_Campaign_Id();
            $formData['media_files_list'] = $this->getCampaignImagesList(
                $campaignId
            );

            $publishBox = new ZendExt_Elements_PublishBox('publish_box');
            $publishBox->populate($formData);
            $this->view->assign('publishBox', $publishBox);

            $form->populate($formData);
        }

    }


    public function publishAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        if ($this->_getParam('id', null) != null) {
            $campaignId = $this->_getParam('id', '');
        } elseif ($this->getRequest()->isPost()) {
            $campaignId = $this->_getParam('campaign_id', '');
        }

        $isLive = $this->_getParam('is_live', null);

        $postDetailsMapper = new Application_Model_CampaignPostDetailsMapper();
        $postDetails = new Application_Model_CampaignPostDetails();

        $loadResult = $postDetailsMapper->loadPostDetailsByCampaignId(
            $campaignId, $postDetails
        );
        if ($loadResult != false) {
            $postDetails->setIs_Live($isLive);

            $campaign = new Application_Model_Campaign();
            $campaignMapper = new Application_Model_CampaignMapper();
            $campaignMapper->loadCampaignById($campaignId, $campaign);

            $site = new Application_Model_Site();
            $siteMapper = new Application_Model_SiteMapper();
            $siteMapper->loadSiteById($campaign->getSite_Id(), $site);

            $mediaFile = new Application_Model_MediaFile();
            $mediaFileMapper = new Application_Model_MediaFileMapper();
            $mediaFileMapper->loadSiteLogo($campaign->getSite_Id(), $mediaFile);

            $locationsMapper = new Application_Model_LocationMapper();
            $locationsList = $locationsMapper->getCampaignLocations(
                $campaignId
            );

            $wordpressAdapter = new ZendExt_Adapters_WordpressApiAdapter();

            $postId = $postDetails->getPost_Id();
            $currentDate = new Zend_Date();

            if (count($locationsList) == 0) {
                throw new Exception(
                    'No locations specified for campaign. Cannot publish to anywhere.'
                );
            }

            if ($postId == null) {
                $publishResult = $wordpressAdapter->newPost(
                    $postDetails, $campaign, $site, $mediaFile, $locationsList
                );
            } else {
                $publishResult = $wordpressAdapter->updatePost(
                    $postDetails, $campaign, $site, $mediaFile, $locationsList
                );
            }

            if (count($publishResult) > 0) {
                if ($postId == null) {
                    $postDetails->setPost_Id($publishResult[0]['post_id']);
                    $postDetails->setPublish_Site_Url(
                        $publishResult[0]['site_url']
                    );

                }
                $postDetails->setPublish_Date(
                    $currentDate->toString('YYYY-MM-dd HH:mm:ss')
                );
                $postDetailsMapper->save($postDetails);
            }
            ZendExt_Facades_CompetitionsFacade::changeCampaignStatus(
                $campaign, CAMPAIGN_STATUS_PUBLISHED
            );
            $this->_helper->redirector(
                'details', 'campaigns', null, array('id' => $campaignId)
            );
        } else {
            throw new Exception(
                'Please inset correct post data before publishing'
            );
        }

    }

    public function publishwinnersAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        if ($this->_getParam('id', null) != null) {
            $campaignId = $this->_getParam('id', '');
        } elseif ($this->getRequest()->isPost()) {
            $campaignId = $this->_getParam('campaign_id', '');
        }


        $postDetailsMapper = new Application_Model_CampaignPostDetailsMapper();
        $postDetails = new Application_Model_CampaignPostDetails();

        $loadResult = $postDetailsMapper->loadPostDetailsByCampaignId(
            $campaignId, $postDetails
        );
        if ($loadResult != false) {
            if ($postDetails->getPost_Id() != null) {

                $campaignTokenCompMapper
                    = new Application_Model_CampaignTokenCompMapper();
                $competitionsList
                    = $campaignTokenCompMapper->getListByCampaignId(
                    $campaignId
                );

                $locationsMapper = new Application_Model_LocationMapper();
                $locationsList = $locationsMapper->getCampaignLocations(
                    $campaignId
                );

                $wordpressAdapter = new ZendExt_Adapters_WordpressApiAdapter();

                $postId = $postDetails->getPost_Id();
                $currentDate = new Zend_Date();

                $publishResult = $wordpressAdapter->publishWinners(
                    $postDetails, $competitionsList, $locationsList
                );

                if (count($publishResult) > 0) {
                    $postDetails->setPublish_Winners_Date(
                        $currentDate->toString('YYYY-MM-dd HH:mm:ss')
                    );
                    $postDetailsMapper->save($postDetails);
                }

                $this->_helper->redirector(
                    'results', 'campaigns', null, array('id' => $campaignId)
                );
            } else {
                throw new Exception(
                    'Please publish post before winners publishing'
                );
            }
        } else {
            throw new Exception(
                'Please inset correct post data before publishing'
            );
        }

    }

    public function processImageUpload($formData)
    {
        $uploadForm = $this->view->form->getSubForm('upload_form');

        $campaignId = $formData['campaign_id'];
        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        if ($uploadForm->isValid($formData['upload_form'])) {

            ZendExt_Utils_FilesystemUtils::configureCampaignUploadFolders(
                $campaign->getExternal_Campaign_Id()
            );

            $config = new Zend_Config_Ini(
                APPLICATION_PATH . '/configs/application.ini', 'development'
            );

            $uploadPath = $config->upload_path->campaigns . '/'
                . $campaign->getExternal_Campaign_Id() . '/images';
            $thumbnailsPath = $config->upload_path->campaigns . '/'
                . $campaign->getExternal_Campaign_Id() . '/thumbnails';

            $upload = new Zend_File_Transfer_Adapter_Http();

            $fileInfo = $upload->getFileInfo();
            $newName = time() . '_' . $fileInfo['site_logo']['name'];

            $imageUrl = '/campaigns/' . $campaign->getExternal_Campaign_Id()
                . '/images/' . $newName;

            $upload->addFilter('Rename', $newName);
            $upload->setDestination($uploadPath);

            /* try {
                 $upload->receive();
             } catch (Zend_File_Transfer_Exception $e) {
                 $e->getMessage();
             }*/
            $upload->receive();

            $fullSizeImage = new ZendExt_Image_FullSizeImage(
                $uploadPath . "/" . $newName
            );
            $thumbnail = new ZendExt_Image_Thumbnail(
                $fullSizeImage, "64", "64"
            );
            $thumbnail->setPath($thumbnailsPath);
            $thumbnail->save();

            $mediaFileMapper = new Application_Model_MediaFileMapper();
            $mediaFile = new Application_Model_MediaFile();
            //$mediaFile->setMedia_Id();
            $mediaFile->setMedia_Location($imageUrl);
            $mediaFile->setMedia_Name($newName);
            $mediaFile->setMedia_Owner_Id($campaignId);
            $mediaFile->setMedia_Owner_Type(MEDIA_OWNER_TYPE_CAMPAIGN);
            $mediaFile->setMedia_Type(MEDIA_TYPE_OTHER);
            $mediaFileMapper->save($mediaFile);

            $this->_helper->redirector(
                'details', 'campaigns', null, array('id' => $campaignId)
            );
        } else {
            $formData['media_files_list'] = $this->getCampaignImagesList(
                $formData['campaign_id']
            );
            $this->view->form->populate($formData);
        }
    }

    public function saveImagesTypes($formData)
    {
        $campaignId = $formData['campaign_id'];

        $mediaFileMapper = new Application_Model_MediaFileMapper();


        foreach ($formData['images_types_list'] as $imageName => $imageType) {
            $mediaFile = new Application_Model_MediaFile();
            $mediaFileMapper->loadMediaByNameAndOwnerId(
                $imageName, $campaignId, $mediaFile
            );

            $mediaFile->setMedia_Type($imageType);
            $mediaFile->setOnclick_Location(
                $formData['onclick_locations_list'][$imageName]
            );
            $mediaFileMapper->save($mediaFile);
        }
    }

    public function getCampaignImagesList($campaignId)
    {
        $mediaFileMapper = new Application_Model_MediaFileMapper();
        $mediaFilesList = $mediaFileMapper->getListByOwnerIdAndOwnerType(
            $campaignId, MEDIA_OWNER_TYPE_CAMPAIGN
        );

        return $mediaFilesList;
    }

    public function deleteimageAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $campaignId = $this->_getParam('owner_id');
        $imageName = $this->_getParam('image_name');

        $mediaFileMapper = new Application_Model_MediaFileMapper();
        $mediaFile = new Application_Model_MediaFile();

        $isLoaded = $mediaFileMapper->loadMediaByNameAndOwnerId(
            $imageName, $campaignId, $mediaFile
        );
        if ($isLoaded == true) {
            $mediaFileMapper->deleteFileById($mediaFile->getMedia_Id());

            $campaign = new Application_Model_Campaign();
            $campaignMapper = new Application_Model_CampaignMapper();
            $campaignMapper->loadCampaignById($campaignId, $campaign);

            ZendExt_Utils_FilesystemUtils::deleteSiteImage(
                $campaign->getExternal_Campaign_Id(), $imageName
            );
        }
        $this->_helper->redirector(
            'details', 'campaigns', null, array('id' => $campaignId)
        );
    }

    public function competitionsAction()
    {
        if ($this->_getParam('id', null) != null) {
            $campaignId = $this->_getParam('id', '');
        } elseif ($this->getRequest()->isPost()) {
            $campaignId = $this->_getParam('campaign_id');
        } else {
            throw new Exception('Invalid campaign id');
        }

        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/application.ini'
        );
        $campaignTokenCompId = $this->_getParam('campaign_comp_id');
        $campaignTokenComp = new Application_Model_CampaignTokenComp();
        $campaignTokenCompMapper
            = new Application_Model_CampaignTokenCompMapper();

        $this->view->assign('campaignId', $campaignId);

        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $form = new ZendExt_Forms_CompetitionsForm();
        $form->setCampaignId($campaignId);
        $form->setSiteUrl($campaign->getSite_Url());

        $referenceDataMapper = new Application_Model_ReferenceDataMapper();

        $campaignStatusesList = $referenceDataMapper->getCampaignSatuses();
        $form->fillCampaignStatus($campaignStatusesList);

        $this->view->assign('campaignName', $campaign->getCampaign_Name());

        $format = $this->getLocaleDateFormat();
        $calendarFormat
            = ZendExt_Utils_StringUtils::reformatDateFormatForCalendar($format);
        $this->view->assign('dateformat', $calendarFormat);

        $siteMapper = new Application_Model_SiteMapper();
        $site = new Application_Model_Site();

        $siteMapper->loadSiteById($campaign->getSite_Id(), $site);
        $this->view->assign('siteUrl', $site->getSite_Url());

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {

                $campaignTokenComp->setCampaign_Comp_Id(
                    $formData['campaign_comp_id']
                );
                $campaignTokenComp->setCampaign_Id($formData['campaign_id']);

                $campaignTokenComp->setCampaign_Comp_Requires_Skill_Question(
                    $formData['requires_skill_question']
                );
                $campaignTokenComp->setCampaign_Comp_Skill_Question(
                    $formData['skill_question']
                );
                $campaignTokenComp->setCampaign_Comp_Skill_Question_Max_Woords(
                    $formData['max_words']
                );
                $campaignTokenComp->setCampaign_Comp_Intro_Text(
                    $formData['intro_text']
                );
                $campaignTokenComp->setCampaign_Comp_Name(
                    $formData['competition_name']
                );
                $campaignTokenComp->setPrimary_Trigger_Url(
                    $formData['trigger_url']
                );
                $campaignTokenComp->setNum_Secondary_Sidebar_Images(
                    $formData['num_secondary_sidebar_images']
                );
                $campaignTokenComp->setCampaign_Comp_Status(
                    $formData['campaign_comp_status']
                );
                $campaignTokenComp->setCampaign_Comp_Type(
                    CAMPAIGN_TYPE_URL_TRIG
                );

                if (isset($formData['start_date'])) {
                    $startDateString = $formData['start_date'] . ' '
                        . $formData['start_time_hours'] . ':'
                        . $formData['start_time_minutes'] . ':00';
                    $startDate = new Zend_Date($startDateString);

                    $endDateString = $formData['start_date'] . ' '
                        . $formData['end_time_hours'] . ':'
                        . $formData['end_time_minutes'] . ':00';
                    $endDate = new Zend_Date($endDateString);
                    $endDate->addDay($formData['competition_length']);

                    $campaignTokenComp->setLocalisedStartDate($startDate);
                    $campaignTokenComp->setLocalisedEndDate($endDate);
                }

                $campaignTokenCompMapper->save($campaignTokenComp);
                $this->_helper->redirector(
                    'competitions', 'campaigns', null,
                    array('id' => $formData['campaign_id'])
                );
            } else {
                $form->populate($formData);
            }
        } elseif ($campaignTokenCompId != null) {
            $campaignTokenCompMapper->loadCampaignTokenCompById(
                $campaignTokenCompId, $campaignTokenComp
            );

            $formData = array();

            $formData['campaign_comp_id'] = $campaignTokenCompId;
            $formData['campaign_id'] = $campaignTokenComp->getCampaign_Id();
            $formData['requires_skill_question']
                = $campaignTokenComp->getCampaign_Comp_Requires_Skill_Question(
            );
            $formData['skill_question']
                = $campaignTokenComp->getCampaign_Comp_Skill_Question();
            $formData['max_words']
                = $campaignTokenComp->getCampaign_Comp_Skill_Question_Max_Woords(
            );
            $formData['intro_text']
                = $campaignTokenComp->getCampaign_Comp_Intro_Text();
            $formData['competition_name']
                = $campaignTokenComp->getCampaign_Comp_Name();
            $formData['trigger_url']
                = $campaignTokenComp->getPrimary_Trigger_Url();
            $formData['num_secondary_sidebar_images']
                = $campaignTokenComp->getNum_Secondary_Sidebar_Images();
            $formData['campaign_comp_status']
                = $campaignTokenComp->getCampaign_Comp_Status();

            if ($campaignTokenComp->getCampaign_Comp_Start_Date() != null) {
                $startDate = $campaignTokenComp->getStartZendDate();

                $formData['start_date'] = $startDate->toString($format);
                $formData['start_time_hours'] = $startDate->toString('HH');
                $formData['start_time_minutes'] = $startDate->toString('mm');

                $endDate = $campaignTokenComp->getEndZendDate();

                $formData['end_date'] = $endDate->toString($format);
                $formData['end_time_hours'] = $endDate->toString('HH');
                $formData['end_time_minutes'] = $endDate->toString('mm');

                $formData['competition_length']
                    = $campaignTokenComp->getCompetitionLength();
            } else {
                $formData['start_date'] = '';
                $formData['end_date'] = '';
            }

            $form->populate($formData);
        }

        $competitionsList = $campaignTokenCompMapper->getListByCampaignId(
            $campaignId
        );
        $this->view->assign(
            'imagePrefix', $config->development->image->img_prefix
        );
        $this->view->assign('competitionsList', $competitionsList);
    }

    public function deletecompetitionAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $campaignTokenCompId = $this->_getParam('campaign_comp_id');
        $campaignId = $this->_getParam('id');

        $campaignTokenCompMapper
            = new Application_Model_CampaignTokenCompMapper();
        $campaignTokenCompMapper->deleteCampaignTokenCompById(
            $campaignTokenCompId
        );

        $this->_helper->redirector(
            'competitions', 'campaigns', null, array('id' => $campaignId)
        );
    }

    public function messagesAction()
    {
        if ($this->_getParam('id', null) != null) {
            $campaignId = $this->_getParam('id', '');
        } elseif ($this->getRequest()->isPost()) {
            $campaignId = $this->_getParam('campaign_id');
        }

        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $this->view->assign('campaignId', $campaignId);
        $this->view->assign('campaignName', $campaign->getCampaign_Name());

        $siteMapper = new Application_Model_SiteMapper();
        $site = new Application_Model_Site();

        $siteMapper->loadSiteById($campaign->getSite_Id(), $site);
        $this->view->assign('siteUrl', $site->getSite_Url());

        $messagesMapper = new Application_Model_CampaignMessageMapper();
        $defaultMessagesList = $messagesMapper->getDefaultMessages();

        $form = new ZendExt_Forms_EditCampaignMessagesForm();
        $form->setDefaultMessages($defaultMessagesList);

        $this->view->form = $form;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                foreach ($formData['messages'] as $messageType => $messageValue)
                {
                    if ($messageValue != null) {
                        $message = new Application_Model_CampaignMessage();
                        $message->setCampaign_Id(null);
                        $message->setMessage_Type($messageType);
                        $message->setMessage_Value($messageValue);
                        $message->setMessage_Is_Default('0');
                        $message->setCampaign_Id($formData['campaign_id']);

                        $messagesMapper->save($message);
                    }
                }
                $this->_helper->redirector(
                    'messages', 'campaigns', null, array('id' => $campaignId)
                );
            } else {
                $form->populate($formData);
            }
        } else {
            $formData = array();
            $defaultMessagesList = $messagesMapper->getCampaignMessages(
                $campaignId
            );

            foreach ($defaultMessagesList as $message) {

                $formData[$message->getMessage_Type()]
                    = $message->getMessage_Value();
            }
            $formData['campaign_id'] = $campaignId;
            $form->populate($formData);
        }
    }

    public function resultsAction()
    {
        if ($this->_getParam('id', null) != null) {
            $campaignId = $this->_getParam('id', '');
        } elseif ($this->getRequest()->isPost()) {
            $campaignId = $this->_getParam('campaign_id');
        }

        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $this->view->assign('campaignId', $campaignId);
        $this->view->assign('campaignName', $campaign->getCampaign_Name());

        $siteMapper = new Application_Model_SiteMapper();
        $site = new Application_Model_Site();

        $siteMapper->loadSiteById($campaign->getSite_Id(), $site);
        $this->view->assign('siteUrl', $site->getSite_Url());

        $postDetails = new Application_Model_CampaignPostDetails();
        $postDetailsMapper = new Application_Model_CampaignPostDetailsMapper();
        $loadResult = $postDetailsMapper->loadPostDetailsByCampaignId(
            $campaignId, $postDetails
        );

        if ($loadResult == true) {
            $formData = array();
            $formData['campaign_id'] = $campaignId;
            $formData['publish_winners_date']
                = $postDetails->getPublish_Winners_Date();
            $formData['post_id'] = $postDetails->getPost_Id();

            $publishWinnersBox = new ZendExt_Elements_PublishWinnersBox(
                'publish_box'
            );
            $publishWinnersBox->populate($formData);
            $this->view->assign('publishWinnersBox', $publishWinnersBox);
        }

        $campaignTokenCompMapper
            = new Application_Model_CampaignTokenCompMapper();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            foreach ($formData['winners_ids'] as $competitionId => $winnerId) {
                $competition = new Application_Model_CampaignTokenComp();
                $campaignTokenCompMapper->loadCampaignTokenCompById(
                    $competitionId, $competition
                );

                $competition->setWinner_Id($winnerId);
                $campaignTokenCompMapper->save($competition);
            }
            $this->_helper->redirector(
                'results', 'campaigns', null, array('id' => $campaignId)
            );

        } else {
            $competitionsList = $campaignTokenCompMapper->getListByCampaignId(
                $campaignId
            );

            $this->view->assign('competitionsList', $competitionsList);
        }
    }

    public function generatecsvAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $campaignId = $this->_getParam('id', '');
        $competitionId = $this->_getParam('campaign_comp_id', '');

        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $entriesMapper = new Application_Model_CampaignCompEntryMapper();
        $entriesList = $entriesMapper->getListByCompetitionId($competitionId);

        $result = new stdClass();

        if (count($entriesList) > 0) {

            ZendExt_Utils_FilesystemUtils::configureCampaignCSVFolders(
                $campaign->getExternal_Campaign_Id()
            );

            $campaignTokenCompMapper
                = new Application_Model_CampaignTokenCompMapper();
            $competition = new Application_Model_CampaignTokenComp();
            $campaignTokenCompMapper->loadCampaignTokenCompById(
                $competitionId, $competition
            );

            //$competitionName = strtolower(str_replace(' ', '_', $competition->getCampaign_Comp_Name()));
            $campaignNameFormatted = strtolower(
                str_replace(' ', '_', $campaign->getCampaign_Name())
            );

            $config = new Zend_Config_Ini(
                APPLICATION_PATH . '/configs/application.ini', 'development'
            );

            $filename = $config->upload_path->csv . '/'
                . $campaign->getExternal_Campaign_Id() . '/wingrin_results_'
                . $campaign->getExternal_Campaign_Id() . '_'
                . $campaignNameFormatted . '.csv';
            $file = fopen($filename, 'w');

            $headerString = array('Site', $campaign->getSite_Url());
            fputcsv($file, $headerString);

            $headerString = array('Campaign', $campaign->getCampaign_Name());
            fputcsv($file, $headerString);

            $headerString = array('Number entries',
                                  $entriesMapper->getTotalEntriesByCompetitionId(
                                      $competitionId
                                  ));
            fputcsv($file, $headerString);

            $headerString = array('Skill question',
                                  $competition->getCampaign_Comp_Skill_Question(
                                  ));
            fputcsv($file, $headerString);

            $headerString = array('Start Date',
                                  $competition->getFormattedStartDateTime());
            fputcsv($file, $headerString);

            $headerString = array('End Date',
                                  $competition->getFormattedEndDateTime());
            fputcsv($file, $headerString);

            $headerString = array();
            fputcsv($file, $headerString);

            $headerString = array('entrant first name', 'entrant last name',
                                  'email address', 'answer', 'entry date',
                                  'entrant ip', 'entrant country',
                                  'entrant session_id');
            fputcsv($file, $headerString);

            foreach ($entriesList as $entry) {
                $data = array(
                    $entry->getEntrant_Firstname(),
                    $entry->getEntrant_Lastname(),
                    $entry->getEntrant_Email(),
                    $entry->getCampaign_Comp_Entry_Answer(),
                    $entry->getFormattedEntryDate(),
                    $entry->getEntrant_Ip(),
                    $entry->getEntrant_Country(),
                    $entry->getEntrant_Session_Id()
                );
                fputcsv($file, $data);
            }

            fclose($file);
            $result->error = 0;

            $competition->setLastReportDate(
                $campaignTokenCompMapper->getCurrentDate()
            );
            $campaignTokenCompMapper->save($competition);
        } else {
            $result->error = 1;
        }

        $json = Zend_Json::encode($result);
        echo $json;
    }

    public function downloadcsvAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $campaignId = $this->_getParam('id', '');

        $competitionId = $this->_getParam('campaign_comp_id', '');

        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $campaignTokenCompMapper
            = new Application_Model_CampaignTokenCompMapper();
        $competition = new Application_Model_CampaignTokenComp();
        $campaignTokenCompMapper->loadCampaignTokenCompById(
            $competitionId, $competition
        );

        //$competitionName = strtolower(str_replace(' ', '_', $competition->getCampaign_Comp_Name()));
        $campaignNameFormatted = strtolower(
            str_replace(' ', '_', $campaign->getCampaign_Name())
        );
        $fileName = 'wingrin_results_' . $campaign->getExternal_Campaign_Id()
            . '_' . $campaignNameFormatted . '.csv';

        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/application.ini', 'development'
        );

        $filePath = $config->upload_path->csv . '/'
            . $campaign->getExternal_Campaign_Id() . '/' . $fileName;

        if (is_file($filePath)) {
            header("Content-Type:text/csv");
            header("Content-length:" . (string)(filesize($filePath)));
            header("Content-Disposition: attachment; filename=" . $fileName);
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            readfile($filePath);
        }
    }

    /*	public function getcurrentAction()
        {
            $this->_helper->viewRenderer->setNoRender(true);
            $this->view->layout()->disableLayout();

            $campaignMapper = new Application_Model_CampaignMapper();
            $campaignsList = $campaignMapper->getCurrentCampaign();

            $json = Zend_Json::encode($campaignsList);
            echo $json;
        }

        public function getmessagesAction()
        {
            $this->_helper->viewRenderer->setNoRender(true);
            $this->view->layout()->disableLayout();


            $campaignId = $this->_getParam('campaign_id');

            $messagesMapper = new Application_Model_CampaignMessageMapper();

            $defaultMessagesList = $messagesMapper->getDefaultMessages();
            $campaignMessagesList = $messagesMapper->getCampaignMessages($campaignId);

            $defaultMessagesArray = array();
            $campaignMessagesArray = array();

            foreach ($defaultMessagesList as $message) {
                $defaultMessagesArray[$message->getMessage_Type()] = $message->getMessage_Value();
            }

            foreach ($campaignMessagesList as $message) {
                $campaignMessagesArray[$message->getMessage_Type()] = $message->getMessage_Value();
            }

            $result = array();
            if (isset($campaignMessagesArray['MESSAGE_TYPE_PRE_CAMPAIGN_MESSAGE'])) {
                $result['MESSAGE_TYPE_PRE_CAMPAIGN_MESSAGE'] = $campaignMessagesArray['MESSAGE_TYPE_PRE_CAMPAIGN_MESSAGE'];
            } else {
                $result['MESSAGE_TYPE_PRE_CAMPAIGN_MESSAGE'] = $defaultMessagesArray['MESSAGE_TYPE_PRE_CAMPAIGN_MESSAGE'];
            }
            if (isset($campaignMessagesArray['MESSAGE_TYPE_ACTIVE_CAMPAIGN'])) {
                $result['MESSAGE_TYPE_ACTIVE_CAMPAIGN'] = $campaignMessagesArray['MESSAGE_TYPE_ACTIVE_CAMPAIGN'];
            } else {
                $result['MESSAGE_TYPE_ACTIVE_CAMPAIGN'] = $defaultMessagesArray['MESSAGE_TYPE_ACTIVE_CAMPAIGN'];
            }
            if (isset($campaignMessagesArray['MESSAGE_TYPE_POST_CAMPAIGN_MESSAGE'])) {
                $result['MESSAGE_TYPE_POST_CAMPAIGN_MESSAGE'] = $campaignMessagesArray['MESSAGE_TYPE_POST_CAMPAIGN_MESSAGE'];
            } else {
                $result['MESSAGE_TYPE_POST_CAMPAIGN_MESSAGE'] = $defaultMessagesArray['MESSAGE_TYPE_POST_CAMPAIGN_MESSAGE'];
            }

            $json = Zend_Json::encode($result);
            echo $json;
        }*/

    public function testingAction()
    {
        if ($this->_getParam('id', null) != null) {
            $campaignId = $this->_getParam('id', '');
        } elseif ($this->getRequest()->isPost()) {
            $campaignId = $this->_getParam('campaign_id');
        }

        //$campaignsTestsMapper = new Application_Model_CampaignsTestsMapper();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaign = new Application_Model_Campaign();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $globalContentMapper = new Application_Model_GlobalContentMapper();
        $testModeContent = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById(
            'USER_TEST_MODE', $testModeContent
        );
        $this->view->assign('testModeContent', $testModeContent);

        $this->view->assign('campaignId', $campaignId);

        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $this->view->assign('campaignName', $campaign->getCampaign_Name());
        $this->view->assign('campaign', $campaign);

        $config = new Zend_Config_Ini(
            APPLICATION_PATH . '/configs/application.ini', 'test_mode'
        );
        $this->view->assign('testUrlPrefix', $config->test_url_prefix);

        $siteMapper = new Application_Model_SiteMapper();
        $site = new Application_Model_Site();

        $siteMapper->loadSiteById($campaign->getSite_Id(), $site);
        $this->view->assign('siteUrl', $site->getSite_Url());
        $this->view->assign('site', $site);
    }

    public function deleteipAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $campaignId = $this->_getParam('id', '');

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();
            $ipsList = $formData['ip_ids'];

            $campaignsTestsMapper = new Application_Model_CampaignsTestsMapper(
            );
            foreach ($ipsList as $ipId) {
                $campaignsTestsMapper->deleteById($ipId);
            }
        }
        $this->_helper->redirector(
            'testing', 'campaigns', null, array('id' => $campaignId)
        );

    }

    public function deleteAction()
    {
        $campaignId = $this->_getParam('id', null);

        $campaignMapper = new Application_Model_CampaignMapper();
        $campaign = new Application_Model_Campaign();

        $loadResult = $campaignMapper->loadCampaignById($campaignId, $campaign);

        if ($loadResult != false) {
            $campaignMapper->deleteCampaignById($campaignId);
        }
        $this->_helper->redirector('index');
    }

}







