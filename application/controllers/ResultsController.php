<?php

class ResultsController extends ZendExt_Controllers_WinGrinAdminController
{

    public function indexAction()
    {
        $form = new ZendExt_Forms_SearchCampaignResultsForm();
        $this->view->form = $form;

        $searchParams = null;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $searchParams = $formData;
            }
            $form->populate($formData);
        }

        $paginateParamsForm = new ZendExt_Forms_PaginateParamsForm();

        $itemsPerPage = (int)$this->_getParam('per_page', 10);
        $this->view->paginateParamsForm = $paginateParamsForm;
        $this->view->paginateParamsForm->populate(
            array('per_page' => $itemsPerPage)
        );

        $pageNumber = (int)$this->_getParam('page', 1);


        $campaignMapper = new Application_Model_CampaignMapper();
        //$competitionsMapper = new Application_Model_CampaignTokenCompMapper();
        //$competitionsMapper->get
        $paginatedCampaigns = $campaignMapper->getCampaignsForPaginate(
            $pageNumber, $itemsPerPage, $searchParams
        );

        $itemsStart = ($pageNumber - 1)
            * $paginatedCampaigns->getCurrentItemCount() + 1;
        $itemsEnd = $pageNumber * $paginatedCampaigns->getCurrentItemCount();
        $totalItems = $paginatedCampaigns->getTotalItemCount();

        $this->view->itemsStart = $itemsStart;
        $this->view->itemsEnd = $itemsEnd;
        $this->view->totalItems = $totalItems;
        $this->view->itemsPerPage = $itemsPerPage;

        $this->view->paginatedCampaigns = $paginatedCampaigns;
    }

    public function entriesAction()
    {
        if ($this->_getParam('id', null) != null) {
            $campaignId = $this->_getParam('id', '');
        } elseif ($this->getRequest()->isPost()) {
            $campaignId = $this->_getParam('campaign_id');
        }

        $campaign = new Application_Model_Campaign();
        $campaignMapper = new Application_Model_CampaignMapper();
        $campaignMapper->loadCampaignById($campaignId, $campaign);

        $statsMapper = new Application_Model_StatsEntriesPerDayMapper();
        $statsList = $statsMapper->getListByCampaignId($campaignId);

        $this->view->assign('statsList', $statsList);
        $this->view->assign('campaign', $campaign);

        $graphDatesRange = array();
        $graphMonthRange = array();

        if (count($statsList) > 0) {

            $firstStatDate = new Zend_Date($statsList[0]->getStat_Date());
            $lastStatDate = new Zend_Date(
                $statsList[count($statsList) - 1]->getStat_Date()
            );
            $dateDiff = ZendExt_Utils_StringUtils::getDateDiff(
                $firstStatDate->getTimestamp(), $lastStatDate->getTimestamp()
            );

            $locale = Zend_Registry::get('Zend_Locale');
            $format = Zend_Locale_Data::getContent($locale, 'date', 'medium');

            $currentDate = new Zend_Date($statsList[0]->getStat_Date());

            while ($currentDate <= $lastStatDate) {
                if (isset($dateDiff['units']) == 'days'
                    && $dateDiff['value'] <= 7
                ) {
                    $graphDatesRange[$currentDate->toString(
                        $this->getLocaleDateFormat()
                    )]
                        = (string)$currentDate->getTimestamp();
                } else {
                    if (count($graphDatesRange) == 0
                        || $currentDate->toString(
                            'dd'
                        ) == '01'
                    ) {
                        $graphDatesRange[$currentDate->toString($format)]
                            = (string)$currentDate->getTimestamp();
                    } else {
                        $graphDatesRange[$currentDate->toString($format)]
                            = (string)$currentDate->getTimestamp();
                    }
                }

                $currentDate->addDay(1);
            }
        }

        $this->view->assign('graphDatesRange', $graphDatesRange);
        $this->view->assign('graphMonthRange', $graphMonthRange);

        $campaignTokenCompMapper
            = new Application_Model_CampaignTokenCompMapper();
        $competitionsList = $campaignTokenCompMapper->getListByCampaignId(
            $campaignId
        );
        $this->view->assign('competitionsList', $competitionsList);
    }

}



