<?php

class UsersController extends ZendExt_Controllers_WinGrinAdminController
{

    public function indexAction()
    {
        $form = new ZendExt_Forms_SearchUsersForm();
        $this->view->form = $form;

        $searchParams = null;

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $searchParams = $formData;
            }
            $form->populate($formData);
        }

        $paginateParamsForm = new ZendExt_Forms_PaginateParamsForm();
        $paginateParamsForm->setAction('/users/index');

        $itemsPerPage = (int)$this->_getParam('per_page', 10);
        $this->view->paginateParamsForm = $paginateParamsForm;
        $this->view->paginateParamsForm->populate(
            array('per_page' => $itemsPerPage)
        );

        $pageNumber = (int)$this->_getParam('page', 1);


        $userMapper = new Application_Model_UsersMapper();
        $paginatedUsers = $userMapper->getUserForPaginate(
            $pageNumber, $itemsPerPage, $searchParams
        );

        $itemsStart = ($pageNumber - 1) * $paginatedUsers->getCurrentItemCount()
            + 1;
        $itemsEnd = $pageNumber * $paginatedUsers->getCurrentItemCount();
        $totalItems = $paginatedUsers->getTotalItemCount();

        $this->view->itemsStart = $itemsStart;
        $this->view->itemsEnd = $itemsEnd;
        $this->view->totalItems = $totalItems;
        $this->view->itemsPerPage = $itemsPerPage;

        $this->view->paginatedUsers = $paginatedUsers;
    }

    public function addAction()
    {
        $form = new ZendExt_Forms_AddUserForm();

        $this->view->form = $form;
        $form->setSitesList(null);

        $countriesMapper = new Application_Model_CountriesMapper();
        $countriesList = $countriesMapper->getListForSelect();
        $this->view->form->fillCountries($countriesList);

        $timezonesMapper = new Application_Model_TimezoneMapper();
        $timezonesList = $timezonesMapper->getListForSelect();
        $form->fillTimezones($timezonesList);

        $referenceDataMapper = new Application_Model_ReferenceDataMapper();
        $paymentTypesList = $referenceDataMapper->getPaymentTypes();

        $form->fillPaymentTypes($paymentTypesList);

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $user = new Application_Model_Users();

                $user->setEmail($formData['email']);
                $user->setFirstname($formData['firstname']);
                $user->setLastname($formData['lastname']);
                $user->setPhone($formData['phone']);

                //$user->setPassword($formData['password']);
                $user->setPassword(md5($formData['password']));
                $user->setRole($formData['primary_role']);
                $user->setEnable($formData['active']);
                //$user->setUserSitesIds($formData['sites']);
                $user->setCountry($formData['country']);
                $user->setTime_Zone_Name($formData['timezone']);
                $user->setPayment_Type($formData['payment_type']);

                $usersFacade = new ZendExt_Facades_UsersFacade();
                $usersFacade->saveUser($user);

                $userSitesMapper = new Application_Model_UserSitesMapper();
                $userSitesMapper->saveUserSites(
                    $formData['email'], $formData['sites']
                );

                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        }
    }

    public function editAction()
    {
        if ($this->_getParam('email', null) == null) {
            throw new Exception('Invalid user email');
        }

        $email = $this->_getParam('email', null);

        $form = new ZendExt_Forms_EditUserForm();
        $countriesMapper = new Application_Model_CountriesMapper();
        $countriesList = $countriesMapper->getListForSelect();
        $form->fillCountries($countriesList);

        $timezonesMapper = new Application_Model_TimezoneMapper();
        $timezonesList = $timezonesMapper->getListForSelect();
        $form->fillTimezones($timezonesList);

        $referenceDataMapper = new Application_Model_ReferenceDataMapper();
        $paymentTypesList = $referenceDataMapper->getPaymentTypes();
        $form->fillPaymentTypes($paymentTypesList);

        $userSitesMapper = new Application_Model_SiteMapper();
        $sitesList = $userSitesMapper->getUserSites($email);
        $form->setSitesList($sitesList);

        $userMapper = new Application_Model_UsersMapper();
        $user = new Application_Model_Users();

        if ($this->getRequest()->isPost()) {
            $formData = $this->getRequest()->getPost();

            $form->setOldEmail($formData['old_email']);
            if ($formData['password'] != '') {
                $form->allowPasswordConfirmation();
            }

            if ($form->isValid($formData)) {

                $user->setEmail($formData['email']);
                $user->setFirstname($formData['firstname']);
                $user->setLastname($formData['lastname']);
                $user->setPhone($formData['phone']);
                $user->setOldEmail($formData['old_email']);
                $user->setRole($formData['primary_role']);
                $user->setEnable($formData['active']);
                //$user->setUserSitesIds($formData['sites']);
                //$user->setPassword($formData['password']);
                if (isset($formData['password'])
                    && $formData['password'] != ''
                ) {
                    $user->setPassword(md5($formData['password']));
                }
                $user->setCountry($formData['country']);
                $user->setTime_Zone_Name($formData['timezone']);
                $user->setPayment_Type($formData['payment_type']);

                $usersFacade = new ZendExt_Facades_UsersFacade();
                $usersFacade->saveUser($user);

                $userSitesMapper = new Application_Model_UserSitesMapper();
                $userSitesMapper->saveUserSites(
                    $formData['email'], $formData['sites']
                );

                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else {
            $loadResult = $userMapper->loadUserByEmail($email, $user);

            if ($loadResult != false) {
                $formData = array();
                $formData['email'] = $user->getEmail();
                $formData['old_email'] = $user->getEmail();
                $formData['firstname'] = $user->getFirstname();
                $formData['lastname'] = $user->getLastname();
                $formData['phone'] = $user->getPhone();
                $formData['primary_role'] = $user->getRole();
                $formData['active'] = $user->getEnable();
                $formData['country'] = $user->getCountry();
                $formData['timezone'] = $user->getTime_Zone_Name();
                $formData['payment_type'] = $user->getPayment_Type();

                $form->setOldEmail($user->getEmail());
                $form->populate($formData);
            }
        }
        $this->view->form = $form;
    }

    public function deleteAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $this->view->layout()->disableLayout();

        $email = $this->_getParam('email', null);

        $user = new Application_Model_Users();
        $userMapper = new Application_Model_UsersMapper();
        $loadResult = $userMapper->loadUserByEmail($email, $user);

        if ($loadResult != false) {
            $usersFacade = new ZendExt_Facades_UsersFacade();
            $usersFacade->deleteUser($user);
        }
        $this->_helper->redirector('index');
    }


}







