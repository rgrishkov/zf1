<?php

class IndexController extends Zend_Controller_Action
{

    public function init()
    {
        $layout = Zend_Layout::getMvcInstance();
        $layout->setLayout("index_layout");
    }

    public function indexAction()
    {
        $form = new ZendExt_Forms_LoginForm();
        $this->view->form = $form;
        $this->view->assign('currentAction', 'login');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $allowRegistration = $config->development->registration->allow_registration;
        $this->view->assign('allowRegistration', $allowRegistration);

        if ($this->getRequest()->isPost()) {
        	$formData = $this->getRequest()->getPost();

        	 if ($form->isValid($formData)) {
                $result = $this->authenticate($formData['email'], $formData['password']);

	            if ($result->isValid()) {
                    $this->redirectToSystem();
	            } else {
	            	$form->setWrongLoginError();
	            	$form->populate($formData);
	            }
        	 } else {
        	 	$form->populate($formData);
        	 }
        }	
    }

    private function authenticate($email, $password)
    {
        $authenticate = Zend_Auth::getInstance();

        $db = $this->_getParam('db');
        $adapter = new Zend_Auth_Adapter_DbTable($db, 'users', 'email', 'password');

        $adapter->setIdentity($email);
        $adapter->setCredential(md5($password));

        $result = $authenticate->authenticate($adapter);
        return $result;
    }

    private function redirectToSystem()
    {
        $authenticate = Zend_Auth::getInstance();

        $userEmail = $authenticate->getIdentity();
        $usersMapper = new Application_Model_UsersMapper();
        $user = new Application_Model_Users();
        $usersMapper->loadUserByEmail($userEmail, $user);

        if ($user->isAdmin()) {
            $this->_helper->redirector('', 'dashboard', null);
        } else {
            $this->_helper->redirector('', 'dashboard', 'user');
        }
    }
    
    public function logoutAction()
    {
    	$this->_helper->viewRenderer->setNoRender(true);
    	$this->view->layout()->disableLayout();
    	
    	$authentication = Zend_Auth::getInstance(); 
    	$authentication->clearIdentity();
    	
    	$this->_helper->redirector('', 'index', null);
    }

    public function registrationAction()
    {
        $form = new ZendExt_Forms_RegistrationForm();
        $this->view->form = $form;
        $this->view->assign('currentAction', 'registration');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $allowRegistration = $config->development->registration->allow_registration;
        $this->view->assign('allowRegistration', $allowRegistration);

        $globalContentMapper = new Application_Model_GlobalContentMapper();
        $quickInfo = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById('REGISTRATION', $quickInfo);
        $this->view->assign('quickInfo', $quickInfo);

        $countriesMapper = new Application_Model_CountriesMapper();
        $countriesList = $countriesMapper->getListForSelect();
        $this->view->form->fillCountries($countriesList);

        $timezonesMapper = new Application_Model_TimezoneMapper();
        $timezonesList = $timezonesMapper->getListForSelect();
        $form->fillTimezones($timezonesList);

        if ((bool)$allowRegistration == true ) {

            if ($this->getRequest()->isPost()) {

                $formData = $this->getRequest()->getPost();

                if ($form->isValid($formData)) {

                    $site = new Application_Model_Site();
                    $siteMapper = new Application_Model_SiteMapper();

                    $externalSiteId = ZendExt_Utils_StringUtils::randomStr(20);

                    $site->setExternal_Site_Id($externalSiteId);
                    //$site->setSite_Name($formData['site_name']);
                    $site->setSite_Url(ZendExt_Utils_StringUtils::clearUrl($formData['site_url']));
                    $site->setSite_Bio('');

                    $siteId = $siteMapper->save($site);
                    if ($siteId != null) {

                        $user = new Application_Model_Users();

                        $user->setEmail($formData['email']);
                        $user->setFirstname($formData['firstname']);
                        $user->setSurname($formData['surname']);
                        //$user->setPhone($formData['phone']);
                        $user->setCountry($formData['country']);
                        $user->setTime_Zone_Name($formData['timezone']);

                        $user->setPassword(md5($formData['password']));
                        $user->setRole(USER_ROLE_USER);
                        $user->setPayment_Type(PAYMENT_TYPE_REQUIRED);
                        $user->setEnable(1);
                        //$user->setUserSitesIds(array($siteId));

                        $usersFacade = new ZendExt_Facades_UsersFacade();
                        $usersFacade->saveUser($user);

                        $userSitesMapper = new Application_Model_UserSitesMapper();
                        $userSitesMapper->saveUserSites($formData['email'], array($siteId));

                        $messagesFacade = new ZendExt_Facades_MessagesFacade();
                        $messagesFacade->sendSystemMessage($user, 'REGISTRATION');

                        $this->authenticate($formData['email'], $formData['password']);
                        $this->_helper->redirector('', 'dashboard', 'user');
                    } else {
                        $this->_helper->redirector('index');
                    }
                } else {
                    $form->populate($formData);
                }
            }
        }
    }

    public function forgotpasswordAction()
    {
        $form = new ZendExt_Forms_ForgotPasswordForm();
        $this->view->form = $form;

        $this->view->assign('currentAction', 'login');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $allowRegistration = $config->development->registration->allow_registration;
        $this->view->assign('allowRegistration', $allowRegistration);

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $email = $formData['email'];

                $user = new Application_Model_Users();
		        $userMapper = new Application_Model_UsersMapper();

                $loadResult = $userMapper->loadUserByEmail($email, $user);

                if ($loadResult != false) {

                    $passwordRecoveryKey = ZendExt_Utils_StringUtils::randomStr(20);
                    $user->setPassword_Recovery_Key($passwordRecoveryKey);

                    $competitionMapper = new Application_Model_CampaignTokenCompMapper();
                    $user->setPassword_Recovery_Date($competitionMapper->getCurrentDate());

                    $usersFacade = new ZendExt_Facades_UsersFacade();
                    $usersFacade->saveUser($user);

                    $recoveryUrl = ZendExt_Utils_ConfigUtils::getBaseUrl().$this->view->url(array('controller' => 'index', 'action' => 'newpassword', 'recovery_key' => $passwordRecoveryKey));

                    $messagesFacade = new ZendExt_Facades_MessagesFacade();
                    $messagesFacade->sendPasswordRecoveryEmail($user, $recoveryUrl, $passwordRecoveryKey);

                    $this->_helper->redirector('recovery', 'index', null);
                } else {
                    throw new Exception('Unknown user account');
                }

            } else {
                $form->populate($formData);
            }
        }
    }

    public function recoveryAction()
    {
        $form = new ZendExt_Forms_PasswordRecoveryForm();
        $this->view->form = $form;

        $this->view->assign('currentAction', 'login');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $allowRegistration = $config->development->registration->allow_registration;
        $this->view->assign('allowRegistration', $allowRegistration);

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                $this->_helper->redirector('newpassword', 'index', null, array('recovery_key' => $formData['recovery_key']));
            } else {
                $form->populate($formData);
            }
        }
    }

    public function newpasswordAction()
    {
        if ($this->_getParam('recovery_key', null) != null) {
    		$recoveryKey = $this->_getParam('recovery_key', '');
        } elseif($this->getRequest()->isPost()) {
    		$recoveryKey = $this->_getParam('recovery_key', '');
    	} else {
            throw new Exception('Unknown or invalid key. Only the last key requested is valid and expires after one hour.');
        }

        $user = new Application_Model_Users();
		$userMapper = new Application_Model_UsersMapper();
        $loadResult = $userMapper->loadUserByRecoveryKey($recoveryKey, $user);

        if ($loadResult == false) {
            throw new Exception('Unknown or invalid key. Only the last key requested is valid and expires after one hour.');
        }

        $form = new ZendExt_Forms_NewPasswordForm();
        $form->setRecoveryKey($recoveryKey);
        $this->view->form = $form;

        $this->view->assign('currentAction', 'login');

        $config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini');
        $allowRegistration = $config->development->registration->allow_registration;
        $this->view->assign('allowRegistration', $allowRegistration);

        if ($this->getRequest()->isPost()) {

            $formData = $this->getRequest()->getPost();

            if ($form->isValid($formData)) {
                //$user->setPassword($formData['password']);
                $user->setPassword(md5($formData['password']));
                $user->setPassword_Recovery_Key(null);
                $user->setPassword_Recovery_Date(null);

                $usersFacade = new ZendExt_Facades_UsersFacade();
                $usersFacade->saveUser($user);

                $this->authenticate($user->getEmail(), $formData['password']);
                $this->redirectToSystem();
            } else {
                $form->populate($formData);
            }
        }
    }
}

