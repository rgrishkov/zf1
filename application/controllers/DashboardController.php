<?php

class DashboardController extends ZendExt_Controllers_WinGrinAdminController
{

    public function indexAction()
    {
        //$campaignMapper = new Application_Model_CampaignMapper();
        //$latestCampaigns = $campaignMapper->getLatestCampaigns();
        $competitionsMapper = new Application_Model_CampaignTokenCompMapper();
        $latestCampaigns = $competitionsMapper->getLatestCompetitions();

        //Zend_Debug::dump($latestCampaigns);
        //die();
        $this->view->assign('campaignsList', $latestCampaigns);

        $globalNewsMapper = new Application_Model_GlobalNewsMapper();
        $latestNewsList = $globalNewsMapper->getLatestNews();
        $this->view->assign('newsList', $latestNewsList);

        $authentication = Zend_Auth::getInstance();
        $userEmail = $authentication->getIdentity();

        $messageMapper = new Application_Model_UserMessageMapper();
        $latestMessagesList = $messageMapper->getLatestUserMessages($userEmail);
        $this->view->assign('messagesList', $latestMessagesList);

        $globalContentMapper = new Application_Model_GlobalContentMapper();
        $quickInfo = new Application_Model_GlobalContent();
        $globalContentMapper->loadContentById(
            'ADMIN_DASHBOARD_QUICK_INFO', $quickInfo
        );
        $this->view->assign('quickInfo', $quickInfo);
    }
}







