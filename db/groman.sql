SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `email` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `enable` tinyint(1) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;
